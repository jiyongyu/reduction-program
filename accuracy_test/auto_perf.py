#!/usr/bin/env python

import os
import os.path
import subprocess

DIR_OF_TEST_PROGS = os.path.abspath(os.path.dirname(__file__))

GXX = "g++"
OPTIONS = [ "-O3",
            "-mavx2",
            "-mfma",
            "-fopenmp",
            "-fabi-version=0" ]

REPEAT = 1000

def createFile(dest, prime_list, fma_list, add_list, standard_list):
    f = open(dest, 'w')
    f.write("dividend\tfma\tadd\tstandard\tfma_err\tadd_err\n")
    fma_err_list = [0.0] * len(prime_list)
    add_err_list = [0.0] * len(prime_list)
    for i in range(0, len(prime_list)):
        fma_err = abs((fma_list[i] - standard_list[i]) / standard_list[i])
        add_err = abs((add_list[i] - standard_list[i]) / standard_list[i])
        fma_err_list.append(fma_err)
        add_err_list.append(add_err)

        string = str(prime_list[i]) + '\t' \
                + str(fma_list[i]) + '\t' \
                + str(add_list[i]) + '\t' \
                + str(standard_list[i]) + '\t' \
                + str(fma_err*100) + '%\t' \
                + str(add_err*100) + '%\t' \
                +'\n'
        f.write(string)

    f.write("average fma err: " + str(sum(fma_err_list) / len(fma_err_list) * 100) + '%\n')
    f.write("average add err: " + str(sum(add_err_list) / len(add_err_list) * 100) + '%\n')

    f.close()

def find_prime(prime_list, lower, upper):
    for num in range (lower, upper+1):
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                    break
            else:
                prime_list.append(num)

def main():

    prime_list = []
    find_prime(prime_list, 1, 1000)
    
    fma_list = []
    add_list = []
    standard_list = []
    
    for filename in os.listdir(DIR_OF_TEST_PROGS) :
        if filename.endswith(".cpp"):
            src_file = os.path.join(DIR_OF_TEST_PROGS, filename)
            exe_file = os.path.splitext(src_file)[0]
            subprocess.check_call([GXX] + OPTIONS + [src_file, "-o", exe_file])
            
            for num in prime_list:
                out_str = subprocess.check_output([exe_file, str(num)])
                out_sum = float(out_str.split("\n")[0].split(" ")[-1])

                if filename == "add_f.cpp":
                    add_list.append(out_sum)
                elif filename == "fma_f.cpp":
                    fma_list.append(out_sum)
                else:
                    standard_list.append(out_sum)

    reportFile = DIR_OF_TEST_PROGS + "/report.txt"
    createFile(reportFile, prime_list, fma_list, add_list, standard_list)

if __name__ == "__main__":
    main()

         

