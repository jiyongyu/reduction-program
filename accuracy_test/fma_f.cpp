#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include "../vector_class/vectorclass.h"

#define TESTING

using namespace std;

float sumFloat(float *arr, int array_len){
    Vec8f a0; 
    Vec8f a1; 
    Vec8f a2; 
    Vec8f a3; 
    Vec8f a4; 
    Vec8f a5; 
    Vec8f a6; 
    Vec8f a7; 
    Vec8f a8; 
    Vec8f a9; 
    Vec8f a10; 
    Vec8f a11; 
    Vec8f a12; 
    Vec8f a13; 
    Vec8f a14; 

    Vec8f sumVec0 = 0.0;
    Vec8f sumVec1 = 0.0;
    Vec8f sumVec2 = 0.0;
    Vec8f sumVec3 = 0.0;
    Vec8f sumVec4 = 0.0;
    Vec8f sumVec5 = 0.0;
    Vec8f sumVec6 = 0.0;
    Vec8f sumVec7 = 0.0;
    Vec8f sumVec8 = 0.0;
    Vec8f sumVec9 = 0.0;
    Vec8f sumVec10 = 0.0;
    Vec8f sumVec11 = 0.0;
    Vec8f sumVec12 = 0.0;
    Vec8f sumVec13 = 0.0;
    Vec8f sumVec14 = 0.0;

    int j;
	
    for(j=0; j<array_len; j+=120){
		a0.load(arr+j+8*0);
		a1.load(arr+j+8*1);
		a2.load(arr+j+8*2);
		a3.load(arr+j+8*3);
		a4.load(arr+j+8*4);
		a5.load(arr+j+8*5);
		a6.load(arr+j+8*6);
		a7.load(arr+j+8*7);
		a8.load(arr+j+8*8);
		a9.load(arr+j+8*9);
		a10.load(arr+j+8*10);
		a11.load(arr+j+8*11);
		a12.load(arr+j+8*12);
		a13.load(arr+j+8*13);
		a14.load(arr+j+8*14);
		sumVec0 = mul_add(sumVec0, 1.0, a0);
		sumVec1 = mul_add(sumVec1, 1.0, a1);
		sumVec2 = mul_add(sumVec2, 1.0, a2);
		sumVec3 = mul_add(sumVec3, 1.0, a3);
		sumVec4 = mul_add(sumVec4, 1.0, a4);
		sumVec5 = mul_add(sumVec5, 1.0, a5);
		sumVec6 = mul_add(sumVec6, 1.0, a6);
		sumVec7 = mul_add(sumVec7, 1.0, a7);
		sumVec8 = mul_add(sumVec8, 1.0, a8);
		sumVec9 = mul_add(sumVec9, 1.0, a9);
		sumVec10 = mul_add(sumVec10, 1.0, a10);
		sumVec11 = mul_add(sumVec11, 1.0, a11);
		sumVec12 = mul_add(sumVec12, 1.0, a12);
		sumVec13 = mul_add(sumVec13, 1.0, a13);
		sumVec14 = mul_add(sumVec14, 1.0, a14);
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    float sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];
    sum += sumVec0[4];
    sum += sumVec0[5];
    sum += sumVec0[6];
    sum += sumVec0[7];

    return sum;
}

int main(int argc, char **argv){

#ifdef TESTING
    int array_len = 1200;
    long long int iter = 1;
#else
    int array_len = atoi(argv[argc-1]);
    long long int iter = 0x2000000000 / array_len;
#endif

	int i, j;
    
    //float *arr_float = new float [array_len];
    float* arr_float = (float*) memalign(64, array_len*4);
#ifdef TESTING
    int dividend = atoi(argv[argc-1]);
    for(i=0; i<array_len; i++){
        arr_float[i] = (float)i / dividend;
    }
#endif

    volatile float passed_sum = 0, returned_sum = 0;
    for (long long int i=0; i<iter; i++){
        returned_sum = sumFloat(arr_float, array_len);
        passed_sum += returned_sum;
    }
    printf("sum = %f\n", passed_sum); 
   
    delete [] arr_float; 
    return 0;
}
