#!/usr/bin/env python

import datetime
import os.path 
import subprocess

DIR_OF_THIS_SCRIPT = os.path.abspath( os.path.dirname( __file__) )

PERF_PROGRAM = 'perf_test'

TEST_PROGRAM = 'reduction_prog'
SRC_FILE = TEST_PROGRAM + '.c'
ASM_FILE = TEST_PROGRAM + '.s'
EXE_FILE = TEST_PROGRAM + '.out'

GXX = 'gcc'
ENA_OPT = '-O3'
ENA_AVX = '-mavx'
DIS_VEC = '-fno-tree-vectorize'

def createFile(dest) :
    # print dest
    if not os.path.isfile(dest) :
        f = open(dest, 'w')
        
        # write perf info
        perf_output = subprocess.check_output(['taskset', '-c', '1', './' + PERF_PROGRAM, './' + EXE_FILE])
        f.write(perf_output)

        f.close()

def main() :

    # assembly options: -O3 , -fno-tree_vectorize
    if_opt = raw_input('-O3 ? [y/n]: ')
    if_vec = raw_input('-fno-tree-vectorize ? [y/n]: ')

    now = datetime.datetime.now()
    # 4 kinds of output 
    if   (if_opt == 'y') and (if_vec == 'y') :
        opt = 'T'
        vec = 'T'
        asm_options = [ENA_OPT, ENA_AVX]
    elif (if_opt == 'y') and (if_vec != 'y') :
        opt = 'T'
        vec = 'F'
        asm_options = [ENA_OPT, DIS_VEC]
    elif (if_opt != 'y') and (if_vec == 'y') :
        opt = 'F'
        vec = 'T'
        asm_options = [ENA_AVX]
    else :
        opt = 'F'
        vec = 'F'
        asm_options = [DIS_VEC]

    # assembly & build
    subprocess.check_call([GXX, '-S', SRC_FILE, '-o', ASM_FILE] + asm_options)
    subprocess.check_call([GXX, ASM_FILE, '-o', EXE_FILE])

    # generate report
    report_filename = "report_opt(" + opt + ")_vec(" + vec + ").txt"
    report_file = os.path.join(DIR_OF_THIS_SCRIPT, report_filename)
    createFile(report_file)

if __name__ == "__main__" :
    main()

