#include <ctime>
#include <math.h>
#include <float.h>
#include <fenv.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <iostream>
#include <omp.h>

using namespace std;

int size = 1600000000;
float *arr;


main(){

	int i, j;
    arr = new float [size];

    float sum0 = 0.0;
    float sum1 = 0.0;

    double t0_start = omp_get_wtime();
    // regular reduction
    for (i=0; i<10; i++){
        for (j=0; j<size; j++){
            sum0 += arr[j];
        }
    }
    double t0_end = omp_get_wtime();

    // for reduction
    omp_set_num_threads(1);
    double t1_start = omp_get_wtime();
    for (i=0; i<10; i++){
    #pragma omp parallel for reduction(+:sum1)
        for (j=0; j<size; j++){
            sum1 += arr[j];
        }
    }    
    double t1_end = omp_get_wtime();

    // section reduction
    cout << "sum0 = " << sum0 << '\n';
    cout << "sum1 = " << sum1 << '\n';
    cout << "Regular reduction consume " << (t0_end - t0_start) << " seconds\n";
    cout << "OpenMP-for reduction consume " << (t1_end - t1_start) << " seconds\n";

}
