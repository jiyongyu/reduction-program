/////////////////////////////////////////////////////////////////
//
//      fma_man_ena.cpp:
//          
//          1. fma; 
//          2. manually vectorize(-mavx2); 
//          3. enable unrolling.
//
////////////////////////////////////////////////////////////////
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>

#include <omp.h>
#include "../vector_class/vectorclass.h"

//#define TESTING

using namespace std;

float *arr;

#ifdef TESTING
int size = 128;
#else
int size = 1600000000;
#endif

main(){

	int i, j;
    arr = new float [size];

#ifdef TESTING
    for(int i=0; i<size; i++){
        arr[i] = i;
    }
#endif
    Vec8f a0;
    Vec8f a1;
    Vec8f a2;
    Vec8f a3;
    Vec8f a4;
    Vec8f a5;
    Vec8f a6;
    Vec8f a7;
    Vec8f sum0 = 0.0;
    Vec8f sum1 = 0.0;
    Vec8f sum2 = 0.0;
    Vec8f sum3 = 0.0;
    Vec8f sum4 = 0.0;
    Vec8f sum5 = 0.0;
    Vec8f sum6 = 0.0;
    Vec8f sum7 = 0.0;

#pragma omp declare reduction(+ : Vec8f : \
	omp_out = omp_out + omp_in)
	
    omp_set_num_threads(1);

    double t0_start = omp_get_wtime();
    for (j=0; j<10; j++){
	#pragma omp parallel for reduction(+:sum0, sum1, sum2, sum3, sum4, sum5, sum6, sum7)
        for (i=0; i<size; i += 64){
            a0.load(arr + i);
            a1.load(arr + i + 8);
            a2.load(arr + i + 16);
            a3.load(arr + i + 24);
            a4.load(arr + i + 32);
            a5.load(arr + i + 40);
            a6.load(arr + i + 48);
            a7.load(arr + i + 56);
            
            sum0 = mul_add(sum0, 1.0, a0);
            sum1 = mul_add(sum1, 1.0, a1);
            sum2 = mul_add(sum2, 1.0, a2);
            sum3 = mul_add(sum3, 1.0, a3);
            sum4 = mul_add(sum4, 1.0, a4);
            sum5 = mul_add(sum5, 1.0, a5);
            sum6 = mul_add(sum6, 1.0, a6);
            sum7 = mul_add(sum7, 1.0, a7);
        }
    }
    
    double t0_end = omp_get_wtime();


    for(i=0; i<8; i++){
    	cout << sum0[i] << '|'
             << sum1[i] << '|'
             << sum2[i] << '|'
             << sum3[i] << '|'
             << sum4[i] << '|'
             << sum5[i] << '|'
             << sum6[i] << '|'
             << sum7[i] << '|'
	     << '\n';
    }
    cout << "OpenMP-for reduction consume " << (t0_end - t0_start) << " seconds\n";
}
