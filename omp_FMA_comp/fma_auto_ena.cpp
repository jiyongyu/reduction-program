/////////////////////////////////////////////////////////////////
//
//      fma_auto_ena.cpp:
//          
//          1. fma; 
//          2. auto vectorize(#pragma simd); 
//          3. enable unrolling.
//
////////////////////////////////////////////////////////////////
#include <ctime>
#include <math.h>
#include <float.h>
#include <fenv.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <iostream>
#include <omp.h>
#include <math.h>
#include "../iacaMarks.h"

//#define TESTING

using namespace std;

#ifdef TESTING
int size = 128;
#else
int size = 1600000000;
#endif

float *arr;



main(){

	int i, j;
    arr = new float [size];

#ifdef TESTING
    for(int i=0; i<size; i++){
        arr[i] = (float)i;
    }
#endif

    float sum0 = 0.0;
    float sum1 = 0.0;
    float sum2 = 0.0;
    float sum3 = 0.0;
    float sum4 = 0.0;
    float sum5 = 0.0;
    float sum6 = 0.0;
    float sum7 = 0.0;
    
    omp_set_num_threads(1);

    double t0_start = omp_get_wtime();
    for (i=0; i<10; i++){
    	#pragma omp parallel
        {
            #pragma omp for simd reduction(+:sum0, sum1, sum2, sum3, sum4, sum5, sum6, sum7) 
            for (j=0; j<size; j++){
//IACA_START
                sum0 = fma(sum0, 1.0, arr[j]);
                sum1 = fma(sum1, 1.0, arr[j+1]);
                sum2 = fma(sum2, 1.0, arr[j+2]);
                sum3 = fma(sum3, 1.0, arr[j+3]);
                sum4 = fma(sum4, 1.0, arr[j+4]);
                sum5 = fma(sum5, 1.0, arr[j+5]);
                sum6 = fma(sum6, 1.0, arr[j+6]);
                sum7 = fma(sum7, 1.0, arr[j+7]);
            }                
//IACA_END
        }
    }    
    double t0_end = omp_get_wtime();

    cout << "sum = " << (sum0+sum1+sum2+sum3+sum4+sum5+sum6+sum7) << '\n';
    cout << "OpenMP-for reduction consume " << (t0_end - t0_start) << " seconds\n";

}
