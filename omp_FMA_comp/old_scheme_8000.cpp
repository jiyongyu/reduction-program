#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <omp.h>
#include "../vector_class/vectorclass.h"
#define FMA

using namespace std;

float *arr;
int size = 0x8000;

//void initA(){
  //for(int i=0;i<size;i++){
    //arr[i] = 1;
  //}
//}

main(){

	int i, j;
    //cout << "Initialization done\n";
    //arr = (float*)memalign(64,size*4);
    //initA();
    //cout << "Initialization done\n";
    arr = new float [size];

    Vec8f a0;
    Vec8f a1;
    Vec8f a2;
    Vec8f a3;
    Vec8f a4;
    Vec8f a5;
    Vec8f a6;
    Vec8f a7;
    Vec8f a8;
    Vec8f a9;
    Vec8f a10;
    Vec8f a11;
    Vec8f a12;
    Vec8f a13;
    Vec8f a14;
    
    Vec8f sumVec0 = 0.0;
    Vec8f sumVec1 = 0.0;
    Vec8f sumVec2 = 0.0;
    Vec8f sumVec3 = 0.0;
    Vec8f sumVec4 = 0.0;
    Vec8f sumVec5 = 0.0;
    Vec8f sumVec6 = 0.0;
    Vec8f sumVec7 = 0.0;
    Vec8f sumVec8 = 0.0;
    Vec8f sumVec9 = 0.0;
    Vec8f sumVec10 = 0.0;
    Vec8f sumVec11 = 0.0;
    Vec8f sumVec12 = 0.0;
    Vec8f sumVec13 = 0.0;
    Vec8f sumVec14 = 0.0;

    double t0_start = omp_get_wtime();
    for (j=0; j<0x200000; j++){
    for (i=0; i<size; i += 120){
        //_mm_prefetch((char*)arr+i+120,_MM_HINT_NTA);
        a0.load(arr + i+8*0);
        a1.load(arr + i+8*1);
        a2.load(arr + i+8*2);
        a3.load(arr + i+8*3);
        a4.load(arr + i+8*4);
        a5.load(arr + i+8*5);
        a6.load(arr + i+8*6);
        a7.load(arr + i+8*7);
        a8.load(arr + i+8*8);
        a9.load(arr + i+8*9);
        a10.load(arr + i+8*10);
        a11.load(arr + i+8*11);
        a12.load(arr + i+8*12);
        a13.load(arr + i+8*13);
        a14.load(arr + i+8*14);
#ifdef FMA
        sumVec0 = mul_add(sumVec0, 1.0, a0);
        sumVec1 = mul_add(sumVec1, 1.0, a1);
        sumVec2 = mul_add(sumVec2, 1.0, a2);
        sumVec3 = mul_add(sumVec3, 1.0, a3);
        sumVec4 = mul_add(sumVec4, 1.0, a4);
        sumVec5 = mul_add(sumVec5, 1.0, a5);
        sumVec6 = mul_add(sumVec6, 1.0, a6);
	    sumVec7 = mul_add(sumVec7, 1.0, a7);
	    sumVec8 = mul_add(sumVec8, 1.0, a8);
	    sumVec9 = mul_add(sumVec9, 1.0, a9);
	    sumVec10 = mul_add(sumVec10, 1.0, a10);
	    sumVec11 = mul_add(sumVec11, 1.0, a11);
	    sumVec12 = mul_add(sumVec12, 1.0, a12);
	    sumVec13 = mul_add(sumVec13, 1.0, a13);
	    sumVec14 = mul_add(sumVec14, 1.0, a14);
#else
	    sumVec0 += a0;
	    sumVec1 += a1;
	    sumVec2 += a2;
	    sumVec3 += a3;
	    sumVec4 += a4;
	    sumVec5 += a5;
	    sumVec6 += a6;
	    sumVec7 += a7;
	    sumVec8 += a8;
	    sumVec9 += a9;
	    sumVec10 += a10;
	    sumVec11 += a11;
	    sumVec12 += a12;
	    sumVec13 += a13;
	    sumVec14 += a14;
#endif
    }
    }
    double t0_end = omp_get_wtime();

    for(i=0; i<8; i++){
        cout<< sumVec0[i] << '|'
            << sumVec1[i] << '|'
            << sumVec2[i] << '|'
            << sumVec3[i] << '|'
            << sumVec4[i] << '|'
            << sumVec5[i] << '|'
            << sumVec6[i] << '|'
            << sumVec7[i] << '|'
            << sumVec8[i] << '|'
            << sumVec9[i] << '|'
            << sumVec10[i] << '|'
            << sumVec11[i] << '|'
            << sumVec12[i] << '|'
            << sumVec13[i] << '|'
            << sumVec14[i] << '|'
            << "\n";
    }
    cout << "duartion = " << (t0_end - t0_start) << " seconds\n";
}
