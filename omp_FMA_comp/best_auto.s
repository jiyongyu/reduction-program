	.file	"best_auto.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.type	main._omp_fn.0, @function
main._omp_fn.0:
.LFB1090:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x68,0x6
	.cfi_escape 0x10,0xd,0x2,0x76,0x78
	.cfi_escape 0x10,0xc,0x2,0x76,0x70
	pushq	%rbx
	movq	%rdi, %r13
	subq	$80, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x60
	movl	size(%rip), %r12d
	call	omp_get_num_threads
	movl	%eax, %ebx
	call	omp_get_thread_num
	movl	%eax, %ecx
	movl	%r12d, %eax
	cltd
	idivl	%ebx
	cmpl	%edx, %ecx
	jge	.L3
	addl	$1, %eax
	xorl	%edx, %edx
.L3:
	imull	%eax, %ecx
	vxorps	%xmm0, %xmm0, %xmm0
	addl	%ecx, %edx
	leal	(%rdx,%rax), %esi
	cmpl	%esi, %edx
	jge	.L12
	movl	%esi, %r9d
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	subl	%edx, %r9d
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	leal	-8(%r9), %r10d
	leal	-1(%r9), %ecx
	movq	arr(%rip), %rax
	shrl	$3, %r10d
	addl	$1, %r10d
	cmpl	$6, %ecx
	leal	0(,%r10,8), %r8d
	jbe	.L10
	movslq	%edx, %rcx
	vmovaps	-80(%rbp), %ymm0
	xorl	%edi, %edi
	leaq	(%rax,%rcx,4), %rcx
    vmovaps .LC0(%rip), %ymm2
.L4:
	vmovups	(%rcx), %xmm1
	addl	$1, %edi
	addq	$32, %rcx
	vinsertf128	$0x1, -16(%rcx), %ymm1, %ymm1
	cmpl	%edi, %r10d
    vfmadd132ps %ymm2, %ymm1, %ymm0
	ja	.L4
	addl	%r8d, %edx
	cmpl	%r8d, %r9d
	vmovaps	%ymm0, -80(%rbp)
	je	.L6
.L10:
	movslq	%edx, %rcx
	vmovss	-80(%rbp), %xmm0
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	1(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	2(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	3(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	4(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	5(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	addl	$6, %edx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	cmpl	%edx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%edx, %rdx
	vaddss	(%rax,%rdx,4), %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
.L6:
	cmpl	%esi, %r12d
	je	.L27
.L8:
	vmovaps	-80(%rbp), %ymm0
	vhaddps	%ymm0, %ymm0, %ymm0
	vhaddps	%ymm0, %ymm0, %ymm1
	vperm2f128	$1, %ymm1, %ymm1, %ymm0
	vaddps	%ymm1, %ymm0, %ymm0
	vzeroupper
.L12:
	leaq	4(%r13), %rcx
	movl	4(%r13), %edx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%eax, %edx
.L9:
	movl	%edx, -84(%rbp)
	movl	%edx, %eax
	vmovss	-84(%rbp), %xmm3
	vaddss	%xmm3, %xmm0, %xmm2
	vmovd	%xmm2, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	jne	.L14
	addq	$80, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	%r12d, 0(%r13)
	jmp	.L8
	.cfi_endproc
.LFE1090:
	.size	main._omp_fn.0, .-main._omp_fn.0
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"sum = "
.LC3:
	.string	"OpenMP-for reduction consume "
.LC4:
	.string	" seconds\n"
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup,"ax",@progbits
.LHOTB5:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1081:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	movl	$10, %ebx
	subq	$56, %rsp
	.cfi_def_cfa_offset 80
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	movl	$8, %edi
	movq	%rax, arr(%rip)
	call	omp_set_num_threads
	call	omp_get_wtime
	vxorps	%xmm1, %xmm1, %xmm1
	vmovsd	%xmm0, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	32(%rsp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$main._omp_fn.0, %edi
	movl	%ebp, 32(%rsp)
	vmovss	%xmm1, 36(%rsp)
	call	GOMP_parallel
	subl	$1, %ebx
	movl	32(%rsp), %ebp
	vmovss	36(%rsp), %xmm1
	jne	.L30
	vmovss	%xmm1, 28(%rsp)
	call	omp_get_wtime
	movl	$.LC2, %esi
	movl	$_ZSt4cout, %edi
	vmovsd	%xmm0, 16(%rsp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vmovss	28(%rsp), %xmm1
	vcvtss2sd	%xmm1, %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	32(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$10, 32(%rsp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vmovsd	16(%rsp), %xmm2
	movl	$.LC3, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	8(%rsp), %xmm2, %xmm0
	vmovsd	%xmm0, 8(%rsp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	8(%rsp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC4, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$56, %rsp
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE1081:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup
.LHOTB6:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_size, @function
_GLOBAL__sub_I_size:
.LFB1089:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE1089:
	.size	_GLOBAL__sub_I_size, .-_GLOBAL__sub_I_size
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_size
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	1600000000
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
    .section    .rodata.cts32,"aM",@progbits,32
    .align 32
.LC0:
    .long   1055353216
    .long   1055353216
    .long   1055353216
    .long   1055353216
    .long   1055353216
    .long   1055353216
    .long   1055353216
    .long   1055353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
