	.file	"fma_auto_ena.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4,,15
	.type	main._omp_fn.0, @function
main._omp_fn.0:
.LFB1090:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movq	%rdi, %rbx
	subq	$320, %rsp
	movl	size(%rip), %r12d
	call	omp_get_num_threads
	movl	%eax, %r13d
	call	omp_get_thread_num
	movl	%eax, %esi
	movl	%r12d, %eax
	cltd
	idivl	%r13d
	cmpl	%edx, %esi
	jge	.L3
	addl	$1, %eax
	xorl	%edx, %edx
.L3:
	imull	%eax, %esi
	addl	%esi, %edx
	leal	(%rdx,%rax), %ecx
	cmpl	%ecx, %edx
	jge	.L38
	movl	%ecx, %eax
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	subl	%edx, %eax
	movq	$0, -288(%rbp)
	movq	$0, -280(%rbp)
	movl	%eax, %esi
	movq	$0, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	arr(%rip), %rdi
	movl	%eax, -324(%rbp)
	subl	$8, %eax
	shrl	$3, %eax
	addl	$1, %eax
	movl	%eax, -328(%rbp)
	sall	$3, %eax
	movl	%eax, -320(%rbp)
	movl	%esi, %eax
	subl	$1, %eax
	cmpl	$6, %eax
	jbe	.L14
	movslq	%edx, %rax
	vmovaps	-80(%rbp), %ymm0
	xorl	%esi, %esi
	leaq	4(,%rax,4), %r14
	leaq	8(,%rax,4), %r13
	leaq	12(,%rax,4), %r11
	leaq	16(,%rax,4), %r10
	leaq	20(,%rax,4), %r9
	leaq	24(,%rax,4), %r8
	leaq	(%rdi,%rax,4), %r15
	leaq	28(,%rax,4), %rax
	vmovaps	-112(%rbp), %ymm2
	vmovaps	-144(%rbp), %ymm3
	addq	%rdi, %r14
	addq	%rdi, %r13
	vmovaps	-176(%rbp), %ymm4
	addq	%rdi, %r11
	addq	%rdi, %r10
	vmovaps	-208(%rbp), %ymm5
	addq	%rdi, %r9
	addq	%rdi, %r8
	vmovaps	-240(%rbp), %ymm6
	addq	%rdi, %rax
	movl	$0, -312(%rbp)
	vmovaps	-272(%rbp), %ymm7
	movl	%edx, -316(%rbp)
	vmovaps	-304(%rbp), %ymm8
	vmovapd	.LC1(%rip), %ymm9
.L4:
	vmovups	(%r15,%rsi), %xmm1
	vcvtps2pd	%xmm8, %ymm10
	vextractf128	$0x1, %ymm8, %xmm8
	addl	$1, -312(%rbp)
	vinsertf128	$0x1, 16(%r15,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm8, %ymm8
	movl	-312(%rbp), %edx
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm8, %ymm1
	vcvtpd2psy	%ymm10, %xmm8
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm8, %ymm8
	vcvtps2pd	%xmm7, %ymm10
	vmovups	(%r14,%rsi), %xmm1
	vextractf128	$0x1, %ymm7, %xmm7
	vinsertf128	$0x1, 16(%r14,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm7, %ymm7
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm7, %ymm1
	vcvtpd2psy	%ymm10, %xmm7
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm7, %ymm7
	vcvtps2pd	%xmm6, %ymm10
	vmovups	0(%r13,%rsi), %xmm1
	vextractf128	$0x1, %ymm6, %xmm6
	vinsertf128	$0x1, 16(%r13,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm6, %ymm6
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm6, %ymm1
	vcvtpd2psy	%ymm10, %xmm6
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm6, %ymm6
	vcvtps2pd	%xmm5, %ymm10
	vmovups	(%r11,%rsi), %xmm1
	vextractf128	$0x1, %ymm5, %xmm5
	vinsertf128	$0x1, 16(%r11,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm5, %ymm5
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm5, %ymm1
	vcvtpd2psy	%ymm10, %xmm5
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm5, %ymm5
	vcvtps2pd	%xmm4, %ymm10
	vmovups	(%r10,%rsi), %xmm1
	vextractf128	$0x1, %ymm4, %xmm4
	vinsertf128	$0x1, 16(%r10,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm4, %ymm4
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm4, %ymm1
	vcvtpd2psy	%ymm10, %xmm4
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm4, %ymm4
	vcvtps2pd	%xmm3, %ymm10
	vmovups	(%r9,%rsi), %xmm1
	vextractf128	$0x1, %ymm3, %xmm3
	vinsertf128	$0x1, 16(%r9,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm3, %ymm3
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm3, %ymm1
	vcvtpd2psy	%ymm10, %xmm3
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm3, %ymm3
	vcvtps2pd	%xmm2, %ymm10
	vmovups	(%r8,%rsi), %xmm1
	vextractf128	$0x1, %ymm2, %xmm2
	vinsertf128	$0x1, 16(%r8,%rsi), %ymm1, %ymm1
	vcvtps2pd	%xmm2, %ymm2
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm2, %ymm1
	vcvtpd2psy	%ymm10, %xmm2
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm2, %ymm2
	vcvtps2pd	%xmm0, %ymm10
	vmovups	(%rax,%rsi), %xmm1
	vextractf128	$0x1, %ymm0, %xmm0
	vinsertf128	$0x1, 16(%rax,%rsi), %ymm1, %ymm1
	addq	$32, %rsi
	cmpl	%edx, -328(%rbp)
	vcvtps2pd	%xmm0, %ymm0
	vcvtps2pd	%xmm1, %ymm11
	vextractf128	$0x1, %ymm1, %xmm1
	vfmadd132pd	%ymm9, %ymm11, %ymm10
	vcvtps2pd	%xmm1, %ymm1
	vfmadd231pd	%ymm9, %ymm0, %ymm1
	vcvtpd2psy	%ymm10, %xmm0
	vcvtpd2psy	%ymm1, %xmm1
	vinsertf128	$0x1, %xmm1, %ymm0, %ymm0
	ja	.L4
	movl	-320(%rbp), %eax
	movl	-316(%rbp), %edx
	vmovaps	%ymm8, -304(%rbp)
	vmovaps	%ymm7, -272(%rbp)
	vmovaps	%ymm6, -240(%rbp)
	addl	%eax, %edx
	cmpl	-324(%rbp), %eax
	vmovaps	%ymm5, -208(%rbp)
	vmovaps	%ymm4, -176(%rbp)
	vmovaps	%ymm3, -144(%rbp)
	vmovaps	%ymm2, -112(%rbp)
	vmovaps	%ymm0, -80(%rbp)
	je	.L31
	vzeroupper
.L14:
	cmpl	%edx, %ecx
	movslq	%edx, %r9
	leal	-2(%rcx), %r8d
	setg	%r10b
	cmpl	$-2147483646, %ecx
	leaq	(%rdi,%r9,4), %rax
	setge	%sil
	testb	%sil, %r10b
	je	.L39
	leal	1(%rdx), %esi
	vmovsd	.LC2(%rip), %xmm0
	cmpl	%r8d, %esi
	jge	.L12
	vxorpd	%xmm11, %xmm11, %xmm11
	vmovss	-80(%rbp), %xmm9
	vxorpd	%xmm13, %xmm13, %xmm13
	vxorpd	%xmm6, %xmm6, %xmm6
	vmovss	-112(%rbp), %xmm14
	vxorpd	%xmm10, %xmm10, %xmm10
	vcvtss2sd	(%rax), %xmm11, %xmm11
	vxorpd	%xmm12, %xmm12, %xmm12
	vcvtss2sd	4(%rax), %xmm13, %xmm13
	vxorpd	%xmm5, %xmm5, %xmm5
	vcvtss2sd	8(%rax), %xmm6, %xmm6
	vmovsd	.LC2(%rip), %xmm0
	vcvtss2sd	16(%rax), %xmm10, %xmm10
	vcvtss2sd	20(%rax), %xmm12, %xmm12
	vcvtss2sd	24(%rax), %xmm5, %xmm5
	vmovss	-144(%rbp), %xmm3
	vmovss	-176(%rbp), %xmm8
	vmovss	-208(%rbp), %xmm15
	vmovss	-240(%rbp), %xmm2
	vmovss	-272(%rbp), %xmm1
	vmovss	-304(%rbp), %xmm7
	.p2align 4,,10
	.p2align 3
.L6:
	vcvtss2sd	%xmm7, %xmm7, %xmm7
	vmovapd	%xmm13, %xmm4
	vmovsd	%xmm12, -312(%rbp)
	vxorpd	%xmm13, %xmm13, %xmm13
	vfmadd132sd	%xmm0, %xmm11, %xmm7
	vcvtss2sd	%xmm8, %xmm8, %xmm8
	vcvtsd2ss	%xmm7, %xmm7, %xmm7
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	vxorpd	%xmm11, %xmm11, %xmm11
	vcvtss2sd	%xmm7, %xmm7, %xmm7
	vfmadd132sd	%xmm0, %xmm10, %xmm8
	vxorpd	%xmm10, %xmm10, %xmm10
	vfmadd132sd	%xmm0, %xmm4, %xmm1
	vcvtsd2ss	%xmm8, %xmm8, %xmm8
	vcvtss2sd	%xmm3, %xmm3, %xmm3
	vfmadd132sd	%xmm0, %xmm4, %xmm7
	vcvtss2sd	%xmm8, %xmm8, %xmm8
	vxorpd	%xmm12, %xmm12, %xmm12
	vmovsd	-312(%rbp), %xmm4
	vcvtsd2ss	%xmm7, %xmm7, %xmm7
	vcvtsd2ss	%xmm1, %xmm1, %xmm1
	vcvtss2sd	%xmm7, %xmm7, %xmm7
	addl	$3, %edx
	addq	$12, %rax
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	vfmadd132sd	%xmm0, %xmm4, %xmm8
	vfmadd132sd	%xmm0, %xmm4, %xmm3
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm8, %xmm8, %xmm8
	vcvtsd2ss	%xmm3, %xmm3, %xmm3
	vcvtss2sd	%xmm8, %xmm8, %xmm8
	vfmadd132sd	%xmm0, %xmm6, %xmm7
	vfmadd132sd	%xmm0, %xmm6, %xmm1
	vcvtss2sd	%xmm3, %xmm3, %xmm3
	vfmadd132sd	%xmm0, %xmm6, %xmm2
	vxorpd	%xmm6, %xmm6, %xmm6
	vcvtss2sd	%xmm14, %xmm14, %xmm14
	vfmadd132sd	%xmm0, %xmm5, %xmm8
	vcvtsd2ss	%xmm2, %xmm2, %xmm2
	vcvtss2sd	(%rax), %xmm11, %xmm11
	vfmadd132sd	%xmm0, %xmm5, %xmm3
	leal	1(%rdx), %esi
	vcvtss2sd	16(%rax), %xmm10, %xmm10
	vfmadd132sd	%xmm0, %xmm5, %xmm14
	vxorpd	%xmm5, %xmm5, %xmm5
	vcvtss2sd	%xmm15, %xmm15, %xmm15
	vcvtsd2ss	%xmm14, %xmm14, %xmm14
	vcvtsd2ss	%xmm1, %xmm1, %xmm1
	vcvtss2sd	%xmm9, %xmm9, %xmm9
	vcvtsd2ss	%xmm3, %xmm3, %xmm3
	vcvtsd2ss	%xmm7, %xmm7, %xmm7
	vcvtss2sd	4(%rax), %xmm13, %xmm13
	vfmadd132sd	%xmm0, %xmm11, %xmm15
	vcvtsd2ss	%xmm15, %xmm15, %xmm15
	vcvtss2sd	20(%rax), %xmm12, %xmm12
	vfmadd132sd	%xmm0, %xmm10, %xmm9
	vcvtsd2ss	%xmm9, %xmm9, %xmm9
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm8, %xmm8, %xmm8
	vcvtss2sd	%xmm15, %xmm15, %xmm15
	vcvtss2sd	%xmm14, %xmm14, %xmm14
	vfmadd132sd	%xmm0, %xmm11, %xmm2
	vcvtsd2ss	%xmm2, %xmm2, %xmm2
	vcvtss2sd	%xmm9, %xmm9, %xmm9
	vfmadd132sd	%xmm0, %xmm13, %xmm15
	vcvtsd2ss	%xmm15, %xmm15, %xmm15
	vcvtss2sd	8(%rax), %xmm6, %xmm6
	vfmadd132sd	%xmm0, %xmm10, %xmm14
	vcvtsd2ss	%xmm14, %xmm14, %xmm14
	vcvtss2sd	24(%rax), %xmm5, %xmm5
	vfmadd132sd	%xmm0, %xmm12, %xmm9
	vcvtsd2ss	%xmm9, %xmm9, %xmm9
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	cmpl	%esi, %r8d
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vcvtss2sd	%xmm15, %xmm15, %xmm15
	vfmadd132sd	%xmm0, %xmm11, %xmm1
	vcvtsd2ss	%xmm1, %xmm1, %xmm1
	vcvtss2sd	%xmm3, %xmm3, %xmm3
	vfmadd132sd	%xmm0, %xmm13, %xmm2
	vcvtsd2ss	%xmm2, %xmm2, %xmm2
	vcvtss2sd	%xmm14, %xmm14, %xmm14
	vfmadd132sd	%xmm0, %xmm6, %xmm15
	vcvtsd2ss	%xmm15, %xmm15, %xmm15
	vcvtss2sd	%xmm9, %xmm9, %xmm9
	vfmadd132sd	%xmm0, %xmm10, %xmm3
	vcvtsd2ss	%xmm3, %xmm3, %xmm3
	vfmadd132sd	%xmm0, %xmm12, %xmm14
	vmovss	%xmm7, -304(%rbp)
	vcvtsd2ss	%xmm14, %xmm14, %xmm14
	vfmadd132sd	%xmm0, %xmm5, %xmm9
	vmovss	%xmm1, -272(%rbp)
	vcvtsd2ss	%xmm9, %xmm9, %xmm9
	vmovss	%xmm2, -240(%rbp)
	vmovss	%xmm15, -208(%rbp)
	vmovss	%xmm8, -176(%rbp)
	vmovss	%xmm3, -144(%rbp)
	vmovss	%xmm14, -112(%rbp)
	vmovss	%xmm9, -80(%rbp)
	jg	.L6
	movslq	%edx, %r9
.L12:
	leaq	(%rdi,%r9,4), %rax
	vmovss	-304(%rbp), %xmm9
	vmovss	-272(%rbp), %xmm8
	vmovss	-240(%rbp), %xmm7
	vmovss	-208(%rbp), %xmm6
	vmovss	-176(%rbp), %xmm5
	vmovss	-144(%rbp), %xmm4
	vmovss	-112(%rbp), %xmm3
	vmovss	-80(%rbp), %xmm1
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$1, %esi
.L7:
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtss2sd	%xmm9, %xmm9, %xmm9
	addq	$4, %rax
	vcvtss2sd	%xmm8, %xmm8, %xmm8
	vcvtss2sd	%xmm7, %xmm7, %xmm7
	vcvtss2sd	-4(%rax), %xmm2, %xmm2
	vcvtss2sd	%xmm6, %xmm6, %xmm6
	vfmadd132sd	%xmm0, %xmm2, %xmm9
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtss2sd	%xmm5, %xmm5, %xmm5
	vcvtss2sd	%xmm4, %xmm4, %xmm4
	vcvtsd2ss	%xmm9, %xmm9, %xmm9
	vcvtss2sd	%xmm3, %xmm3, %xmm3
	vcvtss2sd	(%rax), %xmm2, %xmm2
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	vfmadd132sd	%xmm0, %xmm2, %xmm8
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm8, %xmm8, %xmm8
	vcvtss2sd	4(%rax), %xmm2, %xmm2
	vfmadd132sd	%xmm0, %xmm2, %xmm7
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm7, %xmm7, %xmm7
	vcvtss2sd	8(%rax), %xmm2, %xmm2
	vfmadd132sd	%xmm0, %xmm2, %xmm6
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm6, %xmm6, %xmm6
	vcvtss2sd	12(%rax), %xmm2, %xmm2
	vfmadd132sd	%xmm0, %xmm2, %xmm5
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm5, %xmm5, %xmm5
	vcvtss2sd	16(%rax), %xmm2, %xmm2
	vfmadd132sd	%xmm0, %xmm2, %xmm4
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm4, %xmm4, %xmm4
	vcvtss2sd	20(%rax), %xmm2, %xmm2
	vfmadd132sd	%xmm0, %xmm2, %xmm3
	vxorpd	%xmm2, %xmm2, %xmm2
	vcvtsd2ss	%xmm3, %xmm3, %xmm3
	vcvtss2sd	24(%rax), %xmm2, %xmm2
	cmpl	%esi, %ecx
	vfmadd132sd	%xmm0, %xmm2, %xmm1
	vcvtsd2ss	%xmm1, %xmm1, %xmm1
	jg	.L40
	vmovss	%xmm9, -304(%rbp)
	vmovss	%xmm8, -272(%rbp)
	vmovss	%xmm7, -240(%rbp)
	vmovss	%xmm6, -208(%rbp)
	vmovss	%xmm5, -176(%rbp)
	vmovss	%xmm4, -144(%rbp)
	vmovss	%xmm3, -112(%rbp)
	vmovss	%xmm1, -80(%rbp)
.L9:
	cmpl	%ecx, %r12d
	je	.L41
.L13:
	vxorps	%xmm0, %xmm0, %xmm0
	vaddss	-304(%rbp), %xmm0, %xmm7
	vaddss	-272(%rbp), %xmm0, %xmm6
	vaddss	-240(%rbp), %xmm0, %xmm5
	vaddss	-208(%rbp), %xmm0, %xmm4
	vaddss	-176(%rbp), %xmm0, %xmm3
	vaddss	-144(%rbp), %xmm0, %xmm2
	vaddss	-112(%rbp), %xmm0, %xmm1
	vaddss	-80(%rbp), %xmm0, %xmm0
	vaddss	-300(%rbp), %xmm7, %xmm7
	vaddss	-268(%rbp), %xmm6, %xmm6
	vaddss	-236(%rbp), %xmm5, %xmm5
	vaddss	-204(%rbp), %xmm4, %xmm4
	vaddss	-172(%rbp), %xmm3, %xmm3
	vaddss	-140(%rbp), %xmm2, %xmm2
	vaddss	-108(%rbp), %xmm1, %xmm1
	vaddss	-76(%rbp), %xmm0, %xmm0
	vaddss	-296(%rbp), %xmm7, %xmm7
	vaddss	-264(%rbp), %xmm6, %xmm6
	vaddss	-232(%rbp), %xmm5, %xmm5
	vaddss	-200(%rbp), %xmm4, %xmm4
	vaddss	-168(%rbp), %xmm3, %xmm3
	vaddss	-136(%rbp), %xmm2, %xmm2
	vaddss	-104(%rbp), %xmm1, %xmm1
	vaddss	-72(%rbp), %xmm0, %xmm0
	vaddss	-292(%rbp), %xmm7, %xmm7
	vaddss	-228(%rbp), %xmm5, %xmm5
	vaddss	-196(%rbp), %xmm4, %xmm4
	vaddss	-164(%rbp), %xmm3, %xmm3
	vaddss	-132(%rbp), %xmm2, %xmm2
	vaddss	-100(%rbp), %xmm1, %xmm1
	vaddss	-68(%rbp), %xmm0, %xmm0
	vaddss	-260(%rbp), %xmm6, %xmm6
	vaddss	-288(%rbp), %xmm7, %xmm7
	vaddss	-256(%rbp), %xmm6, %xmm6
	vaddss	-224(%rbp), %xmm5, %xmm5
	vaddss	-192(%rbp), %xmm4, %xmm4
	vaddss	-160(%rbp), %xmm3, %xmm3
	vaddss	-128(%rbp), %xmm2, %xmm2
	vaddss	-96(%rbp), %xmm1, %xmm1
	vaddss	-64(%rbp), %xmm0, %xmm0
	vaddss	-284(%rbp), %xmm7, %xmm7
	vaddss	-252(%rbp), %xmm6, %xmm6
	vaddss	-220(%rbp), %xmm5, %xmm5
	vaddss	-188(%rbp), %xmm4, %xmm4
	vaddss	-156(%rbp), %xmm3, %xmm3
	vaddss	-124(%rbp), %xmm2, %xmm2
	vaddss	-92(%rbp), %xmm1, %xmm1
	vaddss	-60(%rbp), %xmm0, %xmm0
	vaddss	-280(%rbp), %xmm7, %xmm7
	vaddss	-248(%rbp), %xmm6, %xmm6
	vaddss	-216(%rbp), %xmm5, %xmm5
	vaddss	-184(%rbp), %xmm4, %xmm4
	vaddss	-152(%rbp), %xmm3, %xmm3
	vaddss	-120(%rbp), %xmm2, %xmm2
	vaddss	-88(%rbp), %xmm1, %xmm1
	vaddss	-56(%rbp), %xmm0, %xmm0
	vaddss	-276(%rbp), %xmm7, %xmm7
	vaddss	-244(%rbp), %xmm6, %xmm6
	vaddss	-212(%rbp), %xmm5, %xmm5
	vaddss	-180(%rbp), %xmm4, %xmm4
	vaddss	-148(%rbp), %xmm3, %xmm3
	vaddss	-116(%rbp), %xmm2, %xmm2
	vaddss	-84(%rbp), %xmm1, %xmm1
	vaddss	-52(%rbp), %xmm0, %xmm0
.L16:
	vmovss	%xmm0, -340(%rbp)
	vmovss	%xmm1, -336(%rbp)
	vmovss	%xmm2, -332(%rbp)
	vmovss	%xmm3, -328(%rbp)
	vmovss	%xmm4, -324(%rbp)
	vmovss	%xmm5, -320(%rbp)
	vmovss	%xmm6, -316(%rbp)
	vmovss	%xmm7, -312(%rbp)
	call	GOMP_atomic_start
	vmovss	-312(%rbp), %xmm7
	vmovss	-316(%rbp), %xmm6
	vaddss	4(%rbx), %xmm7, %xmm7
	vmovss	-320(%rbp), %xmm5
	vaddss	8(%rbx), %xmm6, %xmm6
	vmovss	-324(%rbp), %xmm4
	vaddss	12(%rbx), %xmm5, %xmm5
	vmovss	-328(%rbp), %xmm3
	vaddss	16(%rbx), %xmm4, %xmm4
	vmovss	-332(%rbp), %xmm2
	vaddss	20(%rbx), %xmm3, %xmm3
	vmovss	-336(%rbp), %xmm1
	vaddss	24(%rbx), %xmm2, %xmm2
	vmovss	-340(%rbp), %xmm0
	vaddss	28(%rbx), %xmm1, %xmm1
	vaddss	32(%rbx), %xmm0, %xmm0
	vmovss	%xmm7, 4(%rbx)
	vmovss	%xmm6, 8(%rbx)
	vmovss	%xmm5, 12(%rbx)
	vmovss	%xmm4, 16(%rbx)
	vmovss	%xmm3, 20(%rbx)
	vmovss	%xmm2, 24(%rbx)
	vmovss	%xmm1, 28(%rbx)
	vmovss	%xmm0, 32(%rbx)
	call	GOMP_atomic_end
	addq	$320, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	vxorps	%xmm0, %xmm0, %xmm0
	vmovaps	%xmm0, %xmm1
	vmovaps	%xmm0, %xmm2
	vmovaps	%xmm0, %xmm3
	vmovaps	%xmm0, %xmm4
	vmovaps	%xmm0, %xmm5
	vmovaps	%xmm0, %xmm6
	vmovaps	%xmm0, %xmm7
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%r12d, (%rbx)
	jmp	.L13
.L31:
	vzeroupper
	jmp	.L9
.L39:
	vmovsd	.LC2(%rip), %xmm0
	leal	1(%rdx), %esi
	jmp	.L12
	.cfi_endproc
.LFE1090:
	.size	main._omp_fn.0, .-main._omp_fn.0
	.section	.text.unlikely
.LCOLDE3:
	.text
.LHOTE3:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"sum = "
.LC5:
	.string	"OpenMP-for reduction consume "
.LC6:
	.string	" seconds\n"
	.section	.text.unlikely
.LCOLDB7:
	.section	.text.startup,"ax",@progbits
.LHOTB7:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1081:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x70,0x6
	.cfi_escape 0x10,0xc,0x2,0x76,0x78
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x68
	movl	$10, %ebx
	subq	$152, %rsp
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	movl	$1, %edi
	movq	%rax, arr(%rip)
	call	omp_set_num_threads
	call	omp_get_wtime
	vxorps	%xmm6, %xmm6, %xmm6
	vmovsd	%xmm0, -120(%rbp)
	vmovaps	%xmm6, %xmm3
	vmovaps	%xmm6, %xmm7
	vmovaps	%xmm6, %xmm2
	vmovaps	%xmm6, %xmm8
	vmovaps	%xmm6, %xmm4
	vmovaps	%xmm6, %xmm5
	vmovaps	%xmm6, %xmm1
	.p2align 4,,10
	.p2align 3
.L44:
	vunpcklps	%xmm6, %xmm3, %xmm3
	leaq	-112(%rbp), %rsi
	xorl	%ecx, %ecx
	vunpcklps	%xmm7, %xmm2, %xmm2
	xorl	%edx, %edx
	vunpcklps	%xmm8, %xmm4, %xmm4
	movl	$main._omp_fn.0, %edi
	vunpcklps	%xmm5, %xmm1, %xmm1
	movl	%r12d, -112(%rbp)
	vmovlhps	%xmm3, %xmm2, %xmm0
	vmovlhps	%xmm4, %xmm1, %xmm1
	vinsertf128	$0x1, %xmm0, %ymm1, %ymm0
	vmovups	%xmm0, -108(%rbp)
	vextractf128	$0x1, %ymm0, -92(%rbp)
	vzeroupper
	call	GOMP_parallel
	subl	$1, %ebx
	movl	-112(%rbp), %r12d
	vmovss	-108(%rbp), %xmm1
	vmovss	-104(%rbp), %xmm5
	vmovss	-100(%rbp), %xmm4
	vmovss	-96(%rbp), %xmm8
	vmovss	-92(%rbp), %xmm2
	vmovss	-88(%rbp), %xmm7
	vmovss	-84(%rbp), %xmm3
	vmovss	-80(%rbp), %xmm6
	jne	.L44
	vmovss	%xmm6, -160(%rbp)
	vmovss	%xmm3, -156(%rbp)
	vmovss	%xmm7, -152(%rbp)
	vmovss	%xmm2, -148(%rbp)
	vmovss	%xmm8, -144(%rbp)
	vmovss	%xmm4, -140(%rbp)
	vmovss	%xmm5, -128(%rbp)
	vmovss	%xmm1, -124(%rbp)
	call	omp_get_wtime
	vmovss	-124(%rbp), %xmm1
	movl	$.LC4, %esi
	vmovss	-128(%rbp), %xmm5
	movl	$_ZSt4cout, %edi
	vaddss	%xmm1, %xmm5, %xmm1
	vmovss	-140(%rbp), %xmm4
	vmovss	-144(%rbp), %xmm8
	vmovss	-148(%rbp), %xmm2
	vmovss	-152(%rbp), %xmm7
	vaddss	%xmm1, %xmm4, %xmm5
	vmovss	-156(%rbp), %xmm3
	vmovss	-160(%rbp), %xmm6
	vmovsd	%xmm0, -136(%rbp)
	vaddss	%xmm5, %xmm8, %xmm4
	vaddss	%xmm4, %xmm2, %xmm1
	vaddss	%xmm1, %xmm7, %xmm2
	vaddss	%xmm2, %xmm3, %xmm1
	vaddss	%xmm1, %xmm6, %xmm7
	vmovss	%xmm7, -124(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-124(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-112(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$10, -112(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vmovsd	-136(%rbp), %xmm9
	movl	$.LC5, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	-120(%rbp), %xmm9, %xmm0
	vmovsd	%xmm0, -120(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	-120(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC6, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1081:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE7:
	.section	.text.startup
.LHOTE7:
	.section	.text.unlikely
.LCOLDB8:
	.section	.text.startup
.LHOTB8:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_size, @function
_GLOBAL__sub_I_size:
.LFB1089:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE1089:
	.size	_GLOBAL__sub_I_size, .-_GLOBAL__sub_I_size
	.section	.text.unlikely
.LCOLDE8:
	.section	.text.startup
.LHOTE8:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_size
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	1600000000
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC1:
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
