	.file	"fma_man_dis.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.type	main._omp_fn.0, @function
main._omp_fn.0:
.LFB5757:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	leaq	16(%rsp), %r13
	.cfi_def_cfa 13, 0
	andq	$-32, %rsp
	pushq	-8(%r13)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r13
	.cfi_escape 0xf,0x3,0x76,0x78,0x6
	pushq	%r12
	pushq	%rbx
	.cfi_escape 0x10,0xc,0x2,0x76,0x70
	.cfi_escape 0x10,0x3,0x2,0x76,0x68
	movq	%rdi, %rbx
	subq	$56, %rsp
	vmovaps	%ymm1, -80(%rbp)
	vzeroupper
	call	omp_get_num_threads
	movl	%eax, %r12d
	call	omp_get_thread_num
	movl	size(%rip), %ecx
	movl	%eax, %edi
	vmovaps	-80(%rbp), %ymm1
	leal	14(%rcx), %esi
	addl	$7, %ecx
	cmovs	%esi, %ecx
	movl	%ecx, %eax
	sarl	$3, %eax
	cltd
	idivl	%r12d
	cmpl	%edx, %edi
	jge	.L5
	addl	$1, %eax
	xorl	%edx, %edx
.L5:
	imull	%eax, %edi
	addl	%edi, %edx
	addl	%edx, %eax
	cmpl	%eax, %edx
	jge	.L3
	sall	$3, %edx
	movq	(%rbx), %rdi
	movq	arr(%rip), %rsi
	movslq	%edx, %rcx
	vmovaps	.LC0(%rip), %ymm2
	sall	$3, %eax
	salq	$2, %rcx
	.p2align 4,,10
	.p2align 3
.L4:
	vmovups	(%rsi,%rcx), %ymm0
	addl	$8, %edx
	addq	$32, %rcx
	cmpl	%edx, %eax
	vmovaps	%ymm0, (%rdi)
	vfmadd132ps	%ymm2, %ymm0, %ymm1
	jg	.L4
.L3:
	vmovaps	%ymm1, -80(%rbp)
	vzeroupper
	call	GOMP_atomic_start
	movq	8(%rbx), %rax
	vmovaps	-80(%rbp), %ymm1
	vaddps	(%rax), %ymm1, %ymm1
	vmovaps	%ymm1, (%rax)
	vzeroupper
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	.cfi_def_cfa 13, 0
	popq	%rbp
	leaq	-16(%r13), %rsp
	.cfi_def_cfa 7, 16
	popq	%r13
	.cfi_def_cfa_offset 8
	jmp	GOMP_atomic_end
	.cfi_endproc
.LFE5757:
	.size	main._omp_fn.0, .-main._omp_fn.0
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"sum = "
.LC4:
	.string	"OpenMP-for reduction consume "
.LC5:
	.string	" seconds\n"
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup,"ax",@progbits
.LHOTB6:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5687:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x68,0x6
	.cfi_escape 0x10,0xd,0x2,0x76,0x78
	.cfi_escape 0x10,0xc,0x2,0x76,0x70
	pushq	%rbx
	subq	$144, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x60
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	vxorps	%xmm0, %xmm0, %xmm0
	movl	$1, %edi
	movq	%rax, arr(%rip)
	vmovaps	%ymm0, -112(%rbp)
	vzeroupper
	leaq	-112(%rbp), %r13
	call	omp_set_num_threads
	leaq	-144(%rbp), %r12
	movl	$10, %ebx
	call	omp_get_wtime
	vmovsd	%xmm0, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	-80(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$main._omp_fn.0, %edi
	movq	%r13, -72(%rbp)
	movq	%r12, -80(%rbp)
	call	GOMP_parallel
	subl	$1, %ebx
	jne	.L11
	call	omp_get_wtime
	vmovaps	-112(%rbp), %ymm2
	vxorps	%xmm1, %xmm1, %xmm1
	movl	$.LC3, %esi
	movl	$_ZSt4cout, %edi
	vmovsd	%xmm0, -168(%rbp)
	vmovups	%ymm2, -80(%rbp)
	vaddss	-80(%rbp), %xmm1, %xmm1
	vaddss	-76(%rbp), %xmm1, %xmm1
	vaddss	-72(%rbp), %xmm1, %xmm1
	vaddss	-68(%rbp), %xmm1, %xmm1
	vaddss	-64(%rbp), %xmm1, %xmm1
	vaddss	-60(%rbp), %xmm1, %xmm1
	vaddss	-56(%rbp), %xmm1, %xmm1
	vaddss	-52(%rbp), %xmm1, %xmm4
	vmovss	%xmm4, -156(%rbp)
	vzeroupper
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-156(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-80(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$10, -80(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vmovsd	-168(%rbp), %xmm3
	movl	$.LC4, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	-152(%rbp), %xmm3, %xmm0
	vmovsd	%xmm0, -152(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	-152(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC5, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$144, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5687:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.text.unlikely
.LCOLDB7:
	.section	.text.startup
.LHOTB7:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_size, @function
_GLOBAL__sub_I_size:
.LFB5756:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5756:
	.size	_GLOBAL__sub_I_size, .-_GLOBAL__sub_I_size
	.section	.text.unlikely
.LCOLDE7:
	.section	.text.startup
.LHOTE7:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_size
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	1600000000
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC0:
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
