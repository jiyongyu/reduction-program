	.file	"old_scheme.cpp"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"\n"
.LC2:
	.string	"duartion = "
.LC3:
	.string	" seconds\n"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB4:
	.section	.text.startup,"ax",@progbits
.LHOTB4:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5687:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x78,0x6
	pushq	%rbx
	subq	$992, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x70
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	movq	%rax, arr(%rip)
	call	omp_get_wtime
	vxorps	%xmm15, %xmm15, %xmm15
	movl	size(%rip), %esi
	vmovsd	%xmm0, -72(%rbp)
	movl	$2097152, %edi
	vmovaps	.LC0(%rip), %ymm0
	vmovaps	%ymm15, %ymm14
	vmovaps	%ymm15, %ymm13
	vmovaps	%ymm15, %ymm12
	vmovaps	%ymm15, %ymm11
	vmovaps	%ymm15, %ymm10
	vmovaps	%ymm15, %ymm9
	vmovaps	%ymm15, %ymm8
	vmovaps	%ymm15, %ymm7
	vmovaps	%ymm15, %ymm6
	vmovaps	%ymm15, %ymm5
	vmovaps	%ymm15, %ymm4
	vmovaps	%ymm15, %ymm3
	vmovaps	%ymm15, %ymm2
	vmovaps	%ymm15, %ymm1
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	arr(%rip), %rdx
	jle	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	addl	$120, %ecx
	vfmadd213ps	(%rdx,%rax), %ymm0, %ymm1
	vfmadd213ps	32(%rdx,%rax), %ymm0, %ymm2
	vfmadd213ps	64(%rdx,%rax), %ymm0, %ymm3
	vfmadd213ps	96(%rdx,%rax), %ymm0, %ymm4
	vfmadd213ps	128(%rdx,%rax), %ymm0, %ymm5
	vfmadd213ps	160(%rdx,%rax), %ymm0, %ymm6
	vfmadd213ps	192(%rdx,%rax), %ymm0, %ymm7
	vfmadd213ps	224(%rdx,%rax), %ymm0, %ymm8
	vfmadd213ps	256(%rdx,%rax), %ymm0, %ymm9
	vfmadd213ps	288(%rdx,%rax), %ymm0, %ymm10
	vfmadd213ps	320(%rdx,%rax), %ymm0, %ymm11
	vfmadd213ps	352(%rdx,%rax), %ymm0, %ymm12
	vfmadd213ps	384(%rdx,%rax), %ymm0, %ymm13
	vfmadd213ps	416(%rdx,%rax), %ymm0, %ymm14
	vfmadd213ps	448(%rdx,%rax), %ymm0, %ymm15
	addq	$480, %rax
	cmpl	%ecx, %esi
	jg	.L4
.L7:
	subl	$1, %edi
	jne	.L3
	vmovaps	%ymm1, -560(%rbp)
	vmovaps	%ymm2, -528(%rbp)
	vmovaps	%ymm3, -496(%rbp)
	vmovaps	%ymm4, -464(%rbp)
	vmovaps	%ymm5, -432(%rbp)
	vmovaps	%ymm6, -400(%rbp)
	vmovaps	%ymm7, -368(%rbp)
	vmovaps	%ymm8, -336(%rbp)
	vmovaps	%ymm9, -304(%rbp)
	vmovaps	%ymm10, -272(%rbp)
	vmovaps	%ymm11, -240(%rbp)
	vmovaps	%ymm12, -208(%rbp)
	vmovaps	%ymm13, -176(%rbp)
	vmovaps	%ymm14, -144(%rbp)
	vmovaps	%ymm15, -112(%rbp)
	vzeroupper
	xorl	%ebx, %ebx
	call	omp_get_wtime
	vmovaps	-528(%rbp), %ymm2
	vmovaps	-496(%rbp), %ymm3
	vmovaps	-464(%rbp), %ymm4
	vmovaps	-432(%rbp), %ymm5
	vmovaps	-400(%rbp), %ymm6
	vmovaps	-368(%rbp), %ymm7
	vmovaps	-336(%rbp), %ymm8
	vmovaps	-304(%rbp), %ymm9
	vmovaps	-272(%rbp), %ymm10
	vmovaps	-240(%rbp), %ymm11
	vmovaps	-208(%rbp), %ymm12
	vmovaps	-176(%rbp), %ymm13
	vmovaps	-144(%rbp), %ymm14
	vmovaps	-112(%rbp), %ymm15
	vmovsd	%xmm0, -80(%rbp)
	vmovaps	%ymm2, -592(%rbp)
	vmovaps	%ymm3, -624(%rbp)
	vmovaps	%ymm4, -656(%rbp)
	vmovaps	%ymm5, -688(%rbp)
	vmovaps	%ymm6, -720(%rbp)
	vmovaps	%ymm7, -752(%rbp)
	vmovaps	%ymm8, -784(%rbp)
	vmovaps	%ymm9, -816(%rbp)
	vmovaps	%ymm10, -848(%rbp)
	vmovaps	%ymm11, -880(%rbp)
	vmovaps	%ymm12, -912(%rbp)
	vmovaps	%ymm13, -944(%rbp)
	vmovaps	%ymm14, -976(%rbp)
	vmovaps	%ymm15, -1008(%rbp)
.L8:
	vmovaps	-1008(%rbp), %ymm1
	movl	$_ZSt4cout, %edi
	vmovaps	-976(%rbp), %ymm3
	vmovups	%ymm1, -48(%rbp)
	vmovaps	-944(%rbp), %ymm5
	vmovss	-48(%rbp,%rbx), %xmm2
	vmovups	%ymm3, -48(%rbp)
	vmovaps	-912(%rbp), %ymm7
	vmovss	-48(%rbp,%rbx), %xmm4
	vmovups	%ymm5, -48(%rbp)
	vmovaps	-880(%rbp), %ymm3
	vmovss	-48(%rbp,%rbx), %xmm6
	vmovups	%ymm7, -48(%rbp)
	vmovaps	-848(%rbp), %ymm7
	vmovss	-48(%rbp,%rbx), %xmm1
	vmovups	%ymm3, -48(%rbp)
	vmovaps	-784(%rbp), %ymm0
	vmovss	-48(%rbp,%rbx), %xmm5
	vmovups	%ymm7, -48(%rbp)
	vmovaps	-816(%rbp), %ymm7
	vmovss	-48(%rbp,%rbx), %xmm3
	vmovups	%ymm7, -48(%rbp)
	vmovaps	-752(%rbp), %ymm8
	vmovss	-48(%rbp,%rbx), %xmm7
	vmovups	%ymm0, -48(%rbp)
	vmovss	%xmm2, -112(%rbp)
	vmovss	-48(%rbp,%rbx), %xmm0
	vmovss	%xmm0, -336(%rbp)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovss	%xmm4, -144(%rbp)
	vmovss	%xmm6, -176(%rbp)
	vmovss	%xmm1, -208(%rbp)
	vmovss	%xmm5, -240(%rbp)
	vmovss	%xmm3, -272(%rbp)
	vmovss	%xmm7, -304(%rbp)
	vmovups	%ymm8, -48(%rbp)
	vmovaps	-720(%rbp), %ymm10
	vmovss	-48(%rbp,%rbx), %xmm9
	vmovaps	-688(%rbp), %ymm12
	vmovups	%ymm10, -48(%rbp)
	vmovaps	-656(%rbp), %ymm14
	vmovss	-48(%rbp,%rbx), %xmm11
	vmovups	%ymm12, -48(%rbp)
	vmovaps	-624(%rbp), %ymm8
	vmovss	-48(%rbp,%rbx), %xmm13
	vmovups	%ymm14, -48(%rbp)
	vmovaps	-592(%rbp), %ymm12
	vmovss	-48(%rbp,%rbx), %xmm15
	vmovups	%ymm8, -48(%rbp)
	vmovaps	-560(%rbp), %ymm8
	vmovss	-48(%rbp,%rbx), %xmm10
	vmovups	%ymm12, -48(%rbp)
	vmovss	%xmm9, -368(%rbp)
	vmovss	-48(%rbp,%rbx), %xmm14
	vmovups	%ymm8, -48(%rbp)
	vmovss	%xmm11, -400(%rbp)
	vcvtss2sd	-48(%rbp,%rbx), %xmm0, %xmm0
	vmovss	%xmm13, -432(%rbp)
	vmovss	%xmm15, -464(%rbp)
	vmovss	%xmm10, -496(%rbp)
	vmovss	%xmm14, -528(%rbp)
	vzeroupper
	addq	$4, %rbx
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-63(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -63(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-528(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-62(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -62(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-496(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-61(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -61(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-464(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-60(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -60(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-432(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-59(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -59(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-400(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-58(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -58(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-368(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-57(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -57(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-336(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-56(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -56(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-304(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-55(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -55(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-272(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-54(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -54(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-240(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-53(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -53(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-208(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-52(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -52(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-176(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-51(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -51(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-144(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-50(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -50(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-112(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-49(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -49(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	$1, %edx
	movl	$.LC1, %esi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	cmpq	$32, %rbx
	jne	.L8
	vmovsd	-80(%rbp), %xmm2
	movl	$.LC2, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	-72(%rbp), %xmm2, %xmm0
	vmovsd	%xmm0, -112(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	-112(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC3, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$992, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5687:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE4:
	.section	.text.startup
.LHOTE4:
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup
.LHOTB5:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_arr, @function
_GLOBAL__sub_I_arr:
.LFB5756:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5756:
	.size	_GLOBAL__sub_I_arr, .-_GLOBAL__sub_I_arr
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_arr
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	4096
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC0:
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
