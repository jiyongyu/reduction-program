/////////////////////////////////////////////////////////////////////////////////
//
//      best_auto.cpp: the best performance attained by #pragma parallel simd
//          
//          Actually the multi-threading version of add_auto_dis.cpp 
//          1. add; 
//          2. auto vectorize(#pragma simd); 
//          3. disable unrolling
//          4. enable multi-threading
//
/////////////////////////////////////////////////////////////////////////////////
#include <ctime>
#include <math.h>
#include <float.h>
#include <fenv.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <iostream>
#include <omp.h>
#include <math.h>
#include "../iacaMarks.h"

//#define TESTING

using namespace std;

#ifdef TESTING
int size = 128;
#else
int size = 1600000000;
#endif

float *arr;



main(){

	int i, j;
    arr = new float [size];

#ifdef TESTING
    for(int i=0; i<size; i++){
        arr[i] = (float)i;
    }
#endif

    float sum0 = 0.0;

    omp_set_num_threads(8);

    double t0_start = omp_get_wtime();
    for (i=0; i<10; i++){
    	#pragma omp parallel
        {
            #pragma omp for simd reduction(+:sum0)
            for (j=0; j<size; j++){
//IACA_START
                sum0 += arr[j];
            } 
//IACA_END
        }
    }    
    double t0_end = omp_get_wtime();

    cout << "sum = " << sum0 << '\n';
    cout << "OpenMP-for reduction consume " << (t0_end - t0_start) << " seconds\n";

}
