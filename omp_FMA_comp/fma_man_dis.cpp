/////////////////////////////////////////////////////////////////
//
//      fma_man_dis.cpp:
//          
//          1. fma; 
//          2. manually vectorize(-mavx2); 
//          3. disable unrolling.
//
////////////////////////////////////////////////////////////////
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>

#include <omp.h>
#include "../vector_class/vectorclass.h"

//#define TESTING

using namespace std;

#ifdef TESTING
int size = 16;
#else
int size = 1600000000;
#endif

float *arr;

main(){

    int i, j;
    arr = new float [size];

#ifdef TESTING
    for(int i=0; i<size; i++){
        arr[i] = (float)i;
    }
#endif
    Vec8f a0;
    Vec8f sum0 = 0.0;

#pragma omp declare reduction(+ : Vec8f : \
	omp_out = omp_out + omp_in)
	
    omp_set_num_threads(1);

    double t0_start = omp_get_wtime();
    for (j=0; j<10; j++){
	#pragma omp parallel for reduction(+:sum0)
        for (i=0; i<size; i += 8){
            a0.load(arr + i);
            sum0 = mul_add(sum0, 1.0, a0);
        }
    }
    
    double t0_end = omp_get_wtime();

    float sum = 0;
    for(i=0; i<8; i++){
	    sum += sum0[i];
    }
    cout << "sum = " << sum <<'\n';    
    cout << "OpenMP-for reduction consume " << (t0_end - t0_start) << " seconds\n";
}
