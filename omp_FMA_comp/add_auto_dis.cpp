/////////////////////////////////////////////////////////////////
//
//      add_auto_dis.cpp:
//          
//          1. add; 
//          2. auto vectorize(#pragma simd); 
//          3. disable unrolling.
//
////////////////////////////////////////////////////////////////
#include <ctime>
#include <math.h>
#include <float.h>
#include <fenv.h>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <iostream>
#include <omp.h>
#include <math.h>
#include "../iacaMarks.h"

//#define TESTING
//#define IACA_TEST

using namespace std;

#ifdef TESTING
int size = 128;
#else
int size = 1600000000;
#endif

float *arr;



main(){

	int i, j;
    arr = new float [size];

#ifdef TESTING
    for(int i=0; i<size; i++){
        arr[i] = (float)i;
    }
#endif

    float sum = 0.0;
       
    omp_set_num_threads(1);

    double t0_start = omp_get_wtime();
    for (i=0; i<10; i++){
    	#pragma omp parallel shared(sum, arr, size, i) private(j)
        {
            #pragma omp for simd reduction(+:sum) 
            for (j=0; j<size; j++){
#ifdef IACA_TEST
IACA_START
#endif
                sum += arr[j];
            } 
#ifdef IACA_TEST
IACA_END
#endif
        }
    }    
    double t0_end = omp_get_wtime();

    cout << "sum = " << sum << '\n';
    cout << "OpenMP-for reduction consume " << (t0_end - t0_start) << " seconds\n";

}
