	.file	"fma_man_ena.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.type	main._omp_fn.0, @function
main._omp_fn.0:
.LFB5757:
	.cfi_startproc
	.cfi_personality 0x3,__gxx_personality_v0
	pushq	%r13
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	leaq	16(%rsp), %r13
	.cfi_def_cfa 13, 0
	andq	$-32, %rsp
	pushq	-8(%r13)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r14
	pushq	%r13
	.cfi_escape 0xf,0x3,0x76,0x70,0x6
	.cfi_escape 0x10,0xe,0x2,0x76,0x78
	pushq	%r12
	pushq	%rbx
	.cfi_escape 0x10,0xc,0x2,0x76,0x68
	.cfi_escape 0x10,0x3,0x2,0x76,0x60
	movq	%rdi, %rbx
	subq	$272, %rsp
	vmovaps	%ymm8, -304(%rbp)
	vmovaps	%ymm7, -272(%rbp)
	vmovaps	%ymm6, -240(%rbp)
	vmovaps	%ymm5, -208(%rbp)
	vmovaps	%ymm4, -176(%rbp)
	vmovaps	%ymm3, -144(%rbp)
	vmovaps	%ymm2, -112(%rbp)
	vmovaps	%ymm1, -80(%rbp)
	vzeroupper
	call	omp_get_num_threads
	movl	%eax, %r12d
	call	omp_get_thread_num
	movl	size(%rip), %ecx
	movl	%eax, %edi
	vmovaps	-80(%rbp), %ymm1
	vmovaps	-112(%rbp), %ymm2
	vmovaps	-144(%rbp), %ymm3
	leal	126(%rcx), %esi
	addl	$63, %ecx
	vmovaps	-176(%rbp), %ymm4
	vmovaps	-208(%rbp), %ymm5
	cmovs	%esi, %ecx
	vmovaps	-240(%rbp), %ymm6
	movl	%ecx, %eax
	vmovaps	-272(%rbp), %ymm7
	sarl	$6, %eax
	vmovaps	-304(%rbp), %ymm8
	cltd
	idivl	%r12d
	cmpl	%edx, %edi
	jge	.L5
	addl	$1, %eax
	xorl	%edx, %edx
.L5:
	imull	%eax, %edi
	addl	%edi, %edx
	addl	%edx, %eax
	cmpl	%eax, %edx
	jge	.L3
	sall	$6, %edx
	movq	(%rbx), %r14
	movq	arr(%rip), %r13
	movq	8(%rbx), %r12
	movq	16(%rbx), %r11
	movslq	%edx, %rcx
	movq	24(%rbx), %r10
	movq	32(%rbx), %r9
	sall	$6, %eax
	movq	40(%rbx), %r8
	movq	48(%rbx), %rdi
	salq	$2, %rcx
	movq	56(%rbx), %rsi
	vmovaps	.LC0(%rip), %ymm0
	.p2align 4,,10
	.p2align 3
.L4:
	vmovups	0(%r13,%rcx), %ymm9
	movq	arr(%rip), %r13
	addl	$64, %edx
	vmovaps	%ymm9, (%r14)
	vmovups	32(%r13,%rcx), %ymm9
	movq	arr(%rip), %r13
	vmovaps	%ymm9, (%r12)
	movq	arr(%rip), %r12
	vmovups	64(%r12,%rcx), %ymm9
	movq	8(%rbx), %r12
	vmovaps	%ymm9, (%r11)
	movq	arr(%rip), %r11
	vmovups	96(%r11,%rcx), %ymm9
	movq	16(%rbx), %r11
	vmovaps	%ymm9, (%r10)
	movq	arr(%rip), %r10
	vmovups	128(%r10,%rcx), %ymm9
	movq	24(%rbx), %r10
	vmovaps	%ymm9, (%r9)
	movq	arr(%rip), %r9
	vmovups	160(%r9,%rcx), %ymm9
	movq	32(%rbx), %r9
	vmovaps	%ymm9, (%r8)
	movq	arr(%rip), %r8
	vmovups	192(%r8,%rcx), %ymm9
	movq	40(%rbx), %r8
	vmovaps	%ymm9, (%rdi)
	vmovups	224(%r13,%rcx), %ymm9
	addq	$256, %rcx
	cmpl	%edx, %eax
	vmovaps	%ymm9, (%rsi)
	vfmadd213ps	(%r14), %ymm0, %ymm1
	vfmadd213ps	(%r12), %ymm0, %ymm2
	vfmadd213ps	(%r11), %ymm0, %ymm3
	vfmadd213ps	(%r10), %ymm0, %ymm4
	vfmadd213ps	(%r9), %ymm0, %ymm5
	vfmadd213ps	(%r8), %ymm0, %ymm6
	movq	48(%rbx), %rdi
	movq	56(%rbx), %rsi
	vfmadd213ps	(%rdi), %ymm0, %ymm7
	vfmadd213ps	(%rsi), %ymm0, %ymm8
	jg	.L4
.L3:
	vmovaps	%ymm8, -304(%rbp)
	vmovaps	%ymm7, -272(%rbp)
	vmovaps	%ymm6, -240(%rbp)
	vmovaps	%ymm5, -208(%rbp)
	vmovaps	%ymm4, -176(%rbp)
	vmovaps	%ymm3, -144(%rbp)
	vmovaps	%ymm2, -112(%rbp)
	vmovaps	%ymm1, -80(%rbp)
	vzeroupper
	call	GOMP_atomic_start
	movq	120(%rbx), %rax
	vmovaps	-80(%rbp), %ymm1
	vmovaps	-112(%rbp), %ymm2
	vaddps	(%rax), %ymm1, %ymm1
	vmovaps	-144(%rbp), %ymm3
	vmovaps	-176(%rbp), %ymm4
	vmovaps	-208(%rbp), %ymm5
	vmovaps	-240(%rbp), %ymm6
	vmovaps	-272(%rbp), %ymm7
	vmovaps	-304(%rbp), %ymm8
	vmovaps	%ymm1, (%rax)
	movq	112(%rbx), %rax
	vaddps	(%rax), %ymm2, %ymm2
	vmovaps	%ymm2, (%rax)
	movq	104(%rbx), %rax
	vaddps	(%rax), %ymm3, %ymm3
	vmovaps	%ymm3, (%rax)
	movq	96(%rbx), %rax
	vaddps	(%rax), %ymm4, %ymm4
	vmovaps	%ymm4, (%rax)
	movq	88(%rbx), %rax
	vaddps	(%rax), %ymm5, %ymm5
	vmovaps	%ymm5, (%rax)
	movq	80(%rbx), %rax
	vaddps	(%rax), %ymm6, %ymm6
	vmovaps	%ymm6, (%rax)
	movq	72(%rbx), %rax
	vaddps	(%rax), %ymm7, %ymm7
	vmovaps	%ymm7, (%rax)
	movq	64(%rbx), %rax
	vaddps	(%rax), %ymm8, %ymm8
	vmovaps	%ymm8, (%rax)
	vzeroupper
	addq	$272, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	.cfi_def_cfa 13, 0
	popq	%r14
	popq	%rbp
	leaq	-16(%r13), %rsp
	.cfi_def_cfa 7, 16
	popq	%r13
	.cfi_def_cfa_offset 8
	jmp	GOMP_atomic_end
	.cfi_endproc
.LFE5757:
	.size	main._omp_fn.0, .-main._omp_fn.0
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"OpenMP-for reduction consume "
.LC3:
	.string	" seconds\n"
	.section	.text.unlikely
.LCOLDB4:
	.section	.text.startup,"ax",@progbits
.LHOTB4:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5687:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	pushq	%rbx
	subq	$704, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	vxorps	%xmm0, %xmm0, %xmm0
	movl	$1, %edi
	movq	%rax, arr(%rip)
	vmovaps	%ymm0, -432(%rbp)
	vmovaps	%ymm0, -400(%rbp)
	vmovaps	%ymm0, -368(%rbp)
	vmovaps	%ymm0, -336(%rbp)
	vmovaps	%ymm0, -304(%rbp)
	vmovaps	%ymm0, -272(%rbp)
	vmovaps	%ymm0, -240(%rbp)
	vmovaps	%ymm0, -208(%rbp)
	vzeroupper
	leaq	-432(%rbp), %r15
	call	omp_set_num_threads
	leaq	-400(%rbp), %r14
	leaq	-368(%rbp), %r13
	leaq	-464(%rbp), %r12
	call	omp_get_wtime
	vmovsd	%xmm0, -744(%rbp)
	movl	$10, %ebx
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	-336(%rbp), %rax
	leaq	-176(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$main._omp_fn.0, %edi
	movq	%r15, -56(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-304(%rbp), %rax
	movq	%r14, -64(%rbp)
	movq	%r13, -72(%rbp)
	movq	%r12, -120(%rbp)
	movq	%rax, -88(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -104(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -112(%rbp)
	leaq	-688(%rbp), %rax
	movq	%rax, -176(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rax, -168(%rbp)
	leaq	-624(%rbp), %rax
	movq	%rax, -160(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-560(%rbp), %rax
	movq	%rax, -144(%rbp)
	leaq	-528(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	-496(%rbp), %rax
	movq	%rax, -128(%rbp)
	call	GOMP_parallel
	subl	$1, %ebx
	jne	.L11
	call	omp_get_wtime
	vmovsd	%xmm0, -752(%rbp)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L12:
	vmovaps	-208(%rbp), %ymm0
	movl	$_ZSt4cout, %edi
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-240(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm1
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-272(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm2
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-304(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm3
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-336(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm4
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-368(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm5
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-400(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm6
	vmovups	%ymm0, -176(%rbp)
	vmovaps	-432(%rbp), %ymm0
	vmovss	-176(%rbp,%rbx), %xmm7
	vmovups	%ymm0, -176(%rbp)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovss	%xmm1, -712(%rbp)
	vmovss	%xmm2, -716(%rbp)
	vcvtss2sd	-176(%rbp,%rbx), %xmm0, %xmm0
	vmovss	%xmm3, -720(%rbp)
	vmovss	%xmm4, -724(%rbp)
	vmovss	%xmm5, -728(%rbp)
	vmovss	%xmm6, -732(%rbp)
	vmovss	%xmm7, -736(%rbp)
	vzeroupper
	addq	$4, %rbx
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-697(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -697(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-736(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-696(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -696(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-732(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-695(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -695(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-728(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-694(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -694(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-724(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-693(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -693(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-720(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-692(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -692(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-716(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-691(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -691(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-712(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-690(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$124, -690(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	leaq	-689(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$10, -689(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	cmpq	$32, %rbx
	jne	.L12
	vmovsd	-752(%rbp), %xmm1
	movl	$.LC2, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	-744(%rbp), %xmm1, %xmm0
	vmovsd	%xmm0, -712(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	-712(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC3, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$704, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5687:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE4:
	.section	.text.startup
.LHOTE4:
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup
.LHOTB5:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_arr, @function
_GLOBAL__sub_I_arr:
.LFB5756:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5756:
	.size	_GLOBAL__sub_I_arr, .-_GLOBAL__sub_I_arr
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_arr
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	1600000000
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC0:
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
