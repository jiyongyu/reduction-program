	.file	"fma_auto_dis.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4,,15
	.type	main._omp_fn.0, @function
main._omp_fn.0:
.LFB1090:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x68,0x6
	.cfi_escape 0x10,0xd,0x2,0x76,0x78
	.cfi_escape 0x10,0xc,0x2,0x76,0x70
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x60
	movq	%rdi, %rbx
	subq	$80, %rsp
	movl	size(%rip), %r12d
	call	omp_get_num_threads
	movl	%eax, %r13d
	call	omp_get_thread_num
	movl	%eax, %edi
	movl	%r12d, %eax
	cltd
	idivl	%r13d
	cmpl	%edx, %edi
	jge	.L3
	addl	$1, %eax
	xorl	%edx, %edx
.L3:
	imull	%eax, %edi
	vxorps	%xmm0, %xmm0, %xmm0
	leal	(%rdi,%rdx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %ecx
	jge	.L11
	movl	%eax, %r9d
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	subl	%ecx, %r9d
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	leal	-8(%r9), %r10d
	leal	-1(%r9), %esi
	movq	arr(%rip), %rdx
	shrl	$3, %r10d
	addl	$1, %r10d
	cmpl	$6, %esi
	leal	0(,%r10,8), %r8d
	jbe	.L9
	movslq	%ecx, %rsi
	vmovaps	-80(%rbp), %ymm0
	xorl	%edi, %edi
	leaq	(%rdx,%rsi,4), %rsi
	vmovapd	.LC1(%rip), %ymm4
.L4:
	vmovups	(%rsi), %xmm2
	vcvtps2pd	%xmm0, %ymm1
	vextractf128	$0x1, %ymm0, %xmm0
	addl	$1, %edi
	addq	$32, %rsi
	vinsertf128	$0x1, -16(%rsi), %ymm2, %ymm2
	cmpl	%edi, %r10d
	vcvtps2pd	%xmm2, %ymm3
	vfmadd231pd	%ymm4, %ymm1, %ymm3
	vcvtps2pd	%xmm0, %ymm1
	vextractf128	$0x1, %ymm2, %xmm0
	vcvtps2pd	%xmm0, %ymm0
	vfmadd231pd	%ymm4, %ymm1, %ymm0
	vcvtpd2psy	%ymm3, %xmm1
	vcvtpd2psy	%ymm0, %xmm0
	vinsertf128	$0x1, %xmm0, %ymm1, %ymm0
	ja	.L4
	addl	%r8d, %ecx
	cmpl	%r8d, %r9d
	vmovaps	%ymm0, -80(%rbp)
	je	.L20
	vzeroupper
.L9:
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%ecx, %rsi
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovsd	.LC2(%rip), %xmm1
	vcvtss2sd	(%rdx,%rsi,4), %xmm2, %xmm2
	leal	1(%rcx), %esi
	vcvtss2sd	-80(%rbp), %xmm0, %xmm0
	cmpl	%esi, %eax
	vfmadd132sd	%xmm1, %xmm2, %xmm0
	vcvtsd2ss	%xmm0, %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%esi, %rsi
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	vcvtss2sd	(%rdx,%rsi,4), %xmm2, %xmm2
	leal	2(%rcx), %esi
	cmpl	%esi, %eax
	vfmadd132sd	%xmm1, %xmm2, %xmm0
	vcvtsd2ss	%xmm0, %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%esi, %rsi
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	vcvtss2sd	(%rdx,%rsi,4), %xmm2, %xmm2
	leal	3(%rcx), %esi
	cmpl	%esi, %eax
	vfmadd132sd	%xmm1, %xmm2, %xmm0
	vcvtsd2ss	%xmm0, %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%esi, %rsi
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	vcvtss2sd	(%rdx,%rsi,4), %xmm2, %xmm2
	leal	4(%rcx), %esi
	cmpl	%esi, %eax
	vfmadd132sd	%xmm1, %xmm2, %xmm0
	vcvtsd2ss	%xmm0, %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%esi, %rsi
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	vcvtss2sd	(%rdx,%rsi,4), %xmm2, %xmm2
	leal	5(%rcx), %esi
	cmpl	%esi, %eax
	vfmadd132sd	%xmm1, %xmm2, %xmm0
	vcvtsd2ss	%xmm0, %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%esi, %rsi
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	addl	$6, %ecx
	cmpl	%ecx, %eax
	vcvtss2sd	(%rdx,%rsi,4), %xmm2, %xmm2
	vfmadd132sd	%xmm1, %xmm2, %xmm0
	vcvtsd2ss	%xmm0, %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	vxorpd	%xmm2, %xmm2, %xmm2
	movslq	%ecx, %rcx
	vcvtss2sd	%xmm0, %xmm0, %xmm0
	vxorps	%xmm7, %xmm7, %xmm7
	vcvtss2sd	(%rdx,%rcx,4), %xmm2, %xmm2
	vfmadd132sd	%xmm0, %xmm2, %xmm1
	vcvtsd2ss	%xmm1, %xmm7, %xmm7
	vmovss	%xmm7, -80(%rbp)
.L6:
	vxorps	%xmm0, %xmm0, %xmm0
	vaddss	-80(%rbp), %xmm0, %xmm0
	vaddss	-76(%rbp), %xmm0, %xmm0
	vaddss	-72(%rbp), %xmm0, %xmm0
	vaddss	-68(%rbp), %xmm0, %xmm0
	vaddss	-64(%rbp), %xmm0, %xmm0
	vaddss	-60(%rbp), %xmm0, %xmm0
	vaddss	-56(%rbp), %xmm0, %xmm0
	vaddss	-52(%rbp), %xmm0, %xmm0
.L11:
	movl	(%rbx), %edx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%eax, %edx
.L8:
	movl	%edx, -84(%rbp)
	movl	%edx, %eax
	vmovss	-84(%rbp), %xmm6
	vaddss	%xmm6, %xmm0, %xmm5
	vmovd	%xmm5, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	cmpl	%eax, %edx
	jne	.L13
	addq	$80, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
.L20:
	.cfi_restore_state
	vzeroupper
	jmp	.L6
	.cfi_endproc
.LFE1090:
	.size	main._omp_fn.0, .-main._omp_fn.0
	.section	.text.unlikely
.LCOLDE3:
	.text
.LHOTE3:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"sum = "
.LC5:
	.string	"OpenMP-for reduction consume "
.LC6:
	.string	" seconds\n"
	.section	.text.unlikely
.LCOLDB7:
	.section	.text.startup,"ax",@progbits
.LHOTB7:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1081:
	.cfi_startproc
	pushq	%rbx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	movl	$10, %ebx
	subq	$48, %rsp
	.cfi_def_cfa_offset 64
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	movl	$1, %edi
	movq	%rax, arr(%rip)
	call	omp_set_num_threads
	call	omp_get_wtime
	vxorps	%xmm1, %xmm1, %xmm1
	vmovsd	%xmm0, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	32(%rsp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$main._omp_fn.0, %edi
	vmovss	%xmm1, 32(%rsp)
	call	GOMP_parallel
	subl	$1, %ebx
	vmovss	32(%rsp), %xmm1
	jne	.L29
	vmovss	%xmm1, 28(%rsp)
	call	omp_get_wtime
	movl	$.LC4, %esi
	movl	$_ZSt4cout, %edi
	vmovsd	%xmm0, 16(%rsp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vmovss	28(%rsp), %xmm1
	vcvtss2sd	%xmm1, %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	32(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$10, 32(%rsp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vmovsd	16(%rsp), %xmm2
	movl	$.LC5, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	8(%rsp), %xmm2, %xmm0
	vmovsd	%xmm0, 8(%rsp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	8(%rsp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC6, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$48, %rsp
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE1081:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE7:
	.section	.text.startup
.LHOTE7:
	.section	.text.unlikely
.LCOLDB8:
	.section	.text.startup
.LHOTB8:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_size, @function
_GLOBAL__sub_I_size:
.LFB1089:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE1089:
	.size	_GLOBAL__sub_I_size, .-_GLOBAL__sub_I_size
	.section	.text.unlikely
.LCOLDE8:
	.section	.text.startup
.LHOTE8:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_size
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	1600000000
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC1:
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
