	.file	"add_auto_ena.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.type	main._omp_fn.0, @function
main._omp_fn.0:
.LFB1090:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x68,0x6
	.cfi_escape 0x10,0xd,0x2,0x76,0x78
	.cfi_escape 0x10,0xc,0x2,0x76,0x70
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x60
	movq	%rdi, %rbx
	subq	$400, %rsp
	movl	size(%rip), %r13d
	call	omp_get_num_threads
	movl	%eax, %r12d
	call	omp_get_thread_num
	leal	14(%r13), %edx
	movl	%r13d, %ecx
	movl	%eax, %esi
	addl	$7, %ecx
	cmovs	%edx, %ecx
	movl	%ecx, %eax
	sarl	$3, %eax
	cltd
	idivl	%r12d
	cmpl	%edx, %esi
	jge	.L3
	addl	$1, %eax
	xorl	%edx, %edx
.L3:
	imull	%eax, %esi
	addl	%esi, %edx
	addl	%edx, %eax
	cmpl	%eax, %edx
	jge	.L30
	leal	0(,%rdx,8), %esi
	leal	0(,%rax,8), %edx
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -288(%rbp)
	cmpl	%edx, %esi
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -264(%rbp)
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movq	$0, -152(%rbp)
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	jge	.L12
	movl	%edx, %eax
	movq	arr(%rip), %r8
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edi
	shrl	$3, %edi
	leal	-7(%rdi), %ecx
	leal	1(%rdi), %r10d
	shrl	$3, %ecx
	addl	$1, %ecx
	cmpl	$55, %eax
	leal	0(,%rcx,8), %r11d
	jbe	.L15
	vmovaps	-80(%rbp), %ymm4
	movslq	%esi, %rax
	xorl	%r9d, %r9d
	vmovaps	-144(%rbp), %ymm6
	leaq	(%r8,%rax,4), %rax
	vmovaps	%ymm4, -336(%rbp)
	vmovaps	-112(%rbp), %ymm4
	vmovaps	%ymm6, -368(%rbp)
	vmovaps	%ymm4, -400(%rbp)
	vmovaps	-176(%rbp), %ymm9
	vmovaps	-208(%rbp), %ymm10
	vmovaps	-240(%rbp), %ymm11
	vmovaps	-272(%rbp), %ymm12
	vmovaps	-304(%rbp), %ymm13
.L5:
	vmovups	(%rax), %xmm0
	addl	$1, %r9d
	addq	$256, %rax
	vmovups	-224(%rax), %xmm7
	vinsertf128	$0x1, -240(%rax), %ymm0, %ymm0
	vmovups	-192(%rax), %xmm2
	vinsertf128	$0x1, -208(%rax), %ymm7, %ymm7
	vmovups	-160(%rax), %xmm6
	vinsertf128	$0x1, -176(%rax), %ymm2, %ymm2
	vmovups	-128(%rax), %xmm4
	vinsertf128	$0x1, -144(%rax), %ymm6, %ymm6
	vmovups	-96(%rax), %xmm8
	vshufps	$136, %ymm7, %ymm0, %ymm14
	vshufps	$221, %ymm7, %ymm0, %ymm7
	vmovups	-64(%rax), %xmm3
	vinsertf128	$0x1, -112(%rax), %ymm4, %ymm4
	vmovups	-32(%rax), %xmm5
	vperm2f128	$3, %ymm14, %ymm14, %ymm1
	vperm2f128	$3, %ymm7, %ymm7, %ymm0
	vinsertf128	$0x1, -80(%rax), %ymm8, %ymm8
	vshufps	$68, %ymm1, %ymm14, %ymm15
	vshufps	$238, %ymm1, %ymm14, %ymm1
	vshufps	$68, %ymm0, %ymm7, %ymm14
	vshufps	$238, %ymm0, %ymm7, %ymm0
	vinsertf128	$1, %xmm1, %ymm15, %ymm1
	vinsertf128	$0x1, -16(%rax), %ymm5, %ymm5
	vinsertf128	$1, %xmm0, %ymm14, %ymm0
	vshufps	$136, %ymm6, %ymm2, %ymm14
	vshufps	$221, %ymm6, %ymm2, %ymm2
	vinsertf128	$0x1, -48(%rax), %ymm3, %ymm3
	cmpl	%ecx, %r9d
	vperm2f128	$3, %ymm14, %ymm14, %ymm7
	vperm2f128	$3, %ymm2, %ymm2, %ymm6
	vshufps	$68, %ymm7, %ymm14, %ymm15
	vshufps	$238, %ymm7, %ymm14, %ymm7
	vshufps	$68, %ymm6, %ymm2, %ymm14
	vshufps	$238, %ymm6, %ymm2, %ymm6
	vinsertf128	$1, %xmm7, %ymm15, %ymm7
	vinsertf128	$1, %xmm6, %ymm14, %ymm6
	vshufps	$136, %ymm8, %ymm4, %ymm14
	vshufps	$221, %ymm8, %ymm4, %ymm8
	vperm2f128	$3, %ymm14, %ymm14, %ymm2
	vperm2f128	$3, %ymm8, %ymm8, %ymm4
	vshufps	$68, %ymm2, %ymm14, %ymm15
	vshufps	$238, %ymm2, %ymm14, %ymm2
	vshufps	$68, %ymm4, %ymm8, %ymm14
	vshufps	$238, %ymm4, %ymm8, %ymm4
	vinsertf128	$1, %xmm2, %ymm15, %ymm2
	vinsertf128	$1, %xmm4, %ymm14, %ymm4
	vshufps	$136, %ymm5, %ymm3, %ymm14
	vshufps	$221, %ymm5, %ymm3, %ymm3
	vperm2f128	$3, %ymm14, %ymm14, %ymm8
	vperm2f128	$3, %ymm3, %ymm3, %ymm5
	vshufps	$68, %ymm8, %ymm14, %ymm15
	vshufps	$238, %ymm8, %ymm14, %ymm8
	vshufps	$68, %ymm5, %ymm3, %ymm14
	vshufps	$238, %ymm5, %ymm3, %ymm5
	vinsertf128	$1, %xmm8, %ymm15, %ymm8
	vinsertf128	$1, %xmm5, %ymm14, %ymm5
	vshufps	$136, %ymm7, %ymm1, %ymm14
	vshufps	$221, %ymm7, %ymm1, %ymm7
	vperm2f128	$3, %ymm14, %ymm14, %ymm3
	vperm2f128	$3, %ymm7, %ymm7, %ymm1
	vshufps	$68, %ymm3, %ymm14, %ymm15
	vshufps	$238, %ymm3, %ymm14, %ymm3
	vshufps	$68, %ymm1, %ymm7, %ymm14
	vshufps	$238, %ymm1, %ymm7, %ymm1
	vinsertf128	$1, %xmm3, %ymm15, %ymm3
	vinsertf128	$1, %xmm1, %ymm14, %ymm1
	vshufps	$136, %ymm8, %ymm2, %ymm14
	vshufps	$221, %ymm8, %ymm2, %ymm2
	vperm2f128	$3, %ymm14, %ymm14, %ymm7
	vperm2f128	$3, %ymm2, %ymm2, %ymm8
	vshufps	$68, %ymm7, %ymm14, %ymm15
	vshufps	$238, %ymm7, %ymm14, %ymm7
	vshufps	$68, %ymm8, %ymm2, %ymm14
	vshufps	$238, %ymm8, %ymm2, %ymm8
	vinsertf128	$1, %xmm7, %ymm15, %ymm7
	vinsertf128	$1, %xmm8, %ymm14, %ymm8
	vshufps	$136, %ymm6, %ymm0, %ymm14
	vshufps	$221, %ymm6, %ymm0, %ymm6
	vperm2f128	$3, %ymm14, %ymm14, %ymm2
	vperm2f128	$3, %ymm6, %ymm6, %ymm0
	vshufps	$68, %ymm2, %ymm14, %ymm15
	vshufps	$238, %ymm2, %ymm14, %ymm2
	vshufps	$68, %ymm0, %ymm6, %ymm14
	vshufps	$238, %ymm0, %ymm6, %ymm0
	vinsertf128	$1, %xmm2, %ymm15, %ymm2
	vinsertf128	$1, %xmm0, %ymm14, %ymm0
	vshufps	$136, %ymm5, %ymm4, %ymm14
	vshufps	$221, %ymm5, %ymm4, %ymm5
	vperm2f128	$3, %ymm14, %ymm14, %ymm6
	vperm2f128	$3, %ymm5, %ymm5, %ymm4
	vshufps	$68, %ymm6, %ymm14, %ymm15
	vshufps	$238, %ymm6, %ymm14, %ymm6
	vshufps	$68, %ymm4, %ymm5, %ymm14
	vshufps	$238, %ymm4, %ymm5, %ymm4
	vinsertf128	$1, %xmm6, %ymm15, %ymm6
	vinsertf128	$1, %xmm4, %ymm14, %ymm4
	vshufps	$136, %ymm7, %ymm3, %ymm14
	vshufps	$221, %ymm7, %ymm3, %ymm3
	vperm2f128	$3, %ymm14, %ymm14, %ymm5
	vshufps	$68, %ymm5, %ymm14, %ymm15
	vshufps	$238, %ymm5, %ymm14, %ymm5
	vshufps	$136, %ymm6, %ymm2, %ymm14
	vshufps	$221, %ymm6, %ymm2, %ymm2
	vinsertf128	$1, %xmm5, %ymm15, %ymm5
	vaddps	%ymm5, %ymm13, %ymm13
	vperm2f128	$3, %ymm14, %ymm14, %ymm5
	vshufps	$68, %ymm5, %ymm14, %ymm15
	vshufps	$238, %ymm5, %ymm14, %ymm5
	vshufps	$136, %ymm8, %ymm1, %ymm14
	vshufps	$221, %ymm8, %ymm1, %ymm1
	vinsertf128	$1, %xmm5, %ymm15, %ymm5
	vaddps	%ymm5, %ymm12, %ymm12
	vperm2f128	$3, %ymm14, %ymm14, %ymm5
	vshufps	$68, %ymm5, %ymm14, %ymm15
	vshufps	$238, %ymm5, %ymm14, %ymm5
	vshufps	$136, %ymm4, %ymm0, %ymm14
	vshufps	$221, %ymm4, %ymm0, %ymm0
	vinsertf128	$1, %xmm5, %ymm15, %ymm5
	vaddps	%ymm5, %ymm11, %ymm11
	vperm2f128	$3, %ymm14, %ymm14, %ymm5
	vshufps	$68, %ymm5, %ymm14, %ymm15
	vshufps	$238, %ymm5, %ymm14, %ymm5
	vinsertf128	$1, %xmm5, %ymm15, %ymm5
	vaddps	%ymm5, %ymm10, %ymm10
	vperm2f128	$3, %ymm3, %ymm3, %ymm5
	vshufps	$68, %ymm5, %ymm3, %ymm7
	vshufps	$238, %ymm5, %ymm3, %ymm5
	vperm2f128	$3, %ymm2, %ymm2, %ymm3
	vinsertf128	$1, %xmm5, %ymm7, %ymm5
	vaddps	%ymm5, %ymm9, %ymm9
	vshufps	$68, %ymm3, %ymm2, %ymm5
	vshufps	$238, %ymm3, %ymm2, %ymm3
	vperm2f128	$3, %ymm1, %ymm1, %ymm2
	vinsertf128	$1, %xmm3, %ymm5, %ymm3
	vaddps	-368(%rbp), %ymm3, %ymm6
	vshufps	$68, %ymm2, %ymm1, %ymm3
	vshufps	$238, %ymm2, %ymm1, %ymm2
	vinsertf128	$1, %xmm2, %ymm3, %ymm2
	vmovaps	%ymm6, -368(%rbp)
	vaddps	-400(%rbp), %ymm2, %ymm1
	vmovaps	%ymm1, -400(%rbp)
	vperm2f128	$3, %ymm0, %ymm0, %ymm1
	vshufps	$68, %ymm1, %ymm0, %ymm2
	vshufps	$238, %ymm1, %ymm0, %ymm1
	vinsertf128	$1, %xmm1, %ymm2, %ymm1
	vaddps	-336(%rbp), %ymm1, %ymm4
	vmovaps	%ymm4, -336(%rbp)
	jb	.L5
	vmovaps	-368(%rbp), %ymm4
	cmpl	%r10d, %r11d
	leal	(%rsi,%r11,8), %ecx
	vmovaps	-400(%rbp), %ymm1
	vmovaps	-336(%rbp), %ymm6
	vmovaps	%ymm13, -304(%rbp)
	vmovaps	%ymm12, -272(%rbp)
	vmovaps	%ymm11, -240(%rbp)
	vmovaps	%ymm10, -208(%rbp)
	vmovaps	%ymm9, -176(%rbp)
	vmovaps	%ymm4, -144(%rbp)
	vmovaps	%ymm1, -112(%rbp)
	vmovaps	%ymm6, -80(%rbp)
	je	.L31
	vzeroupper
.L4:
	movslq	%ecx, %rax
	vmovss	-80(%rbp), %xmm0
	leaq	(%r8,%rax,4), %rax
	vmovss	-112(%rbp), %xmm1
	vmovss	-144(%rbp), %xmm2
	vmovss	-176(%rbp), %xmm3
	vmovss	-208(%rbp), %xmm4
	vmovss	-240(%rbp), %xmm5
	vmovss	-272(%rbp), %xmm6
	vmovss	-304(%rbp), %xmm7
	.p2align 4,,10
	.p2align 3
.L7:
	addl	$8, %ecx
	vaddss	(%rax), %xmm7, %xmm7
	addq	$32, %rax
	vaddss	-28(%rax), %xmm6, %xmm6
	vaddss	-24(%rax), %xmm5, %xmm5
	vaddss	-20(%rax), %xmm4, %xmm4
	vaddss	-16(%rax), %xmm3, %xmm3
	vaddss	-12(%rax), %xmm2, %xmm2
	vaddss	-8(%rax), %xmm1, %xmm1
	vaddss	-4(%rax), %xmm0, %xmm0
	cmpl	%ecx, %edx
	jg	.L7
	vmovss	%xmm7, -304(%rbp)
	vmovss	%xmm6, -272(%rbp)
	vmovss	%xmm5, -240(%rbp)
	vmovss	%xmm4, -208(%rbp)
	vmovss	%xmm3, -176(%rbp)
	vmovss	%xmm2, -144(%rbp)
	vmovss	%xmm1, -112(%rbp)
	vmovss	%xmm0, -80(%rbp)
.L9:
	leal	8(%rsi,%rdi,8), %esi
.L12:
	cmpl	%esi, %r13d
	jle	.L32
.L10:
	vxorps	%xmm0, %xmm0, %xmm0
	vaddss	-304(%rbp), %xmm0, %xmm7
	vaddss	-272(%rbp), %xmm0, %xmm6
	vaddss	-240(%rbp), %xmm0, %xmm5
	vaddss	-208(%rbp), %xmm0, %xmm4
	vaddss	-176(%rbp), %xmm0, %xmm3
	vaddss	-144(%rbp), %xmm0, %xmm2
	vaddss	-112(%rbp), %xmm0, %xmm1
	vaddss	-80(%rbp), %xmm0, %xmm0
	vaddss	-300(%rbp), %xmm7, %xmm7
	vaddss	-268(%rbp), %xmm6, %xmm6
	vaddss	-236(%rbp), %xmm5, %xmm5
	vaddss	-204(%rbp), %xmm4, %xmm4
	vaddss	-172(%rbp), %xmm3, %xmm3
	vaddss	-140(%rbp), %xmm2, %xmm2
	vaddss	-108(%rbp), %xmm1, %xmm1
	vaddss	-76(%rbp), %xmm0, %xmm0
	vaddss	-296(%rbp), %xmm7, %xmm7
	vaddss	-264(%rbp), %xmm6, %xmm6
	vaddss	-232(%rbp), %xmm5, %xmm5
	vaddss	-200(%rbp), %xmm4, %xmm4
	vaddss	-168(%rbp), %xmm3, %xmm3
	vaddss	-136(%rbp), %xmm2, %xmm2
	vaddss	-104(%rbp), %xmm1, %xmm1
	vaddss	-72(%rbp), %xmm0, %xmm0
	vaddss	-292(%rbp), %xmm7, %xmm7
	vaddss	-228(%rbp), %xmm5, %xmm5
	vaddss	-196(%rbp), %xmm4, %xmm4
	vaddss	-164(%rbp), %xmm3, %xmm3
	vaddss	-132(%rbp), %xmm2, %xmm2
	vaddss	-100(%rbp), %xmm1, %xmm1
	vaddss	-68(%rbp), %xmm0, %xmm0
	vaddss	-260(%rbp), %xmm6, %xmm6
	vaddss	-288(%rbp), %xmm7, %xmm7
	vaddss	-256(%rbp), %xmm6, %xmm6
	vaddss	-224(%rbp), %xmm5, %xmm5
	vaddss	-192(%rbp), %xmm4, %xmm4
	vaddss	-160(%rbp), %xmm3, %xmm3
	vaddss	-128(%rbp), %xmm2, %xmm2
	vaddss	-96(%rbp), %xmm1, %xmm1
	vaddss	-64(%rbp), %xmm0, %xmm0
	vaddss	-284(%rbp), %xmm7, %xmm7
	vaddss	-252(%rbp), %xmm6, %xmm6
	vaddss	-220(%rbp), %xmm5, %xmm5
	vaddss	-188(%rbp), %xmm4, %xmm4
	vaddss	-156(%rbp), %xmm3, %xmm3
	vaddss	-124(%rbp), %xmm2, %xmm2
	vaddss	-92(%rbp), %xmm1, %xmm1
	vaddss	-60(%rbp), %xmm0, %xmm0
	vaddss	-280(%rbp), %xmm7, %xmm7
	vaddss	-248(%rbp), %xmm6, %xmm6
	vaddss	-216(%rbp), %xmm5, %xmm5
	vaddss	-184(%rbp), %xmm4, %xmm4
	vaddss	-152(%rbp), %xmm3, %xmm3
	vaddss	-120(%rbp), %xmm2, %xmm2
	vaddss	-88(%rbp), %xmm1, %xmm1
	vaddss	-56(%rbp), %xmm0, %xmm0
	vaddss	-276(%rbp), %xmm7, %xmm7
	vaddss	-244(%rbp), %xmm6, %xmm6
	vaddss	-212(%rbp), %xmm5, %xmm5
	vaddss	-180(%rbp), %xmm4, %xmm4
	vaddss	-148(%rbp), %xmm3, %xmm3
	vaddss	-116(%rbp), %xmm2, %xmm2
	vaddss	-84(%rbp), %xmm1, %xmm1
	vaddss	-52(%rbp), %xmm0, %xmm0
.L13:
	vmovss	%xmm0, -420(%rbp)
	vmovss	%xmm1, -416(%rbp)
	vmovss	%xmm2, -412(%rbp)
	vmovss	%xmm3, -408(%rbp)
	vmovss	%xmm4, -404(%rbp)
	vmovss	%xmm5, -400(%rbp)
	vmovss	%xmm6, -368(%rbp)
	vmovss	%xmm7, -336(%rbp)
	call	GOMP_atomic_start
	vmovss	-336(%rbp), %xmm7
	vmovss	-368(%rbp), %xmm6
	vaddss	4(%rbx), %xmm7, %xmm7
	vmovss	-400(%rbp), %xmm5
	vaddss	8(%rbx), %xmm6, %xmm6
	vmovss	-404(%rbp), %xmm4
	vaddss	12(%rbx), %xmm5, %xmm5
	vmovss	-408(%rbp), %xmm3
	vaddss	16(%rbx), %xmm4, %xmm4
	vmovss	-412(%rbp), %xmm2
	vaddss	20(%rbx), %xmm3, %xmm3
	vmovss	-416(%rbp), %xmm1
	vaddss	24(%rbx), %xmm2, %xmm2
	vmovss	-420(%rbp), %xmm0
	vaddss	28(%rbx), %xmm1, %xmm1
	vaddss	32(%rbx), %xmm0, %xmm0
	vmovss	%xmm7, 4(%rbx)
	vmovss	%xmm6, 8(%rbx)
	vmovss	%xmm5, 12(%rbx)
	vmovss	%xmm4, 16(%rbx)
	vmovss	%xmm3, 20(%rbx)
	vmovss	%xmm2, 24(%rbx)
	vmovss	%xmm1, 28(%rbx)
	vmovss	%xmm0, 32(%rbx)
	call	GOMP_atomic_end
	addq	$400, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movl	%esi, (%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	vxorps	%xmm0, %xmm0, %xmm0
	vmovaps	%xmm0, %xmm1
	vmovaps	%xmm0, %xmm2
	vmovaps	%xmm0, %xmm3
	vmovaps	%xmm0, %xmm4
	vmovaps	%xmm0, %xmm5
	vmovaps	%xmm0, %xmm6
	vmovaps	%xmm0, %xmm7
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%esi, %ecx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L31:
	vzeroupper
	jmp	.L9
	.cfi_endproc
.LFE1090:
	.size	main._omp_fn.0, .-main._omp_fn.0
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"sum = "
.LC3:
	.string	"OpenMP-for reduction consume "
.LC4:
	.string	" seconds\n"
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup,"ax",@progbits
.LHOTB5:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1081:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	movabsq	$2287828610704211968, %rcx
	movq	$-1, %rdi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x70,0x6
	.cfi_escape 0x10,0xc,0x2,0x76,0x78
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x68
	movl	$10, %ebx
	subq	$152, %rsp
	movslq	size(%rip), %rdx
	leaq	0(,%rdx,4), %rax
	cmpq	%rcx, %rdx
	cmovbe	%rax, %rdi
	call	_Znam
	movl	$1, %edi
	movq	%rax, arr(%rip)
	call	omp_set_num_threads
	call	omp_get_wtime
	vxorps	%xmm6, %xmm6, %xmm6
	vmovsd	%xmm0, -120(%rbp)
	vmovaps	%xmm6, %xmm3
	vmovaps	%xmm6, %xmm7
	vmovaps	%xmm6, %xmm2
	vmovaps	%xmm6, %xmm8
	vmovaps	%xmm6, %xmm4
	vmovaps	%xmm6, %xmm5
	vmovaps	%xmm6, %xmm1
	.p2align 4,,10
	.p2align 3
.L35:
	vunpcklps	%xmm6, %xmm3, %xmm3
	leaq	-112(%rbp), %rsi
	xorl	%ecx, %ecx
	vunpcklps	%xmm7, %xmm2, %xmm2
	xorl	%edx, %edx
	vunpcklps	%xmm8, %xmm4, %xmm4
	movl	$main._omp_fn.0, %edi
	vunpcklps	%xmm5, %xmm1, %xmm1
	movl	%r12d, -112(%rbp)
	vmovlhps	%xmm3, %xmm2, %xmm0
	vmovlhps	%xmm4, %xmm1, %xmm1
	vinsertf128	$0x1, %xmm0, %ymm1, %ymm0
	vmovups	%xmm0, -108(%rbp)
	vextractf128	$0x1, %ymm0, -92(%rbp)
	vzeroupper
	call	GOMP_parallel
	subl	$1, %ebx
	movl	-112(%rbp), %r12d
	vmovss	-108(%rbp), %xmm1
	vmovss	-104(%rbp), %xmm5
	vmovss	-100(%rbp), %xmm4
	vmovss	-96(%rbp), %xmm8
	vmovss	-92(%rbp), %xmm2
	vmovss	-88(%rbp), %xmm7
	vmovss	-84(%rbp), %xmm3
	vmovss	-80(%rbp), %xmm6
	jne	.L35
	vmovss	%xmm6, -160(%rbp)
	vmovss	%xmm3, -156(%rbp)
	vmovss	%xmm7, -152(%rbp)
	vmovss	%xmm2, -148(%rbp)
	vmovss	%xmm8, -144(%rbp)
	vmovss	%xmm4, -140(%rbp)
	vmovss	%xmm5, -128(%rbp)
	vmovss	%xmm1, -124(%rbp)
	call	omp_get_wtime
	vmovss	-124(%rbp), %xmm1
	movl	$.LC2, %esi
	vmovss	-128(%rbp), %xmm5
	movl	$_ZSt4cout, %edi
	vaddss	%xmm1, %xmm5, %xmm1
	vmovss	-140(%rbp), %xmm4
	vmovss	-144(%rbp), %xmm8
	vmovss	-148(%rbp), %xmm2
	vmovss	-152(%rbp), %xmm7
	vaddss	%xmm1, %xmm4, %xmm5
	vmovss	-156(%rbp), %xmm3
	vmovss	-160(%rbp), %xmm6
	vmovsd	%xmm0, -136(%rbp)
	vaddss	%xmm5, %xmm8, %xmm4
	vaddss	%xmm4, %xmm2, %xmm1
	vaddss	%xmm1, %xmm7, %xmm2
	vaddss	%xmm2, %xmm3, %xmm1
	vaddss	%xmm1, %xmm6, %xmm7
	vmovss	%xmm7, -124(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vxorpd	%xmm0, %xmm0, %xmm0
	movq	%rax, %rdi
	vcvtss2sd	-124(%rbp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	leaq	-112(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	movb	$10, -112(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	vmovsd	-136(%rbp), %xmm9
	movl	$.LC3, %esi
	movl	$_ZSt4cout, %edi
	vsubsd	-120(%rbp), %xmm9, %xmm0
	vmovsd	%xmm0, -120(%rbp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	-120(%rbp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC4, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1081:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup
.LHOTB6:
	.p2align 4,,15
	.type	_GLOBAL__sub_I_size, @function
_GLOBAL__sub_I_size:
.LFB1089:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE1089:
	.size	_GLOBAL__sub_I_size, .-_GLOBAL__sub_I_size
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_size
	.globl	arr
	.bss
	.align 8
	.type	arr, @object
	.size	arr, 8
arr:
	.zero	8
	.globl	size
	.data
	.align 4
	.type	size, @object
	.size	size, 4
size:
	.long	1600000000
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
