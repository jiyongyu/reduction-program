
Each sample represents 0.02 seconds.
Total run time: 306.34 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
  98.2    300.66       0.0      0.00     "run"
  90.9    278.38       3.3     10.22     "lapply"
  87.5    268.16      65.9    201.90     "FUN"
   8.4     25.82       8.4     25.80     "Reduce"
   7.8     23.96       7.8     23.96     "sum"
   4.6     14.20       4.6     14.20     "*"
   3.3     10.16       3.3     10.16     "-"
   2.4      7.24       2.4      7.24     "exp"
   1.9      5.68       0.0      0.00     "setup"
   1.6      4.88       1.6      4.88     "+"
   1.3      3.86       1.3      3.86     "/"
   0.6      1.86       0.6      1.86     "("
   0.6      1.68       0.6      1.68     "list"
   0.1      0.34       0.0      0.08     "matrix"
   0.1      0.26       0.1      0.26     ".External"
   0.1      0.26       0.0      0.00     "runif"
   0.1      0.18       0.1      0.18     "c"
   0.0      0.04       0.0      0.04     "cat"
   0.0      0.04       0.0      0.00     "colSums"
   0.0      0.04       0.0      0.00     "is.data.frame"
   0.0      0.02       0.0      0.02     "seq_len"


   %        self       %      total
  self    seconds    total   seconds    name
  65.9    201.90      87.5    268.16     "FUN"
   8.4     25.80       8.4     25.82     "Reduce"
   7.8     23.96       7.8     23.96     "sum"
   4.6     14.20       4.6     14.20     "*"
   3.3     10.22      90.9    278.38     "lapply"
   3.3     10.16       3.3     10.16     "-"
   2.4      7.24       2.4      7.24     "exp"
   1.6      4.88       1.6      4.88     "+"
   1.3      3.86       1.3      3.86     "/"
   0.6      1.86       0.6      1.86     "("
   0.6      1.68       0.6      1.68     "list"
   0.1      0.26       0.1      0.26     ".External"
   0.1      0.18       0.1      0.18     "c"
   0.0      0.08       0.1      0.34     "matrix"
   0.0      0.04       0.0      0.04     "cat"
   0.0      0.02       0.0      0.02     "seq_len"
