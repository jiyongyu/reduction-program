
Each sample represents 0.02 seconds.
Total run time: 15.74 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
  74.7     11.76      15.8      2.48     "Reduce"
  74.7     11.76       0.0      0.00     "run"
  74.5     11.72       7.5      1.18     "lapply"
  67.0     10.54      19.6      3.08     "FUN"
  47.4      7.46      47.4      7.46     "tcrossprod"
  25.3      3.98       0.0      0.00     "setup"
   7.6      1.20       7.6      1.20     "list"
   2.2      0.34       0.5      0.08     "matrix"
   1.6      0.26       1.6      0.26     ".External"
   1.6      0.26       0.0      0.00     "runif"


   %        self       %      total
  self    seconds    total   seconds    name
  47.4      7.46      47.4      7.46     "tcrossprod"
  19.6      3.08      67.0     10.54     "FUN"
  15.8      2.48      74.7     11.76     "Reduce"
   7.6      1.20       7.6      1.20     "list"
   7.5      1.18      74.5     11.72     "lapply"
   1.6      0.26       1.6      0.26     ".External"
   0.5      0.08       2.2      0.34     "matrix"
