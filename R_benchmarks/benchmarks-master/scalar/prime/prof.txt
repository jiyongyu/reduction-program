
Each sample represents 0.02 seconds.
Total run time: 3.5 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
 100.0      3.50      74.9      2.62     "run"
   8.0      0.28       8.0      0.28     "<="
   6.9      0.24       6.9      0.24     "+"
   5.1      0.18       5.1      0.18     "=="
   4.0      0.14       4.0      0.14     "%%"
   1.1      0.04       1.1      0.04     "("


   %        self       %      total
  self    seconds    total   seconds    name
  74.9      2.62     100.0      3.50     "run"
   8.0      0.28       8.0      0.28     "<="
   6.9      0.24       6.9      0.24     "+"
   5.1      0.18       5.1      0.18     "=="
   4.0      0.14       4.0      0.14     "%%"
   1.1      0.04       1.1      0.04     "("
