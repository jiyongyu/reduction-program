
Each sample represents 0.02 seconds.
Total run time: 4.56 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
 100.0      4.56       0.4      0.02     "run"
  99.6      4.54      46.0      2.10     "check"
  46.5      2.12      33.8      1.54     "tree"
   9.2      0.42       9.2      0.42     "=="
   3.1      0.14       3.1      0.14     "-"
   2.2      0.10       2.2      0.10     "*"
   2.2      0.10       2.2      0.10     "c"
   1.8      0.08       1.8      0.08     "+"
   1.8      0.08       0.0      0.00     "cat"
   1.3      0.06       1.3      0.06     "list"


   %        self       %      total
  self    seconds    total   seconds    name
  46.0      2.10      99.6      4.54     "check"
  33.8      1.54      46.5      2.12     "tree"
   9.2      0.42       9.2      0.42     "=="
   3.1      0.14       3.1      0.14     "-"
   2.2      0.10       2.2      0.10     "*"
   2.2      0.10       2.2      0.10     "c"
   1.8      0.08       1.8      0.08     "+"
   1.3      0.06       1.3      0.06     "list"
   0.4      0.02     100.0      4.56     "run"
