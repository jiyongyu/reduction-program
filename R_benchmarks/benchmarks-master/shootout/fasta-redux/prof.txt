
Each sample represents 0.02 seconds.
Total run time: 7.32 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
 100.0      7.32       0.0      0.00     "run"
  91.0      6.66      34.1      2.50     "random_fasta"
  44.3      3.24      34.1      2.50     "random_next_lookup"
  18.9      1.38      12.6      0.92     "cat"
   9.0      0.66       0.6      0.04     "repeat_fasta"
   6.0      0.44       6.0      0.44     "paste"
   4.6      0.34       4.6      0.34     "*"
   2.2      0.16       2.2      0.16     "+"
   1.9      0.14       1.9      0.14     "/"
   0.8      0.06       0.8      0.06     "%%"
   0.8      0.06       0.8      0.06     "("
   0.8      0.06       0.6      0.04     "which"
   0.6      0.04       0.6      0.04     "double"
   0.3      0.02       0.3      0.02     ":"
   0.3      0.02       0.3      0.02     "<"
   0.3      0.02       0.3      0.02     "min"
   0.3      0.02       0.3      0.02     "stdout"


   %        self       %      total
  self    seconds    total   seconds    name
  34.1      2.50      91.0      6.66     "random_fasta"
  34.1      2.50      44.3      3.24     "random_next_lookup"
  12.6      0.92      18.9      1.38     "cat"
   6.0      0.44       6.0      0.44     "paste"
   4.6      0.34       4.6      0.34     "*"
   2.2      0.16       2.2      0.16     "+"
   1.9      0.14       1.9      0.14     "/"
   0.8      0.06       0.8      0.06     "%%"
   0.8      0.06       0.8      0.06     "("
   0.6      0.04       9.0      0.66     "repeat_fasta"
   0.6      0.04       0.8      0.06     "which"
   0.6      0.04       0.6      0.04     "double"
   0.3      0.02       0.3      0.02     ":"
   0.3      0.02       0.3      0.02     "<"
   0.3      0.02       0.3      0.02     "min"
   0.3      0.02       0.3      0.02     "stdout"
