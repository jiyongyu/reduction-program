
Each sample represents 0.02 seconds.
Total run time: 1.94 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
 100.0      1.94       0.0      0.00     "run"
  72.2      1.40       0.0      0.00     "cat"
  71.1      1.38      71.1      1.38     "gregexpr"
  71.1      1.38       0.0      0.00     "match_count"
  20.6      0.40      20.6      0.40     "gsub"
   7.2      0.14       2.1      0.04     "paste"
   5.2      0.10       5.2      0.10     "readLines"
   1.0      0.02       1.0      0.02     "nchar"


   %        self       %      total
  self    seconds    total   seconds    name
  71.1      1.38      71.1      1.38     "gregexpr"
  20.6      0.40      20.6      0.40     "gsub"
   5.2      0.10       5.2      0.10     "readLines"
   2.1      0.04       7.2      0.14     "paste"
   1.0      0.02       1.0      0.02     "nchar"
