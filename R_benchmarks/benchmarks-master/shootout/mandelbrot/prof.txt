
Each sample represents 0.02 seconds.
Total run time: 0.74 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
 100.0      0.74       0.0      0.00     "run"
  54.0      0.40      54.0      0.40     "*"
  40.5      0.30      40.5      0.30     "+"
   5.4      0.04       2.7      0.02     "colSums"
   2.7      0.02       2.7      0.02     "matrix"
   2.7      0.02       0.0      0.00     "is.data.frame"


   %        self       %      total
  self    seconds    total   seconds    name
  54.0      0.40      54.0      0.40     "*"
  40.5      0.30      40.5      0.30     "+"
   2.7      0.02       5.4      0.04     "colSums"
   2.7      0.02       2.7      0.02     "matrix"
