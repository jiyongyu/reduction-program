
Each sample represents 0.02 seconds.
Total run time: 0.06 seconds.

Total seconds: time spent in function and callees.
Self seconds: time spent in function alone.

   %       total       %        self
 total    seconds     self    seconds    name
 100.0      0.06     100.0      0.06     "ifelse"
 100.0      0.06       0.0      0.00     "run"


   %        self       %      total
  self    seconds    total   seconds    name
 100.0      0.06     100.0      0.06     "ifelse"
