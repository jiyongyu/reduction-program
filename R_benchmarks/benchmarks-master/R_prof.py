#!/usr/bin/env python

import os.path
import subprocess

DIR_OF_THIS_SCRIPT = os.path.abspath(os.path.dirname(__file__))

BENCHMARKS = ['algorithm']
              #'shootout', \
              #'scalar', \
              #'misc', \
              #'R-benchmark-25']


def main():
    
    for benchmark in BENCHMARKS:
        
        benchmark_dir  = DIR_OF_THIS_SCRIPT + '/' + benchmark
        benchmark_list = benchmark_dir + '/' + benchmark + '.list'
        
        with open(benchmark_list) as ld_f:
            data = ld_f.readlines()
            
            for line in data:
                individual_file = benchmark_dir + '/' + line
                individual_dir  = benchmark_dir + '/' + line.split('/')[0]

                os.chdir(individual_dir)

                boot_file   = individual_dir + '/boot.out'
                result_file = individual_dir + '/prof.txt'
                
                print '====== Start benchmark (' + line.split('/')[0] + ') in benchmark [' + benchmark + '] ======'
                
                subprocess.check_call(['Rscript', line.split('/')[1]])
                output = subprocess.check_output(['R', 'CMD', 'Rprof', boot_file])
                
                st_f = open(result_file, 'w')
                st_f.write(output)
                st_f.close()

                print '------ Finish benchmark (' + line.split('/')[0] + ') in benchmark [' + benchmark + '] ------'

if __name__ == "__main__":
    main()
