#/usr/bin/env python

import os.path
import subprocess

DIR_OF_CODE = '/home/jiyongyu/CGO/reduction-program/R_benchmarks/genbench-master/code'

#BENCHMARKS =  [ '/clinical/humanlivercohort.R', \\ 
                #'/integration/humanLiverCohort.R', \\
                #'/microarray/limma.R', \\
                #'/mrna_seq/edgeR_voom.R', \\
                #'/mutation/mutation.R', \\
                #'/protein/rppa.R', \\
                #'/simulated_GEO_matrix/chocolate_R_benchmark.R'] 

BENCHMARKS =  { 'clinical'             : 'humanlivercohort.R',
                'integration'          : 'humanLiverCohort.R',
                'microarray'           : 'limma.R',
                'mrna_seq'             : 'edgeR_voom.R',
                'mutation'             : 'mutation.R',
                'protein'              : 'rppa.R',
                'simulated_GEO_matrix' : 'chocolate_R_benchmark.R'}

def main():
    
    for benchmark in BENCHMARKS:
        
        benchmark_dir  = DIR_OF_CODE + '/' + benchmark
        benchmark_file = benchmark_dir + '/' + BENCHMARKS[benchmark]
        
        os.chdir(benchmark_dir)

        boot_file   = benchmark_dir + '/boot.out'
        result_file = benchmark_dir + '/prof.txt'
        
        print '====== Start benchmark [' + benchmark + '/' + BENCHMARKS[benchmark] + '] ======'
        
        subprocess.check_call(['Rscript', BENCHMARKS[benchmark]])
        output = subprocess.check_output(['R', 'CMD', 'Rprof', boot_file])
        
        st_f = open(result_file, 'w')
        st_f.write(output)
        st_f.close()

        print '------ Finish benchmark [' + benchmark + '/' + BENCHMARKS[benchmark] + '] ------'

if __name__ == "__main__":
    main()
