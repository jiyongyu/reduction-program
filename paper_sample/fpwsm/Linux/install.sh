#!/usr/bin/env bash

## get the directory this script is in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
UPDIR=${DIR}/..
ESCDIR=`echo ${UPDIR} | sed 's:/:\\\/:g'`

echo "*** Installing to ${DIR}"
cat ${DIR}/fpwsm.desktop.template | sed 's|@FPWSM_LINUX_DIR@|'${DIR}'|g' > ${DIR}/fpwsm.desktop
chmod +x ${DIR}/fpwsm.desktop
