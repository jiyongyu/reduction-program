#!/usr/bin/env python

import argparse, os, sys, copy, pickle, subprocess, re, math, tempfile, string, random;
from StringIO import StringIO;
from PyQt4.QtGui import *;
from PyQt4.QtCore import *;
from PyQt4.QtXml import *;
from popplerqt4 import *; #for pdf rendering
from multiprocessing import Process;
from collections import deque;
import enchant; #for spell checking

#-Utility Functions------------------------------------------------------------
#-Utility functions to simplify code complexity
#------------------------------------------------------------------------------
def list_check(l, val): #bounds checking
  return len(l)>=val;


## put text into a file only if the file's contents are not already equal to the text
def write_file_dep(fname, ftext):
    do_write = False
    if os.path.exists(fname):
      inf = open(fname, 'r')
      indat = inf.readlines()
      inf.close()

      textdat = ftext.split('\n')
      for i in range(0, len(textdat)):
        if i >= len(indat):
          break
        if textdat[i]+'\n' != indat[i] and textdat[i] != indat[i]:
          do_write = True
          break
    else:
      do_write = True

    if do_write:
      out = open(fname,'w');
      out.writelines(ftext);
      out.close();

    return do_write
  

## TODO: this performs slowly since it iterates over all pixels
def imgToGrayscale(img):
  for h in range(0,img.height()):
    for w in range(0,img.width()):
      pix = img.pixel(w, h)
      rgb = QColor(pix).getRgbF()
      gray = qGray(pix)
      if not (gray == 1.0 or gray == 0.0):
        img.setPixel(w, h, qRgb(gray, gray, gray))

def nice_error_log(errmsg, rawlog, failedcmds):
  errsep = '==============================\n'
  annot = 'Error occurred while '+errmsg+'\n\n'
#  annot += 'Current working directory: '+os.getcwd()+'\n\n'
  annot += 'Failed Commands: ' + str(failedcmds) + '\n\n'

  lines = rawlog.split('\n')
  highlight = ''
  raw = ''
  for i in range(0, len(lines)):
    thisline = str(i+1) + ':\t' + lines[i] + '\n'
    if lines[i].find('error') != -1 or lines[i].find('Error') != -1:
      if lines[i].find('Executing:') == -1:
        highlight += thisline
    raw += thisline

  annot += 'Error Summary\n' + errsep + highlight + '\n'
  annot += 'Full Log\n' + errsep + raw

  return annot

def run_shell_command(cmd):
    tmpfname = tempfile.mktemp()
    tmpf = open(tmpfname, 'w')
    res = subprocess.call(cmd, stderr=subprocess.STDOUT, stdout=tmpf)
    tmpf.close()

    tmpf = open(tmpfname, 'r')
    d = tmpf.readlines()
    tmpf.close()
    os.remove(tmpfname)

    d.insert(0, 'Executing: ' + string.join(cmd, ' ') + '\n')
    return res, string.join(d)

def get_word_count(texfile):
  wccmd = ["texcount", texfile]
  if not os.path.isfile(texfile): print 'no file found'; return None
  ret, log = run_shell_command(wccmd)
  return ret, log

#-Node State-------------------------------------------------------------------
#-Keeps contextual info about each node
#------------------------------------------------------------------------------
class node_state:
  def __init__(self):
    self.depth=0; #Depth in tree
    self.sec_depth=0; #Section depth
    self.list_depth=0; #itemized and section depth
    self.omitted=0; #omitted flag
    self.is_comment=0; #cmt flag
    self.is_red_comment=0; #rcmt flag
    self.is_yellow_comment=0; #ycmt flag
    self.is_blue_comment=0; #bcmt flag
    self.start_line=None; #starting line in final tex output
    self.end_line=None; #ending line in final tex output


#-FPWSM Tree Node--------------------------------------------------------------
#-The key data structure for FPWSM
#------------------------------------------------------------------------------
class node:
  def __init__(self, anch="blank", txt=""):
    self.ntype=None; #type of node (i.e., paragraph, section etc)
    self.is_rendered=0; #check if figure is rendered ##DOES NOT WORK FOR SOEM REASONE
    self.snippet=''; #text after the tag name (everytihng after first ':')
    self.params=[]; #the list of parameters after the : (for relevant tag types)

    self.anchor=str(QString(anch).toAscii()); #text to appear in the tree view
    self.text=str(QString(txt).toAscii()); #text in the main text box
    self.linked_gui_item=None; #children managed through here (tree_item)
    self.state=node_state();

  def kids(self):
    kid_list=[];
    for i in range(self.linked_gui_item.childCount()):
      kid_list.append(self.linked_gui_item.child(i).linked_node);
    return kid_list;

  def chunk(self): #returns entire text chunk
    if(len(self.text)):
      return str(QString(self.anchor+'\n'+self.text).toAscii());
    else: return str(QString(self.anchor).toAscii());

  def check_and_update(self, s, alt=[], has_params=False):
    if(len(self.chunk().split('$'+s+':'))>1):
      self.ntype=s; self.snippet='';
      if(has_params): #checks if is parametarized tag, if so snag params and skip line for snippet
        self.params=self.chunk().split('$'+s+':')[1].split('\n')[0].split(':');
        for i in self.chunk().split('$'+s+':')[1].split('\n')[1:]: self.snippet+=i+'\n';
      else: self.snippet=self.chunk().split('$'+s+':')[1];
      return True;
    if(len(self.chunk().split('$'+s+'!'))>1): #support !
      self.ntype=s; self.snippet='';
      if(has_params):
        self.params=self.chunk().split('$'+s+'!')[1].split('\n')[0].split(':');
        for i in self.chunk().split('$'+s+'!')[1].split('\n')[1:]: self.snippet+=i+'\n';
      else: self.snippet=self.chunk().split('$'+s+'!')[1];
      return True;
    for j in alt: #also check alternative tags
      if(len(self.chunk().split('$'+j+':'))>1):
        self.ntype=s; self.snippet='';
        if(has_params):
          self.params=self.chunk().split('$'+j+':')[1].split('\n')[0].split(':');
          for i in self.chunk().split('$'+j+':')[1].split('\n')[1:]: self.snippet+=i+'\n';
        else: self.snippet=self.chunk().split('$'+j+':')[1];
        return True;
      if(len(self.chunk().split('$'+j+'!'))>1):
        self.ntype=s; self.snippet='';
        if(has_params):
          self.params=self.chunk().split('$'+j+'!')[1].split('\n')[0].split(':');
          for i in self.chunk().split('$'+j+'!')[1].split('\n')[1:]: self.snippet+=i+'\n';
        else: self.snippet=self.chunk().split('$'+j+'!')[1];
        return True;
    return False;

  def save(self, outfile):
    self.anchor=str(QString(self.anchor).toAscii())
    self.text=str(QString(self.text).toAscii())
    pickle.dump(self.anchor, outfile);
    pickle.dump(self.text, outfile);
    pickle.dump(len(self.kids()), outfile);
    for i in self.kids():
      i.save(outfile);

  def search_update_line_stat(self):
    if(self.state.start_line!=None): return (self.state.start_line, self.state.end_line);
    for i in self.kids():
      (self.state.start_line, self.state.end_line)=i.search_update_line_stat();
      if(self.state.start_line!=None): break;
    return (self.state.start_line, self.state.end_line);

  def update_all_line_stats(self):
    self.search_update_line_stat();
    for i in self.kids():
      i.search_update_line_stat();

  def process_type(self):
    self.ntype=None; ret=0;
    if(self.check_and_update("omit")): ret=1; #omit! - omits entire subtree
    elif(self.check_and_update("title",["tit"])): ret=1; #title: snippet
    elif(self.check_and_update("head_tex",["head"])): ret=1; #head: snippet
    elif(self.check_and_update("tail_tex",["tail"])): ret=1; #tail: snippet
    elif(self.check_and_update("abstract",["abs","a"])): ret=1; #abs: snippet
    elif(self.check_and_update("abstract_paragraph",["absp","ap"])): ret=1; #absp: snippet
    elif(self.check_and_update("chapter",["chap","c"])): ret=1; #c: snippet
    elif(self.check_and_update("chapter*",["chap*","c*"])): ret=1; #c*: snippet
    elif(self.check_and_update("section",["sec","s"])): ret=1; #s: snippet
    elif(self.check_and_update("section*",["sec*","s*"])): ret=1; #s*: snippet
    elif(self.check_and_update("paragraph",["par","p"])): ret=1; #p: snippet
    elif(self.check_and_update("text",["t"])): ret=1; #t:snippet
    elif(self.check_and_update("cmt_on")): ret=1; #TODO: obsolete
    elif(self.check_and_update("cmt_off")): ret=1; #TODO: obsolete
    elif(self.check_and_update("comment",["cmt", "gcmt"])): ret=1; #cmt: snippet
    elif(self.check_and_update("redcomment",["rcmt"])): ret=1; #rcmt: snippet
    elif(self.check_and_update("yellowcomment",["ycmt"])): ret=1; #ycmt: snippet
    elif(self.check_and_update("bluecomment",["bcmt"])): ret=1; #bcmt: snippet
    elif(self.check_and_update("list",["l"],True)): ret=1; #l:type (i.e., l:enumerate)
    elif(self.check_and_update("item",["li","i"])): ret=1; #li: snippet
    elif(self.check_and_update("bibtex",["bib"])): ret=1; #bib: snippet
    elif(self.check_and_update("class_file",["class"],True)): ret=1; #class:name \\ snippet
    elif(self.check_and_update("style_file",["style","sty"],True)): ret=1; #style:name \\ snippet
    elif(self.check_and_update("dump_file",["output","create","embed","dump"],True)): ret=1; #dump:name \\ snippet
    elif(self.check_and_update("bib_style",[],True)): ret=1; #bib_style: snippet
    elif(self.check_and_update("inline_code",["icode","code"])): ret=1; #code: snippet
    elif(self.check_and_update("inline_table",["inline_tab","itab"],True)): ret=1; #itab:l:l:c:... snippet
    elif(self.check_and_update("figure",["fig","f"],True)): ret=1; #fig:name,sz:name,sz:nl:name,sz: (where sz is width/column, nl is new line)
    elif(self.check_and_update("figure*",["fig*","f*"],True)): ret=1; #fig*:name,sz:name,sz:
    elif(self.check_and_update("sub_figure",["subfigure","sub_fig","sf"],True)): ret=1; #sub_fig:name:caption:name,sz:...
    elif(self.check_and_update("sub_figure*",["subfigure","sub_fig*","sf*"],True)): ret=1; #sub_fig:name:caption:name,sz:...
    elif(self.check_and_update("code_figure",["code_fig"],True)): ret=1; #code:name:caption \\ snippet
    elif(self.check_and_update("table_figure",["table_fig","tab_fig"],True)): ret=1; #tab_fig:name:caption:l:l:c:...
    elif(self.check_and_update("table",["tab"],True)): ret=1; #tab:name:caption:l:l:c:...
    elif(self.check_and_update("table*",["tab*"],True)): ret=1; #tab*:name:caption:l:l:c:...
    elif(self.check_and_update("pdf_figure",["pdf_fig","pdf"],True)): ret=1; #pdf_fig:name:caption
    elif(self.check_and_update("pdf_table",["pdf_tab"],True)): ret = 1; #pdf_tab:name:caption
    elif(self.check_and_update("graph_figure",["graph_fig","graph"],True)): ret=1; #graph:name:caption \\ snippet
    elif(self.check_and_update("R_figure",["R_fig"],True)): ret=1; #graph:name:caption \\snippet
    self.snippet=str.strip(self.snippet);
    for i in range(len(self.params)): self.params[i]=str.strip(self.params[i]);
    return ret;


#-Epic Generator---------------------------------------------------------------
#-Resposible for generating tex output and other source files from node tree
#------------------------------------------------------------------------------
class epic_gen:
  def __init__(self, gui):
    self.gui=gui;
    self.cur_node=None;
    self.cmt_switch=1;
    self.head=[];
    self.abstract=[];
    self.title=['',None];
    self.tail=[];
    self.core=[];
    self.bibtex='';
    self.final_output='';
    self.work_log='';
    self.failures=[];
    self.fig_links={}; #Maintains link between figs and thier objects, format: {key : [fig_node, object_node]}
    self.gen_jobs=[]; #Graph generation jobs to be run in parallel

  def reset(self):
    self.cur_node=None;
    self.cmt_switch=1;
    self.head=[];
    self.abstract=[];
    self.title=['',None];
    self.tail=[];
    self.core=[];
    self.bibtex='';
    self.final_output='';
    self.work_log='';
    self.failure=[];
    self.fig_links={}; #Maintains link between figs and thier objects, format: {key : [fig_node, object_node]}
    self.gen_jobs=[]; #Graph generation jobs to be run in parallel

  def epic_generate(self, node):
    if not os.path.exists(self.gui.out_dir): os.mkdir(self.gui.out_dir)
    if not os.path.exists(self.gui.gen_dir): os.mkdir(self.gui.gen_dir)

    (tex_out,dummy)=self.render(node);
    self.dump_file(self.gui.fnhead+'.tex',tex_out,mydir=self.gui.gen_dir);
    self.dump_file('bib.bib',self.bibtex);
    self.generator_pass(node);
    for i in self.gen_jobs: i.start();
    for i in self.gen_jobs: i.join();

    is_successful=1;

    syscmd=["pdflatex", "-halt-on-error", "-interaction=nonstopmode", self.gui.fnhead + ".tex"];
    bibcmd=["bibtex", "-min-crossrefs=100", self.gui.fnhead];

    savedir=os.getcwd()
    os.chdir(self.gui.gen_dir);

    ret, log = run_shell_command(syscmd)
    self.work_log += log; is_successful=(is_successful and (ret==0));
    if ret != 0: self.failure.append(syscmd)

    if(len(tex_out.split("\cite"))>1):
      ret, log = run_shell_command(bibcmd)
      self.work_log += log; is_successful=(is_successful and (ret==0));
      if ret != 0: self.failure.append(bibcmd)

    ret, log = run_shell_command(syscmd)
    self.work_log += log; is_successful=(is_successful and (ret==0));
    if ret != 0: self.failure.append(syscmd)

    ret, log = run_shell_command(syscmd)
    self.work_log += log; is_successful=(is_successful and (ret==0));
    if ret != 0: self.failure.append(syscmd)

    os.chdir(savedir)

    if(is_successful):
      syscmd="cp "+self.gui.gen_dir+self.gui.fnhead+".pdf "+self.gui.out_dir;
      runcmd=subprocess.Popen(syscmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE,shell=True);
      self.work_log+=runcmd.communicate()[0]; is_successful=(is_successful and (runcmd.returncode==0));
      if runcmd.returncode != 0: self.failure.append(syscmd)

    return is_successful; #returns true if paper compile successfull

  def render(self, node):
    self.reset(); #clears out all member fields
    self.structure_pass(node, node_state());
    self.tex_render_pass(node);
    node_links=[]
    for i in self.head:
      i[1].state.start_line=self.final_output.count('\n'); #sets line range in final tex output in node
      self.final_output+=i[0];
      i[1].state.end_line=self.final_output.count('\n');
      dist=i[1].state.end_line-i[1].state.start_line;
      for j in range(dist): node_links.append(i[1]);

    if(len(self.title[0])):
      self.title[1].state.start_line=self.final_output.count('\n');
      self.final_output+='\n'+self.title[0]+'\n';
      self.title[1].state.end_line=self.final_output.count('\n');
      dist=self.title[1].state.end_line-self.title[1].state.start_line;
      for j in range(dist): node_links.append(self.title[1]);
    if(len(self.abstract)):
      self.final_output+="\\begin{abstract}\n"
      node_links.append(None);
      for i in self.abstract:
        i[1].state.start_line=self.final_output.count('\n');
        self.final_output+=i[0];
        i[1].state.end_line=self.final_output.count('\n');
        dist=i[1].state.end_line-i[1].state.start_line;
        for j in range(dist): node_links.append(i[1]);
      self.final_output+="\n\\end{abstract}\n";
      node_links.append(None);
      node_links.append(None);
    for i in self.core:
      i[1].state.start_line=self.final_output.count('\n');
      self.final_output+=i[0];
      i[1].state.end_line=self.final_output.count('\n');
      dist=i[1].state.end_line-i[1].state.start_line;
      for j in range(dist): node_links.append(i[1]);
    for i in self.tail:
      i[1].state.start_line=self.final_output.count('\n');
      self.final_output+=i[0];
      i[1].state.end_line=self.final_output.count('\n');
      dist=i[1].state.end_line-i[1].state.start_line;
      for j in range(dist): node_links.append(i[1]);
    for key in self.fig_links: #also give figure objects pointers into the tex
      if(self.fig_links[key][0]==None or self.fig_links[key][1]==None): continue;
      self.fig_links[key][1].state.start_line=self.fig_links[key][0].state.start_line;
      self.fig_links[key][1].state.end_line=self.fig_links[key][0].state.end_line;
    node.update_all_line_stats();
    return self.final_output, node_links;

  def structure_pass(self, node, inherit_state):
    node.process_type(); #does double work with item/text change triggers (used here to guarantee correctness)
    node.state=copy.copy(inherit_state);
    node.state.depth+=1;
    if(node.ntype=='section'): node.state.sec_depth+=1;
    elif(node.ntype=='list'): node.state.list_depth+=1;
    elif(node.ntype=='omit'): node.state.omitted=1; return;
    elif(node.ntype=='comment'): node.state.is_comment=1;
    elif(node.ntype=='redcomment'): node.state.is_red_comment=1;
    elif(node.ntype=='yellowcomment'): node.state.is_yellow_comment=1;
    elif(node.ntype=='bluecomment'): node.state.is_blue_comment=1;
    elif(node.ntype=='figure' or node.ntype=='figure*'):
      for i in node.params:
        f_id=i.split(',')[0];
        if(f_id in self.fig_links):
          if(self.fig_links[f_id][0]==None): self.fig_links[f_id][0]=node;
        else: self.fig_links[f_id]=[node, None];
    elif(node.ntype=='code_figure' or node.ntype=='pdf_figure' or node.ntype=='pdf_table' or
        node.ntype=='graph_figure' or node.ntype=='R_figure' or node.ntype=='table_figure'):
      if(list_check(node.params, 1)):
        f_id=node.params[0];
        if(f_id in self.fig_links):
          if(self.fig_links[f_id][1]==None): self.fig_links[f_id][1]=node;
        else: self.fig_links[f_id]=[None, node];
    for i in node.kids():
      self.structure_pass(i,node.state);

  def tex_render_pass(self, node):
    if(node.state.omitted): return;
    if(node.ntype=='title'): self.title=["\n\\title{"+node.snippet+"}\n\\maketitle\n",node];
    elif(node.ntype=='head_tex'): self.head.append([node.snippet+'\n',node]);
    elif(node.ntype=='tail_tex'): self.tail.append([node.snippet+'\n',node]);
    elif(node.ntype=='abstract'): self.abstract.append([node.snippet+'\n',node]);
    elif(node.ntype=='abstract_paragraph'): self.abstract.append(['\n'+node.snippet+'\n',node]);
    elif(node.ntype=='chapter'): self.core.append([self.proc_chapter(node),node]);
    elif(node.ntype=='chapter*'): self.core.append([self.proc_chapter(node,1),node]);
    elif(node.ntype=='section'): self.core.append([self.proc_section(node),node]);
    elif(node.ntype=='section*'): self.core.append([self.proc_section(node,1),node]);
    elif(node.ntype=='paragraph'): self.core.append(['\n'+node.snippet+'\n',node]);
    elif(node.ntype=='text'): self.core.append([node.snippet+'\n',node]);
    elif(node.ntype=='cmt_on'): self.cmt_switch=1;
    elif(node.ntype=='cmt_off'): self.cmt_switch=0;
    elif(node.ntype=='comment' and self.cmt_switch): self.core.append(['{\\greencomment{ '+node.snippet+' }}\n',node]);
    elif(node.ntype=='redcomment' and self.cmt_switch): self.core.append(['{\\redcomment{ '+node.snippet+' }}\n',node]);
    elif(node.ntype=='yellowcomment' and self.cmt_switch): self.core.append(['{\\yellowcomment{ '+node.snippet+' }}\n',node]);
    elif(node.ntype=='bluecomment' and self.cmt_switch): self.core.append(['{\\bluecomment{ '+node.snippet+' }}\n',node]);
    elif(node.ntype=='list'):
      if(list_check(node.params, 1)):
        self.core.append(["\\begin{"+node.params[0]+"}\n",node]);
        for i in node.kids(): self.tex_render_pass(i);
        self.core.append(["\\end{"+node.params[0]+"}\n",node]);
        return;
    elif(node.ntype=='item'): self.core.append(["\\item "+node.snippet+'\n',node]);
    elif(node.ntype=='bibtex'): self.bibtex+=node.snippet+'\n';
    elif(node.ntype=='inline_code'): self.core.append([self.proc_code(node),node]);
    elif(node.ntype=='inline_table'): self.core.append([self.proc_inline_table(node,False),node]);
    elif(node.ntype=='figure'): self.core.append([self.proc_figure(node,False),node]);
    elif(node.ntype=='figure*'): self.core.append([self.proc_figure(node,True),node]);
    elif(node.ntype=='sub_figure'): self.core.append([self.proc_sub_figure(node,False),node]);
    elif(node.ntype=='sub_figure*'): self.core.append([self.proc_sub_figure(node,True),node]);
#    elif(node.ntype=='code_figure'): #not needed as these are processed during structure pass
#    elif(node.ntype=='table_figure'): #not needed as these are processed during structure pass
#    elif(node.ntype=='pdf_figure'): #not needed as these are processed during structure pass
    elif(node.ntype=='table'): self.core.append([self.proc_table(node,False),node]);
    elif(node.ntype=='table*'): self.core.append([self.proc_table(node,True),node]);
    for i in node.kids(): self.tex_render_pass(i);

  def proc_chapter(self, node, starred=0):
    ret='';
    label='';
    for i in node.snippet.split(): label+=i;
    if(not starred):
      ret+="\\chapter{"+node.snippet+"}\n";
    else:
      ret+="\\chapter*{"+node.snippet+"}\n";
    ret+="\\label{chap:"+label+"}\n";
    return ret;

  def proc_section(self, node, starred=0):
    ret='';
    label='';
    for i in node.snippet.split(): label+=i;
    if(node.state.sec_depth==1):
      if(not starred): ret+="\\section{"+node.snippet+"}\n";
      else: ret+="\\section*{"+node.snippet+"}\n";
      ret+="\\label{sec:"+label+"}\n";
    elif(node.state.sec_depth==2):
      if(not starred): ret+="\\subsection{"+node.snippet+"}\n";
      else: ret+="\\subsection*{"+node.snippet+"}\n";
      ret+="\\label{subsec:"+label+"}\n";
    elif(node.state.sec_depth==3):
      if(not starred): ret+="\\subsubsection{"+node.snippet+"}\n";
      else: ret+="\\subsubsection*{"+node.snippet+"}\n";
      ret+="\\label{subsubsec:"+label+"}\n";
    elif(node.state.sec_depth==4):
      if(not starred): ret+="\\paragraph{"+node.snippet+"}\n";
      else: ret+="\\paragraph*{"+node.snippet+"}\n";
      ret+="\\label{par:"+label+"}\n";
    elif(node.state.sec_depth>=5):
      if(not starred): ret+="\\subparagraph{"+node.snippet+"}\n";
      else: ret+="\\subparagraph*{"+node.snippet+"}\n";
      ret+="\\label{subpar:"+label+"}\n";
    return ret;

  def dump_file(self, fn, s, mydir=None):
    if(mydir==None):
      mydir=self.gui.gen_dir
    f=open(mydir+fn,'w');
    f.writelines(s);
    f.close();

  def proc_code(self, node):
    ret='\\begin{lstlisting}[frame=single]\n';
    ret+=node.snippet+'\n';
    ret+='\\end{lstlisting}\n';
    return ret;

  def proc_inline_table(self, node, in_figure):
    if(not list_check(node.params,1)): return "";
    ret='\\begin{center}\\begin{tabular}{';
    if(in_figure):
      if(not list_check(node.params,3)): return "";
      params=node.params[2:]
    else: params=node.params;
    for i in range(len(params)):
      if(i>0): ret+='|';
      ret+=params[i];
    ret+='}\n'
    ret+=node.snippet+'\n';
    ret+='\\end{tabular}\\end{center}\n';
    return ret;

  def proc_table(self, node, is_starred):
    if(not list_check(node.params,2)): return "";
    if(is_starred):
      ret='\\begin{table*}[h]\\footnotesize\n';
    else: ret='\\begin{table}[h]\\footnotesize\n';
    ret+='\\caption{'+node.params[1]+'}\n'+\
        '\\label{tab:'+node.params[0]+'}\n';
    ret+=self.proc_inline_table(node,True);
    ret+='\\end{table}\n';
    return ret;

  def proc_figure(self, node, is_starred):
    if(not list_check(node.params, 1)): return '';
    header="";
    body="";
    footer="";
    header+="\\begin{";
    tag="figure";
    hfill='\\hfill';
    for i in range(len(node.params)):
      if(node.params[i]=="nl" or node.params[i]=="\\\\"): hfill='\\hfill\\\\\n'; continue;
      fig_spec=node.params[i].split(","); #is of format figlable,[size]
      if(not fig_spec[0] in self.fig_links): continue; #FIXME: Error output here-should not be possible
      cur_node=self.fig_links[fig_spec[0]][1];
      if(cur_node==None): continue; #FIXME: Error output (no figure object found)
      if(i>0): body+=hfill; hfill='\\hfill\n';
      body+='\\begin{minipage}[t]{'+fig_spec[1]+'\columnwidth}\n';
      body+='\\centering\n';
      if(cur_node.ntype=='code_figure' and list_check(cur_node.params,1)):
        body+=self.proc_code(cur_node);
        body+='\\caption{'+cur_node.params[1]+'}\n'
      if(cur_node.ntype=='table_figure' and list_check(cur_node.params,3)):
        body+=self.proc_inline_table(cur_node,True);
        body+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='pdf_figure' and list_check(cur_node.params,3)):
        body+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        body+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='pdf_table' and list_check(cur_node.params,3)):
        tag="table";
        body+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        body+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='graph_figure' and list_check(cur_node.params,3)):
        body+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        body+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='R_figure' and list_check(cur_node.params,3)):
        body+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        body+='\\caption{'+cur_node.params[1]+'}\n'
      body+='\\label{fig:'+fig_spec[0]+'}\n';
      body+='\\end{minipage}\n';

    if(is_starred):
      tag+="*";
    header+=tag;
    header+="}\n\\centering\n";
        
    footer+="\\end{";
    footer+=tag;
    footer+="}\n";
    return header+body+footer;

  def proc_sub_figure(self, node, is_starred):
    if(not list_check(node.params, 2)): return '';
    ret="";
    if(is_starred):
      ret+='\\begin{figure*}\n';
      ret+='\\centering\n';
    else:
      ret+='\\begin{figure}\n';
      ret+='\\centering\n';
    hfill='\\hfill\n';
    fig_name=node.params[0];
    fig_caption=node.params[1];
    params=node.params[2:];
    for i in range(len(params)):
      if(params[i]=="nl" or params[i]=="\\\\"): hfill='\\hfill\\\\\n'; continue;
      fig_spec=params[i].split(","); #is of format figlable,[size]
      if(not fig_spec[0] in self.fig_links): continue; #FIXME: Error output here-should not be possible
      cur_node=self.fig_links[fig_spec[0]][1];
      if(cur_node==None): continue; #FIXME: Error output (no figure object found)
      if(i>0): ret+=hfill; hfill='\\hfill\n';
      ret+='\\begin{subfigure}[t]{'+fig_spec[1]+'\columnwidth}\n';
      ret+='\\centering\n';
      if(cur_node.ntype=='code_figure' and list_check(cur_node.params,1)):
        ret+=self.proc_code(cur_node);
        ret+='\\caption{'+cur_node.params[1]+'}\n'
      if(cur_node.ntype=='table_figure' and list_check(cur_node.params,3)):
        ret+=self.proc_inline_table(cur_node,True);
        ret+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='pdf_figure' and list_check(cur_node.params,3)):
        ret+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        ret+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='pdf_table' and list_check(cur_node.params,3)):
        ret+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        ret+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='graph_figure' and list_check(cur_node.params,3)):
        ret+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        ret+='\\caption{'+cur_node.params[1]+'}\n'
      elif(cur_node.ntype=='R_figure' and list_check(cur_node.params,3)):
        ret+='\\epsfig{figure='+fig_spec[0]+', width='+cur_node.params[2]+'\columnwidth}\n'
        ret+='\\caption{'+cur_node.params[1]+'}\n'
      ret+='\\label{fig:'+fig_spec[0]+'}\n';
      ret+='\\end{subfigure}\n';
    ret+='\\caption{'+fig_caption+'}\n'
    ret+='\\label{fig:'+fig_name+'}\n';
    if(is_starred):
      ret+='\\end{figure*}\n';
    else:
      ret+='\\end{figure}\n';
    return ret;

  def generator_pass(self, node):
    if(node.state.omitted): return;
    if(node.ntype=='class_file'): self.dump_file(node.params[0]+'.cls', node.snippet);
    elif(node.ntype=='style_file'): self.dump_file(node.params[0]+'.sty', node.snippet);
    elif(node.ntype=='dump_file'): self.dump_file(node.params[0], node.snippet);
    elif(node.ntype=='bib_style'): self.dump_file(node.params[0]+'.bst', node.snippet);
    elif(node.ntype=='graph_figure'):
      self.gen_jobs.append(Process(target=self.gen_pygraph, args=(node,)));
    elif(node.ntype=='R_figure'):
      self.gen_jobs.append(Process(target=self.gen_Rgraph, args=(node,)));
    elif(node.ntype=='pdf_figure'):
      self.gen_pdf_fig(node);
    elif(node.ntype=='pdf_table'):
      self.gen_pdf_fig(node);
    for i in node.kids(): self.generator_pass(i);

  def gen_pdf_fig(self, node):
    syscmd="cp "+self.gui.pdf_dir+node.params[0]+".pdf "+self.gui.gen_dir;
    self.work_log+= "Executing: "+syscmd;
    runcmd=subprocess.Popen(syscmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE,shell=True);
    self.work_log+=runcmd.communicate()[0];
    return runcmd.returncode==0;

  def gen_pygraph(self, node):
    if(node.is_rendered==1): return 1;
    text='';
    if("$FPWSM_GRAPH_PDF_OUT" in node.snippet):
      text=node.snippet.replace("$FPWSM_GRAPH_PDF_OUT",
        "import os as mars_awesome_os;\n"+
        "import matplotlib.pyplot as mars_awesome_plt;\n"+
        "mars_awesome_plt.savefig('"+node.params[0]+".eps', bbox_inches='tight');\n"+
        "mars_awesome_os.popen('epstopdf "+node.params[0]+".eps');\n");
    else:
      text=node.snippet+"\n\nimport os as mars_awesome_os;\n"+"import matplotlib.pyplot as mars_awesome_plt;\n"+\
           "mars_awesome_plt.savefig('"+node.params[0]+".eps', bbox_inches='tight');\n"+\
           "mars_awesome_os.popen('epstopdf "+node.params[0]+".eps');\n";

    do_run = write_file_dep(self.gui.gen_dir+node.params[0]+".py", text)
    if do_run:
      syscmd="cd "+self.gui.gen_dir+"; python "+node.params[0]+".py";
      self.work_log+= "Executing: "+syscmd;
      runcmd=subprocess.Popen(syscmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE,shell=True);
      self.work_log+=runcmd.communicate()[0];
      if(runcmd.returncode==0): node.is_rendered=1;
      return runcmd.returncode==0; #self.out_dir+self.gui.gen_dir+node.params[0]+".pdf";
    else:
      return True

  def gen_Rgraph(self, node):
    if(node.is_rendered==1): return 1;

    replace = {"$FPWSM_R_PDF_INIT(": "pdf('"+node.params[0]+".pdf'", "$FPWSM_R_PDF_FINI()": "dev.off()"}
    text = node.snippet
    for k in replace.keys():
      text = text.replace(k, replace[k])

    rname = self.gui.gen_dir+node.params[0]+".R"
    do_run = write_file_dep(rname, text)

    if do_run:
      syscmd="cd "+self.gui.gen_dir+"; Rscript "+node.params[0]+".R";
      self.work_log+= "Executing: "+syscmd;
      runcmd=subprocess.Popen(syscmd,stderr=subprocess.STDOUT,stdout=subprocess.PIPE,shell=True);
      self.work_log+=runcmd.communicate()[0];
      if(runcmd.returncode==0): node.is_rendered=1;
      return runcmd.returncode==0; #self.out_dir+self.gui.gen_dir+node.params[0]+".pdf";
    else:
      return True


#-Custom Node Edit Box---------------------------------------------------------
#-Overloaded QTextEdit for fancy editing
#------------------------------------------------------------------------------
class SpellAction(QAction):
    '''
        A special QAction that returns the text in a signal.
        '''
    correct = pyqtSignal(unicode)
    def __init__(self, *args):
        QAction.__init__(self, *args)
        self.triggered.connect(lambda x: self.correct.emit(
            unicode(self.text())))


class fancy_text(QTextEdit):
  mouse_clicked = pyqtSignal(QPoint);
  def __init__(self, parent=None, line_hl=True):
    QTextEdit.__init__(self, parent);
    self.setAcceptRichText(False);
    self.line_hl=line_hl;
    self.hl_color =  QColor('lightblue').lighter(130);
    self.cursorPositionChanged.connect(self.cur_pos_change)
    self.py_highlighter = py_highlighter(self.document())
    self.dictionary = enchant.Dict();
    self.tex_highlighter = tex_highlighter(self.document())
    self.tex_highlighter.set_dict(self.dictionary); 

  def cur_pos_change(self):
    if(self.line_hl):
      selection =  QTextEdit.ExtraSelection()
      selection.cursor = self.textCursor()
      selection.cursor.clearSelection()
      selection.format.setBackground(self.hl_color)
      selection.format.setProperty( QTextFormat.FullWidthSelection, True)
      self.setExtraSelections([selection]);

  def highlight_region(self, begin, end):
      if(begin>=end): return;
      if(end>len(str(self.toPlainText().toAscii()))-1): end=len(str(self.toPlainText().toAscii()))-1;
      selection =  QTextEdit.ExtraSelection()
      selection.cursor = self.textCursor()
      selection.cursor.clearSelection()
      selection.cursor.setPosition(begin, QTextCursor.MoveAnchor);
      selection.cursor.setPosition(end, QTextCursor.KeepAnchor);
      selection.format.setBackground(QColor('lightgreen').lighter(100))
      self.setExtraSelections([selection]);

  def mousePressEvent(self, event):
      pos = event.pos()
      if(QApplication.keyboardModifiers()==Qt.ShiftModifier):
        self.mouse_clicked.emit(pos)
      if(event.button() == Qt.RightButton):
        event = QMouseEvent(QEvent.MouseButtonPress, event.pos(),
            Qt.LeftButton, Qt.LeftButton, Qt.NoModifier);
      return QTextEdit.mousePressEvent(self, event);

  def contextMenuEvent(self, event):
    popup_menu = self.createStandardContextMenu()
    if(popup_menu==None):return;
    
    # Select the word under the cursor.
    cursor = self.textCursor()
    cursor.select(QTextCursor.WordUnderCursor)
    self.setTextCursor(cursor)
    
    # Check if the selected word is misspelled and offer spelling
    # suggestions if it is.
    if self.textCursor().hasSelection():
        text = unicode(self.textCursor().selectedText())
        if not self.dictionary.check(text):
            spell_menu = QMenu('Spelling Suggestions')
            for word in self.dictionary.suggest(text):
                action = SpellAction(word, spell_menu)
                action.correct.connect(self.correctWord)
                spell_menu.addAction(action)
            # Only add the spelling suggests to the menu if there are
            # suggestions.
            if len(spell_menu.actions()) != 0:
                popup_menu.insertSeparator(popup_menu.actions()[0])
                popup_menu.insertMenu(popup_menu.actions()[0], spell_menu)
    popup_menu.exec_(event.globalPos())

  def correctWord(self, word):
    '''
        Replaces the selected text with word.
        '''
    cursor = self.textCursor()
    cursor.beginEditBlock()
    cursor.removeSelectedText()
    cursor.insertText(word)
    cursor.endEditBlock()

#-Item Edit View---------------------------------------------------------------
#-Custom item edit view
#------------------------------------------------------------------------------
class node_editor(fancy_text):
  def __init__(self, parent=None, line_hl=True):
    fancy_text.__init__(self, parent, line_hl);
    self.setMaximumWidth(400);
    self.setMinimumWidth(2);
    self.document().setTextWidth(400);
    self.document().contentsChanged.connect(self.size_update);

  def size_update(self):
    size=self.document().size().height();
    self.setFixedHeight(size);


#-GUI Tree Item----------------------------------------------------------------
#-Overloaded QTreeWidgetItem for linking with FPWSM Tree Node
#------------------------------------------------------------------------------
class tree_item(QTreeWidgetItem):
  def __init__(self, linked_tree_widget, anch="Baby Node", text=""):
    super(tree_item, self).__init__();
    self.setFlags(self.flags() | Qt.ItemIsEditable);
#    self.setChildIndicatorPolicy(QTreeWidgetItem.ShowIndicator);
    self.setText(0,anch);
    self.linked_node=node(anch, text);
    self.linked_node.linked_gui_item=self;
    self.linked_tree_widget=linked_tree_widget;

  def clone(self):
    ret=tree_item(self.linked_tree_widget, self.linked_node.anchor, self.linked_node.text);
    for i in range(self.childCount()):
      ret.addChild(self.child(i).clone());
    return ret;

  def expand_all(self, expand_flag=True):
    self.setExpanded(expand_flag);
    for i in range(self.childCount()):
      self.child(i).expand_all(expand_flag);

  def found_in_subtree(self, item): #test to see if item is a child of this subtree
    if(self==item): return True;
    for i in range(self.childCount()):
      if(self.child(i)==item): return True;
      if(self.child(i).found_in_subtree(item)): return True;
    return False;

  def depth_to_color(self): #set color based on depth
    depth=0; item=self;
    omitted=0;
    while(item.parent()!=None): 
      depth+=1; 
      if(item.linked_node.ntype=='omit'):
        self.setBackgroundColor(0,QColor(245,245,245));
        self.setTextColor(0, QColor(150,150,150));
        omitted=1; break;
      item=item.parent();
    if(not omitted):
      random.seed(depth);
      self.setTextColor(0, QColor(0,0,0));
      self.setBackgroundColor(0,QColor(
          220+random.randrange(0,35),
          220+random.randrange(0,35),
          220+random.randrange(0,35)));

    for i in range(self.childCount()):
      self.child(i).depth_to_color();

class item_delegate(QStyledItemDelegate):
    def __init__(self, parent):
      self.width=400;
      QStyledItemDelegate.__init__(self,parent);

    def sizeHint(self, option, index):
        default = QStyledItemDelegate.sizeHint(self, option, index)
        doc=QTextDocument(index.model().data(index, Qt.DisplayRole).toString());
        item=self.parent().currentItem();
        if(item!=None):
          doc.setTextWidth(self.width);
        else: self.width=400;
        return QSize(self.width, doc.size().height())

    def createEditor(self, parent, option, index):
        editor = node_editor(parent)
        return editor

    def setEditorData(self, editor, index):
        text = index.model().data(index, Qt.DisplayRole).toString()
        editor.setText(text)
        cursor=editor.textCursor();
        cursor.setPosition(0);
        cursor.setPosition(len(str(text.toAscii())), QTextCursor.KeepAnchor);
        editor.setTextCursor(cursor);

    def setModelData(self, editor, model, index):
        model.setData(index, QVariant(editor.toPlainText()))
        item=self.parent().currentItem();
        if(item!=None):
          self.width=self.parent().visualItemRect(item).width();

    def eventFilter(self, editor, event):
        if (event.type() == QEvent.KeyPress and
            event.key() == Qt.Key_Return and not (event.modifiers() & Qt.ShiftModifier)):
            self.commitData.emit(editor)
            self.closeEditor.emit(editor, QAbstractItemDelegate.NoHint)
            return True
        return QStyledItemDelegate.eventFilter(self,editor,event)


#-GUI Tree View----------------------------------------------------------------
#-Overloaded QTreeWidget for showing FPWSM Tree Structure and manages undo/redo
#------------------------------------------------------------------------------
class tree_widget(QTreeWidget):
  def __init__(self, main_widget):
    super(tree_widget, self).__init__();
    self.linked_main_widget=main_widget;
    self.undo_stack=QUndoStack(self);
    self.copied=None; #For copy/cut/paste
    self.undo_signal_stomp=0; #to squelch itemChange signal when undoing
    self.setItemDelegate(item_delegate(self))
    self.setWordWrap(True);
#    self.setDragEnabled(True)
#    self.setAcceptDrops(True)
#    self.setDropIndicatorShown(True)
#    self.viewport().setAcceptDrops(True);
    self.setDragDropMode(QAbstractItemView.DragDrop);
    self.installEventFilter(self);
    self.connect(self, SIGNAL("itemChanged(QTreeWidgetItem*, int)"), self.on_item_change);

  def on_item_change(self, item, col):
    if(not self.undo_signal_stomp):
      com=self.command_mod_tree(self,2,None,col,item,"Changed node text");
      self.undo_stack.push(com);

  def eventFilter(self, sender, event):
    if (event.type() == QEvent.ChildRemoved):
      self.linked_main_widget.update_tex_output();
    return False; # don't actually interrupt anything

  def dropMimeData(self, parent, row, data, action):
    if(parent==None): return True;
    if(self.selectedItems()[0].found_in_subtree(parent)): return True; #cant move a parent into its own subtree
    com=self.command_mod_tree(self,3,parent,row,self.selectedItems()[0],"Added dropped node");
    self.undo_stack.push(com);
    return True;

  class command_mod_tree(QUndoCommand): #mod type: 0 - add node, 1 - remove node, 2 - edit node, 3 - drag/drop node
    def __init__(self, linked_tree_widget, mod_type, parent, index, item, description):
      super(tree_widget.command_mod_tree, self).__init__(description);
      self.linked_tree_widget=linked_tree_widget;
      self.mod_type=mod_type;
      self.parent=parent;
      self.index=index;
      self.item=item;
      self.description=description;
      if(mod_type==2): #for mode type 2
        self.old_text=copy.copy(item.linked_node.anchor);
        self.new_text=copy.copy(item.text(index).trimmed());
      elif(mod_type==3): #for drag and drop
        self.dropped_item=self.item.clone();
        self.dragged_from_parent=self.item.parent();
        self.dragged_from_index=self.item.parent().indexOfChild(self.item);

    def redo(self):
      if(self.mod_type==0):
        self.parent.insertChild(self.index,self.item); #remember there is a linked node associated with item
        self.parent.setExpanded(True);
        self.linked_tree_widget.setCurrentItem(self.item);
        if(not self.linked_tree_widget.undo_stack.canRedo()): self.linked_tree_widget.editItem(self.item);
      elif(self.mod_type==1):
        self.parent.removeChild(self.item);
      elif(self.mod_type==2):
        self.linked_tree_widget.undo_signal_stomp=1;
        self.item.linked_node.anchor=self.new_text;
        self.item.linked_node.process_type();
        self.item.setText(self.index,self.new_text); #index is column here
        self.linked_tree_widget.undo_signal_stomp=0;
      elif(self.mod_type==3):
        self.parent.insertChild(self.index,self.dropped_item); 
        self.dragged_from_parent.removeChild(self.item);
        self.dropped_item.depth_to_color();
        self.linked_tree_widget.setCurrentItem(self.dropped_item);

    def undo(self):
      if(self.mod_type==0):
        self.parent.removeChild(self.item);
      elif(self.mod_type==1):
        self.parent.insertChild(self.index,self.item);
        self.linked_tree_widget.setCurrentItem(self.item);
      elif(self.mod_type==2):
        self.linked_tree_widget.undo_signal_stomp=1;
        self.item.linked_node.anchor=self.old_text;
        self.item.setText(self.index,self.old_text);
        self.linked_tree_widget.undo_signal_stomp=0;
      elif(self.mod_type==3):
        self.parent.removeChild(self.dropped_item);
        self.dragged_from_parent.insertChild(self.dragged_from_index,self.item);
        self.item.depth_to_color();
        self.linked_tree_widget.setCurrentItem(self.item);

  def event(self, event):
    if(event.type() == QEvent.KeyPress and event.key() == Qt.Key_Backspace and len(self.selectedItems())):
      if(type(self.selectedItems()[0].parent())==tree_item): #Check if not parent node
        my_index=self.selectedItems()[0].parent().indexOfChild(self.selectedItems()[0]);
        com=self.command_mod_tree(self,1,self.selectedItems()[0].parent(),my_index,self.selectedItems()[0],"Removed node");
        self.undo_stack.push(com);
      else: self.linked_main_widget.statusBar().showMessage('Cannot remove head node!');
      return True
    elif(event.type() == QEvent.KeyPress and event.key() == Qt.Key_Tab):
      if(len(self.selectedItems())):
        clength=self.selectedItems()[0].childCount();
        com=self.command_mod_tree(self,0,self.selectedItems()[0],clength,tree_item(self),"Added child node");
        self.undo_stack.push(com);
      return True
    elif(event.type() == QEvent.KeyPress and event.key() == Qt.Key_Return and (event.modifiers() & Qt.ShiftModifier) and
        len(self.selectedItems())):
      if(type(self.selectedItems()[0].parent())==tree_item):
        my_index=self.selectedItems()[0].parent().indexOfChild(self.selectedItems()[0])+1;
        com=self.command_mod_tree(self,0,self.selectedItems()[0].parent(),my_index,tree_item(self),"Added sibling node");
        self.undo_stack.push(com);
      else: #Special case when on head node
        if(len(self.selectedItems())):
          clength=self.selectedItems()[0].childCount();
          com=self.command_mod_tree(self,0,self.selectedItems()[0],clength,tree_item(self),"Added child node");
          self.undo_stack.push(com);
      return True;
    elif(event.type() == QEvent.KeyPress and event.key() == Qt.Key_C and (event.modifiers() & Qt.ControlModifier)):
      if(len(self.selectedItems())):
        self.copied=self.selectedItems()[0].clone();
      return True;
    elif(event.type() == QEvent.KeyPress and event.key() == Qt.Key_X and (event.modifiers() & Qt.ControlModifier) and len(self.selectedItems())):
      if(type(self.selectedItems()[0].parent())==tree_item): #Check if not parent node
        self.copied=copy.deepcopy(self.selectedItems()[0]);
        my_index=self.selectedItems()[0].parent().indexOfChild(self.selectedItems()[0]);
        com=self.command_mod_tree(self,1,self.selectedItems()[0].parent(),my_index,self.selectedItems()[0],"Removed node");
        self.undo_stack.push(com);
      else: self.linked_main_widget.statusBar().showMessage('Cannot remove head node!');
      return True;
    elif(event.type() == QEvent.KeyPress and event.key() == Qt.Key_V and (event.modifiers() & Qt.ControlModifier)):
      if(len(self.selectedItems()) and self.copied!=None):
        clength=self.selectedItems()[0].childCount();
        com=self.command_mod_tree(self,0,self.selectedItems()[0],clength,self.copied.clone(),"Added child node");
        self.undo_stack.push(com);
      return True;
    elif(event.type() == QEvent.KeyPress and event.key() == Qt.Key_Z and (event.modifiers() & Qt.ControlModifier)):
      if (event.modifiers() & Qt.ShiftModifier):
        self.undo_stack.redo();
      else:
        self.undo_stack.undo();
      return True;
#    elif(event.type()==QEvent.KeyPress and event.key()==Qt.Key_Return and len(self.selectedItems())):
#      if(type(self.selectedItems()[0].parent())==tree_item):
#        my_index=self.selectedItems()[0].parent().indexOfChild(self.selectedItems()[0]);
#        self.editItem(self.selectedItems()[0].parent().child(my_index));
#      return True;
    elif(event.type()==QEvent.KeyPress and event.key() == Qt.Key_Return and len(self.selectedItems())):
#         and ((not event.modifiers()) or ((event.modifiers() & Qt.ShiftModifier)))):
      if(type(self.selectedItems()[0].parent())==tree_item):
        my_index=self.selectedItems()[0].parent().indexOfChild(self.selectedItems()[0]);
        self.editItem(self.selectedItems()[0].parent().child(my_index));
      return True;
    else:
      return QTreeWidget.event(self, event)


#-Python Highligting-----------------------------------------------------------
#-Implements Python syntax highlighting
#------------------------------------------------------------------------------
class py_highlighter(QSyntaxHighlighter):
  class BraketsInfo:
    def __init__(self, character, position):
      self.character = character
      self.position = position

  class TextBlockData(QTextBlockUserData):
    def __init__(self, parent=None):
      super(py_highlighter.TextBlockData, self).__init__()
      self.braces = []
      self.valid = False

    def insert_brackets_info(self, info):
      self.valid = True
      self.braces.append(info)

    def isValid(self):
      return self.valid

  preprocessors = ['import', 'from', 'as', 'False', 'None',
        'True', '__name__', '__debug__']

  keywords = ['and', 'assert', 'break', 'continue', 'del', 'elif', 'else',
        'except', 'exec', 'finally', 'for', 'global', 'if', 'in', 'is',
        'lambda', 'not', 'or', 'pass', 'print', 'raise', 'try', 'while', 'yield',
        'def', 'class', 'return']

  specials = ['ArithmeticError', 'AssertionError', 'AttributeError', 'EnvironmentError',
        'EOFError', 'Exception', 'FloatingPointError', 'ImportError', 'IndentationError',
        'IndexError', 'IOError', 'KeyboardInterrupt', 'KeyError', 'LookupError', 'MemoryError',
        'NameError', 'NotImplementedError', 'OSError', 'OverflowError', 'ReferenceError',
        'RuntimeError', 'StandardError', 'StopIteration', 'SyntaxError', 'SystemError',
        'SystemExit', 'TabError', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError',
        'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'ValueError',
        'WindowsError', 'ZeroDivisionError',

        'Warning', 'UserWarning', 'DeprecationWarning', 'PendingDeprecationWarning',
        'SyntaxWarning', 'OverflowWarning', 'RuntimeWarning', 'FutureWarning',

        '__import__', 'abs', 'apply', 'basestring', 'bool', 'buffer', 'callable',
        'chr', 'classmethod', 'cmp', 'coerce', 'compile', 'complex', 'delattr', 'dict',
        'dir', 'divmod', 'enumerate', 'eval', 'execfile', 'file', 'filter', 'float',
        'getattr', 'globals', 'hasattr', 'hash', 'hex', 'id', 'input', 'int', 'intern',
        'isinstance', 'issubclass', 'iter', 'len', 'list', 'locals', 'long', 'map',
        'max', 'min', 'object', 'oct', 'open', 'ord', 'pow', 'property', 'range', 'raw_input',
        'reduce', 'reload', 'repr', 'round', 'setattr', 'self', 'slice', 'staticmethod',
        'str', 'sum', 'super', 'tuple', 'type', 'unichr', 'unicode', 'vars', 'xrange', 'zip']

  operators = ['=',
                 # Comparison
                '==', '!=', '<', '<=', '>', '>=',
                # Arithmetic
                '\+', '-', '\*', '/', '//', '\%', '\*\*',
                # In-place
                '\+=', '-=', '\*=', '/=', '\%=',
                # Bitwise
                '\^', '\|', '\&', '\~', '>>', '<<',]

    # Python braces
  braces = ['\{', '\}', '\(', '\)', '\[', '\]',]

  def __init__(self, document):
    super(py_highlighter, self).__init__(document)
    STYLES = {'preprocessor': self.format('darkMagenta'),
    'keyword': self.format('darkOrange'),
    'special': self.format('darkMagenta'),
    'operator': self.format('darkMagenta'),
    'brace': self.format('darkGray'),
    'defclass': self.format('blue'),
    'string': self.format('green'),
    'string2': self.format('green'),
    'comment': self.format('lightGrey'),
    'framework': self.format('blue'),}

    # Multi-line strings (expression, flag, style)
    # FIXME: The triple-quotes in these two lines will mess up the
    # syntax highlighting from this point onward
    self.tri_double = ( QRegExp(r'''"""(?!')'''), 2, STYLES['string2'])
    self.tri_single = ( QRegExp(r"""'''(?!")"""), 1, STYLES['string2'])

    #Brace rule
    self.braces = QRegExp('(\{|\}|\(|\)|\[|\])')

    rules = []
    rules += [(r'\b%s\b' % w, 0, STYLES['keyword']) for w in py_highlighter.keywords]
    rules += [(r'\b%s\b' % w, 0, STYLES['preprocessor']) for w in py_highlighter.preprocessors]
    rules += [(r'\b%s\b' % w, 0, STYLES['special']) for w in py_highlighter.specials]
    rules += [(r'%s' % o, 0, STYLES['operator']) for o in py_highlighter.operators]
    rules += [(r'%s' % b, 0, STYLES['brace']) for b in py_highlighter.braces]

    rules += [# Framework PyQt
            (r'\bPyQt4\b|\bPySide\b|\bQt?[A-Z][a-z]\w+\b',0,STYLES['framework']),
            # 'def' followed by an identifier
            (r'\bdef\b\s*(\w+)', 1, STYLES['defclass']),
            # 'class' followed by an identifier
            (r'\bclass\b\s*(\w+)', 1, STYLES['defclass']),
            # Double-quoted string, possibly containing escape sequences
            #(r'"[^"\\]*(\\.[^"\\]*)*"', 0, STYLES['string']),
            (r"""(:?"["]".*"["]"|'''.*''')""", 0, STYLES['string']),
            # Single-quoted string, possibly containing escape sequences
            #(r"'[^'\\]*(\\.[^'\\]*)*'", 0, STYLES['string']),
            (r"""(?:'[^']*'|"[^"]*")""", 0, STYLES['string']),
            # From '#' until a newline
            (r'#[^\n]*', 0, STYLES['comment'])]

    # Build a QRegExp for each pattern
    self.rules = [( QRegExp(pat), index, fmt) for (pat, index, fmt) in rules]
    self.rules[-2][0].setMinimal(True)
    self.rules[-3][0].setMinimal(True)

  def format(self,color, style=''):
    """Return a QTextCharFormat with the given attributes.
    """
    _color =  QColor()
    _color.setNamedColor(color)

    _format =  QTextCharFormat()
    _format.setForeground(_color)
    if 'bold' in style:
        _format.setFontWeight(QFont.Bold)
    if 'italic' in style:
        _format.setFontItalic(True)
    if 'underline' in style:
        _format.setFontUnderline(True)

    return _format

  def highlightBlock(self, text):
    """Apply syntax highlighting to the given block of text.
    """
    braces = self.braces
    block_data = self.TextBlockData()

    # Brackets
    index = braces.indexIn(text, 0)

    while index >= 0:
        matched_brace = str(braces.capturedTexts()[0].toAscii())
        info = self.BraketsInfo(matched_brace, index)
        block_data.insert_brackets_info(info)
        index = braces.indexIn(text, index + 1)

    self.setCurrentBlockUserData(block_data)

    # Do other syntax formatting
    for expression, nth, format in self.rules:
        index = expression.indexIn(text, 0)

        while index >= 0:
            # We actually want the index of the nth match
            index = expression.pos(nth)
            length = len(expression.cap(nth))
            self.setFormat(index, length, format)
            index = expression.indexIn(text, index + length)

    self.setCurrentBlockState(0)

    # Do multi-line strings
    self.match_multiline(text, *self.tri_single)
    self.match_multiline(text, *self.tri_double)

  def match_multiline(self, text, delimiter, in_state, style):
    """Do highlighting of multi-line strings. ``delimiter`` should be a
    ``QRegExp`` for triple-single-quotes or triple-double-quotes, and
    ``in_state`` should be a unique integer to represent the corresponding
    state changes when inside those strings. Returns True if we're still
    inside a multi-line string when this function is finished.
    """
    # If inside triple-single quotes, start at 0
    if self.previousBlockState() == in_state:
        start = 0
        add = 0
    # Otherwise, look for the delimiter on this line
    else:
        start = delimiter.indexIn(text)
        # Move past this match
        add = delimiter.matchedLength()

    # As long as there's a delimiter match on this line...
    while start >= 0:
        # Look for the ending delimiter
        end = delimiter.indexIn(text, start + add)
        # Ending delimiter on this line?
        if end >= add:
            length = end - start + add + delimiter.matchedLength()
            self.setCurrentBlockState(0)
        # No; multi-line string
        else:
            self.setCurrentBlockState(in_state)
            length = len(text )- start + add
        # Apply formatting
        self.setFormat(start, length, style)
        # Look for the next match
        start = delimiter.indexIn(text, start + length)

    # Return True if still inside a multi-line string, False otherwise
    if self.currentBlockState() == in_state:
        return True
    else:
        return False


#-Latex Highligting------------------------------------------------------------
#-Implements Latex syntax highlighting (overloaded from py_highlighter)
#------------------------------------------------------------------------------
class tex_highlighter(py_highlighter):
  preprocessors=['input','include','includeonly','usepackage'];
  keywords=['\\','begin','end','documentclass','newcommand',
    'newenvironment','newtheorem','newfont','part','chapter','section',
    'subsection','subsubsection','paragraph','subparagraph','page',
    'equation','figure','table','footnote','footnotemark','footnotetext',
    'mpfootnote','enumi','enumii','enumiii','enumiv','label','pageref','ref',
    'onecolumn','twocolumn','cite','title','maketitle'];
  specials=['emph','textbf','texttt','item'];
  keywords=[];
  preprocessors=[];
  operators=[];
  
  words='(?iu)[\w\']+';

  def __init__(self, document):
    super(tex_highlighter, self).__init__(document)
    STYLES = {
    'preprocessor': self.format('darkMagenta'),
    'keyword': self.format('darkOrange'),
    'special': self.format('darkMagenta'),
    'operator': self.format('darkMagenta'),
    'brace': self.format('darkGray'),
    'defclass': self.format('blue'),
    'string': self.format('green'),
    'string2': self.format('green'),
    'comment': self.format('lightGrey'),
    'framework': self.format('blue'),
    }
    self.dictionary=None;

    #Brace rule
    self.braces = QRegExp('(\{|\}|\(|\)|\[|\])')

    rules = []
    rules += [(r'\b%s\b' % w, 0, STYLES['keyword']) for w in tex_highlighter.keywords]
    rules += [(r'\b%s\b' % w, 0, STYLES['preprocessor']) for w in tex_highlighter.preprocessors]
    rules += [(r'\b%s\b' % w, 0, STYLES['special']) for w in tex_highlighter.specials]
    rules += [(r'%s' % o, 0, STYLES['operator']) for o in tex_highlighter.operators]
    rules += [(r'%s' % b, 0, STYLES['brace']) for b in tex_highlighter.braces]

    rules += [#Latex specials
            (r'\\[a-zA-Z]+',0,STYLES['keyword']),
            #comment
            (r'[^\\]%.*$',0,STYLES['comment']),
            #inline equation
            (r'\{[^\}]*\}',0,STYLES['special']),
            #inline equation
            (r'\$[^\$]*\$',0,STYLES['operator']),
            #inline equation
            (r'\$[^:]*:',0,STYLES['defclass']),]

    # Build a QRegExp for each pattern
    self.rules = [( QRegExp(pat), index, fmt) for (pat, index, fmt) in rules]
    self.rules[-2][0].setMinimal(True)

  def set_dict(self, d):
    self.dictionary=d;
  
  def highlightBlock(self, text):
    if(self.dictionary):
        text = unicode(text)
        format = QTextCharFormat()
        format.setUnderlineColor(Qt.red)
        format.setUnderlineStyle(QTextCharFormat.SpellCheckUnderline)
        for word_object in re.finditer(self.words, text):
            if not self.dictionary.check(word_object.group()):
                self.setFormat(word_object.start(),
                                word_object.end() - word_object.start(), format)

    py_highlighter.highlightBlock(self,text);




#-Find/Replace Dialog----------------------------------------------------------
#-Dialog and functionality for find/replace
#------------------------------------------------------------------------------
class find_replace_dlg(QDialog):
    def __init__(self, gui, parent=None):
        super(find_replace_dlg, self).__init__(parent)
        self.gui = gui;
        self.found_list = deque([])
        self.setup_ui(self)
        self.findButton.setFocusPolicy(Qt.NoFocus)
        self.replaceButton.setFocusPolicy(Qt.NoFocus)
        self.replaceAllButton.setFocusPolicy(Qt.NoFocus)
        self.closeButton.setFocusPolicy(Qt.NoFocus)
        self.update_ui()

    def setup_ui(self, find_replace_dlg):
        find_replace_dlg.setObjectName("find_replace_dlg")
        find_replace_dlg.resize(363, 192)
        self.hboxlayout = QHBoxLayout(find_replace_dlg)
        self.hboxlayout.setMargin(9)
        self.hboxlayout.setSpacing(6)
        self.hboxlayout.setObjectName("hboxlayout")
        self.vboxlayout = QVBoxLayout()
        self.vboxlayout.setMargin(0)
        self.vboxlayout.setSpacing(6)
        self.vboxlayout.setObjectName("vboxlayout")
        self.gridlayout = QGridLayout()
        self.gridlayout.setMargin(0)
        self.gridlayout.setSpacing(6)
        self.gridlayout.setObjectName("gridlayout")
        self.replaceLineEdit = QLineEdit(find_replace_dlg)
        self.replaceLineEdit.setObjectName("replaceLineEdit")
        self.gridlayout.addWidget(self.replaceLineEdit, 1, 1, 1, 1)
        self.findLineEdit = QLineEdit(find_replace_dlg)
        self.findLineEdit.setObjectName("findLineEdit")
        self.gridlayout.addWidget(self.findLineEdit, 0, 1, 1, 1)
        self.label_2 = QLabel(find_replace_dlg)
        self.label_2.setObjectName("label_2")
        self.gridlayout.addWidget(self.label_2, 1, 0, 1, 1)
        self.label = QLabel(find_replace_dlg)
        self.label.setObjectName("label")
        self.gridlayout.addWidget(self.label, 0, 0, 1, 1)
        self.vboxlayout.addLayout(self.gridlayout)
        self.hboxlayout1 = QHBoxLayout()
        self.hboxlayout1.setMargin(0)
        self.hboxlayout1.setSpacing(6)
        self.hboxlayout1.setObjectName("hboxlayout1")
        self.caseCheckBox = QCheckBox(find_replace_dlg)
        self.caseCheckBox.setObjectName("caseCheckBox")
        self.hboxlayout1.addWidget(self.caseCheckBox)
        self.wholeCheckBox = QCheckBox(find_replace_dlg)
        self.wholeCheckBox.setChecked(True)
        self.wholeCheckBox.setObjectName("wholeCheckBox")
        self.hboxlayout1.addWidget(self.wholeCheckBox)
        self.vboxlayout.addLayout(self.hboxlayout1)
        self.hboxlayout2 = QHBoxLayout()
        self.hboxlayout2.setMargin(0)
        self.hboxlayout2.setSpacing(6)
        self.hboxlayout2.setObjectName("hboxlayout2")
        self.label_3 = QLabel(find_replace_dlg)
        self.label_3.setObjectName("label_3")
        self.hboxlayout2.addWidget(self.label_3)
        self.syntaxComboBox = QComboBox(find_replace_dlg)
        self.syntaxComboBox.setObjectName("syntaxComboBox")
        self.syntaxComboBox.addItem("")
        self.syntaxComboBox.addItem("")
        self.hboxlayout2.addWidget(self.syntaxComboBox)
        self.vboxlayout.addLayout(self.hboxlayout2)
        spacerItem = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vboxlayout.addItem(spacerItem)
        self.hboxlayout.addLayout(self.vboxlayout)
        self.line = QFrame(find_replace_dlg)
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)
        self.line.setObjectName("line")
        self.hboxlayout.addWidget(self.line)
        self.vboxlayout1 = QVBoxLayout()
        self.vboxlayout1.setMargin(0)
        self.vboxlayout1.setSpacing(6)
        self.vboxlayout1.setObjectName("vboxlayout1")
        self.findButton = QPushButton(find_replace_dlg)
        self.findButton.setObjectName("findButton")
        self.vboxlayout1.addWidget(self.findButton)
        self.replaceButton = QPushButton(find_replace_dlg)
        self.replaceButton.setObjectName("replaceButton")
        self.vboxlayout1.addWidget(self.replaceButton)
        self.replaceAllButton = QPushButton(find_replace_dlg)
        self.replaceAllButton.setObjectName("replaceAllButton")
        self.vboxlayout1.addWidget(self.replaceAllButton)
        spacerItem1 = QSpacerItem(20, 16, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.vboxlayout1.addItem(spacerItem1)
        self.closeButton = QPushButton(find_replace_dlg)
        self.closeButton.setObjectName("closeButton")
        self.vboxlayout1.addWidget(self.closeButton)
        self.hboxlayout.addLayout(self.vboxlayout1)
        self.label_2.setBuddy(self.replaceLineEdit)
        self.label.setBuddy(self.findLineEdit)
        self.label_3.setBuddy(self.syntaxComboBox)

        self.retranslateUi(find_replace_dlg)
        QObject.connect(self.closeButton, SIGNAL("clicked()"), find_replace_dlg.reject)
        QMetaObject.connectSlotsByName(find_replace_dlg)
        find_replace_dlg.setTabOrder(self.findLineEdit, self.replaceLineEdit)
        find_replace_dlg.setTabOrder(self.replaceLineEdit, self.caseCheckBox)
        find_replace_dlg.setTabOrder(self.caseCheckBox, self.wholeCheckBox)
        find_replace_dlg.setTabOrder(self.wholeCheckBox, self.syntaxComboBox)
        find_replace_dlg.setTabOrder(self.syntaxComboBox, self.findButton)
        find_replace_dlg.setTabOrder(self.findButton, self.replaceButton)
        find_replace_dlg.setTabOrder(self.replaceButton, self.replaceAllButton)
        find_replace_dlg.setTabOrder(self.replaceAllButton, self.closeButton)

    def retranslateUi(self, find_replace_dlg):
        find_replace_dlg.setWindowTitle(QApplication.translate("find_replace_dlg", "Find and Replace", None))
        self.label_2.setText(QApplication.translate("find_replace_dlg", "Replace w&ith:", None))
        self.label.setText(QApplication.translate("find_replace_dlg", "Find &what:", None))
        self.caseCheckBox.setText(QApplication.translate("find_replace_dlg", "&Case sensitive", None))
        self.wholeCheckBox.setText(QApplication.translate("find_replace_dlg", "Wh&ole words", None))
        self.label_3.setText(QApplication.translate("find_replace_dlg", "&Syntax:", None))
        self.syntaxComboBox.setItemText(0, QApplication.translate("find_replace_dlg", "Literal text", None))
        self.syntaxComboBox.setItemText(1, QApplication.translate("find_replace_dlg", "Regular expression", None))
        self.findButton.setText(QApplication.translate("find_replace_dlg", "&Find (next)", None))
        self.replaceButton.setText(QApplication.translate("find_replace_dlg", "&Replace", None))
        self.replaceAllButton.setText(QApplication.translate("find_replace_dlg", "Replace &All", None))
        self.closeButton.setText(QApplication.translate("find_replace_dlg", "Close", None))

    def on_findLineEdit_textEdited(self, text):
        self.found_list = deque([]);
        self.update_ui()

    def keyPressEvent(self, e):
      if (e.key() == Qt.Key_Return):
        self.on_findButton_clicked();
      QDialog.keyPressEvent(self, e);

    def makeRegex(self):
        findText = unicode(self.findLineEdit.text())
        if unicode(self.syntaxComboBox.currentText()) == "Literal":
            findText = re.escape(findText)
        flags = re.MULTILINE|re.DOTALL|re.UNICODE
        if not self.caseCheckBox.isChecked():
            flags |= re.IGNORECASE
        if self.wholeCheckBox.isChecked():
            findText = r"\b%s\b" % findText
        return re.compile(findText, flags)

    def find_in_subtree(self, regex, item):
      match=regex.search(item.linked_node.chunk());
      if(match!=None): self.found_list.append(item)
      for i in range(item.childCount()):
        self.find_in_subtree(regex,item.child(i));

    def on_findButton_clicked(self):
      item=self.gui.selected_item;
      if(len(self.found_list)):
        self.emit(SIGNAL("found(QTreeWidgetItem, int)"), self.found_list.popleft(), 0); return;
      else:
        regex = self.makeRegex()
        self.find_in_subtree(regex, item);
        if(len(self.found_list)==0):
          self.emit(SIGNAL("found(QTreeWidgetItem, int)"), None, 0); return;
        else: self.emit(SIGNAL("found(QTreeWidgetItem, int)"), self.found_list.popleft(), 0); return;

    def on_replaceButton_clicked(self):
        self.found_list=deque([]);
        item=self.gui.selected_item;
        regex = self.makeRegex()
        self.find_in_subtree(regex, item);
        if(len(self.found_list)):
          if(regex.search(self.found_list[0].linked_node.chunk())!=None):
            self.found_list[0].setText(0, regex.sub(unicode(self.replaceLineEdit.text()),
                                str(self.found_list[0].linked_node.anchor), 1));
          self.found_list[0].linked_node.text = regex.sub(unicode(self.replaceLineEdit.text()),
                                str(self.found_list[0].linked_node.text), 1)
          self.emit(SIGNAL("found(QTreeWidgetItem, int)"), self.found_list[0], 1); return;
        else: self.emit(SIGNAL("found(QTreeWidgetItem, int)"), None, 0); return;

    def on_replaceAllButton_clicked(self):
        self.found_list=deque([]);
        item=self.gui.selected_item;
        regex = self.makeRegex()
        self.find_in_subtree(regex, item);
        count=len(self.found_list)
        if(count):
          cur=None;
          while(len(self.found_list)):
            cur=self.found_list.popleft();
            if(regex.search(cur.linked_node.chunk())!=None):
              cur.setText(0, regex.sub(unicode(self.replaceLineEdit.text()),
                                str(cur.linked_node.anchor)))
            cur.linked_node.text = regex.sub(unicode(self.replaceLineEdit.text()),
                                str(cur.linked_node.text))
          self.emit(SIGNAL("found(QTreeWidgetItem, int)"), cur, count); return;
        else: self.emit(SIGNAL("found(QTreeWidgetItem, int)"), None, 0); return;

    def update_ui(self):
        enable = not self.findLineEdit.text().isEmpty()
        self.findButton.setEnabled(enable)
        self.replaceButton.setEnabled(enable)
        self.replaceAllButton.setEnabled(enable)



#-Paper View Widget------------------------------------------------------------
#-Widget for viewing paper
#------------------------------------------------------------------------------
class paper_view(QWidget):
  def __init__(self, gui):
    super(paper_view, self).__init__()
    self.gui=gui;
    self.is_paper_loaded=False; #used externally to check if paper valid
    self.pdf=None;
    self.cur_page=0;
    self.page=[];
    self.page_line_link=[];
    self.pview=QLabel();
    self.pview.setAlignment(Qt.AlignHCenter);
    self.setup();

  def setup(self):
#    self.resize(1000, 1000)
    self.setWindowTitle('Paper View')

    hbox=QHBoxLayout(self);
    hbox.addWidget(self.pview);
    self.setLayout(hbox);
    self.setWindowState((self.windowState() & ~(Qt.WindowMinimized | Qt.WindowFullScreen))
                           | Qt.WindowMaximized);

  def snag_page_line_links(self):
    fn=self.gui.gen_dir+self.gui.fnhead+".pdfsync";
    if(not os.path.isfile(fn)): self.page_line_link=[]; return;
    data=open(self.gui.gen_dir+self.gui.fnhead+".pdfsync", 'r').read();
    data=data.split('\n');
    last_line=0;
    self.page_line_link=[];
    for i in data:
      if(not list_check(i.split(),1)): continue;
      if(i.split()[0]=='l'):
        last_line=int(i.split()[2]);
      elif(i.split()[0]=='s'):
        self.page_line_link.append(last_line);
    self.page_line_link.append(last_line);

  def display_pdf(self):
    self.snag_page_line_links();
    fn=self.gui.out_dir+self.gui.fnhead+".pdf";
    if(not os.path.isfile(fn)): self.is_paper_loaded=False; return;
    self.pdf=Poppler.Document.load(fn);
    self.pdf.setRenderHint(Poppler.Document.Antialiasing);
    self.pdf.setRenderHint(Poppler.Document.TextAntialiasing);
    if(self.pdf==None): return; #TODO: Error output
    self.page=[];
    for i in range(self.pdf.numPages()):
      self.page.append(self.pdf.page(i));
    self.is_paper_loaded=True;
    self.show_cur_page();

  def get_cur_page(self):
    line=self.gui.selected_item.linked_node.state.start_line;
    if(line!=None or not len(self.page_line_link)):
      line+=1;
      self.cur_page=0;
      for i in self.page_line_link:
        if(line > i): self.cur_page+=1;
        else: break;
    if(self.cur_page>len(self.page)-1): self.cur_page=0;
    return self.page[self.cur_page];

  def show_cur_page(self, page_detection=True):
    if(self.cur_page>len(self.page)):print "Error: 1296, if(self.cur_page>len(self.page))",self.cur_page,len(self.page); return;
    cur_page=self.page[self.cur_page];
    if(page_detection): cur_page=self.get_cur_page();
    dpi=self.pview.height()/(cur_page.pageSizeF().height()/72)
    img=cur_page.renderToImage(dpi,dpi);
    if self.gui.view_grayscale:
      imgToGrayscale(img)
    pixmap = QPixmap.fromImage(img);
    self.pview.setPixmap(pixmap);
    self.pview.setMinimumHeight(2);
    self.setFixedWidth(img.width()+10);

  def resizeEvent(self, event):
    self.display_pdf();

  def keyPressEvent(self, e):
    if (e.key() == Qt.Key_Up and self.cur_page > 0):
      self.cur_page-=1;
      self.show_cur_page(False);
    elif (e.key() == Qt.Key_Down and self.cur_page < (self.pdf.numPages()-1)):
      self.cur_page+=1;
      self.show_cur_page(False);


#-GUI Worker Thread------------------------------------------------------------
#-The GUI worker thread to prevent gui locking
#------------------------------------------------------------------------------
class tex_gen_worker(QThread):
  def __init__(self, gui):
    QThread.__init__(self);
    self.gui=gui;
    self.job_queue=deque([]);
    self.last_gen=''; #the tex source of last generated paper

  def run(self):
    while(len(self.job_queue)):
      j=self.job_queue.popleft();
      if(j==0): self.update_tex(); #Update tex output view job
      elif(j==1): self.show_fig(); #Show figure of highlighted node
      elif(j==2): self.gen_paper(); #Generate paper
      elif(j==3): self.gen_figure(); #Generate current figure
      elif(j==4): self.show_paper(); #Generate show paper at location
      elif(j==5): self.update_tex_full(); #Show full tex (only if user not typing)
      self.msleep(500);

  def update_tex_full(self):
    start_line=self.gui.selected_item.linked_node.state.start_line; #start...
    end_line=self.gui.selected_item.linked_node.state.end_line; #...and end line for highlighting
    if(start_line==None): start_line=0;
    if(end_line==None): end_line=0;
    #Next is to grab text
    (out, self.gui.node_links)=self.gui.texout.render(self.gui.main_tree.topLevelItem(0).linked_node);
    out=out.split('\n');
    #Now we calculate the highlighting
    hl_begin=0; hl_end=0; char_count=0; line_count=0; ret='';
    for i in out:
      if(line_count==start_line):hl_begin=char_count;
      ret+=i+'\n';
      char_count+=len(i+'\n');
      line_count+=1;
      if(line_count<=end_line):hl_end=char_count;

    self.emit( SIGNAL('tex_update(QString, int, int)'), ret, hl_begin, hl_end)
    return;

  def update_tex(self):
    start_line=self.gui.selected_item.linked_node.state.start_line; #start...
    end_line=self.gui.selected_item.linked_node.state.end_line; #...and end line for highlighting
    font=QFontMetrics(self.gui.tex_view.font());
    view_length=round(self.gui.tex_view.height()/font.lineSpacing()) #this is in # of lines
    view_width=self.gui.tex_view.width(); #this is in pixels
    (out, self.gui.node_links)=self.gui.texout.render(self.gui.main_tree.topLevelItem(0).linked_node);
    self.gui.main_tree.topLevelItem(0).linked_node.update_all_line_stats();
    out=out.split('\n')
    #caculcate view are and highligting below (FIXME: Can be simpler)
    hl_start_line=0;
    hl_end_line=0;
    head=0; tail=len(out)-1;
    if(start_line!=None):
      half_length=(view_length/2);
      head=start_line;
      total_lines=0;
      while(half_length>0 and head>0):
        lines_covered=int(math.ceil(float(font.width(out[head]))/float(view_width)));
        if(lines_covered<1): lines_covered=1;
        half_length-=lines_covered;
        if(half_length<0): break;
        head-=1;
        total_lines+=lines_covered;
      half_length=(view_length/2);
      tail=start_line+1;
      while(total_lines<view_length and tail<(len(out)-1)):
        lines_covered=int(math.ceil(float(font.width(out[tail]))/float(view_width)));
        if(lines_covered<1): lines_covered=1;
        total_lines+=lines_covered;
        tail+=1;
      hl_start_line=start_line-head;
      hl_end_line=hl_start_line+(end_line-start_line);
#      if(start>0):start+=1; #weird bugfix that i dont get but works
    hl_begin=0; hl_end=0;
    ret=''; line_count=0; char_count=0;
    self.gui.tex_view.start_line=int(head);
    for i in out[int(head):int(tail)]:
      if(line_count==int(hl_start_line)):hl_begin=char_count;
#      ret+=str(int(head)+line_count)+'\t'+i+'\n';
      ret+=i+'\n';
      char_count+=len(i+'\n');
      line_count+=1;
      if(line_count<=int(hl_end_line)):hl_end=char_count;
    open_file=QFile(self.gui.filename+".bak");
    open_file.open(QIODevice.WriteOnly);
    for i in range(self.gui.main_tree.topLevelItemCount()):
      self.gui.main_tree.topLevelItem(i).linked_node.save(open_file);
    open_file.close();
    self.emit( SIGNAL('tex_update(QString, int, int)'), ret, hl_begin, hl_end)
    return;

  def show_fig(self):
    item=self.gui.selected_item;
#    display_width=self.gui.fig_view.width();
    display_height=self.gui.height()/3;
    fn=self.gui.gen_dir+item.linked_node.params[0]+".pdf";
    if(os.path.isfile(fn)): #send figure image only if pdf file exists
      pdf=Poppler.Document.load(fn);
      pdf.setRenderHint(Poppler.Document.Antialiasing);
      pdf.setRenderHint(Poppler.Document.TextAntialiasing);
      page=pdf.page(0);
      dpi=display_height/(page.pageSizeF().height()/72);
      img=page.renderToImage(dpi, dpi);
      if self.gui.view_grayscale:
        imgToGrayscale(img)
      self.emit( SIGNAL('fig_update(QImage)'), img )

  def show_paper(self):
    item=self.gui.selected_item;
    display_width=300#self.gui.fig_view.width()/1.5;
    if(self.gui.paper_view.is_paper_loaded):
      page=self.gui.paper_view.get_cur_page();
      dpi=display_width/(page.pageSizeF().width()/72);
      img=page.renderToImage(dpi, dpi);
    else:
      img=QImage();      

    if self.gui.view_grayscale:
      imgToGrayscale(img)

    self.emit( SIGNAL('fig_update(QImage)'), img )

  def gen_paper(self):
    (self.last_gen,dummy)=self.gui.texout.render(self.gui.main_tree.topLevelItem(0).linked_node);
    is_success=self.gui.texout.epic_generate(self.gui.main_tree.topLevelItem(0).linked_node);
    self.emit( SIGNAL('tex_gen_done(bool)'), is_success )

  def gen_figure(self):
    item=self.gui.selected_item;
    if(item.linked_node.ntype=="graph_figure"):
      is_success=self.gui.texout.gen_pygraph(item.linked_node);
      self.emit( SIGNAL('fig_gen_done(bool)'), is_success )
    elif(item.linked_node.ntype=="R_figure"):
      is_success=self.gui.texout.gen_Rgraph(item.linked_node);
      self.emit( SIGNAL('fig_gen_done(bool)'), is_success )
    elif(item.linked_node.ntype=="pdf_figure"):
      is_success=self.gui.texout.gen_pdf_fig(item.linked_node);
      self.emit( SIGNAL('fig_gen_done(bool)'), is_success )
    elif(item.linked_node.ntype=="pdf_table"):
      is_success=self.gui.texout.gen_pdf_fig(item.linked_node);
      self.emit( SIGNAL('fig_gen_done(bool)'), is_success )

  def begin(self, job_num):
    if(not job_num in self.job_queue):
      self.job_queue.append(job_num);
      self.start();

#-Help-------------------------------------------------------------------------
#-Help/Info--------------------------------------------------------------------
#------------------------------------------------------------------------------
class help_view(QWidget):
  def __init__(self):
    super(help_view, self).__init__()
    self.setup();

  def setup(self):
    self.setWindowTitle('Help')
    self.view=QTextEdit();
    self.view.setReadOnly(True);
    hbox=QHBoxLayout(self);
    hbox.addWidget(self.view);
    self.setLayout(hbox);

  def show_commands(self):
    text="Note: \"Ctrl\" is Command key on Mac\n\n\
Hierarchal Editor \n\n\
\tDelete Node\tBackspace\n\
\tCreate Child Node\tTab\n\
\tCreate Sibling Node\tShift+Enter\n\
\tExpand/Collapse All\tCtrl+E\n\
\tEdit Node\t\tEnter\n\
\tCopy\t\tCtrl+C\n\
\tCut\t\tCtrl+X\n\
\tPaste\t\tCtrl+V\n\
\tUndo\t\tCtrl+Z\n\
\tRedo\t\tCtrl+Shift+Z\n\n\
Realtime Tex Viewer\n\n\
\tGo to Node\t\tShift+Click\n\n\
Commands\n\n\
\tFind/Replace\tCtrl+F \n\t\t(Functional but finiky search and replace)\n\
\tRender Figure\tCtrl+R \n\t\t(to display inline figures for figure and pdf tags)\n\
\tGen Full Paper\tCtrl+G \n\t\t(Generate paper, saving does this automatically)\n\
\tWord Count\t\tCtrl+W \n\t\t(Display number of words in document)\n\
\tToggle Realtime Tex View\tCtrl+D \n\t\t(Disables the realtime tex if it slows things down)\n\
";
    self.view.setText(text);
    self.view.setStyleSheet("background-color:#F8F8F8");
    self.resize(550, 500)
    self.show();

  def show_tags(self):
    text="Below is the complete list of all tags and how to use them. The format is\n\
    \"tag\",[\"nicknames\",...]  - example\n\n\
    \"omit\" \t\t\t\t- $omit! - omits entire subtree\n\
    \"title\",[\"tit\"] \t\t\t- $title: snippet\n\
    \"head_tex\",[\"head\"] \t\t\t- $head: snippet\n\
    \"tail_tex\",[\"tail\"] \t\t\t- $tail: snippet\n\
    \"abstract\",[\"abs\",\"a\"] \t\t\t- $abs: snippet\n\
    \"abstract_paragraph\",[\"absp\",\"ap\"] \t\t- $absp: snippet\n\
    \"chapter\",[\"chap\",\"c\"] \t\t\t- $c: snippet\n\
    \"chapter*\",[\"chap*\",\"c*\"] \t\t- $c*: snippet\n\
    \"section\",[\"sec\",\"s\"] \t\t\t- $s: snippet\n\
    \"section*\",[\"sec*\",\"s*\"] \t\t\t- $s*: snippet\n\
    \"paragraph\",[\"par\",\"p\"] \t\t- $p: snippet\n\
    \"text\",[\"t\"] \t\t\t- $t:snippet\n\
    \"cmt_on\" \t\t\t\t- $TODO: obsolete\n\
    \"cmt_off\" \t\t\t\t- $TODO: obsolete\n\
    \"comment\",[\"cmt\", \"gcmt\"] \t\t- $cmt: snippet\n\
    \"redcomment\",[\"rcmt\"] \t\t- $rcmt: snippet\n\
    \"yellowcomment\",[\"ycmt\"] \t\t- $ycmt: snippet\n\
    \"bluecomment\",[\"bcmt\"] \t\t- $bcmt: snippet\n\
    \"list\",[\"l\"] \t\t\t\t- $l:type (i.e., l:enumerate)\n\
    \"item\",[\"li\",\"i\"] \t\t\t- $li: snippet\n\
    \"bibtex\",[\"bib\"] \t\t\t- $bib: snippet\n\
    \"class_file\",[\"class\"] \t\t\t- $class:name \\ snippet\n\
    \"style_file\",[\"style\",\"sty\"] \t\t- $style:name \\ snippet\n\
    \"dump_file\",[\"output\",\"create\",\"embed\",\"dump\"] \t- $dump:name \\ snippet\n\
    \"bib_style\",[] \t\t\t- $bib_style: snippet\n\
    \"inline_code\",[\"icode\",\"code\"] \t\t- $code: snippet\n\
    \"inline_table\",[\"inline_tab\",\"itab\"] \t\t- $itab:l:l:c:... snippet\n\
    \"figure\",[\"fig\",\"f\"] \t\t\t- $fig:name,sz:name,sz:nl:name,sz: (sz is width/column, nl is new line)\n\
    \"figure*\",[\"fig*\",\"f*\"] \t\t\t- $fig*:name,sz:name,sz:\n\
    \"sub_figure\",[\"subfigure\",\"sub_fig\",\"sf\"] \t- $sub_fig:name:caption:name,sz:...\n\
    \"sub_figure*\",[\"subfigure\",\"sub_fig*\",\"sf*\"] \t- $sub_fig:name:caption:name,sz:...\n\
    \"code_figure\",[\"code_fig\"] \t\t- $code:name:caption \\ snippet\n\
    \"table_figure\",[\"table_fig\",\"tab_fig\"] \t\t- $tab_fig:name:caption:l:l:c:...\n\
    \"table\",[\"tab\"] \t\t\t- $tab:name:caption:l:l:c:...\n\
    \"table*\",[\"tab*\"] \t\t\t- $tab*:name:caption:l:l:c:...\n\
    \"pdf_figure\",[\"pdf_fig\",\"pdf\"] \t\t- $pdf_fig:name:caption\n\
    \"graph_figure\",[\"graph_fig\",\"graph\"] \t- $graph:name:caption \\ snippet\n\
    \"R_figure\",[\"R_fig\"] \t\t\t- $graph:name:caption \\snippet\n";
    self.view.setText(text);
    self.view.setStyleSheet("background-color:#F8F8F8");
    self.resize(750, 800)
    self.show();

#-Main GUI Window--------------------------------------------------------------
#-Main GUI Window with views for everything
#------------------------------------------------------------------------------
class main_view(QMainWindow):
  def __init__(self, filename='untitled.fws',out_dir='out', gen_dir='gen',
               pdf_dir='figs'):
    super(main_view, self).__init__();
    self.filename=filename; #Name of current file being edited
    self.fnhead=filename.split('.')[0].split('/')[len(filename.split('.')[0].split('/'))-2];
    self.out_dname=out_dir;
    self.gen_dname=gen_dir;
    self.pdf_dname=pdf_dir;
    self.out_dir=out_dir+'/';
    self.gen_dir=out_dir+'/'+gen_dir+'/';
    self.pdf_dir=pdf_dir+'/';
    self.node_links=None; #for pointers from text file back to nodes
    self.texout=epic_gen(self); #source file generator
    self.selected_item=None; #currently selected tree_item
    self.realtime_disabled=False; #User option for enabling/disabling realtime
    self.init_ui(filename); #Initialize UI

  def init_ui(self, filename='untitled.fws'):
    self.resize(1000,1000);
    self.center();
    self.statusBar().showMessage('Ready');
    self.setWindowTitle('Fancy Paper Writing System from Mars');

    self.setup_main_ui();
    self.init_menu();
    self.work_thread=tex_gen_worker(self);
    self.setup_events();
    self.setup_default_paper();
    self.show();
    self.raise_()
    # Load file if it is not "untitled.fws"
    if filename != 'untitled.fws':
      self.load_file(filename)


  def setup_main_ui(self):
    self.find_replace_dlg=find_replace_dlg(self);
#    self.paper_view=paper_view(self);
    self.help_view=help_view();

    self.main_tree=tree_widget(self);
    self.main_tree.setHeaderLabels(["Awesome Paper"]);
#    self.main_tree.setDragDropMode(QAbstractItemView.InternalMove)
    self.main_text=fancy_text();
    self.main_text_timer=QTimer();

    self.tex_view=fancy_text(self, False);
    self.tex_view.setReadOnly(True);
    self.tex_view.setStyleSheet("background-color:#F8F8F8");
    self.tex_view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff);
#    self.tex_view.setTextInteractionFlags(Qt.NoTextInteraction);
    self.tex_view.mouse_clicked.connect(self.on_text_clicked);
    self.fig_view=QLabel();
    self.fig_view.setAlignment(Qt.AlignHCenter);
    self.fig_view.setMinimumWidth(2);


    self.main_split=QSplitter(Qt.Vertical);
    self.main_split.addWidget(self.group_it(self.main_tree,"Main Outliner"));
#    self.main_split.addWidget(self.group_it(self.fig_view,"Figure Viewer"));
    self.spec_split=QSplitter(Qt.Vertical);
#    self.spec_split.addWidget(self.group_it(self.main_text,"Node Editor"));
    self.spec_split.addWidget(self.group_it(self.main_text,"Node Editor", 150));
    self.spec_split.addWidget(self.group_it(self.tex_view,"Realtime Tex Viewer"));
    self.spec_split.addWidget(self.fig_view);

    self.view_split=QSplitter(Qt.Horizontal);
    self.view_split.addWidget(self.main_split);
    self.view_split.addWidget(self.spec_split);
    self.setCentralWidget(self.view_split);

#    self.main_split.setStretchFactor(0,4);
#    self.main_split.setStretchFactor(1,1);
    self.spec_split.setStretchFactor(0,1);
    self.spec_split.setStretchFactor(1,6);
    self.view_split.setStretchFactor(0,6);
    self.view_split.setStretchFactor(1,6);

  def group_it(self, widget, name, fixed_height=None):
    groupbox=QGroupBox(name);
    layout=QVBoxLayout();
    layout.addWidget(widget);
    groupbox.setLayout(layout);
    if(fixed_height!=None): groupbox.setMinimumHeight(fixed_height);
    return groupbox;

  def setup_events(self):
    self.connect(self.main_tree, SIGNAL("currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)"), self.cur_item_change);
    self.connect(self.main_text, SIGNAL("textChanged()"), self.on_text_change);
#    self.connect(self.tex_view, SIGNAL("cursorPositionChanged()"), self.on_text_clicked);
    self.connect( self.find_replace_dlg, SIGNAL("found(QTreeWidgetItem, int)"), self.search_done);
    self.connect( self.work_thread, SIGNAL("tex_update(QString, int, int)"), self.texview_updated);
    self.connect( self.work_thread, SIGNAL("fig_update(QImage)"), self.show_fig);
    self.connect( self.work_thread, SIGNAL("tex_gen_done(bool)"), self.tex_gen_done);
    self.connect( self.work_thread, SIGNAL("fig_gen_done(bool)"), self.fig_gen_done);
    self.connect( self.main_text_timer, SIGNAL("timeout()"), self.test);

  def on_text_clicked(self, pos):
     cursor = self.tex_view.cursorForPosition(pos)
     line = self.tex_view.start_line+str(self.tex_view.toPlainText().toAscii())[:cursor.position()].count('\n');
     self.main_tree.setCurrentItem(self.node_links[line].linked_gui_item);

  def texview_updated(self, tex, begin, end):
    self.tex_view.blockSignals(True);
    self.tex_view.setText(tex);
    self.tex_view.highlight_region(begin,end);
    self.tex_view.verticalScrollBar().setValue(begin/(len(tex.split('\n')))*QFontMetrics(self.tex_view.font()).lineSpacing());
    self.tex_view.blockSignals(False);

  def search_done(self, item, count):
    if(item==None):
      self.statusBar().showMessage('Text not found!');
    else:
      self.main_tree.setCurrentItem(item);
      item.setExpanded(True);
      if(count):
        self.statusBar().showMessage('Text found! '+str(count)+' replacments made!');
      else:
        self.statusBar().showMessage('Text found!');

  def fig_gen_done(self, is_successful):
    if(is_successful):
      self.statusBar().showMessage('Generating figure...success!!');
      self.work_thread.begin(1);
    else:
      self.statusBar().showMessage('Generating figure...failed!!');
      self.tex_view.setText(nice_error_log("generating figure", self.texout.work_log, self.texout.failure));
      self.texout.work_log=''; #clears out worklog, only needed for gen_pygraph

  def tex_gen_done(self, is_successful):
    if(is_successful):
      self.statusBar().showMessage('Generating paper...success!!');
#      self.paper_view.display_pdf();
#      self.paper_view.show();
#      self.work_thread.begin(4);
#      self.tex_view.setText("Successful paper build:\n"+self.texout.work_log);
    else:
      self.statusBar().showMessage('Generating paper...failed!!');
      self.tex_view.setText(nice_error_log("generating paper", self.texout.work_log, self.texout.failure));

  def show_fig(self, img):
    if(img.isNull()):
      self.fig_view.clear(); self.fig_view.hide();
    else:
      pixmap = QPixmap.fromImage(img);
      self.fig_view.setPixmap(pixmap);
      self.fig_view.setMaximumHeight(img.height());
      self.fig_view.show();

  def cur_item_change(self, item_now, item_before): #User clicks node in tree view
    if(item_now==None): return;
    self.selected_item=item_now;
#    self.tex_view.blockSignals(True);
    self.main_text.blockSignals(True);
    self.main_text.setText(str(item_now.linked_node.text));
    self.main_text.blockSignals(False);
    self.update_tex_output();
    if(item_now.linked_node.ntype=="graph_figure" or item_now.linked_node.ntype=="pdf_figure" or item_now.linked_node.ntype=="pdf_table"):
      self.main_text.py_highlighter.setDocument(self.main_text.document()); #use python highliting
#      self.tex_view.parent().hide();
      self.work_thread.begin(1);
    else:
      self.main_text.tex_highlighter.setDocument(self.main_text.document());
      self.tex_view.parent().show();
#      self.work_thread.begin(4);
#    if(self.paper_view.is_paper_loaded):
#      self.paper_view.show_cur_page();
#    self.tex_view.blockSignals(False);

  def on_text_change(self): #User is editing the contents of the node editor
    if(self.selected_item==None): return;
    self.selected_item.linked_node.text=str(self.main_text.toPlainText().toAscii());
    self.selected_item.linked_node.is_rendered=0; #for rendering only on change
    self.selected_item.linked_node.process_type(); #done eagerly item by item, is also done in render for whole tree
    self.update_tex_output();

  def setup_default_paper(self):
    basic_doc="$head: \\documentclass[letterpaper, twosided, twocolumn]{article}";
    prereqs="$head: \\usepackage{epsfig}\n\\usepackage{color}\n\\usepackage{graphicx}\n"+\
            "\\usepackage{listings}\n\\usepackage{pdfsync}\n\\usepackage{caption}\n\\usepackage{subcaption}\n"+\
            "\\usepackage{todonotes}\n\\newcommand{\\redcomment}[1]{\\todo[inline,color=red!40,size=\\small]{#1}}\n"+\
            "\\newcommand{\\greencomment}[1]{\\todo[inline,color=green!40,size=\\small]{#1}}\n"+\
            "\\newcommand{\\bluecomment}[1]{\\todo[inline,color=blue!40,size=\\small]{#1}}\n"+\
            "\\newcommand{\\yellowcomment}[1]{\\todo[inline,color=yellow!40,size=\\small]{#1}}\n"+\
            "\\lstset{language=C++, basicstyle=\\footnotesize}";
    recommend="$head: \\usepackage{balance}\n\\usepackage{cite}"
    startdoc="$head: \\begin{document}\n";
    tail="$tail: \\bibliographystyle{abbrv}\n%\\singlespacing\n%\\scriptsize\n"+\
         "\\bibliography{bib}\n\\end{document}";

    starter=tree_item(self.main_tree,"Your Next Top Tier Paper", "$title: The Point of Existence: Uncovering the God Formula");
    starter.addChild(tree_item(self.main_tree,"Main Sections"));
    starter.child(0).addChild(tree_item(self.main_tree,"Introduction","$s:Introduction"));
    starter.child(0).child(0).addChild(tree_item(self.main_tree,"Some Text","$p:Some introductory text~\cite{}."));
    starter.addChild(tree_item(self.main_tree,"Latex config"));
    starter.child(1).addChild(tree_item(self.main_tree,"Classfile", "You can add your custom classfile here."));
    starter.child(1).addChild(tree_item(self.main_tree,"Head"));
    starter.child(1).child(1).addChild(tree_item(self.main_tree,"Doc class",basic_doc));
    starter.child(1).child(1).addChild(tree_item(self.main_tree,"Prereqs",prereqs));
    starter.child(1).child(1).addChild(tree_item(self.main_tree,"Recommended",recommend));
    starter.child(1).child(1).addChild(tree_item(self.main_tree,"Your stuff"));
    starter.child(1).child(1).addChild(tree_item(self.main_tree,"Start doc",startdoc));
    starter.child(1).addChild(tree_item(self.main_tree,"Tail"));
    starter.child(1).child(2).addChild(tree_item(self.main_tree,"Standard Tail",tail));
    self.main_tree.addTopLevelItem(starter);
    self.main_tree.setCurrentItem(self.main_tree.topLevelItem(0));
    self.main_tree.topLevelItem(0).setExpanded(True);

  def test(self): #Main call for realtime tex viewing
    if(self.selected_item==None or #self.selected_item.linked_node.ntype==None or
       self.selected_item.linked_node.ntype=='omit' or
       self.realtime_disabled #Has the user disabled realtime tex view
       ): self.tex_view.setText("");return; #dont highlight if omitted or None
    self.work_thread.begin(5);

  def update_tex_output(self): #Main call for realtime tex viewing
    if(self.selected_item==None or #self.selected_item.linked_node.ntype==None or
       self.realtime_disabled #Has the user disabled realtime tex view
       ): self.tex_view.setText(""); return; #dont highlight if omitted or None
    if(not self.selected_item.linked_node.ntype=='omit'): self.work_thread.begin(0);
    self.selected_item.depth_to_color();
    self.main_text_timer.setInterval(3000); #Full latex render timer, work in progress
    self.main_text_timer.setSingleShot(True);
#    self.main_text_timer.start();


  def init_menu(self):
    self.view_grayscale = False

    self.statusBar().showMessage("Bring it on!");
    menubar = self.menuBar()
    fileMenu = menubar.addMenu('&File')
    Action = QAction('&New', self)
    Action.setShortcut('Ctrl+N')
    Action.setStatusTip('Create New Paper')
    Action.triggered.connect(self.new_event)
    fileMenu.addAction(Action)
    Action = QAction('&Open', self)
    Action.setShortcut('Ctrl+O')
    Action.setStatusTip('Load Paper')
    Action.triggered.connect(self.load_event)
    fileMenu.addAction(Action)
    Action = QAction('&Save', self)
    Action.setShortcut('Ctrl+S')
    Action.setStatusTip('Save Paper')
    Action.triggered.connect(self.save_event)
    fileMenu.addAction(Action)
    Action = QAction('&Close', self)
    Action.setShortcut('Ctrl+Q')
    Action.setStatusTip('Exit application')
    Action.triggered.connect(self.closeEvent)
    fileMenu.addAction(Action)

    actionMenu = menubar.addMenu('&Action')
    Action = QAction('&Find/Replace', self)
    Action.setShortcut('Ctrl+F')
    Action.setStatusTip('Search and replace text in entire tree')
    Action.triggered.connect(self.find_event)
    actionMenu.addAction(Action)
    Action = QAction('&Expand/Collapse All', self)
    Action.setShortcut('Ctrl+E')
    Action.setStatusTip('Toggles expand or collapse on selected item')
    Action.triggered.connect(self.expand_event)
    actionMenu.addAction(Action)
    Action = QAction('&Render Figure', self)
    Action.setShortcut('Ctrl+R')
    Action.setStatusTip('Generate selected figure')
    Action.triggered.connect(self.fig_gen_event)
    actionMenu.addAction(Action)
    Action = QAction('&Gen Full Paper', self)
    Action.setShortcut('Ctrl+G')
    Action.setStatusTip('Generate full paper')
    Action.triggered.connect(self.tex_gen_event)
    actionMenu.addAction(Action)
    Action = QAction('&Word Count', self)
    Action.setShortcut('Ctrl+W')
    Action.setStatusTip('Do a word count')
    Action.triggered.connect(self.word_count_event)
    actionMenu.addAction(Action) 
    Action = QAction('&Disable/Enable Realtime View', self)
    Action.setShortcut('Ctrl+D')
    Action.setStatusTip('Disable/Enable the realtime tex viewer')
    Action.triggered.connect(self.disable_realtime_event)
    actionMenu.addAction(Action) 

    viewMenu = menubar.addMenu('&View')
    Action = QAction('&Grayscale', self, checkable=True)
    Action.setShortcut('Ctrl+M')
    Action.setStatusTip('View paper/images in grayscale')
    Action.triggered.connect(self.grayscale_event)
    viewMenu.addAction(Action)
    
    helpMenu = menubar.addMenu('&Help')
    Action = QAction('&Show Commands', self)
    Action.setShortcut('F1')
    Action.setStatusTip('Show FPWSM commands')
    Action.triggered.connect(self.help_view.show_commands)
    helpMenu.addAction(Action)
    Action = QAction('&Show Tags', self)
    Action.setShortcut('F2')
    Action.setStatusTip('Show FPWSM tags')
    Action.triggered.connect(self.help_view.show_tags)
    helpMenu.addAction(Action)

  def find_event(self, event):
    self.find_replace_dlg.show();
    self.find_replace_dlg.activateWindow();

  def expand_event(self, event):
    if(self.selected_item.isExpanded()):
      self.selected_item.expand_all(False);
    else: self.selected_item.expand_all(True);

  def tex_gen_event(self, event):
    self.statusBar().showMessage('Generating paper... (please wait)');
    self.work_thread.begin(2);

  def word_count_event(self, event):
    texfile = self.gen_dir + '/' + self.fnhead + '.tex'
    if not os.path.isfile(texfile):
      tex_gen_event(self, event)

    ret, log = get_word_count(self.gen_dir + '/' + self.fnhead+".tex")
      
    extr = ''
    if ret != 0:
      extr = 'Error collecting word count\n\n' 
    self.tex_view.setText(extr+log)
  
  def disable_realtime_event(self, event):
    self.realtime_disabled=not self.realtime_disabled;

  def grayscale_event(self, event):
    self.view_grayscale = not self.view_grayscale
    self.tex_gen_event(event)

  def fig_gen_event(self, event):
    ntype=self.selected_item.linked_node.ntype;
    if(ntype=="graph_figure" or ntype=="R_figure" or ntype=="pdf_figure" or ntype=="pdf_table"):
      self.statusBar().showMessage('Generating figure... (please wait)');
      self.work_thread.begin(3);
    else:
      self.statusBar().showMessage('Not a valid type for generation...');

  def closeEvent(self, event):
    reply = QMessageBox.question(self, 'Save File',
      "Would you like to save?", QMessageBox.Yes |
      QMessageBox.No);
    if(reply == QMessageBox.Yes):
      self.save_event(None);
    self.find_replace_dlg.close();
#    self.paper_view.close();
    self.help_view.close();

  def save_event(self, event):
    if(self.filename=='untitled.fws'):
      self.filename=QFileDialog.getSaveFileName(self,
         "Save File", self.filename, "Paper files (*.fws)")
      if(self.filename.isEmpty()): self.filename='untitled.fws';
    self.filename=str(self.filename);
    self.fnhead=self.filename.split('.')[0].split('/')[len(self.filename.split('.')[0].split('/'))-1];
    self.out_dir=os.path.dirname(self.filename)+'/'+self.out_dname+'/';
    self.gen_dir=os.path.dirname(self.filename)+'/'+self.out_dname+'/'+self.gen_dname+'/';
    self.pdf_dir=os.path.dirname(self.filename)+'/'+self.pdf_dname+'/';
    open_file=QFile(self.filename);
    open_file.open(QIODevice.WriteOnly);
    for i in range(self.main_tree.topLevelItemCount()):
      self.main_tree.topLevelItem(i).linked_node.save(open_file);
    open_file.close();
    if(self.work_thread.last_gen!=self.texout.render(self.main_tree.topLevelItem(0).linked_node)):
      self.statusBar().showMessage('Generating paper... (please wait)');
      self.work_thread.begin(2);

  def load_event(self, event):
    self.filename = QFileDialog.getOpenFileName(self,
        "Open File", ".",
        "Paper files (*.fws)");
    if(self.filename.isEmpty()): self.filename="untitled.fws"; return;
    else:
      self.filename=str(self.filename);
      self.load_file(self.filename);

  def load_file(self, fn):
    self.fnhead=self.filename.split('.')[0].split('/')[len(self.filename.split('.')[0].split('/'))-1];
    self.out_dir=os.path.dirname(self.filename)+'/'+self.out_dname+'/';
    self.gen_dir=os.path.dirname(self.filename)+'/'+self.out_dname+'/'+self.gen_dname+'/';
    self.pdf_dir=os.path.dirname(self.filename)+'/'+self.pdf_dname+'/';
    open_file=open(fn, "r");
    self.main_tree.clear();
#    self.paper_view.is_paper_loaded=False;
    self.main_tree.addTopLevelItem(self.load_tree(open_file));
    self.main_tree.topLevelItem(0).setExpanded(True);
    self.selected_item=self.main_tree.topLevelItem(0);
    self.main_text.setText(self.selected_item.linked_node.text);
    self.update_tex_output();
    open_file.close();

  def new_event(self, event):
    self.closeEvent(None); #Ask if user wants to save
    self.filename = "untitled.fws"
    self.main_tree.clear();
    self.setup_default_paper();
#    self.paper_view.is_paper_loaded=False;

  def load_tree(self, infile):
    item=tree_item(self.main_tree,pickle.load(infile));
    item.linked_node.text=pickle.load(infile);
    num_children=pickle.load(infile);
    for i in range(int(num_children)):
      item.addChild(self.load_tree(infile));
    return item;

  def center(self):
    qr = self.frameGeometry()
    cp = QDesktopWidget().availableGeometry().center()
    qr.moveCenter(cp)
    self.move(qr.topLeft())

def main():
  parser = argparse.ArgumentParser(description='Argument Parser.')
  # Load file from terminal
  parser.add_argument("-l", "--load-file", type=str, help="*.fws file to load")
  # TODO
  # Whether just compile the paper from terminal without starting the GUI
  """
  parser.add_argument("-c", "--compile-only", action="store_true",
                      help="whether to compile the paper only without starting the GUI")
  """

  # Argument list
  args = parser.parse_args()
  arg_load_file = args.load_file

  # Check if the required file exists
  if not arg_load_file or not os.path.isfile(arg_load_file):
    arg_load_file = "untitled.fws"
  else:
    arg_load_file = os.path.abspath(arg_load_file)

  app = QApplication(sys.argv);
  view = main_view(filename=arg_load_file)
  sys.exit(app.exec_());

main();
