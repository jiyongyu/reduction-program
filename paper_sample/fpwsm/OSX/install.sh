#!/usr/bin/env bash

## get the directory this script is in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
UPDIR=${DIR}/..
ESCDIR=`echo ${UPDIR} | sed 's:/:\\\/:g'`


## set up application to refer to this directory
echo "Setting up application..."
DOCROOT=${DIR}/Mars_FPWSM.app/Contents/document.wflow
sed "s/@AWAITING_FPWSM_INSTALLER@/${ESCDIR}/g" ${DOCROOT}.template > ${DOCROOT}


## associate fws file types with this application
echo "Associating file extention .fws with application..."
EXT_APP="fws"
PATH_APP="com.apple.automator.mars_fpwsm"

defaults read com.apple.LaunchServices LSHandlers | grep ${EXT_APP} > /dev/null
r=${?}

if [ "${r}" != 0 ]; then
    defaults write com.apple.LaunchServices \
	LSHandlers -array-add \
	"<dict>
    <key>LSHandlerContentTag</key>
    <string>$EXT_APP</string>
    <key>LSHandlerContentTagClass</key>
    <string>public.filename-extension</string>
    <key>LSHandlerRoleAll</key>
    <string>$PATH_APP</string>
  </dict>"

    /System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -seed -kill -r -domain local -domain system -domain user
else
    echo -e "\tFile type .fws already associated"
fi


## install application to dock
echo "Installing application to dock..."
PATH_APP="${DIR}/Mars_FPWSM.app"
NAME_APP="Mars_FPWSM"

defaults write com.apple.dock \
  persistent-apps -array-add \
  "<dict>
    <key>tile-data</key>
    <dict>
      <key>file-data</key>
      <dict>
        <key>_CFURLString</key>
        <string>$PATH_APP</string>
        <key>_CFURLStringType</key>
        <integer>0</integer>
      </dict>
      <key>file-label</key>
      <string>$NAME_APP</string>
      <key>file-type</key>
      <integer>41</integer>
    </dict>
    <key>tile-type</key>
    <string>file-tile</string>
  </dict>"

echo "Restarting dock..."
killall Dock


