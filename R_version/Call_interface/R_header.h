dyn.load("/home/jiyongyu/CGO/reduction-program/R_version/Call_interface/doubleArrSum.so")

mysum <- function(vec){
    if (!is.vector(vec)){
        stop("vec must be a vector")
        sum(vec)
    }
    else if (!is.double(vec)){
        vec <- as.double(vec)
        .Call("doubleArrSum", vec)
    }else{
        .Call("doubleArrSum", vec)
    }
}
