dyn.load("doubleArrSum.so")

# add wrapper
vecSum <- function(vec){
    if (!is.vector(vec))
        stop("vec must be a vector")
    if (!is.double(vec))
        vec <- as.double(vec)
    .Call("doubleArrSum", vec)
}

array_size_list1 <- c(0x10, 0x20, 0x40, 0x80, 0xc0)
array_size_list2 <- c(0x100, 0x200, 0x400, 0x800, 0xc00)
array_size_list3 <- c(0x1000, 0x2000, 0x4000, 0x8000, 0xc000)
array_size_list4 <- c(0x10000, 0x20000, 0x40000, 0x80000, 0xc0000)
array_size_list5 <- c(0x100000, 0x200000, 0x400000, 0x800000, 0xc00000)

array_size_list <- c(array_size_list1, array_size_list2, array_size_list3, array_size_list4, array_size_list5)
#array_size_list <- c(0x10)

built_in_result <- array_size_list
built_in_time <- array_size_list
FMA_result <- array_size_list
FMA_time <- array_size_list

correct <- vector(length = length(array_size_list))
speedup1 <- vector(length = length(array_size_list))
speedup2 <- vector(length = length(array_size_list))

const <- 0x40000000
#const <- 0x100

for (j in 1:length(array_size_list)){
    
    v <- seq(0,array_size_list[j],by=0.0625)
    iter <- const / array_size_list[j]

    # timing the built-in sum function
    passed_sum <- 0.0
    ptm <- proc.time()
    for (i in 1:iter){
        passed_sum <- passed_sum + sum(v)
    }
    ptm <- proc.time() - ptm
        
    t <- as.data.frame(as.list(ptm))
    built_in_time[j] <- t$elapsed
    built_in_result[j] <- passed_sum

    # timing the user-defined sum function
    passed_sum <- 0.0
    ptm <- proc.time()
    for (i in 1:iter){
        passed_sum <- passed_sum + vecSum(v)
    }
    ptm <- proc.time() - ptm

    t <- as.data.frame(as.list(ptm))
    FMA_time[j] <- t$elapsed
    FMA_result[j] <- passed_sum

    # timing the for loop
    #passed_sum <- 0.0
    #ptm <- proc.time()
    #for (i in 1:iter){
        #for (k in 1:length(v)){
            #passed_sum <- passed_sum + v[k]
        #}
    #}
    #ptm <- proc.time() - ptm
    
    #t <- as.data.frame(as.list(ptm))
    #for_time[j] <- t$elapsed
    #for_result[j] <- passed_sum

    correct[j] <- (built_in_result[j] == FMA_result[j])
    speedup1[j] <- (built_in_time[j] / FMA_time[j])
    #speedup2[j] <- (for_time[j] / FMA_time[j])
}

measured_data <- data.frame(
    array_length  = array_size_list,
    array_size_KB = (array_size_list / 8),
    array_size_MB = (array_size_list / 8 / 1024),
    built_in_time = built_in_time,
    FMA_time      = FMA_time,
    #for_time      = for_time,
    built_in_result = built_in_result,
    FMA_result      = FMA_result,
   #for_result      = for_result,
    Correctness = correct,
    Speed_up1   = speedup1
    #Speed_up2   = speedup2
)

write.csv(measured_data, "output.csv")
