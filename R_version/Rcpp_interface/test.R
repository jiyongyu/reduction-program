library(FMAstyle)

iter = 100000

v <- seq(0,5000, by=0.1)
empty_v <- vector(mode="numeric", length=0)

# timing the build-in sum
built_in_sum <- 0.0
zero_sum <- 0.0

start <- proc.time()
for (i in 1:iter){
    built_in_sum <- built_in_sum + sum(v)
}
reg_end <- proc.time()
for (i in 1:iter){
    zero_sum <- sum(empty_v)
}
empty_end <- proc.time()
built_in_time <- (reg_end - start) - (empty_end - reg_end)

# timing the add sum
add_sum <- 0.0
zero_sum <- 0.0

start <- proc.time()
for (i in 1:iter){
    add_sum <- add_sum + arrADDSum(v)
}
reg_end <- proc.time()
for (i in 1:iter){
    zero_sum <- arrADDSum(empty_v)
}
empty_end <- proc.time()
add_time <- (reg_end - start) - (empty_end - reg_end)

# timing the fma sum
fma_sum <- 0.0
zero_sum <- 0.0

start <- proc.time()
for (i in 1:iter){
    fma_sum <- fma_sum + arrFMASum(v)
}
reg_end <- proc.time()
for (i in 1:iter){
    zero_sum <- arrFMASum(empty_v)
}
empty_end <- proc.time()
fma_time <- (reg_end - start) - (empty_end - reg_end)

# print info
print ("built-in info")
print (built_in_time)
print (built_in_sum)

print ("addSum info")
print (add_time)
print (add_sum)

print ("fmaSum info")
print (fma_time)
print (fma_sum)
