#!/usr/bin/env python
import os
import subprocess

compFile = '/home/jiyongyu/CGO/reduction-program/R_version/Rcpp_interface/FMAstyle/compileAttr.R'

# sometimes doesn't work
# if the RcppExports.cpp cannot be generated, do this manually
f = open(compFile, 'w')
f.write('Rcpp::compileAttributes(verbose=TRUE)')
f.close()

package_name = "FMAstyle"
version_postfix = "_1.0.tar.gz"
subprocess.call("Rscript " + compFile, shell=True)
subprocess.call("R CMD build " + package_name, shell=True)
subprocess.call("R CMD check " + package_name + version_postfix, shell=True)

os.remove(compFile)
