#include <immintrin.h>
#include <stdio.h>
#include <Rcpp.h>
#include <cassert>

using namespace Rcpp;

/****** core code ******/
double _FMA_sum(double *arr, int array_len);
double _ADD_sum(double *arr, int array_len);

/****** export functions ******/
double arrFMASum(NumericVector& arr);
double arrADDSum(NumericVector& arr);
NumericVector vecFMASum(List& list_of_vec, int n_vec, int n_ele);
NumericVector vecADDSum(List& list_of_vec, int n_vec, int n_ele);
//NumericMatrix matListSum(List list_of_mat, int num_list, int n_row, int n_col);

/****** Definitions ******/
// [[Rcpp::export]]
double arrFMASum(NumericVector& arr){

    int arr_len = arr.size();
    volatile double returned_sum = 0;    
    returned_sum = _FMA_sum(&arr[0], arr_len);

    return returned_sum;
}

// [[Rcpp::export]]
double arrADDSum(NumericVector& arr){

    int arr_len = arr.size();
    volatile double returned_sum = 0;
    returned_sum = _ADD_sum(&arr[0], arr_len);

    return returned_sum;
}

// [[Rcpp::export]]
NumericVector vecFMASum(List& list_of_vec, int n_vec, int n_ele){ 
   
    NumericVector ret_vec(n_ele);
    double* vec_addr[n_vec];
    
    for(int i=0; i<n_vec; i++){
        vec_addr[i] = &((as<NumericVector>(list_of_vec[i]))[0]);
        printf("vec_addr[%d] from 0x%p\n", i, (void*) vec_addr[i]);
        printf("vec_addr[%d]  to  0x%p\n", i, (void*) (vec_addr[i]+n_ele));
    }
    //for(int i=0; i<n_vec; i++){
        //printf("======== Sample %d ========\n", i+1);
        //for(int j=0; j<n_ele/10; j++){
            //for(int k=0; k<10; k++){
                //printf("%f ", vec_addr[i][j*10+k]);
            //}
            //printf("\n");
        //}
        //printf ("\n");
    //}    
    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    __m256d vec_sum0, vec_sum1, vec_sum2, vec_sum3, vec_sum4,
            vec_sum5, vec_sum6, vec_sum7, vec_sum8, vec_sum9,
            vec_sum10, vec_sum11, vec_sum12, vec_sum13, vec_sum14;

    __m256d constReg = _mm256_set_pd(1.0, 1.0, 1.0, 1.0);

    int tmp;
   
    for(int i=0; i<n_ele; i+=60){ 
        vec_sum0 = _mm256_setzero_pd();
        vec_sum1 = _mm256_setzero_pd();
        vec_sum2 = _mm256_setzero_pd();
        vec_sum3 = _mm256_setzero_pd();
        vec_sum4 = _mm256_setzero_pd();
        vec_sum5 = _mm256_setzero_pd();
        vec_sum6 = _mm256_setzero_pd();
        vec_sum7 = _mm256_setzero_pd();
        vec_sum8 = _mm256_setzero_pd();
        vec_sum9 = _mm256_setzero_pd();
        vec_sum10 = _mm256_setzero_pd();
        vec_sum11 = _mm256_setzero_pd();
        vec_sum12 = _mm256_setzero_pd();
        vec_sum13 = _mm256_setzero_pd();
        vec_sum14 = _mm256_setzero_pd();

        for(int j=0; j<n_vec; j+=1){
            
            a0  = _mm256_load_pd(vec_addr[j]+i+4*0);
            a1  = _mm256_load_pd(vec_addr[j]+i+4*1);
            a2  = _mm256_load_pd(vec_addr[j]+i+4*2);
            a3  = _mm256_load_pd(vec_addr[j]+i+4*3);
            a4  = _mm256_load_pd(vec_addr[j]+i+4*4);
            a5  = _mm256_load_pd(vec_addr[j]+i+4*5);
            a6  = _mm256_load_pd(vec_addr[j]+i+4*6);
            a7  = _mm256_load_pd(vec_addr[j]+i+4*7);
            a8  = _mm256_load_pd(vec_addr[j]+i+4*8);
            a9  = _mm256_load_pd(vec_addr[j]+i+4*9);
            a10 = _mm256_load_pd(vec_addr[j]+i+4*10);
            a11 = _mm256_load_pd(vec_addr[j]+i+4*11);
            a12 = _mm256_load_pd(vec_addr[j]+i+4*12);
            a13 = _mm256_load_pd(vec_addr[j]+i+4*13);
            a14 = _mm256_load_pd(vec_addr[j]+i+4*14);
        
            vec_sum0 = _mm256_fmadd_pd(a0, constReg, vec_sum0);
            vec_sum1 = _mm256_fmadd_pd(a1, constReg, vec_sum1);
            vec_sum2 = _mm256_fmadd_pd(a2, constReg, vec_sum2);
            vec_sum3 = _mm256_fmadd_pd(a3, constReg, vec_sum3);
            vec_sum4 = _mm256_fmadd_pd(a4, constReg, vec_sum4);
            vec_sum5 = _mm256_fmadd_pd(a5, constReg, vec_sum5);
            vec_sum6 = _mm256_fmadd_pd(a6, constReg, vec_sum6);
            vec_sum7 = _mm256_fmadd_pd(a7, constReg, vec_sum7);
            vec_sum8 = _mm256_fmadd_pd(a8, constReg, vec_sum8);
            vec_sum9 = _mm256_fmadd_pd(a9, constReg, vec_sum9);
            vec_sum10 = _mm256_fmadd_pd(a10, constReg, vec_sum10);
            vec_sum11 = _mm256_fmadd_pd(a11, constReg, vec_sum11);
            vec_sum12 = _mm256_fmadd_pd(a12, constReg, vec_sum12);
            vec_sum13 = _mm256_fmadd_pd(a13, constReg, vec_sum13);
            vec_sum14 = _mm256_fmadd_pd(a14, constReg, vec_sum14);
        }
        ret_vec[i + 0] = ((double*)&vec_sum0)[0];
        ret_vec[i + 1] = ((double*)&vec_sum0)[1];
        ret_vec[i + 2] = ((double*)&vec_sum0)[2];
        ret_vec[i + 3] = ((double*)&vec_sum0)[3];
        ret_vec[i + 4] = ((double*)&vec_sum1)[0];
        ret_vec[i + 5] = ((double*)&vec_sum1)[1];
        ret_vec[i + 6] = ((double*)&vec_sum1)[2];
        ret_vec[i + 7] = ((double*)&vec_sum1)[3];
        ret_vec[i + 8] = ((double*)&vec_sum2)[0];
        ret_vec[i + 9] = ((double*)&vec_sum2)[1];
        ret_vec[i +10] = ((double*)&vec_sum2)[2];
        ret_vec[i +11] = ((double*)&vec_sum2)[3];
        ret_vec[i +12] = ((double*)&vec_sum3)[0];
        ret_vec[i +13] = ((double*)&vec_sum3)[1];
        ret_vec[i +14] = ((double*)&vec_sum3)[2];
        ret_vec[i +15] = ((double*)&vec_sum3)[3];
        ret_vec[i +16] = ((double*)&vec_sum4)[0];
        ret_vec[i +17] = ((double*)&vec_sum4)[1];
        ret_vec[i +18] = ((double*)&vec_sum4)[2];
        ret_vec[i +19] = ((double*)&vec_sum4)[3];
        ret_vec[i +20] = ((double*)&vec_sum5)[0];
        ret_vec[i +21] = ((double*)&vec_sum5)[1];
        ret_vec[i +22] = ((double*)&vec_sum5)[2];
        ret_vec[i +23] = ((double*)&vec_sum5)[3];
        ret_vec[i +24] = ((double*)&vec_sum6)[0];
        ret_vec[i +25] = ((double*)&vec_sum6)[1];
        ret_vec[i +26] = ((double*)&vec_sum6)[2];
        ret_vec[i +27] = ((double*)&vec_sum6)[3];
        ret_vec[i +28] = ((double*)&vec_sum7)[0];
        ret_vec[i +29] = ((double*)&vec_sum7)[1];
        ret_vec[i +30] = ((double*)&vec_sum7)[2];
        ret_vec[i +31] = ((double*)&vec_sum7)[3];
        ret_vec[i +32] = ((double*)&vec_sum8)[0];
        ret_vec[i +33] = ((double*)&vec_sum8)[1];
        ret_vec[i +34] = ((double*)&vec_sum8)[2];
        ret_vec[i +35] = ((double*)&vec_sum8)[3];
        ret_vec[i +36] = ((double*)&vec_sum9)[0];
        ret_vec[i +37] = ((double*)&vec_sum9)[1];
        ret_vec[i +38] = ((double*)&vec_sum9)[2];
        ret_vec[i +39] = ((double*)&vec_sum9)[3];
        ret_vec[i +40] = ((double*)&vec_sum10)[0];
        ret_vec[i +41] = ((double*)&vec_sum10)[1];
        ret_vec[i +42] = ((double*)&vec_sum10)[2];
        ret_vec[i +43] = ((double*)&vec_sum10)[3];
        ret_vec[i +44] = ((double*)&vec_sum11)[0];
        ret_vec[i +45] = ((double*)&vec_sum11)[1];
        ret_vec[i +46] = ((double*)&vec_sum11)[2];
        ret_vec[i +47] = ((double*)&vec_sum11)[3];
        ret_vec[i +48] = ((double*)&vec_sum12)[0];
        ret_vec[i +49] = ((double*)&vec_sum12)[1];
        ret_vec[i +50] = ((double*)&vec_sum12)[2];
        ret_vec[i +51] = ((double*)&vec_sum12)[3];
        ret_vec[i +52] = ((double*)&vec_sum13)[0];
        ret_vec[i +53] = ((double*)&vec_sum13)[1];
        ret_vec[i +54] = ((double*)&vec_sum13)[2];
        ret_vec[i +55] = ((double*)&vec_sum13)[3];
        ret_vec[i +56] = ((double*)&vec_sum14)[0];
        ret_vec[i +57] = ((double*)&vec_sum14)[1];
        ret_vec[i +58] = ((double*)&vec_sum14)[2];
        ret_vec[i +59] = ((double*)&vec_sum14)[3];
            
    }

    return ret_vec;
}

// [[Rcpp::export]]
NumericVector vecADDSum(List& list_of_vec, int n_vec, int n_ele){ 
   
    NumericVector ret_vec(n_ele);
    double* vec_addr[n_vec];
    
    for(int i=0; i<n_vec; i++){
        vec_addr[i] = &((as<NumericVector>(list_of_vec[i]))[0]);
    }
    //for(int i=0; i<n_vec; i++){
        //printf("======== Sample %d ========\n", i+1);
        //for(int j=0; j<n_ele/10; j++){
            //for(int k=0; k<10; k++){
                //printf("%f ", vec_addr[i][j*10+k]);
            //}
            //printf("\n");
        //}
        //printf ("\n");
    //}    
    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    __m256d vec_sum0, vec_sum1, vec_sum2, vec_sum3, vec_sum4,
            vec_sum5, vec_sum6, vec_sum7, vec_sum8, vec_sum9,
            vec_sum10, vec_sum11, vec_sum12, vec_sum13, vec_sum14;

    int tmp;
   
    for(int i=0; i<n_ele; i+=60){ 
        vec_sum0 = _mm256_setzero_pd();
        vec_sum1 = _mm256_setzero_pd();
        vec_sum2 = _mm256_setzero_pd();
        vec_sum3 = _mm256_setzero_pd();
        vec_sum4 = _mm256_setzero_pd();
        vec_sum5 = _mm256_setzero_pd();
        vec_sum6 = _mm256_setzero_pd();
        vec_sum7 = _mm256_setzero_pd();
        vec_sum8 = _mm256_setzero_pd();
        vec_sum9 = _mm256_setzero_pd();
        vec_sum10 = _mm256_setzero_pd();
        vec_sum11 = _mm256_setzero_pd();
        vec_sum12 = _mm256_setzero_pd();
        vec_sum13 = _mm256_setzero_pd();
        vec_sum14 = _mm256_setzero_pd();

        for(int j=0; j<n_vec; j+=1){
            
            a0  = _mm256_load_pd(vec_addr[j]+i+4*0);
            a1  = _mm256_load_pd(vec_addr[j]+i+4*1);
            a2  = _mm256_load_pd(vec_addr[j]+i+4*2);
            a3  = _mm256_load_pd(vec_addr[j]+i+4*3);
            a4  = _mm256_load_pd(vec_addr[j]+i+4*4);
            a5  = _mm256_load_pd(vec_addr[j]+i+4*5);
            a6  = _mm256_load_pd(vec_addr[j]+i+4*6);
            a7  = _mm256_load_pd(vec_addr[j]+i+4*7);
            a8  = _mm256_load_pd(vec_addr[j]+i+4*8);
            a9  = _mm256_load_pd(vec_addr[j]+i+4*9);
            a10 = _mm256_load_pd(vec_addr[j]+i+4*10);
            a11 = _mm256_load_pd(vec_addr[j]+i+4*11);
            a12 = _mm256_load_pd(vec_addr[j]+i+4*12);
            a13 = _mm256_load_pd(vec_addr[j]+i+4*13);
            a14 = _mm256_load_pd(vec_addr[j]+i+4*14);
        
            vec_sum0  = _mm256_add_pd(a0, vec_sum0);
            vec_sum1  = _mm256_add_pd(a1, vec_sum1);
            vec_sum2  = _mm256_add_pd(a2, vec_sum2);
            vec_sum3  = _mm256_add_pd(a3, vec_sum3);
            vec_sum4  = _mm256_add_pd(a4, vec_sum4);
            vec_sum5  = _mm256_add_pd(a5, vec_sum5);
            vec_sum6  = _mm256_add_pd(a6, vec_sum6);
            vec_sum7  = _mm256_add_pd(a7, vec_sum7);
            vec_sum8  = _mm256_add_pd(a8, vec_sum8);
            vec_sum9  = _mm256_add_pd(a9, vec_sum9);
            vec_sum10 = _mm256_add_pd(a10, vec_sum10);
            vec_sum11 = _mm256_add_pd(a11, vec_sum11);
            vec_sum12 = _mm256_add_pd(a12, vec_sum12);
            vec_sum13 = _mm256_add_pd(a13, vec_sum13);
            vec_sum14 = _mm256_add_pd(a14, vec_sum14);
        }
        ret_vec[i + 0] = ((double*)&vec_sum0)[0];
        ret_vec[i + 1] = ((double*)&vec_sum0)[1];
        ret_vec[i + 2] = ((double*)&vec_sum0)[2];
        ret_vec[i + 3] = ((double*)&vec_sum0)[3];
        ret_vec[i + 4] = ((double*)&vec_sum1)[0];
        ret_vec[i + 5] = ((double*)&vec_sum1)[1];
        ret_vec[i + 6] = ((double*)&vec_sum1)[2];
        ret_vec[i + 7] = ((double*)&vec_sum1)[3];
        ret_vec[i + 8] = ((double*)&vec_sum2)[0];
        ret_vec[i + 9] = ((double*)&vec_sum2)[1];
        ret_vec[i +10] = ((double*)&vec_sum2)[2];
        ret_vec[i +11] = ((double*)&vec_sum2)[3];
        ret_vec[i +12] = ((double*)&vec_sum3)[0];
        ret_vec[i +13] = ((double*)&vec_sum3)[1];
        ret_vec[i +14] = ((double*)&vec_sum3)[2];
        ret_vec[i +15] = ((double*)&vec_sum3)[3];
        ret_vec[i +16] = ((double*)&vec_sum4)[0];
        ret_vec[i +17] = ((double*)&vec_sum4)[1];
        ret_vec[i +18] = ((double*)&vec_sum4)[2];
        ret_vec[i +19] = ((double*)&vec_sum4)[3];
        ret_vec[i +20] = ((double*)&vec_sum5)[0];
        ret_vec[i +21] = ((double*)&vec_sum5)[1];
        ret_vec[i +22] = ((double*)&vec_sum5)[2];
        ret_vec[i +23] = ((double*)&vec_sum5)[3];
        ret_vec[i +24] = ((double*)&vec_sum6)[0];
        ret_vec[i +25] = ((double*)&vec_sum6)[1];
        ret_vec[i +26] = ((double*)&vec_sum6)[2];
        ret_vec[i +27] = ((double*)&vec_sum6)[3];
        ret_vec[i +28] = ((double*)&vec_sum7)[0];
        ret_vec[i +29] = ((double*)&vec_sum7)[1];
        ret_vec[i +30] = ((double*)&vec_sum7)[2];
        ret_vec[i +31] = ((double*)&vec_sum7)[3];
        ret_vec[i +32] = ((double*)&vec_sum8)[0];
        ret_vec[i +33] = ((double*)&vec_sum8)[1];
        ret_vec[i +34] = ((double*)&vec_sum8)[2];
        ret_vec[i +35] = ((double*)&vec_sum8)[3];
        ret_vec[i +36] = ((double*)&vec_sum9)[0];
        ret_vec[i +37] = ((double*)&vec_sum9)[1];
        ret_vec[i +38] = ((double*)&vec_sum9)[2];
        ret_vec[i +39] = ((double*)&vec_sum9)[3];
        ret_vec[i +40] = ((double*)&vec_sum10)[0];
        ret_vec[i +41] = ((double*)&vec_sum10)[1];
        ret_vec[i +42] = ((double*)&vec_sum10)[2];
        ret_vec[i +43] = ((double*)&vec_sum10)[3];
        ret_vec[i +44] = ((double*)&vec_sum11)[0];
        ret_vec[i +45] = ((double*)&vec_sum11)[1];
        ret_vec[i +46] = ((double*)&vec_sum11)[2];
        ret_vec[i +47] = ((double*)&vec_sum11)[3];
        ret_vec[i +48] = ((double*)&vec_sum12)[0];
        ret_vec[i +49] = ((double*)&vec_sum12)[1];
        ret_vec[i +50] = ((double*)&vec_sum12)[2];
        ret_vec[i +51] = ((double*)&vec_sum12)[3];
        ret_vec[i +52] = ((double*)&vec_sum13)[0];
        ret_vec[i +53] = ((double*)&vec_sum13)[1];
        ret_vec[i +54] = ((double*)&vec_sum13)[2];
        ret_vec[i +55] = ((double*)&vec_sum13)[3];
        ret_vec[i +56] = ((double*)&vec_sum14)[0];
        ret_vec[i +57] = ((double*)&vec_sum14)[1];
        ret_vec[i +58] = ((double*)&vec_sum14)[2];
        ret_vec[i +59] = ((double*)&vec_sum14)[3];
            
    }

    return ret_vec;
}

//// [[Rcpp::export]]
//NumericMatrix matListSum(List list_of_mat, int num_list, int n_row, int n_col){
    //// list of vectors
    //NumericMatrix ret_mat(n_row, n_col);
    ////Numeric* mat_addr[num_list];

    ////// get the address of matrices
    ////for(int i=0; i<n; i++){
        ////mat_addr[i] = & as<NumericMatrix> (list[i])
    ////}
    //printf("start\n");
    //NumericMatrix* ret_mat_p = (NumericMatrix*) & (as<NumericMatrix> (list_of_mat[0]));
    //for(int i=0; i<n_row; i++){
        //for(int j=0; i<n_col; j++){
            //printf("%f ",(*ret_mat_p)(i,j));
        //}
        //printf("\n");
    //}
    //return ret_mat;
//}
double _FMA_sum(double *arr, int array_len){
    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d constReg = _mm256_set_pd(1.0, 1.0, 1.0, 1.0);
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = array_len - array_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_fmadd_pd(a0, constReg, vec_sum0);
        vec_sum1 = _mm256_fmadd_pd(a1, constReg, vec_sum1);
        vec_sum2 = _mm256_fmadd_pd(a2, constReg, vec_sum2);
        vec_sum3 = _mm256_fmadd_pd(a3, constReg, vec_sum3);
        vec_sum4 = _mm256_fmadd_pd(a4, constReg, vec_sum4);
        vec_sum5 = _mm256_fmadd_pd(a5, constReg, vec_sum5);
        vec_sum6 = _mm256_fmadd_pd(a6, constReg, vec_sum6);
        vec_sum7 = _mm256_fmadd_pd(a7, constReg, vec_sum7);
        vec_sum8 = _mm256_fmadd_pd(a8, constReg, vec_sum8);
        vec_sum9 = _mm256_fmadd_pd(a9, constReg, vec_sum9);
        vec_sum10 = _mm256_fmadd_pd(a10, constReg, vec_sum10);
        vec_sum11 = _mm256_fmadd_pd(a11, constReg, vec_sum11);
        vec_sum12 = _mm256_fmadd_pd(a12, constReg, vec_sum12);
        vec_sum13 = _mm256_fmadd_pd(a13, constReg, vec_sum13);
        vec_sum14 = _mm256_fmadd_pd(a14, constReg, vec_sum14);
    }

    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum1);
    vec_sum2 = _mm256_add_pd(vec_sum2, vec_sum3);
    vec_sum4 = _mm256_add_pd(vec_sum4, vec_sum5);
    vec_sum6 = _mm256_add_pd(vec_sum6, vec_sum7);
    vec_sum8 = _mm256_add_pd(vec_sum8, vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum2);
    vec_sum4 = _mm256_add_pd(vec_sum4, vec_sum6);
    vec_sum8 = _mm256_add_pd(vec_sum8, vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum4);
    vec_sum8 = _mm256_add_pd(vec_sum8, vec_sum12);
    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
   
    for(; i<array_len; i++){
       sum += arr[i];
    }

    return sum;
}

double _ADD_sum(double *arr, int array_len){
    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d constReg = _mm256_set_pd(1.0, 1.0, 1.0, 1.0);
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = array_len - array_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_add_pd(a0, vec_sum0);
        vec_sum1 = _mm256_add_pd(a1, vec_sum1);
        vec_sum2 = _mm256_add_pd(a2, vec_sum2);
        vec_sum3 = _mm256_add_pd(a3, vec_sum3);
        vec_sum4 = _mm256_add_pd(a4, vec_sum4);
        vec_sum5 = _mm256_add_pd(a5, vec_sum5);
        vec_sum6 = _mm256_add_pd(a6, vec_sum6);
        vec_sum7 = _mm256_add_pd(a7, vec_sum7);
        vec_sum8 = _mm256_add_pd(a8, vec_sum8);
        vec_sum9 = _mm256_add_pd(a9, vec_sum9);
        vec_sum10 = _mm256_add_pd(a10, vec_sum10);
        vec_sum11 = _mm256_add_pd(a11, vec_sum11);
        vec_sum12 = _mm256_add_pd(a12, vec_sum12);
        vec_sum13 = _mm256_add_pd(a13, vec_sum13);
        vec_sum14 = _mm256_add_pd(a14, vec_sum14);
    }

    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum1);
    vec_sum2 = _mm256_add_pd(vec_sum2, vec_sum3);
    vec_sum4 = _mm256_add_pd(vec_sum4, vec_sum5);
    vec_sum6 = _mm256_add_pd(vec_sum6, vec_sum7);
    vec_sum8 = _mm256_add_pd(vec_sum8, vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum2);
    vec_sum4 = _mm256_add_pd(vec_sum4, vec_sum6);
    vec_sum8 = _mm256_add_pd(vec_sum8, vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum4);
    vec_sum8 = _mm256_add_pd(vec_sum8, vec_sum12);
    vec_sum0 = _mm256_add_pd(vec_sum0, vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
   
    for(; i<array_len; i++){
       sum += arr[i];
    }

    return sum;
}
