#!/usr/bin/env python

import os.path
import subprocess

DIR_OF_THIS_SCRIPT = os.path.abspath(os.path.dirname(__file__))

R_SCRIPT = DIR_OF_THIS_SCRIPT + '/test_FMA_sum.R'

subprocess.check_call(['Rscript', R_SCRIPT])
