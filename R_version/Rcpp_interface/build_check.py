#!/usr/bin/env python
import subprocess

package_name = "FMAstyle"
version_postfix = "_1.0.tar.gz"
subprocess.call("R CMD build ../" + package_name, shell=True)
subprocess.call("R CMD check ../" + package_name + version_postfix, shell=True)
