#!/usr/bin/env python

import os.path
import subprocess

HEX = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f']
SIZE_UNITS = ['B', 'KB', 'MB', 'GB']
DIR_OF_PERF_PROGS = "/home/jiyongyu/CGO/perf_progs/" 
DIR_OF_TEST_PROGS = "/home/jiyongyu/CGO/reduction-program/test_arr_size/"

PERF_PROGRAM = "IPC.out"
TEST_PROGRAMS = ["omp_add_f", "fma_f", "add_f"]

FLOAT_ARR_LEN  = [  0x1000, 0x2000, 0x4000, 0x6000, 0x8000, 0xa000, 0xc000, \
                    0x10000, 0x20000, 0x30000, 0x40000, \
                    0x50000, 0x60000, 0x70000, 0x80000, \
                    0x90000, 0x98000, 0xa0000, 0xa8000, \
                    0xb0000, 0xb8000, 0xc0000, 0xc8000, \
                    0xd0000, 0xd8000, 0xe0000, 0xe8000, \
                    0xf0000, 0xf8000, \
                    0x100000, 0x110000, 0x120000, 0x130000, 0x140000, \
                    0x140000, 0x160000, 0x180000, \
                    0x1a0000, 0x1c0000, 0x1e0000, \
                    0x200000, 0x280000, 0x300000, 0x380000, \
                    0x400000, 0x500000, 0x600000, 0x700000, 0x800000, \
                    0x900000, 0xa00000, 0xb00000, 0xc00000, \
                    0xd00000, 0xe00000, 0xf00000, \
                    0x1000000, 0x2000000, 0x4000000, 0x8000000, 0xc000000,\
                    0x10000000]

EXE_REPEAT = 2

GXX = "g++"
OPTIONS = [ "-O3",
            "-mavx2",
            "-mfma",
            "-fopenmp",
            "-fabi-version=0" ]

def createFile(dest, add_IPC, add_runtime, fma_IPC, fma_runtime):
    f = open(dest, 'w')
    f.write("arr_len\tarr_len(hex)\tarr_size\tadd_IPC\tfma_IPC\tadd_time\tfma_time\n")
    for i in range (0, len(FLOAT_ARR_LEN)):
        size = float(FLOAT_ARR_LEN[i] * 4.0) # bytes
        unit = 0; # byte
        while size >= 1024:
            size = size / 1024
            unit = unit + 1
        string = str(FLOAT_ARR_LEN[i]) + '\t' \
                    + str(hex(FLOAT_ARR_LEN[i])) + '\t' \
                    + str(size)+SIZE_UNITS[unit] + '\t' \
                    + str(add_IPC[i]) + '\t' \
                    + str(fma_IPC[i]) + '\t' \
                    + str(add_runtime[i]) + '\t' \
                    + str(fma_runtime[i]) + '\n' 
        f.write(string)
    f.close()

def main():

    add_IPC = []
    fma_IPC = []
    add_runtime = []
    fma_runtime = []
    
    for prog in TEST_PROGRAMS :

        perf_prog = os.path.join(DIR_OF_PERF_PROGS, PERF_PROGRAM)
        test_prog = os.path.join(DIR_OF_TEST_PROGS, prog)

        src_file = test_prog + ".cpp"
        subprocess.check_call([GXX] + OPTIONS + [src_file, "-o", test_prog])
         
        for arr_len in FLOAT_ARR_LEN:
            IPC_list = [0.0] * EXE_REPEAT
            TIME_list = [0.0] * EXE_REPEAT
            for i in range(0, EXE_REPEAT):
                perf_output = subprocess.check_output([perf_prog, test_prog, str(arr_len)])
                print perf_output
                TIME_list[i] = float(perf_output.split("\n")[0].split(" ")[-2])
                IPC_list[i] = float(perf_output.split("\n")[-2].split(" ")[-1])
            TIME = sum(TIME_list) / EXE_REPEAT
            IPC = sum(IPC_list) / EXE_REPEAT
            print "run " + prog + " with arr_len = 0x%X" %arr_len + ", IPC = " + str(IPC) + " time = " + str(TIME) + "seconds"
            if prog == "add_f":
                add_IPC.append(IPC)
                add_runtime.append(TIME)
            else:
                fma_IPC.append(IPC)
                fma_runtime.append(TIME)
    
    reportFile = DIR_OF_TEST_PROGS + "report.txt"
    createFile(reportFile, add_IPC, add_runtime, fma_IPC, fma_runtime)

if __name__ == "__main__":
    main()

         

