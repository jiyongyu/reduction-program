	.file	"add_d.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.globl	_Z9sumDoublePdi
	.type	_Z9sumDoublePdi, @function
_Z9sumDoublePdi:
.LFB5700:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	subl	$16, %esi
	vxorpd	%xmm14, %xmm14, %xmm14
	testl	%esi, %esi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x78,0x6
	vmovapd	%ymm14, %ymm0
	vmovapd	%ymm14, %ymm13
	vmovapd	%ymm14, %ymm8
	vmovapd	%ymm14, %ymm12
	vmovapd	%ymm14, %ymm5
	vmovapd	%ymm14, %ymm7
	vmovapd	%ymm14, %ymm11
	vmovapd	%ymm14, %ymm1
	vmovapd	%ymm14, %ymm3
	vmovapd	%ymm14, %ymm6
	vmovapd	%ymm14, %ymm2
	vmovapd	%ymm14, %ymm4
	vmovapd	%ymm14, %ymm10
	vmovapd	%ymm14, %ymm9
	jle	.L2
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	addl	$60, %eax
	vaddpd	(%rdi), %ymm14, %ymm14
	addq	$480, %rdi
	vaddpd	-448(%rdi), %ymm0, %ymm0
	vaddpd	-416(%rdi), %ymm13, %ymm13
	vaddpd	-384(%rdi), %ymm8, %ymm8
	vaddpd	-352(%rdi), %ymm12, %ymm12
	vaddpd	-320(%rdi), %ymm5, %ymm5
	vaddpd	-288(%rdi), %ymm7, %ymm7
	vaddpd	-256(%rdi), %ymm11, %ymm11
	vaddpd	-224(%rdi), %ymm1, %ymm1
	vaddpd	-192(%rdi), %ymm3, %ymm3
	vaddpd	-160(%rdi), %ymm6, %ymm6
	vaddpd	-128(%rdi), %ymm2, %ymm2
	vaddpd	-96(%rdi), %ymm4, %ymm4
	vaddpd	-64(%rdi), %ymm10, %ymm10
	vaddpd	-32(%rdi), %ymm9, %ymm9
	cmpl	%esi, %eax
	jl	.L3
.L2:
	vaddpd	%ymm3, %ymm1, %ymm3
	vaddpd	%ymm0, %ymm14, %ymm0
	vaddpd	%ymm8, %ymm13, %ymm8
	vaddpd	%ymm5, %ymm12, %ymm5
	vaddpd	%ymm11, %ymm7, %ymm7
	vaddpd	%ymm2, %ymm6, %ymm2
	vaddpd	%ymm10, %ymm4, %ymm4
	vaddpd	%ymm8, %ymm0, %ymm0
	vaddpd	%ymm7, %ymm5, %ymm7
	vaddpd	%ymm2, %ymm3, %ymm2
	vaddpd	%ymm9, %ymm4, %ymm1
	vaddpd	%ymm7, %ymm0, %ymm7
	vxorpd	%xmm0, %xmm0, %xmm0
	vaddpd	%ymm1, %ymm2, %ymm1
	vaddpd	%ymm1, %ymm7, %ymm1
	vmovupd	%ymm1, -48(%rbp)
	vaddsd	-48(%rbp), %xmm0, %xmm0
	vaddsd	-40(%rbp), %xmm0, %xmm0
	vaddsd	-32(%rbp), %xmm0, %xmm0
	vaddsd	-24(%rbp), %xmm0, %xmm0
	vzeroupper
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5700:
	.size	_Z9sumDoublePdi, .-_Z9sumDoublePdi
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%f\n"
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup,"ax",@progbits
.LHOTB5:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5701:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movslq	%edi, %rdi
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movl	$10, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	movq	-8(%rsi,%rdi,8), %rdi
	xorl	%esi, %esi
	call	strtol
	movslq	%eax, %rcx
	movq	%rax, %r14
	movl	%eax, %r13d
	movabsq	$68719476736, %rax
	movq	$-1, %rdi
	cqto
	idivq	%rcx
	movabsq	$1143914305352105984, %rdx
	cmpq	%rdx, %rcx
	movq	%rax, %r12
	leaq	0(,%rcx,8), %rax
	cmovbe	%rax, %rdi
	xorl	%ebx, %ebx
	call	_Znam
	movq	%rax, %rbp
	call	clock
	testq	%r12, %r12
	movq	%rax, %r15
	jle	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r13d, %esi
	movq	%rbp, %rdi
	addq	$1, %rbx
	call	_Z9sumDoublePdi
	vmovsd	%xmm0, 24(%rsp)
	cmpq	%r12, %rbx
	vmovsd	24(%rsp), %xmm1
	vmovsd	16(%rsp), %xmm0
	vaddsd	%xmm1, %xmm0, %xmm0
	vmovsd	%xmm0, 16(%rsp)
	jne	.L14
.L11:
	call	clock
	vxorps	%xmm2, %xmm2, %xmm2
	subq	%r15, %rax
	vmovsd	16(%rsp), %xmm1
	movq	%rax, %rdx
	movl	%r14d, %ecx
	movl	$.LC3, %esi
	movl	$1, %edi
	vcvtsi2ssq	%rax, %xmm2, %xmm2
	vdivss	.LC2(%rip), %xmm2, %xmm2
	movl	$2, %eax
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vmovapd	%xmm2, %xmm0
	vmovsd	%xmm2, 8(%rsp)
	call	__printf_chk
	vmovsd	8(%rsp), %xmm2
	movl	$.LC4, %esi
	movl	$1, %edi
	movl	$1, %eax
	vmovapd	%xmm2, %xmm0
	call	__printf_chk
	movq	%rbp, %rdi
	call	_ZdaPv
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5701:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup
.LHOTB6:
	.p2align 4,,15
	.type	_GLOBAL__sub_I__Z9sumDoublePdi, @function
_GLOBAL__sub_I__Z9sumDoublePdi:
.LFB5764:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5764:
	.size	_GLOBAL__sub_I__Z9sumDoublePdi, .-_GLOBAL__sub_I__Z9sumDoublePdi
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z9sumDoublePdi
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1232348160
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
