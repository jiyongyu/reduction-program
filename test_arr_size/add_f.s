	.file	"add_f.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.globl	_Z8sumFloatPfi
	.type	_Z8sumFloatPfi, @function
_Z8sumFloatPfi:
.LFB5700:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	subl	$16, %esi
	vxorps	%xmm14, %xmm14, %xmm14
	testl	%esi, %esi
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x78,0x6
	vmovaps	%ymm14, %ymm0
	vmovaps	%ymm14, %ymm13
	vmovaps	%ymm14, %ymm8
	vmovaps	%ymm14, %ymm12
	vmovaps	%ymm14, %ymm5
	vmovaps	%ymm14, %ymm7
	vmovaps	%ymm14, %ymm11
	vmovaps	%ymm14, %ymm1
	vmovaps	%ymm14, %ymm3
	vmovaps	%ymm14, %ymm6
	vmovaps	%ymm14, %ymm2
	vmovaps	%ymm14, %ymm4
	vmovaps	%ymm14, %ymm10
	vmovaps	%ymm14, %ymm9
	jle	.L2
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	addl	$120, %eax
	vaddps	(%rdi), %ymm14, %ymm14
	addq	$480, %rdi
	vaddps	-448(%rdi), %ymm0, %ymm0
	vaddps	-416(%rdi), %ymm13, %ymm13
	vaddps	-384(%rdi), %ymm8, %ymm8
	vaddps	-352(%rdi), %ymm12, %ymm12
	vaddps	-320(%rdi), %ymm5, %ymm5
	vaddps	-288(%rdi), %ymm7, %ymm7
	vaddps	-256(%rdi), %ymm11, %ymm11
	vaddps	-224(%rdi), %ymm1, %ymm1
	vaddps	-192(%rdi), %ymm3, %ymm3
	vaddps	-160(%rdi), %ymm6, %ymm6
	vaddps	-128(%rdi), %ymm2, %ymm2
	vaddps	-96(%rdi), %ymm4, %ymm4
	vaddps	-64(%rdi), %ymm10, %ymm10
	vaddps	-32(%rdi), %ymm9, %ymm9
	cmpl	%esi, %eax
	jl	.L3
.L2:
	vaddps	%ymm3, %ymm1, %ymm3
	vaddps	%ymm0, %ymm14, %ymm0
	vaddps	%ymm8, %ymm13, %ymm8
	vaddps	%ymm5, %ymm12, %ymm5
	vaddps	%ymm11, %ymm7, %ymm7
	vaddps	%ymm2, %ymm6, %ymm2
	vaddps	%ymm10, %ymm4, %ymm4
	vaddps	%ymm8, %ymm0, %ymm0
	vaddps	%ymm7, %ymm5, %ymm7
	vaddps	%ymm2, %ymm3, %ymm2
	vaddps	%ymm9, %ymm4, %ymm1
	vaddps	%ymm7, %ymm0, %ymm7
	vxorps	%xmm0, %xmm0, %xmm0
	vaddps	%ymm1, %ymm2, %ymm1
	vaddps	%ymm1, %ymm7, %ymm1
	vmovups	%ymm1, -48(%rbp)
	vaddss	-48(%rbp), %xmm0, %xmm0
	vaddss	-44(%rbp), %xmm0, %xmm0
	vaddss	-40(%rbp), %xmm0, %xmm0
	vaddss	-36(%rbp), %xmm0, %xmm0
	vaddss	-32(%rbp), %xmm0, %xmm0
	vaddss	-28(%rbp), %xmm0, %xmm0
	vaddss	-24(%rbp), %xmm0, %xmm0
	vaddss	-20(%rbp), %xmm0, %xmm0
	vzeroupper
	popq	%r10
	.cfi_def_cfa 10, 0
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5700:
	.size	_Z8sumFloatPfi, .-_Z8sumFloatPfi
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%f\n"
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup,"ax",@progbits
.LHOTB5:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5701:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movslq	%edi, %rdi
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movl	$10, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$40, %rsp
	.cfi_def_cfa_offset 96
	movq	-8(%rsi,%rdi,8), %rdi
	xorl	%esi, %esi
	call	strtol
	movslq	%eax, %rcx
	movq	%rax, %r14
	movl	%eax, %r13d
	movabsq	$137438953472, %rax
	movq	$-1, %rdi
	cqto
	idivq	%rcx
	movabsq	$2287828610704211968, %rdx
	cmpq	%rdx, %rcx
	movq	%rax, %r12
	leaq	0(,%rcx,4), %rax
	cmovbe	%rax, %rdi
	xorl	%ebx, %ebx
	call	_Znam
	movq	%rax, %rbp
	call	clock
	testq	%r12, %r12
	movq	%rax, %r15
	jle	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r13d, %esi
	movq	%rbp, %rdi
	addq	$1, %rbx
	call	_Z8sumFloatPfi
	vmovss	%xmm0, 28(%rsp)
	cmpq	%r12, %rbx
	vmovss	28(%rsp), %xmm1
	vmovss	24(%rsp), %xmm0
	vaddss	%xmm1, %xmm0, %xmm0
	vmovss	%xmm0, 24(%rsp)
	jne	.L14
.L11:
	call	clock
	vxorps	%xmm2, %xmm2, %xmm2
	subq	%r15, %rax
	vmovss	24(%rsp), %xmm1
	movq	%rax, %rdx
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	movl	%r14d, %ecx
	movl	$.LC3, %esi
	vcvtsi2ssq	%rax, %xmm2, %xmm2
	vdivss	.LC2(%rip), %xmm2, %xmm2
	movl	$1, %edi
	movl	$2, %eax
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vmovapd	%xmm2, %xmm0
	vmovsd	%xmm2, 8(%rsp)
	call	__printf_chk
	vmovsd	8(%rsp), %xmm2
	movl	$.LC4, %esi
	movl	$1, %edi
	movl	$1, %eax
	vmovapd	%xmm2, %xmm0
	call	__printf_chk
	movq	%rbp, %rdi
	call	_ZdaPv
	addq	$40, %rsp
	.cfi_def_cfa_offset 56
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5701:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup
.LHOTB6:
	.p2align 4,,15
	.type	_GLOBAL__sub_I__Z8sumFloatPfi, @function
_GLOBAL__sub_I__Z8sumFloatPfi:
.LFB5764:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5764:
	.size	_GLOBAL__sub_I__Z8sumFloatPfi, .-_GLOBAL__sub_I__Z8sumFloatPfi
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z8sumFloatPfi
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1232348160
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
