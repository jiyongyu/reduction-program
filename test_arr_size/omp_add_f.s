	.file	"omp_add_f.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.type	_Z8sumFloatPfi._omp_fn.0, @function
_Z8sumFloatPfi._omp_fn.0:
.LFB5765:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x68,0x6
	.cfi_escape 0x10,0xd,0x2,0x76,0x78
	.cfi_escape 0x10,0xc,0x2,0x76,0x70
	pushq	%rbx
	movq	%rdi, %r13
	subq	$80, %rsp
	.cfi_escape 0x10,0x3,0x2,0x76,0x60
	movl	8(%rdi), %eax
	leal	-16(%rax), %ebx
	call	omp_get_num_threads
	movl	%eax, %r12d
	call	omp_get_thread_num
	movl	%eax, %ecx
	movl	%ebx, %eax
	cltd
	idivl	%r12d
	cmpl	%edx, %ecx
	jge	.L3
	addl	$1, %eax
	xorl	%edx, %edx
.L3:
	imull	%eax, %ecx
	vxorps	%xmm0, %xmm0, %xmm0
	addl	%ecx, %edx
	leal	(%rdx,%rax), %esi
	cmpl	%esi, %edx
	jge	.L12
	movl	%esi, %r9d
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	subl	%edx, %r9d
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	leal	-8(%r9), %r10d
	leal	-1(%r9), %ecx
	movq	0(%r13), %rax
	shrl	$3, %r10d
	addl	$1, %r10d
	cmpl	$6, %ecx
	leal	0(,%r10,8), %r8d
	jbe	.L10
	movslq	%edx, %rcx
	vmovaps	-80(%rbp), %ymm0
	xorl	%edi, %edi
	leaq	(%rax,%rcx,4), %rcx
.L4:
	vmovups	(%rcx), %xmm1
	addl	$1, %edi
	addq	$32, %rcx
	vinsertf128	$0x1, -16(%rcx), %ymm1, %ymm1
	cmpl	%edi, %r10d
	vaddps	%ymm1, %ymm0, %ymm0
	ja	.L4
	addl	%r8d, %edx
	cmpl	%r9d, %r8d
	vmovaps	%ymm0, -80(%rbp)
	je	.L21
	vzeroupper
.L10:
	movslq	%edx, %rcx
	vmovss	-80(%rbp), %xmm0
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	1(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	2(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	3(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	4(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	leal	5(%rdx), %ecx
	cmpl	%ecx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%ecx, %rcx
	addl	$6, %edx
	vaddss	(%rax,%rcx,4), %xmm0, %xmm0
	cmpl	%edx, %esi
	vmovss	%xmm0, -80(%rbp)
	jle	.L6
	movslq	%edx, %rdx
	vaddss	(%rax,%rdx,4), %xmm0, %xmm0
	vmovss	%xmm0, -80(%rbp)
.L6:
	cmpl	%esi, %ebx
	je	.L28
.L8:
	vxorps	%xmm0, %xmm0, %xmm0
	vaddss	-80(%rbp), %xmm0, %xmm0
	vaddss	-76(%rbp), %xmm0, %xmm0
	vaddss	-72(%rbp), %xmm0, %xmm0
	vaddss	-68(%rbp), %xmm0, %xmm0
	vaddss	-64(%rbp), %xmm0, %xmm0
	vaddss	-60(%rbp), %xmm0, %xmm0
	vaddss	-56(%rbp), %xmm0, %xmm0
	vaddss	-52(%rbp), %xmm0, %xmm0
.L12:
	leaq	16(%r13), %rcx
	movl	16(%r13), %edx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%eax, %edx
.L9:
	movl	%edx, -84(%rbp)
	movl	%edx, %eax
	vmovss	-84(%rbp), %xmm3
	vaddss	%xmm3, %xmm0, %xmm2
	vmovd	%xmm2, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	jne	.L14
	addq	$80, %rsp
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	%ebx, 12(%r13)
	jmp	.L8
.L21:
	vzeroupper
	jmp	.L6
	.cfi_endproc
.LFE5765:
	.size	_Z8sumFloatPfi._omp_fn.0, .-_Z8sumFloatPfi._omp_fn.0
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.text.unlikely
.LCOLDB2:
	.text
.LHOTB2:
	.p2align 4,,15
	.globl	_Z8sumFloatPfi
	.type	_Z8sumFloatPfi, @function
_Z8sumFloatPfi:
.LFB5700:
	.cfi_startproc
	subq	$40, %rsp
	.cfi_def_cfa_offset 48
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rdi, (%rsp)
	movl	%esi, 8(%rsp)
	movl	$_Z8sumFloatPfi._omp_fn.0, %edi
	movq	%rsp, %rsi
	movl	$0x00000000, 16(%rsp)
	movl	$0, 12(%rsp)
	call	GOMP_parallel
	vmovss	16(%rsp), %xmm0
	addq	$40, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5700:
	.size	_Z8sumFloatPfi, .-_Z8sumFloatPfi
	.section	.text.unlikely
.LCOLDE2:
	.text
.LHOTE2:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"%f\n"
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup,"ax",@progbits
.LHOTB6:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5701:
	.cfi_startproc
	pushq	%r15
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movslq	%edi, %rdi
	pushq	%r13
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movl	$10, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$72, %rsp
	.cfi_def_cfa_offset 128
	movq	-8(%rsi,%rdi,8), %rdi
	xorl	%esi, %esi
	call	strtol
	movslq	%eax, %rcx
	movq	%rax, %r14
	movl	%eax, %r13d
	movabsq	$137438953472, %rax
	movl	$1, %edi
	cqto
	idivq	%rcx
	movq	%rax, %r12
	call	omp_set_num_threads
	leal	0(,%r14,4), %esi
	movl	$64, %edi
	movslq	%esi, %rsi
	call	memalign
	movq	%rax, %rbp
	call	clock
	testq	%r12, %r12
	movq	%rax, %r15
	jle	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	32(%rsp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$_Z8sumFloatPfi._omp_fn.0, %edi
	movl	$0x00000000, 48(%rsp)
	movq	%rbp, 32(%rsp)
	movl	%r13d, 40(%rsp)
	movl	$0, 44(%rsp)
	addq	$1, %rbx
	call	GOMP_parallel
	vmovss	48(%rsp), %xmm0
	cmpq	%r12, %rbx
	vmovss	%xmm0, 28(%rsp)
	vmovss	28(%rsp), %xmm1
	vmovss	24(%rsp), %xmm0
	vaddss	%xmm1, %xmm0, %xmm0
	vmovss	%xmm0, 24(%rsp)
	jne	.L37
.L35:
	call	clock
	vxorps	%xmm2, %xmm2, %xmm2
	subq	%r15, %rax
	vmovss	24(%rsp), %xmm1
	movq	%rax, %rdx
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	movl	%r14d, %ecx
	movl	$.LC4, %esi
	vcvtsi2ssq	%rax, %xmm2, %xmm2
	vdivss	.LC3(%rip), %xmm2, %xmm2
	movl	$1, %edi
	movl	$2, %eax
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vmovapd	%xmm2, %xmm0
	vmovsd	%xmm2, 8(%rsp)
	call	__printf_chk
	vmovsd	8(%rsp), %xmm2
	movl	$.LC5, %esi
	movl	$1, %edi
	movl	$1, %eax
	vmovapd	%xmm2, %xmm0
	call	__printf_chk
	testq	%rbp, %rbp
	je	.L39
	movq	%rbp, %rdi
	call	_ZdaPv
.L39:
	addq	$72, %rsp
	.cfi_def_cfa_offset 56
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 48
	popq	%rbp
	.cfi_def_cfa_offset 40
	popq	%r12
	.cfi_def_cfa_offset 32
	popq	%r13
	.cfi_def_cfa_offset 24
	popq	%r14
	.cfi_def_cfa_offset 16
	popq	%r15
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE5701:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.text.unlikely
.LCOLDB7:
	.section	.text.startup
.LHOTB7:
	.p2align 4,,15
	.type	_GLOBAL__sub_I__Z8sumFloatPfi, @function
_GLOBAL__sub_I__Z8sumFloatPfi:
.LFB5764:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5764:
	.size	_GLOBAL__sub_I__Z8sumFloatPfi, .-_GLOBAL__sub_I__Z8sumFloatPfi
	.section	.text.unlikely
.LCOLDE7:
	.section	.text.startup
.LHOTE7:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z8sumFloatPfi
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	1232348160
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
