#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include "../vector_class/vectorclass.h"

#define FLOAT   0
#define DOUBLE  1
using namespace std;

float sumFloat(float *arr, int array_len){
    Vec8f a0; 
    Vec8f a1; 
    Vec8f a2; 
    Vec8f a3; 
    Vec8f a4; 
    Vec8f a5; 
    Vec8f a6; 
    Vec8f a7; 
    Vec8f a8; 
    Vec8f a9; 
    Vec8f a10; 
    Vec8f a11; 
    Vec8f a12; 
    Vec8f a13; 
    Vec8f a14; 

    Vec8f sumVec0 = 0.0;
    Vec8f sumVec1 = 0.0;
    Vec8f sumVec2 = 0.0;
    Vec8f sumVec3 = 0.0;
    Vec8f sumVec4 = 0.0;
    Vec8f sumVec5 = 0.0;
    Vec8f sumVec6 = 0.0;
    Vec8f sumVec7 = 0.0;
    Vec8f sumVec8 = 0.0;
    Vec8f sumVec9 = 0.0;
    Vec8f sumVec10 = 0.0;
    Vec8f sumVec11 = 0.0;
    Vec8f sumVec12 = 0.0;
    Vec8f sumVec13 = 0.0;
    Vec8f sumVec14 = 0.0;

    int j;
	
    for(j=0; j<array_len-16; j+=120){
		a0.load(arr+j+8*0);
		a1.load(arr+j+8*1);
		a2.load(arr+j+8*2);
		a3.load(arr+j+8*3);
		a4.load(arr+j+8*4);
		a5.load(arr+j+8*5);
		a6.load(arr+j+8*6);
		a7.load(arr+j+8*7);
		a8.load(arr+j+8*8);
		a9.load(arr+j+8*9);
		a10.load(arr+j+8*10);
		a11.load(arr+j+8*11);
		a12.load(arr+j+8*12);
		a13.load(arr+j+8*13);
		a14.load(arr+j+8*14);
		sumVec0 = mul_add(sumVec0, 1.0, a0);
		sumVec1 = mul_add(sumVec1, 1.0, a1);
		sumVec2 = mul_add(sumVec2, 1.0, a2);
		sumVec3 = mul_add(sumVec3, 1.0, a3);
		sumVec4 = mul_add(sumVec4, 1.0, a4);
		sumVec5 = mul_add(sumVec5, 1.0, a5);
		sumVec6 = mul_add(sumVec6, 1.0, a6);
		sumVec7 = mul_add(sumVec7, 1.0, a7);
		sumVec8 = mul_add(sumVec8, 1.0, a8);
		sumVec9 = mul_add(sumVec9, 1.0, a9);
		sumVec10 = mul_add(sumVec10, 1.0, a10);
		sumVec11 = mul_add(sumVec11, 1.0, a11);
		sumVec12 = mul_add(sumVec12, 1.0, a12);
		sumVec13 = mul_add(sumVec13, 1.0, a13);
		sumVec14 = mul_add(sumVec14, 1.0, a14);
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    float sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];
    sum += sumVec0[4];
    sum += sumVec0[5];
    sum += sumVec0[6];
    sum += sumVec0[7];

    return sum;
}

double sumDouble(double *arr, int array_len){
    Vec4d a0; 
    Vec4d a1; 
    Vec4d a2; 
    Vec4d a3; 
    Vec4d a4; 
    Vec4d a5; 
    Vec4d a6; 
    Vec4d a7; 
    Vec4d a8; 
    Vec4d a9; 
    Vec4d a10; 
    Vec4d a11; 
    Vec4d a12; 
    Vec4d a13; 
    Vec4d a14; 

    Vec4d sumVec0 = 0.0;
    Vec4d sumVec1 = 0.0;
    Vec4d sumVec2 = 0.0;
    Vec4d sumVec3 = 0.0;
    Vec4d sumVec4 = 0.0;
    Vec4d sumVec5 = 0.0;
    Vec4d sumVec6 = 0.0;
    Vec4d sumVec7 = 0.0;
    Vec4d sumVec8 = 0.0;
    Vec4d sumVec9 = 0.0;
    Vec4d sumVec10 = 0.0;
    Vec4d sumVec11 = 0.0;
    Vec4d sumVec12 = 0.0;
    Vec4d sumVec13 = 0.0;
    Vec4d sumVec14 = 0.0;

    int j;
	
    for(j=0; j<array_len-16; j+=60){
		a0.load(arr+j+4*0);
		a1.load(arr+j+4*1);
		a2.load(arr+j+4*2);
		a3.load(arr+j+4*3);
		a4.load(arr+j+4*4);
		a5.load(arr+j+4*5);
		a6.load(arr+j+4*6);
		a7.load(arr+j+4*7);
		a8.load(arr+j+4*8);
		a9.load(arr+j+4*9);
		a10.load(arr+j+4*10);
		a11.load(arr+j+4*11);
		a12.load(arr+j+4*12);
		a13.load(arr+j+4*13);
		a14.load(arr+j+4*14);
		sumVec0 = mul_add(sumVec0, 1.0, a0);
		sumVec1 = mul_add(sumVec1, 1.0, a1);
		sumVec2 = mul_add(sumVec2, 1.0, a2);
		sumVec3 = mul_add(sumVec3, 1.0, a3);
		sumVec4 = mul_add(sumVec4, 1.0, a4);
		sumVec5 = mul_add(sumVec5, 1.0, a5);
		sumVec6 = mul_add(sumVec6, 1.0, a6);
		sumVec7 = mul_add(sumVec7, 1.0, a7);
		sumVec8 = mul_add(sumVec8, 1.0, a8);
		sumVec9 = mul_add(sumVec9, 1.0, a9);
		sumVec10 = mul_add(sumVec10, 1.0, a10);
		sumVec11 = mul_add(sumVec11, 1.0, a11);
		sumVec12 = mul_add(sumVec12, 1.0, a12);
		sumVec13 = mul_add(sumVec13, 1.0, a13);
		sumVec14 = mul_add(sumVec14, 1.0, a14);
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    double sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];

    return sum;
}

int main(int argc, char **argv){

    bool data_type = FLOAT;
    int array_len;

    while((c=getopt(argc, argv, "t:s:h")) != -1)
        switch (c){
            case 't':   // data type
                if(optarg == "double")
                    data_type = DOUBLE;
                break;
            case 's':   // array size
                array_len = atoi(optarg);
                break;
            case 'h':
                cout<< "Usage:\n"
                    << "      -t <data type>: float or double\n"
                    << "      -s <array size>\n";
                exit(0);
                break;
            default:
                abort();
        }

	int i, j;
    
    if (data_type == FLOAT){
        float *arr_float = new float [array_len];
        volatile float passed_sum, returned_sum;
        clock_t t;
        t = clock();
        for (long long int i=0; i<0x200000; i++){
            returned_sum = sumFloat(arr_float, array_len);
            passed_sum += returned_sum;
        }
        t = clock() - t;
        double seconds = ((float)t) / CLOCKS_PER_SEC;
	    printf("time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n", t, seconds, array_len, passed_sum);
        printf("%f\n", seconds);

        delete [] arr_float;
    }
    else{
        double *arr_double = new double [array_len]; 
        volatile double passed_sum, returned_sum;
        clock_t t;
        t = clock();
        for (long long int i=0; i<0x200000; i++){
            returned_sum = sumDouble(arr_double, array_len);
            passed_sum += returned_sum;
        }
        t = clock() - t;
        double seconds = ((float)t) / CLOCKS_PER_SEC;
	    printf("time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n", t, seconds, array_len, passed_sum);
        printf("%f\n", seconds);
    
        delete [] arr_double;
    }
    return 0;
}
