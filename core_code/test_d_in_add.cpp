#include <immintrin.h>
#include <malloc.h>
#include <stdio.h>
#include <time.h>


double d_in_sum_add(double* arr, int arr_len){

    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = arr_len - arr_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_add_pd(a0, vec_sum0);
        vec_sum1 = _mm256_add_pd(a1, vec_sum1);
        vec_sum2 = _mm256_add_pd(a2, vec_sum2);
        vec_sum3 = _mm256_add_pd(a3, vec_sum3);
        vec_sum4 = _mm256_add_pd(a4, vec_sum4);
        vec_sum5 = _mm256_add_pd(a5, vec_sum5);
        vec_sum6 = _mm256_add_pd(a6, vec_sum6);
        vec_sum7 = _mm256_add_pd(a7, vec_sum7);
        vec_sum8 = _mm256_add_pd(a8, vec_sum8);
        vec_sum9 = _mm256_add_pd(a9, vec_sum9);
        vec_sum10 = _mm256_add_pd(a10, vec_sum10);
        vec_sum11 = _mm256_add_pd(a11, vec_sum11);
        vec_sum12 = _mm256_add_pd(a12, vec_sum12);
        vec_sum13 = _mm256_add_pd(a13, vec_sum13);
        vec_sum14 = _mm256_add_pd(a14, vec_sum14);
    }

    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_pd(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_pd(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    
    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}

int main(){

    int i=0;
    int arr_len = 0x1000;
    int itera = 0x200000;

    mallopt(M_MMAP_MAX, 0);
    //double* d_arr = new double [arr_len];
    double *d_arr = (double*) memalign(64, arr_len*8);

    for (i=0; i<arr_len; i++){
        d_arr[i] = i/97.0;
    }

    double result = 0.0;
    clock_t t;
    t = clock();
    for(i=0; i<itera; i++){
        result += d_in_sum_add(d_arr, arr_len);
    }
    t = clock() - t;

    double d_in_add_seconds = ((float)t) / CLOCKS_PER_SEC;

    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t, d_in_add_seconds, arr_len, result); 

    delete d_arr;
}

