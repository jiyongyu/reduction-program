	.file	"test_d_in_add.cpp"
	.text
	.p2align 4,,15
	.globl	_Z12d_in_sum_addPdi
	.type	_Z12d_in_sum_addPdi, @function
_Z12d_in_sum_addPdi:
.LFB1060:
	.cfi_startproc
	movl	%esi, %eax
	movl	$-2004318071, %edx
	imull	%edx
	movl	%esi, %eax
	sarl	$31, %eax
	addl	%esi, %edx
	sarl	$5, %edx
	subl	%eax, %edx
	movl	$60, %eax
	imull	%eax, %edx
	testl	%edx, %edx
	jle	.L6
	vxorpd	%xmm14, %xmm14, %xmm14
	movq	%rdi, %rcx
	xorl	%r8d, %r8d
	vmovapd	%ymm14, %ymm0
	vmovapd	%ymm14, %ymm1
	vmovapd	%ymm14, %ymm2
	vmovapd	%ymm14, %ymm3
	vmovapd	%ymm14, %ymm4
	vmovapd	%ymm14, %ymm5
	vmovapd	%ymm14, %ymm6
	vmovapd	%ymm14, %ymm7
	vmovapd	%ymm14, %ymm8
	vmovapd	%ymm14, %ymm9
	vmovapd	%ymm14, %ymm10
	vmovapd	%ymm14, %ymm11
	vmovapd	%ymm14, %ymm12
	vmovapd	%ymm14, %ymm13
	.p2align 4,,10
	.p2align 3
.L3:
	addl	$60, %r8d
	vaddpd	(%rcx), %ymm14, %ymm14
	addq	$480, %rcx
	vaddpd	-448(%rcx), %ymm13, %ymm13
	vaddpd	-416(%rcx), %ymm12, %ymm12
	vaddpd	-384(%rcx), %ymm11, %ymm11
	vaddpd	-352(%rcx), %ymm10, %ymm10
	vaddpd	-320(%rcx), %ymm9, %ymm9
	vaddpd	-288(%rcx), %ymm8, %ymm8
	vaddpd	-256(%rcx), %ymm7, %ymm7
	vaddpd	-224(%rcx), %ymm6, %ymm6
	vaddpd	-192(%rcx), %ymm5, %ymm5
	vaddpd	-160(%rcx), %ymm4, %ymm4
	vaddpd	-128(%rcx), %ymm3, %ymm3
	vaddpd	-96(%rcx), %ymm2, %ymm2
	vaddpd	-64(%rcx), %ymm1, %ymm1
	vaddpd	-32(%rcx), %ymm0, %ymm0
	cmpl	%r8d, %edx
	jg	.L3
.L2:
	vaddpd	%ymm1, %ymm2, %ymm2
	cmpl	%r8d, %esi
	vaddpd	%ymm13, %ymm14, %ymm13
	vaddpd	%ymm11, %ymm12, %ymm11
	vaddpd	%ymm9, %ymm10, %ymm9
	vaddpd	%ymm7, %ymm8, %ymm7
	vaddpd	%ymm5, %ymm6, %ymm5
	vaddpd	%ymm3, %ymm4, %ymm3
	vaddpd	%ymm0, %ymm2, %ymm0
	vaddpd	%ymm11, %ymm13, %ymm11
	vaddpd	%ymm7, %ymm9, %ymm7
	vaddpd	%ymm3, %ymm5, %ymm3
	vaddpd	%ymm7, %ymm11, %ymm1
	vaddpd	%ymm0, %ymm3, %ymm0
	vaddpd	%ymm0, %ymm1, %ymm1
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovapd	%xmm1, %xmm2
	vextractf128	$0x1, %ymm1, %xmm1
	vaddsd	%xmm2, %xmm0, %xmm0
	vunpckhpd	%xmm2, %xmm2, %xmm2
	vaddsd	%xmm0, %xmm2, %xmm2
	vmovapd	%xmm1, %xmm0
	vunpckhpd	%xmm1, %xmm1, %xmm1
	vaddsd	%xmm2, %xmm0, %xmm0
	vaddsd	%xmm0, %xmm1, %xmm0
	jle	.L7
	subl	$1, %esi
	movslq	%r8d, %rax
	subl	%r8d, %esi
	leaq	(%rdi,%rax,8), %rdx
	addq	%rsi, %rax
	leaq	8(%rdi,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L5:
	vaddsd	(%rdx), %xmm0, %xmm0
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.L5
.L7:
	vzeroupper
	ret
.L6:
	vxorpd	%xmm14, %xmm14, %xmm14
	xorl	%r8d, %r8d
	vmovapd	%ymm14, %ymm0
	vmovapd	%ymm14, %ymm1
	vmovapd	%ymm14, %ymm2
	vmovapd	%ymm14, %ymm3
	vmovapd	%ymm14, %ymm4
	vmovapd	%ymm14, %ymm5
	vmovapd	%ymm14, %ymm6
	vmovapd	%ymm14, %ymm7
	vmovapd	%ymm14, %ymm8
	vmovapd	%ymm14, %ymm9
	vmovapd	%ymm14, %ymm10
	vmovapd	%ymm14, %ymm11
	vmovapd	%ymm14, %ymm12
	vmovapd	%ymm14, %ymm13
	jmp	.L2
	.cfi_endproc
.LFE1060:
	.size	_Z12d_in_sum_addPdi, .-_Z12d_in_sum_addPdi
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1061:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32768, %esi
	movl	$64, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	andq	$-32, %rsp
	addq	$-128, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	memalign
	movq	%rax, %rbx
	andl	$31, %eax
	shrq	$3, %rax
	negq	%rax
	andl	$3, %eax
	je	.L21
	cmpl	$1, %eax
	movq	$0, (%rbx)
	jbe	.L22
	vmovsd	.LC1(%rip), %xmm1
	cmpl	$2, %eax
	vmovsd	%xmm1, 8(%rbx)
	jbe	.L23
	vmovsd	.LC2(%rip), %xmm1
	movl	$4093, %r8d
	movl	$3, %ecx
	vmovsd	%xmm1, 16(%rbx)
.L10:
	leal	6(%rcx), %r12d
	leal	4(%rcx), %r11d
	leal	2(%rcx), %r10d
	leal	1(%rcx), %edx
	movl	$4096, %edi
	movl	%r12d, 104(%rsp)
	leal	7(%rcx), %r12d
	movl	%r11d, 112(%rsp)
	vmovd	104(%rsp), %xmm1
	leal	5(%rcx), %r11d
	movl	%r10d, 120(%rsp)
	leal	3(%rcx), %r10d
	subl	%eax, %edi
	movl	%eax, %eax
	vpinsrd	$1, %r12d, %xmm1, %xmm0
	vmovd	112(%rsp), %xmm1
	movl	%edi, %esi
	leaq	(%rbx,%rax,8), %rax
	vpinsrd	$1, %r11d, %xmm1, %xmm2
	vmovd	120(%rsp), %xmm1
	movl	%ecx, 120(%rsp)
	vmovd	120(%rsp), %xmm4
	shrl	$3, %esi
	vpinsrd	$1, %r10d, %xmm1, %xmm1
	leal	0(,%rsi,8), %r9d
	vpinsrd	$1, %edx, %xmm4, %xmm3
	vmovdqa	.LC3(%rip), %ymm4
	xorl	%edx, %edx
	vpunpcklqdq	%xmm0, %xmm2, %xmm0
	vpunpcklqdq	%xmm1, %xmm3, %xmm1
	vinserti128	$0x1, %xmm0, %ymm1, %ymm0
	vmovapd	.LC4(%rip), %ymm1
	jmp	.L16
.L12:
	vmovdqa	%ymm2, %ymm0
.L16:
	vcvtdq2pd	%xmm0, %ymm3
	vpaddd	%ymm4, %ymm0, %ymm2
	vextracti128	$0x1, %ymm0, %xmm0
	addl	$1, %edx
	addq	$64, %rax
	vcvtdq2pd	%xmm0, %ymm0
	vdivpd	%ymm1, %ymm3, %ymm3
	vdivpd	%ymm1, %ymm0, %ymm0
	vmovapd	%ymm3, -64(%rax)
	vmovapd	%ymm0, -32(%rax)
	cmpl	%esi, %edx
	jb	.L12
	movl	%r8d, %eax
	addl	%r9d, %ecx
	subl	%r9d, %eax
	cmpl	%edi, %r9d
	je	.L14
	vcvtsi2sd	%ecx, %xmm1, %xmm1
	movslq	%ecx, %rdx
	cmpl	$1, %eax
	vmovsd	.LC5(%rip), %xmm0
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rdx,8)
	leal	1(%rcx), %edx
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$2, %eax
	leal	2(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$3, %eax
	leal	3(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$4, %eax
	leal	4(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$5, %eax
	leal	5(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	addl	$6, %ecx
	cmpl	$6, %eax
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%ecx, %xmm1, %xmm1
	movslq	%ecx, %rax
	vdivsd	%xmm0, %xmm1, %xmm0
	vmovsd	%xmm0, (%rbx,%rax,8)
.L14:
	vzeroupper
	call	clock
	vmovsd	32640(%rbx), %xmm1
	movq	%rax, %r12
	movl	$2097152, %esi
	vmovsd	%xmm1, 96(%rsp)
	vmovsd	32648(%rbx), %xmm1
	vmovsd	%xmm1, 120(%rsp)
	vmovsd	32656(%rbx), %xmm1
	vmovsd	%xmm1, 112(%rsp)
	vmovsd	32664(%rbx), %xmm1
	vmovsd	%xmm1, 64(%rsp)
	vmovsd	32672(%rbx), %xmm1
	vmovsd	%xmm1, 72(%rsp)
	vmovsd	32680(%rbx), %xmm1
	vmovsd	%xmm1, 80(%rsp)
	vmovsd	32688(%rbx), %xmm1
	vmovsd	%xmm1, 88(%rsp)
	vmovsd	32696(%rbx), %xmm1
	vmovsd	%xmm1, 56(%rsp)
	vmovsd	32704(%rbx), %xmm1
	vmovsd	%xmm1, 48(%rsp)
	vmovsd	32712(%rbx), %xmm1
	vmovsd	%xmm1, 40(%rsp)
	vmovsd	32720(%rbx), %xmm1
	vmovsd	%xmm1, 32(%rsp)
	vmovsd	32728(%rbx), %xmm1
	vmovsd	%xmm1, 24(%rsp)
	vmovsd	32736(%rbx), %xmm1
	vmovsd	%xmm1, 16(%rsp)
	vmovsd	32744(%rbx), %xmm1
	vmovsd	%xmm1, 8(%rsp)
	vmovsd	32752(%rbx), %xmm1
	vmovsd	%xmm1, (%rsp)
	vmovsd	32760(%rbx), %xmm1
	vmovsd	%xmm1, 104(%rsp)
	vxorpd	%xmm1, %xmm1, %xmm1
	.p2align 4,,10
	.p2align 3
.L17:
	vxorpd	%xmm15, %xmm15, %xmm15
	movq	%rbx, %rdx
	xorl	%ecx, %ecx
	vmovapd	%ymm15, %ymm14
	vmovapd	%ymm15, %ymm13
	vmovapd	%ymm15, %ymm12
	vmovapd	%ymm15, %ymm11
	vmovapd	%ymm15, %ymm10
	vmovapd	%ymm15, %ymm9
	vmovapd	%ymm15, %ymm8
	vmovapd	%ymm15, %ymm7
	vmovapd	%ymm15, %ymm6
	vmovapd	%ymm15, %ymm5
	vmovapd	%ymm15, %ymm4
	vmovapd	%ymm15, %ymm3
	vmovapd	%ymm15, %ymm2
	vmovapd	%ymm15, %ymm0
	.p2align 4,,10
	.p2align 3
.L20:
	addl	$60, %ecx
	vaddpd	(%rdx), %ymm0, %ymm0
	addq	$480, %rdx
	vaddpd	-448(%rdx), %ymm2, %ymm2
	vaddpd	-416(%rdx), %ymm3, %ymm3
	vaddpd	-384(%rdx), %ymm4, %ymm4
	vaddpd	-352(%rdx), %ymm5, %ymm5
	vaddpd	-320(%rdx), %ymm6, %ymm6
	vaddpd	-288(%rdx), %ymm7, %ymm7
	vaddpd	-256(%rdx), %ymm8, %ymm8
	vaddpd	-224(%rdx), %ymm9, %ymm9
	vaddpd	-192(%rdx), %ymm10, %ymm10
	vaddpd	-160(%rdx), %ymm11, %ymm11
	vaddpd	-128(%rdx), %ymm12, %ymm12
	vaddpd	-96(%rdx), %ymm13, %ymm13
	vaddpd	-64(%rdx), %ymm14, %ymm14
	vaddpd	-32(%rdx), %ymm15, %ymm15
	cmpl	$4080, %ecx
	jne	.L20
	vaddpd	%ymm2, %ymm0, %ymm0
	subl	$1, %esi
	vaddpd	%ymm4, %ymm3, %ymm3
	vaddpd	%ymm6, %ymm5, %ymm5
	vxorpd	%xmm6, %xmm6, %xmm6
	vaddpd	%ymm8, %ymm7, %ymm7
	vaddpd	%ymm10, %ymm9, %ymm9
	vaddpd	%ymm12, %ymm11, %ymm11
	vaddpd	%ymm14, %ymm13, %ymm13
	vaddpd	%ymm3, %ymm0, %ymm0
	vaddpd	%ymm7, %ymm5, %ymm5
	vaddpd	%ymm11, %ymm9, %ymm9
	vaddpd	%ymm15, %ymm13, %ymm13
	vaddpd	%ymm5, %ymm0, %ymm0
	vaddpd	%ymm13, %ymm9, %ymm9
	vaddpd	%ymm9, %ymm0, %ymm0
	vmovapd	%xmm0, %xmm2
	vextractf128	$0x1, %ymm0, %xmm0
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm2, %xmm2, %xmm2
	vaddsd	%xmm6, %xmm3, %xmm3
	vaddsd	%xmm2, %xmm3, %xmm2
	vmovapd	%xmm0, %xmm3
	vunpckhpd	%xmm0, %xmm0, %xmm0
	vaddsd	%xmm3, %xmm2, %xmm2
	vaddsd	%xmm0, %xmm2, %xmm0
	vaddsd	96(%rsp), %xmm0, %xmm0
	vaddsd	120(%rsp), %xmm0, %xmm0
	vaddsd	112(%rsp), %xmm0, %xmm0
	vaddsd	64(%rsp), %xmm0, %xmm0
	vaddsd	72(%rsp), %xmm0, %xmm0
	vaddsd	80(%rsp), %xmm0, %xmm0
	vaddsd	88(%rsp), %xmm0, %xmm0
	vaddsd	56(%rsp), %xmm0, %xmm0
	vaddsd	48(%rsp), %xmm0, %xmm0
	vaddsd	40(%rsp), %xmm0, %xmm0
	vaddsd	32(%rsp), %xmm0, %xmm0
	vaddsd	24(%rsp), %xmm0, %xmm0
	vaddsd	16(%rsp), %xmm0, %xmm0
	vaddsd	8(%rsp), %xmm0, %xmm0
	vaddsd	(%rsp), %xmm0, %xmm0
	vaddsd	104(%rsp), %xmm0, %xmm0
	vaddsd	%xmm0, %xmm1, %xmm1
	jne	.L17
	vmovsd	%xmm1, 120(%rsp)
	vzeroupper
	call	clock
	subq	%r12, %rax
	vmovsd	120(%rsp), %xmm1
	vcvtsi2ssq	%rax, %xmm0, %xmm0
	movq	%rax, %rdx
	movl	$4096, %ecx
	movl	$.LC7, %esi
	movl	$1, %edi
	movl	$2, %eax
	vdivss	.LC6(%rip), %xmm0, %xmm0
	vunpcklps	%xmm0, %xmm0, %xmm0
	vcvtps2pd	%xmm0, %xmm0
	call	__printf_chk
	movq	%rbx, %rdi
	call	_ZdlPv
	leaq	-16(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movl	$4096, %r8d
	xorl	%ecx, %ecx
	jmp	.L10
.L23:
	movl	$4094, %r8d
	movl	$2, %ecx
	jmp	.L10
.L22:
	movl	$4095, %r8d
	movl	$1, %ecx
	jmp	.L10
	.cfi_endproc
.LFE1061:
	.size	main, .-main
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	3940743189
	.long	1065688327
	.align 8
.LC2:
	.long	3940743189
	.long	1066736903
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC3:
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.align 32
.LC4:
	.long	0
	.long	1079525376
	.long	0
	.long	1079525376
	.long	0
	.long	1079525376
	.long	0
	.long	1079525376
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1079525376
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC6:
	.long	1232348160
	.ident	"GCC: (Ubuntu 4.8.5-2ubuntu1~14.04.1) 4.8.5"
	.section	.note.GNU-stack,"",@progbits
