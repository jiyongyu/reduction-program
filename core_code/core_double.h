#ifndef __CORE_DOUBLE_H__
#define __CORE_DOUBLE_H__

#ifndef OPTIONS
#define OPTIONS
#define FMA 1
#define ADD 0
#define INTR 1
#define VECT 0
#endif

// using <vectorclass.h>
double d_vc_sum_add(double *arr, int arr_len);
double d_vc_sum_fma(double *arr, int arr_len);

// using <immintrin.h>
double d_in_sum_add(double* arr, int arr_len);
double d_in_sum_fma(double* arr, int arr_len);

// test function
//double double_test(int method, int op, double* arr, int arr_len, int itera);

#endif
