// all float methods

#include <immintrin.h>
#include <stdio.h>
#include "core_float.h"
#include "../vector_class/vectorclass.h"


float f_vc_sum_add(float *arr, int arr_len){
    
    Vec8f a0, a1, a2, a3, a4, a5, a6, a7,
          a8, a9, a10, a11, a12, a13, a14; 

    Vec8f sumVec0 = 0.0;
    Vec8f sumVec1 = 0.0;
    Vec8f sumVec2 = 0.0;
    Vec8f sumVec3 = 0.0;
    Vec8f sumVec4 = 0.0;
    Vec8f sumVec5 = 0.0;
    Vec8f sumVec6 = 0.0;
    Vec8f sumVec7 = 0.0;
    Vec8f sumVec8 = 0.0;
    Vec8f sumVec9 = 0.0;
    Vec8f sumVec10 = 0.0;
    Vec8f sumVec11 = 0.0;
    Vec8f sumVec12 = 0.0;
    Vec8f sumVec13 = 0.0;
    Vec8f sumVec14 = 0.0;

    int i;
	int simd_bound = arr_len - arr_len % 120;

    for(i=0; i<simd_bound; i+=120){
		a0.load(arr+i+8*0);
		a1.load(arr+i+8*1);
		a2.load(arr+i+8*2);
		a3.load(arr+i+8*3);
		a4.load(arr+i+8*4);
		a5.load(arr+i+8*5);
		a6.load(arr+i+8*6);
		a7.load(arr+i+8*7);
		a8.load(arr+i+8*8);
		a9.load(arr+i+8*9);
		a10.load(arr+i+8*10);
		a11.load(arr+i+8*11);
		a12.load(arr+i+8*12);
		a13.load(arr+i+8*13);
		a14.load(arr+i+8*14);
		sumVec0 += a0;
		sumVec1 += a1;
		sumVec2 += a2;
		sumVec3 += a3;
		sumVec4 += a4;
		sumVec5 += a5;
		sumVec6 += a6;
		sumVec7 += a7;
		sumVec8 += a8;
		sumVec9 += a9;
		sumVec10 += a10;
		sumVec11 += a11;
		sumVec12 += a12;
		sumVec13 += a13;
		sumVec14 += a14;
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    float sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];
    sum += sumVec0[4];
    sum += sumVec0[5];
    sum += sumVec0[6];
    sum += sumVec0[7];

    for(; i<arr_len; i++){
       sum += arr[i];
    }

    return sum;
}

float f_vc_sum_fma(float *arr, int arr_len){
    
    Vec8f a0, a1, a2, a3, a4, a5, a6, a7,
          a8, a9, a10, a11, a12, a13, a14; 

    Vec8f sumVec0 = 0.0;
    Vec8f sumVec1 = 0.0;
    Vec8f sumVec2 = 0.0;
    Vec8f sumVec3 = 0.0;
    Vec8f sumVec4 = 0.0;
    Vec8f sumVec5 = 0.0;
    Vec8f sumVec6 = 0.0;
    Vec8f sumVec7 = 0.0;
    Vec8f sumVec8 = 0.0;
    Vec8f sumVec9 = 0.0;
    Vec8f sumVec10 = 0.0;
    Vec8f sumVec11 = 0.0;
    Vec8f sumVec12 = 0.0;
    Vec8f sumVec13 = 0.0;
    Vec8f sumVec14 = 0.0;

    int i;
	int simd_bound = arr_len - arr_len % 120;
    
    for(i=0; i<simd_bound; i+=120){
		a0.load(arr+i+8*0);
		a1.load(arr+i+8*1);
		a2.load(arr+i+8*2);
		a3.load(arr+i+8*3);
		a4.load(arr+i+8*4);
		a5.load(arr+i+8*5);
		a6.load(arr+i+8*6);
		a7.load(arr+i+8*7);
		a8.load(arr+i+8*8);
		a9.load(arr+i+8*9);
		a10.load(arr+i+8*10);
		a11.load(arr+i+8*11);
		a12.load(arr+i+8*12);
		a13.load(arr+i+8*13);
		a14.load(arr+i+8*14);
		sumVec0 = mul_add(sumVec0, 1.0, a0);
		sumVec1 = mul_add(sumVec1, 1.0, a1);
		sumVec2 = mul_add(sumVec2, 1.0, a2);
		sumVec3 = mul_add(sumVec3, 1.0, a3);
		sumVec4 = mul_add(sumVec4, 1.0, a4);
		sumVec5 = mul_add(sumVec5, 1.0, a5);
		sumVec6 = mul_add(sumVec6, 1.0, a6);
		sumVec7 = mul_add(sumVec7, 1.0, a7);
		sumVec8 = mul_add(sumVec8, 1.0, a8);
		sumVec9 = mul_add(sumVec9, 1.0, a9);
		sumVec10 = mul_add(sumVec10, 1.0, a10);
		sumVec11 = mul_add(sumVec11, 1.0, a11);
		sumVec12 = mul_add(sumVec12, 1.0, a12);
		sumVec13 = mul_add(sumVec13, 1.0, a13);
		sumVec14 = mul_add(sumVec14, 1.0, a14);
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    float sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];
    sum += sumVec0[4];
    sum += sumVec0[5];
    sum += sumVec0[6];
    sum += sumVec0[7];

    for(; i<arr_len; i++){
       sum += arr[i];
    }

    return sum;
}

float f_in_sum_add(float *arr, int arr_len){

    __m256 a0, a1, a2, a3, a4, a5, a6, a7,
           a8, a9, a10, a11, a12, a13, a14; 

    __m256 vec_sum0 = _mm256_setzero_ps();
    __m256 vec_sum1 = _mm256_setzero_ps();
    __m256 vec_sum2 = _mm256_setzero_ps();
    __m256 vec_sum3 = _mm256_setzero_ps();
    __m256 vec_sum4 = _mm256_setzero_ps();
    __m256 vec_sum5 = _mm256_setzero_ps();
    __m256 vec_sum6 = _mm256_setzero_ps();
    __m256 vec_sum7 = _mm256_setzero_ps();
    __m256 vec_sum8 = _mm256_setzero_ps();
    __m256 vec_sum9 = _mm256_setzero_ps();
    __m256 vec_sum10 = _mm256_setzero_ps();
    __m256 vec_sum11 = _mm256_setzero_ps();
    __m256 vec_sum12 = _mm256_setzero_ps();
    __m256 vec_sum13 = _mm256_setzero_ps();
    __m256 vec_sum14 = _mm256_setzero_ps();
    
    int i;
    int simd_bound = arr_len - arr_len % 120;

    for(i=0; i<simd_bound; i+=120){
        // loading data to register
        a0 = _mm256_load_ps(arr+i+8*0);
        a1 = _mm256_load_ps(arr+i+8*1);
        a2 = _mm256_load_ps(arr+i+8*2);
        a3 = _mm256_load_ps(arr+i+8*3);
        a4 = _mm256_load_ps(arr+i+8*4);
        a5 = _mm256_load_ps(arr+i+8*5);
        a6 = _mm256_load_ps(arr+i+8*6);
        a7 = _mm256_load_ps(arr+i+8*7);
        a8 = _mm256_load_ps(arr+i+8*8);
        a9 = _mm256_load_ps(arr+i+8*9);
        a10 = _mm256_load_ps(arr+i+8*10);
        a11 = _mm256_load_ps(arr+i+8*11);
        a12 = _mm256_load_ps(arr+i+8*12);
        a13 = _mm256_load_ps(arr+i+8*13);
        a14 = _mm256_load_ps(arr+i+8*14);

        // arith operation
        vec_sum0 = _mm256_add_ps(a0, vec_sum0);
        vec_sum1 = _mm256_add_ps(a1, vec_sum1);
        vec_sum2 = _mm256_add_ps(a2, vec_sum2);
        vec_sum3 = _mm256_add_ps(a3, vec_sum3);
        vec_sum4 = _mm256_add_ps(a4, vec_sum4);
        vec_sum5 = _mm256_add_ps(a5, vec_sum5);
        vec_sum6 = _mm256_add_ps(a6, vec_sum6);
        vec_sum7 = _mm256_add_ps(a7, vec_sum7);
        vec_sum8 = _mm256_add_ps(a8, vec_sum8);
        vec_sum9 = _mm256_add_ps(a9, vec_sum9);
        vec_sum10 = _mm256_add_ps(a10, vec_sum10);
        vec_sum11 = _mm256_add_ps(a11, vec_sum11);
        vec_sum12 = _mm256_add_ps(a12, vec_sum12);
        vec_sum13 = _mm256_add_ps(a13, vec_sum13);
        vec_sum14 = _mm256_add_ps(a14, vec_sum14);
    }

    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_ps(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_ps(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_ps(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum8);
    
    float sum = 0.0;
    float* res = (float*)&vec_sum0;

    sum += res[0];
    sum += res[1];
    sum += res[2];
    sum += res[3];
    sum += res[4];
    sum += res[5];
    sum += res[6];
    sum += res[7];

    for(; i<arr_len; i++){
       sum += arr[i];
    }

    return sum;
}

float f_in_sum_fma(float* arr, int arr_len){

    __m256 a0, a1, a2, a3, a4, a5, a6, a7,
           a8, a9, a10, a11, a12, a13, a14; 

    __m256 constReg = _mm256_set_ps(1.0, 1.0, 1.0, 1.0,
                                    1.0, 1.0, 1.0, 1.0);

    __m256 vec_sum0 = _mm256_setzero_ps();
    __m256 vec_sum1 = _mm256_setzero_ps();
    __m256 vec_sum2 = _mm256_setzero_ps();
    __m256 vec_sum3 = _mm256_setzero_ps();
    __m256 vec_sum4 = _mm256_setzero_ps();
    __m256 vec_sum5 = _mm256_setzero_ps();
    __m256 vec_sum6 = _mm256_setzero_ps();
    __m256 vec_sum7 = _mm256_setzero_ps();
    __m256 vec_sum8 = _mm256_setzero_ps();
    __m256 vec_sum9 = _mm256_setzero_ps();
    __m256 vec_sum10 = _mm256_setzero_ps();
    __m256 vec_sum11 = _mm256_setzero_ps();
    __m256 vec_sum12 = _mm256_setzero_ps();
    __m256 vec_sum13 = _mm256_setzero_ps();
    __m256 vec_sum14 = _mm256_setzero_ps();
    
    int i;
    int simd_bound = arr_len - arr_len % 120;

    for(i=0; i<simd_bound; i+=120){
        // loading data to register
        a0 = _mm256_load_ps(arr+i+8*0);
        a1 = _mm256_load_ps(arr+i+8*1);
        a2 = _mm256_load_ps(arr+i+8*2);
        a3 = _mm256_load_ps(arr+i+8*3);
        a4 = _mm256_load_ps(arr+i+8*4);
        a5 = _mm256_load_ps(arr+i+8*5);
        a6 = _mm256_load_ps(arr+i+8*6);
        a7 = _mm256_load_ps(arr+i+8*7);
        a8 = _mm256_load_ps(arr+i+8*8);
        a9 = _mm256_load_ps(arr+i+8*9);
        a10 = _mm256_load_ps(arr+i+8*10);
        a11 = _mm256_load_ps(arr+i+8*11);
        a12 = _mm256_load_ps(arr+i+8*12);
        a13 = _mm256_load_ps(arr+i+8*13);
        a14 = _mm256_load_ps(arr+i+8*14);

        // arith operation
        vec_sum0 = _mm256_fmadd_ps(a0, constReg, vec_sum0);
        vec_sum1 = _mm256_fmadd_ps(a1, constReg, vec_sum1);
        vec_sum2 = _mm256_fmadd_ps(a2, constReg, vec_sum2);
        vec_sum3 = _mm256_fmadd_ps(a3, constReg, vec_sum3);
        vec_sum4 = _mm256_fmadd_ps(a4, constReg, vec_sum4);
        vec_sum5 = _mm256_fmadd_ps(a5, constReg, vec_sum5);
        vec_sum6 = _mm256_fmadd_ps(a6, constReg, vec_sum6);
        vec_sum7 = _mm256_fmadd_ps(a7, constReg, vec_sum7);
        vec_sum8 = _mm256_fmadd_ps(a8, constReg, vec_sum8);
        vec_sum9 = _mm256_fmadd_ps(a9, constReg, vec_sum9);
        vec_sum10 = _mm256_fmadd_ps(a10, constReg, vec_sum10);
        vec_sum11 = _mm256_fmadd_ps(a11, constReg, vec_sum11);
        vec_sum12 = _mm256_fmadd_ps(a12, constReg, vec_sum12);
        vec_sum13 = _mm256_fmadd_ps(a13, constReg, vec_sum13);
        vec_sum14 = _mm256_fmadd_ps(a14, constReg, vec_sum14);
    }

    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_ps(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_ps(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_ps(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum8);
    
    float sum = 0.0;
    float* res = (float*)&vec_sum0;

    sum += res[0];
    sum += res[1];
    sum += res[2];
    sum += res[3];
    sum += res[4];
    sum += res[5];
    sum += res[6];
    sum += res[7];

    for(; i<arr_len; i++){
       sum += arr[i];
    }

    return sum;
}
