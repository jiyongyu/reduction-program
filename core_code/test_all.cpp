//
// A comprehensive test
//
#include "core_float.h"
#include "core_double.h"
#include <stdio.h>
#include <time.h>


float float_test(int method, int op, float* arr, int arr_len, int itera){
    int i = 0;
    float result = 0.0;
    

    if (method == INTR){
        if (op == FMA){
            //printf("FLOAT INTR FMA\n");
            for (i=0; i<itera; i++) result += f_in_sum_fma(arr, arr_len);
        } else { // ADD
            //printf("FLOAT INTR ADD\n");
            for (i=0; i<itera; i++) result += f_in_sum_add(arr, arr_len);
        }
    } else { // VECT
        if (op == FMA){
            //printf("FLOAT VECT FMA\n");
            for (i=0; i<itera; i++) result += f_vc_sum_fma(arr, arr_len);
        } else { // ADD
            //printf("FLOAT VECT ADD\n");
            for (i=0; i<itera; i++) result += f_vc_sum_add(arr, arr_len);
        }
    }

    return result;
}

double double_test(int method, int op, double* arr, int arr_len, int itera){
    int i = 0;
    double result = 0.0;

    if (method == INTR){
        if (op == FMA){
            //printf("DOUBLE INTR FMA\n");
            for (i=0; i<itera; i++) result += d_in_sum_fma(arr, arr_len);
        } else { // ADD
            //printf("DOUBLE INTR ADD\n");
            for (i=0; i<itera; i++) result += d_in_sum_add(arr, arr_len);
        }
    } else { // VECT
        if (op == FMA){
            //printf("DOUBLE VECT FMA\n");
            for (i=0; i<itera; i++) result += d_vc_sum_fma(arr, arr_len);
        } else { // ADD
            //printf("DOUBLE VECT ADD\n");
            for (i=0; i<itera; i++) result += d_vc_sum_add(arr, arr_len);
        }
    }

    return result;
}


int main(){

    int i=0, j=0;
    int arr_len = 0x1000;
    int itera = 0x200000;

    float* f_arr = new float [arr_len];
    double* d_arr = new double [arr_len];
    
    for (i=0; i<arr_len; i++){
        f_arr[i] = i/97.0;
        d_arr[i] = i/97.0;
    }

    double f_result = 0;
    double d_result = 0;
    for (i=0; i<itera; i++){ 
        for (j=0; j<arr_len; j++){
            f_result += f_arr[j];
            d_result += d_arr[j];
        }
    }
    printf("====== Correct Result ======\n");
    printf("float result: %f\n", f_result);
    printf("double result: %f\n", d_result);

    clock_t t0, t1, t2, t3, t4;

    /****** float part ******/
    t0 = clock();
    float f_in_fma_result = float_test(INTR, FMA, f_arr, arr_len, itera);
    t1 = clock();
    float f_in_add_result = float_test(INTR, ADD, f_arr, arr_len, itera);
    t2 = clock();
    float f_vc_fma_result = float_test(VECT, FMA, f_arr, arr_len, itera);
    t3 = clock();
    float f_vc_add_result = float_test(VECT, ADD, f_arr, arr_len, itera);
    t4 = clock();

    t4 = t4 - t3;
    t3 = t3 - t2;
    t2 = t2 - t1;
    t1 = t1 - t0;

    double f_in_fma_seconds = ((float)t1) / CLOCKS_PER_SEC;
    double f_in_add_seconds = ((float)t2) / CLOCKS_PER_SEC;
    double f_vc_fma_seconds = ((float)t3) / CLOCKS_PER_SEC;
    double f_vc_add_seconds = ((float)t4) / CLOCKS_PER_SEC;

    printf("====== FLOAT ======\n");
    printf("--- intrinsic methods ---\n");
    printf("< fma result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t1, f_in_fma_seconds, arr_len, f_in_fma_result); 
    printf("< add result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t2, f_in_add_seconds, arr_len, f_in_add_result); 
    printf("--- vector class methods ---\n");
    printf("< fma result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t3, f_vc_fma_seconds, arr_len, f_vc_fma_result); 
    printf("< add result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t4, f_vc_add_seconds, arr_len, f_vc_add_result); 
    
    /****** double part ******/
    t0 = clock();
    double d_in_fma_result = double_test(INTR, FMA, d_arr, arr_len, itera);
    t1 = clock();
    double d_in_add_result = double_test(INTR, ADD, d_arr, arr_len, itera);
    t2 = clock();
    double d_vc_fma_result = double_test(VECT, FMA, d_arr, arr_len, itera);
    t3 = clock();
    double d_vc_add_result = double_test(VECT, ADD, d_arr, arr_len, itera);
    t4 = clock();

    t4 = t4 - t3;
    t3 = t3 - t2;
    t2 = t2 - t1;
    t1 = t1 - t0;

    double d_in_fma_seconds = ((float)t1) / CLOCKS_PER_SEC;
    double d_in_add_seconds = ((float)t2) / CLOCKS_PER_SEC;
    double d_vc_fma_seconds = ((float)t3) / CLOCKS_PER_SEC;
    double d_vc_add_seconds = ((float)t4) / CLOCKS_PER_SEC;

    printf("====== DOUBLE ======\n");
    printf("--- intrinsic methods ---\n");
    printf("< fma result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t1, d_in_fma_seconds, arr_len, d_in_fma_result); 
    printf("< add result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t2, d_in_add_seconds, arr_len, d_in_add_result); 
    printf("--- vector class methods ---\n");
    printf("< fma result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t3, d_vc_fma_seconds, arr_len, d_vc_fma_result); 
    printf("< add result >:\n");
    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t4, d_vc_add_seconds, arr_len, d_vc_add_result); 

    delete f_arr, d_arr;
}

