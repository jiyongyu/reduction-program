#ifndef __CORE_FLOAT_H__
#define __CORE_FLOAT_H__

#ifndef OPTIONS
#define OPTIONS
#define FMA 1
#define ADD 0
#define INTR 1
#define VECT 0
#endif

// using <vectorclass.h>
float f_vc_sum_add(float *arr, int arr_len);
float f_vc_sum_fma(float *arr, int arr_len);

// using <immintrin.h>
float f_in_sum_add(float *arr, int arr_len);
float f_in_sum_fma(float* arr, int arr_len);

// test function
//float float_test(int op, int method, float* arr, int arr_len, int itera);

#endif
