// all double methods

#include <immintrin.h>
#include <stdio.h>
#include "core_double.h"
#include "../vector_class/vectorclass.h"


double d_vc_sum_add(double *arr, int arr_len){

    Vec4d a0, a1, a2, a3, a4, a5, a6, a7,
          a8, a9, a10, a11, a12, a13, a14; 

    Vec4d sumVec0 = 0.0;
    Vec4d sumVec1 = 0.0;
    Vec4d sumVec2 = 0.0;
    Vec4d sumVec3 = 0.0;
    Vec4d sumVec4 = 0.0;
    Vec4d sumVec5 = 0.0;
    Vec4d sumVec6 = 0.0;
    Vec4d sumVec7 = 0.0;
    Vec4d sumVec8 = 0.0;
    Vec4d sumVec9 = 0.0;
    Vec4d sumVec10 = 0.0;
    Vec4d sumVec11 = 0.0;
    Vec4d sumVec12 = 0.0;
    Vec4d sumVec13 = 0.0;
    Vec4d sumVec14 = 0.0;

    int i;
    int simd_bound = arr_len - arr_len % 60;
	
    for(i=0; i<simd_bound; i+=60){
		a0.load(arr+i+4*0);
		a1.load(arr+i+4*1);
		a2.load(arr+i+4*2);
		a3.load(arr+i+4*3);
		a4.load(arr+i+4*4);
		a5.load(arr+i+4*5);
		a6.load(arr+i+4*6);
		a7.load(arr+i+4*7);
		a8.load(arr+i+4*8);
		a9.load(arr+i+4*9);
		a10.load(arr+i+4*10);
		a11.load(arr+i+4*11);
		a12.load(arr+i+4*12);
		a13.load(arr+i+4*13);
		a14.load(arr+i+4*14);
		sumVec0 += a0;
		sumVec1 += a1;
		sumVec2 += a2;
		sumVec3 += a3;
		sumVec4 += a4;
		sumVec5 += a5;
		sumVec6 += a6;
		sumVec7 += a7;
		sumVec8 += a8;
		sumVec9 += a9;
		sumVec10 += a10;
		sumVec11 += a11;
		sumVec12 += a12;
		sumVec13 += a13;
		sumVec14 += a14;
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    double sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];

    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}

double d_vc_sum_fma(double *arr, int arr_len){

    Vec4d a0, a1, a2, a3, a4, a5, a6, a7,
          a8, a9, a10, a11, a12, a13, a14; 
    
    Vec4d sumVec0 = 0.0;
    Vec4d sumVec1 = 0.0;
    Vec4d sumVec2 = 0.0;
    Vec4d sumVec3 = 0.0;
    Vec4d sumVec4 = 0.0;
    Vec4d sumVec5 = 0.0;
    Vec4d sumVec6 = 0.0;
    Vec4d sumVec7 = 0.0;
    Vec4d sumVec8 = 0.0;
    Vec4d sumVec9 = 0.0;
    Vec4d sumVec10 = 0.0;
    Vec4d sumVec11 = 0.0;
    Vec4d sumVec12 = 0.0;
    Vec4d sumVec13 = 0.0;
    Vec4d sumVec14 = 0.0;

    int i;
    int simd_bound = arr_len - arr_len % 60;
	
    for(i=0; i<simd_bound; i+=60){
		a0.load(arr+i+4*0);
		a1.load(arr+i+4*1);
		a2.load(arr+i+4*2);
		a3.load(arr+i+4*3);
		a4.load(arr+i+4*4);
		a5.load(arr+i+4*5);
		a6.load(arr+i+4*6);
		a7.load(arr+i+4*7);
		a8.load(arr+i+4*8);
		a9.load(arr+i+4*9);
		a10.load(arr+i+4*10);
		a11.load(arr+i+4*11);
		a12.load(arr+i+4*12);
		a13.load(arr+i+4*13);
		a14.load(arr+i+4*14);
		sumVec0 = mul_add(sumVec0, 1.0, a0); 
		sumVec1 = mul_add(sumVec1, 1.0, a1); 
		sumVec2 = mul_add(sumVec2, 1.0, a2); 
		sumVec3 = mul_add(sumVec3, 1.0, a3); 
		sumVec4 = mul_add(sumVec4, 1.0, a4); 
		sumVec5 = mul_add(sumVec5, 1.0, a5); 
		sumVec6 = mul_add(sumVec6, 1.0, a6); 
		sumVec7 = mul_add(sumVec7, 1.0, a7); 
		sumVec8 = mul_add(sumVec8, 1.0, a8); 
		sumVec9 = mul_add(sumVec9, 1.0, a9); 
		sumVec10 = mul_add(sumVec10, 1.0, a10); 
		sumVec11 = mul_add(sumVec11, 1.0, a11); 
		sumVec12 = mul_add(sumVec12, 1.0, a12); 
		sumVec13 = mul_add(sumVec13, 1.0, a13); 
		sumVec14 = mul_add(sumVec14, 1.0, a14); 
	}
    
    sumVec0 += sumVec1;
    sumVec2 += sumVec3;
    sumVec4 += sumVec5;
    sumVec6 += sumVec7;
    sumVec8 += sumVec9;
    sumVec10 += sumVec11;
    sumVec12 += sumVec13;
    sumVec0 += sumVec2;
    sumVec4 += sumVec6;
    sumVec8 += sumVec10;
    sumVec12 += sumVec14;
    sumVec0 += sumVec4;
    sumVec8 += sumVec12;
    sumVec0 += sumVec8;

    double sum = 0.0;
    sum += sumVec0[0];
    sum += sumVec0[1];
    sum += sumVec0[2];
    sum += sumVec0[3];

    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}


double d_in_sum_add(double* arr, int arr_len){

    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = arr_len - arr_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_add_pd(a0, vec_sum0);
        vec_sum1 = _mm256_add_pd(a1, vec_sum1);
        vec_sum2 = _mm256_add_pd(a2, vec_sum2);
        vec_sum3 = _mm256_add_pd(a3, vec_sum3);
        vec_sum4 = _mm256_add_pd(a4, vec_sum4);
        vec_sum5 = _mm256_add_pd(a5, vec_sum5);
        vec_sum6 = _mm256_add_pd(a6, vec_sum6);
        vec_sum7 = _mm256_add_pd(a7, vec_sum7);
        vec_sum8 = _mm256_add_pd(a8, vec_sum8);
        vec_sum9 = _mm256_add_pd(a9, vec_sum9);
        vec_sum10 = _mm256_add_pd(a10, vec_sum10);
        vec_sum11 = _mm256_add_pd(a11, vec_sum11);
        vec_sum12 = _mm256_add_pd(a12, vec_sum12);
        vec_sum13 = _mm256_add_pd(a13, vec_sum13);
        vec_sum14 = _mm256_add_pd(a14, vec_sum14);
    }

    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_pd(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_pd(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    
    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}

double d_in_sum_fma(double* arr, int arr_len){

    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d constReg = _mm256_set_pd(1.0, 1.0, 1.0, 1.0);
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = arr_len - arr_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_fmadd_pd(a0, constReg, vec_sum0);
        vec_sum1 = _mm256_fmadd_pd(a1, constReg, vec_sum1);
        vec_sum2 = _mm256_fmadd_pd(a2, constReg, vec_sum2);
        vec_sum3 = _mm256_fmadd_pd(a3, constReg, vec_sum3);
        vec_sum4 = _mm256_fmadd_pd(a4, constReg, vec_sum4);
        vec_sum5 = _mm256_fmadd_pd(a5, constReg, vec_sum5);
        vec_sum6 = _mm256_fmadd_pd(a6, constReg, vec_sum6);
        vec_sum7 = _mm256_fmadd_pd(a7, constReg, vec_sum7);
        vec_sum8 = _mm256_fmadd_pd(a8, constReg, vec_sum8);
        vec_sum9 = _mm256_fmadd_pd(a9, constReg, vec_sum9);
        vec_sum10 = _mm256_fmadd_pd(a10, constReg, vec_sum10);
        vec_sum11 = _mm256_fmadd_pd(a11, constReg, vec_sum11);
        vec_sum12 = _mm256_fmadd_pd(a12, constReg, vec_sum12);
        vec_sum13 = _mm256_fmadd_pd(a13, constReg, vec_sum13);
        vec_sum14 = _mm256_fmadd_pd(a14, constReg, vec_sum14);
    }

    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_pd(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_pd(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    
    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}
