	.file	"test_d_in_fma.cpp"
	.text
	.p2align 4,,15
	.globl	_Z12d_in_sum_fmaPdi
	.type	_Z12d_in_sum_fmaPdi, @function
_Z12d_in_sum_fmaPdi:
.LFB1060:
	.cfi_startproc
	movl	%esi, %eax
	movl	$-2004318071, %edx
	imull	%edx
	movl	%esi, %eax
	sarl	$31, %eax
	addl	%esi, %edx
	sarl	$5, %edx
	subl	%eax, %edx
	movl	$60, %eax
	imull	%eax, %edx
	testl	%edx, %edx
	jle	.L6
	vxorpd	%xmm15, %xmm15, %xmm15
	vmovapd	.LC0(%rip), %ymm0
	movq	%rdi, %rcx
	xorl	%r8d, %r8d
	vmovapd	%ymm15, %ymm1
	vmovapd	%ymm15, %ymm2
	vmovapd	%ymm15, %ymm3
	vmovapd	%ymm15, %ymm4
	vmovapd	%ymm15, %ymm5
	vmovapd	%ymm15, %ymm6
	vmovapd	%ymm15, %ymm7
	vmovapd	%ymm15, %ymm8
	vmovapd	%ymm15, %ymm9
	vmovapd	%ymm15, %ymm10
	vmovapd	%ymm15, %ymm11
	vmovapd	%ymm15, %ymm12
	vmovapd	%ymm15, %ymm13
	vmovapd	%ymm15, %ymm14
	.p2align 4,,10
	.p2align 3
.L3:
	addl	$60, %r8d
	vfmadd231pd	(%rcx), %ymm0, %ymm15
	vfmadd231pd	32(%rcx), %ymm0, %ymm14
	vfmadd231pd	64(%rcx), %ymm0, %ymm13
	vfmadd231pd	96(%rcx), %ymm0, %ymm12
	vfmadd231pd	128(%rcx), %ymm0, %ymm11
	vfmadd231pd	160(%rcx), %ymm0, %ymm10
	vfmadd231pd	192(%rcx), %ymm0, %ymm9
	vfmadd231pd	224(%rcx), %ymm0, %ymm8
	vfmadd231pd	256(%rcx), %ymm0, %ymm7
	vfmadd231pd	288(%rcx), %ymm0, %ymm6
	vfmadd231pd	320(%rcx), %ymm0, %ymm5
	vfmadd231pd	352(%rcx), %ymm0, %ymm4
	vfmadd231pd	384(%rcx), %ymm0, %ymm3
	vfmadd231pd	416(%rcx), %ymm0, %ymm2
	vfmadd231pd	448(%rcx), %ymm0, %ymm1
	addq	$480, %rcx
	cmpl	%r8d, %edx
	jg	.L3
.L2:
	vaddpd	%ymm2, %ymm3, %ymm2
	cmpl	%r8d, %esi
	vxorpd	%xmm0, %xmm0, %xmm0
	vaddpd	%ymm14, %ymm15, %ymm14
	vaddpd	%ymm12, %ymm13, %ymm12
	vaddpd	%ymm10, %ymm11, %ymm10
	vaddpd	%ymm8, %ymm9, %ymm8
	vaddpd	%ymm6, %ymm7, %ymm6
	vaddpd	%ymm4, %ymm5, %ymm4
	vaddpd	%ymm1, %ymm2, %ymm1
	vaddpd	%ymm12, %ymm14, %ymm12
	vaddpd	%ymm8, %ymm10, %ymm8
	vaddpd	%ymm4, %ymm6, %ymm4
	vaddpd	%ymm8, %ymm12, %ymm8
	vaddpd	%ymm1, %ymm4, %ymm1
	vaddpd	%ymm1, %ymm8, %ymm1
	vmovapd	%xmm1, %xmm2
	vextractf128	$0x1, %ymm1, %xmm1
	vaddsd	%xmm2, %xmm0, %xmm0
	vunpckhpd	%xmm2, %xmm2, %xmm2
	vaddsd	%xmm0, %xmm2, %xmm2
	vmovapd	%xmm1, %xmm0
	vunpckhpd	%xmm1, %xmm1, %xmm1
	vaddsd	%xmm2, %xmm0, %xmm0
	vaddsd	%xmm0, %xmm1, %xmm0
	jle	.L7
	subl	$1, %esi
	movslq	%r8d, %rax
	subl	%r8d, %esi
	leaq	(%rdi,%rax,8), %rdx
	addq	%rsi, %rax
	leaq	8(%rdi,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L5:
	vaddsd	(%rdx), %xmm0, %xmm0
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.L5
.L7:
	vzeroupper
	ret
.L6:
	vxorpd	%xmm15, %xmm15, %xmm15
	xorl	%r8d, %r8d
	vmovapd	%ymm15, %ymm1
	vmovapd	%ymm15, %ymm2
	vmovapd	%ymm15, %ymm3
	vmovapd	%ymm15, %ymm4
	vmovapd	%ymm15, %ymm5
	vmovapd	%ymm15, %ymm6
	vmovapd	%ymm15, %ymm7
	vmovapd	%ymm15, %ymm8
	vmovapd	%ymm15, %ymm9
	vmovapd	%ymm15, %ymm10
	vmovapd	%ymm15, %ymm11
	vmovapd	%ymm15, %ymm12
	vmovapd	%ymm15, %ymm13
	vmovapd	%ymm15, %ymm14
	jmp	.L2
	.cfi_endproc
.LFE1060:
	.size	_Z12d_in_sum_fmaPdi, .-_Z12d_in_sum_fmaPdi
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1061:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32768, %esi
	movl	$64, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	andq	$-32, %rsp
	subq	$144, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	memalign
	movq	%rax, %rbx
	andl	$31, %eax
	shrq	$3, %rax
	negq	%rax
	andl	$3, %eax
	je	.L21
	cmpl	$1, %eax
	movq	$0, (%rbx)
	jbe	.L22
	vmovsd	.LC2(%rip), %xmm6
	cmpl	$2, %eax
	vmovsd	%xmm6, 8(%rbx)
	jbe	.L23
	vmovsd	.LC3(%rip), %xmm7
	movl	$4093, %r8d
	movl	$3, %ecx
	vmovsd	%xmm7, 16(%rbx)
.L10:
	leal	6(%rcx), %r12d
	leal	4(%rcx), %r11d
	leal	2(%rcx), %r10d
	leal	1(%rcx), %edx
	movl	$4096, %edi
	vmovdqa	.LC4(%rip), %ymm4
	movl	%r12d, 120(%rsp)
	movl	%r11d, 128(%rsp)
	leal	7(%rcx), %r12d
	vmovd	120(%rsp), %xmm6
	movl	%r10d, 136(%rsp)
	leal	5(%rcx), %r11d
	vmovd	128(%rsp), %xmm7
	leal	3(%rcx), %r10d
	subl	%eax, %edi
	vpinsrd	$1, %r12d, %xmm6, %xmm0
	vmovd	136(%rsp), %xmm6
	movl	%ecx, 136(%rsp)
	vpinsrd	$1, %r11d, %xmm7, %xmm2
	vmovd	136(%rsp), %xmm7
	vpinsrd	$1, %r10d, %xmm6, %xmm1
	movl	%edi, %esi
	movl	%eax, %eax
	vpinsrd	$1, %edx, %xmm7, %xmm3
	shrl	$3, %esi
	leaq	(%rbx,%rax,8), %rax
	vpunpcklqdq	%xmm0, %xmm2, %xmm0
	leal	0(,%rsi,8), %r9d
	xorl	%edx, %edx
	vpunpcklqdq	%xmm1, %xmm3, %xmm1
	vinserti128	$0x1, %xmm0, %ymm1, %ymm0
	vmovapd	.LC5(%rip), %ymm1
	jmp	.L16
.L12:
	vmovdqa	%ymm2, %ymm0
.L16:
	vcvtdq2pd	%xmm0, %ymm3
	vpaddd	%ymm4, %ymm0, %ymm2
	vextracti128	$0x1, %ymm0, %xmm0
	addl	$1, %edx
	addq	$64, %rax
	vcvtdq2pd	%xmm0, %ymm0
	vdivpd	%ymm1, %ymm3, %ymm3
	vdivpd	%ymm1, %ymm0, %ymm0
	vmovapd	%ymm3, -64(%rax)
	vmovapd	%ymm0, -32(%rax)
	cmpl	%esi, %edx
	jb	.L12
	movl	%r8d, %eax
	addl	%r9d, %ecx
	subl	%r9d, %eax
	cmpl	%edi, %r9d
	je	.L14
	vcvtsi2sd	%ecx, %xmm1, %xmm1
	movslq	%ecx, %rdx
	cmpl	$1, %eax
	vmovsd	.LC6(%rip), %xmm0
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rdx,8)
	leal	1(%rcx), %edx
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$2, %eax
	leal	2(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$3, %eax
	leal	3(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$4, %eax
	leal	4(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	cmpl	$5, %eax
	leal	5(%rcx), %edx
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%edx, %xmm1, %xmm1
	movslq	%edx, %rsi
	addl	$6, %ecx
	cmpl	$6, %eax
	vdivsd	%xmm0, %xmm1, %xmm1
	vmovsd	%xmm1, (%rbx,%rsi,8)
	je	.L14
	vcvtsi2sd	%ecx, %xmm1, %xmm1
	movslq	%ecx, %rax
	vdivsd	%xmm0, %xmm1, %xmm0
	vmovsd	%xmm0, (%rbx,%rax,8)
.L14:
	vzeroupper
	call	clock
	vmovsd	32640(%rbx), %xmm4
	movq	%rax, %r12
	vmovsd	32648(%rbx), %xmm6
	movl	$2097152, %esi
	vmovsd	32656(%rbx), %xmm7
	movq	$0, 136(%rsp)
	vmovsd	%xmm4, 104(%rsp)
	vmovsd	32664(%rbx), %xmm4
	vmovsd	%xmm6, 128(%rsp)
	vmovsd	%xmm7, 120(%rsp)
	vmovsd	32672(%rbx), %xmm6
	vmovsd	32680(%rbx), %xmm7
	vmovsd	%xmm4, 72(%rsp)
	vmovsd	32688(%rbx), %xmm4
	vmovsd	%xmm6, 80(%rsp)
	vmovsd	%xmm7, 88(%rsp)
	vmovsd	32696(%rbx), %xmm6
	vmovsd	32704(%rbx), %xmm7
	vmovsd	%xmm4, 96(%rsp)
	vmovsd	32712(%rbx), %xmm4
	vmovsd	%xmm6, 64(%rsp)
	vmovsd	%xmm7, 56(%rsp)
	vmovsd	32720(%rbx), %xmm6
	vmovsd	32728(%rbx), %xmm7
	vmovsd	%xmm4, 48(%rsp)
	vmovsd	32736(%rbx), %xmm4
	vmovsd	%xmm6, 40(%rsp)
	vmovsd	%xmm7, 32(%rsp)
	vmovsd	%xmm4, 24(%rsp)
	vmovsd	32744(%rbx), %xmm6
	vmovsd	32752(%rbx), %xmm7
	vmovsd	32760(%rbx), %xmm4
	vmovsd	%xmm6, 16(%rsp)
	vmovsd	%xmm7, 8(%rsp)
	vmovsd	%xmm4, 112(%rsp)
	vmovapd	.LC0(%rip), %ymm0
	.p2align 4,,10
	.p2align 3
.L17:
	vxorpd	%xmm15, %xmm15, %xmm15
	movq	%rbx, %rdx
	xorl	%ecx, %ecx
	vmovapd	%ymm15, %ymm14
	vmovapd	%ymm15, %ymm13
	vmovapd	%ymm15, %ymm12
	vmovapd	%ymm15, %ymm11
	vmovapd	%ymm15, %ymm10
	vmovapd	%ymm15, %ymm9
	vmovapd	%ymm15, %ymm8
	vmovapd	%ymm15, %ymm7
	vmovapd	%ymm15, %ymm6
	vmovapd	%ymm15, %ymm5
	vmovapd	%ymm15, %ymm4
	vmovapd	%ymm15, %ymm3
	vmovapd	%ymm15, %ymm2
	vmovapd	%ymm15, %ymm1
	.p2align 4,,10
	.p2align 3
.L20:
	addl	$60, %ecx
	vfmadd231pd	(%rdx), %ymm0, %ymm1
	vfmadd231pd	32(%rdx), %ymm0, %ymm2
	vfmadd231pd	64(%rdx), %ymm0, %ymm3
	vfmadd231pd	96(%rdx), %ymm0, %ymm4
	vfmadd231pd	128(%rdx), %ymm0, %ymm5
	vfmadd231pd	160(%rdx), %ymm0, %ymm6
	vfmadd231pd	192(%rdx), %ymm0, %ymm7
	vfmadd231pd	224(%rdx), %ymm0, %ymm8
	vfmadd231pd	256(%rdx), %ymm0, %ymm9
	vfmadd231pd	288(%rdx), %ymm0, %ymm10
	vfmadd231pd	320(%rdx), %ymm0, %ymm11
	vfmadd231pd	352(%rdx), %ymm0, %ymm12
	vfmadd231pd	384(%rdx), %ymm0, %ymm13
	vfmadd231pd	416(%rdx), %ymm0, %ymm14
	vfmadd231pd	448(%rdx), %ymm0, %ymm15
	addq	$480, %rdx
	cmpl	$4080, %ecx
	jne	.L20
	vaddpd	%ymm4, %ymm3, %ymm3
	subl	$1, %esi
	vaddpd	%ymm2, %ymm1, %ymm1
	vaddpd	%ymm6, %ymm5, %ymm5
	vxorpd	%xmm6, %xmm6, %xmm6
	vaddpd	%ymm8, %ymm7, %ymm7
	vaddpd	%ymm10, %ymm9, %ymm9
	vaddpd	%ymm12, %ymm11, %ymm11
	vaddpd	%ymm14, %ymm13, %ymm13
	vaddpd	%ymm3, %ymm1, %ymm1
	vaddpd	%ymm7, %ymm5, %ymm5
	vaddpd	%ymm11, %ymm9, %ymm9
	vaddpd	%ymm15, %ymm13, %ymm13
	vaddpd	%ymm5, %ymm1, %ymm1
	vaddpd	%ymm13, %ymm9, %ymm9
	vaddpd	%ymm9, %ymm1, %ymm1
	vmovapd	%xmm1, %xmm2
	vextractf128	$0x1, %ymm1, %xmm1
	vmovapd	%xmm2, %xmm3
	vunpckhpd	%xmm2, %xmm2, %xmm2
	vaddsd	%xmm6, %xmm3, %xmm3
	vaddsd	%xmm2, %xmm3, %xmm2
	vmovapd	%xmm1, %xmm3
	vunpckhpd	%xmm1, %xmm1, %xmm1
	vaddsd	%xmm3, %xmm2, %xmm2
	vaddsd	%xmm1, %xmm2, %xmm1
	vaddsd	104(%rsp), %xmm1, %xmm1
	vaddsd	128(%rsp), %xmm1, %xmm1
	vaddsd	120(%rsp), %xmm1, %xmm1
	vaddsd	72(%rsp), %xmm1, %xmm1
	vaddsd	80(%rsp), %xmm1, %xmm1
	vaddsd	88(%rsp), %xmm1, %xmm1
	vaddsd	96(%rsp), %xmm1, %xmm1
	vaddsd	64(%rsp), %xmm1, %xmm1
	vaddsd	56(%rsp), %xmm1, %xmm1
	vaddsd	48(%rsp), %xmm1, %xmm1
	vaddsd	40(%rsp), %xmm1, %xmm1
	vaddsd	32(%rsp), %xmm1, %xmm1
	vaddsd	24(%rsp), %xmm1, %xmm1
	vaddsd	16(%rsp), %xmm1, %xmm1
	vaddsd	8(%rsp), %xmm1, %xmm1
	vaddsd	112(%rsp), %xmm1, %xmm1
	vaddsd	136(%rsp), %xmm1, %xmm4
	vmovsd	%xmm4, 136(%rsp)
	jne	.L17
	vzeroupper
	call	clock
	subq	%r12, %rax
	vmovsd	136(%rsp), %xmm1
	vcvtsi2ssq	%rax, %xmm0, %xmm0
	movq	%rax, %rdx
	movl	$4096, %ecx
	movl	$.LC8, %esi
	movl	$1, %edi
	movl	$2, %eax
	vdivss	.LC7(%rip), %xmm0, %xmm0
	vunpcklps	%xmm0, %xmm0, %xmm0
	vcvtps2pd	%xmm0, %xmm0
	call	__printf_chk
	movq	%rbx, %rdi
	call	_ZdlPv
	leaq	-16(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L21:
	.cfi_restore_state
	movl	$4096, %r8d
	xorl	%ecx, %ecx
	jmp	.L10
.L23:
	movl	$4094, %r8d
	movl	$2, %ecx
	jmp	.L10
.L22:
	movl	$4095, %r8d
	movl	$1, %ecx
	jmp	.L10
	.cfi_endproc
.LFE1061:
	.size	main, .-main
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC0:
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.long	0
	.long	1072693248
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	3940743189
	.long	1065688327
	.align 8
.LC3:
	.long	3940743189
	.long	1066736903
	.section	.rodata.cst32
	.align 32
.LC4:
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.align 32
.LC5:
	.long	0
	.long	1079525376
	.long	0
	.long	1079525376
	.long	0
	.long	1079525376
	.long	0
	.long	1079525376
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	0
	.long	1079525376
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC7:
	.long	1232348160
	.ident	"GCC: (Ubuntu 4.8.5-2ubuntu1~14.04.1) 4.8.5"
	.section	.note.GNU-stack,"",@progbits
