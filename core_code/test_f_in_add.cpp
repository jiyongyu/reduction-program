#include <immintrin.h>
#include <stdio.h>
#include <time.h>
#include <malloc.h>


float f_in_sum_add(float *arr, int arr_len){

    __m256 a0, a1, a2, a3, a4, a5, a6, a7,
           a8, a9, a10, a11, a12, a13, a14; 

    __m256 vec_sum0 = _mm256_setzero_ps();
    __m256 vec_sum1 = _mm256_setzero_ps();
    __m256 vec_sum2 = _mm256_setzero_ps();
    __m256 vec_sum3 = _mm256_setzero_ps();
    __m256 vec_sum4 = _mm256_setzero_ps();
    __m256 vec_sum5 = _mm256_setzero_ps();
    __m256 vec_sum6 = _mm256_setzero_ps();
    __m256 vec_sum7 = _mm256_setzero_ps();
    __m256 vec_sum8 = _mm256_setzero_ps();
    __m256 vec_sum9 = _mm256_setzero_ps();
    __m256 vec_sum10 = _mm256_setzero_ps();
    __m256 vec_sum11 = _mm256_setzero_ps();
    __m256 vec_sum12 = _mm256_setzero_ps();
    __m256 vec_sum13 = _mm256_setzero_ps();
    __m256 vec_sum14 = _mm256_setzero_ps();
    
    int i;
    int simd_bound = arr_len - arr_len % 120;

    for(i=0; i<simd_bound; i+=120){
        // loading data to register
        a0 = _mm256_load_ps(arr+i+8*0);
        a1 = _mm256_load_ps(arr+i+8*1);
        a2 = _mm256_load_ps(arr+i+8*2);
        a3 = _mm256_load_ps(arr+i+8*3);
        a4 = _mm256_load_ps(arr+i+8*4);
        a5 = _mm256_load_ps(arr+i+8*5);
        a6 = _mm256_load_ps(arr+i+8*6);
        a7 = _mm256_load_ps(arr+i+8*7);
        a8 = _mm256_load_ps(arr+i+8*8);
        a9 = _mm256_load_ps(arr+i+8*9);
        a10 = _mm256_load_ps(arr+i+8*10);
        a11 = _mm256_load_ps(arr+i+8*11);
        a12 = _mm256_load_ps(arr+i+8*12);
        a13 = _mm256_load_ps(arr+i+8*13);
        a14 = _mm256_load_ps(arr+i+8*14);

        // arith operation
        vec_sum0 = _mm256_add_ps(a0, vec_sum0);
        vec_sum1 = _mm256_add_ps(a1, vec_sum1);
        vec_sum2 = _mm256_add_ps(a2, vec_sum2);
        vec_sum3 = _mm256_add_ps(a3, vec_sum3);
        vec_sum4 = _mm256_add_ps(a4, vec_sum4);
        vec_sum5 = _mm256_add_ps(a5, vec_sum5);
        vec_sum6 = _mm256_add_ps(a6, vec_sum6);
        vec_sum7 = _mm256_add_ps(a7, vec_sum7);
        vec_sum8 = _mm256_add_ps(a8, vec_sum8);
        vec_sum9 = _mm256_add_ps(a9, vec_sum9);
        vec_sum10 = _mm256_add_ps(a10, vec_sum10);
        vec_sum11 = _mm256_add_ps(a11, vec_sum11);
        vec_sum12 = _mm256_add_ps(a12, vec_sum12);
        vec_sum13 = _mm256_add_ps(a13, vec_sum13);
        vec_sum14 = _mm256_add_ps(a14, vec_sum14);
    }

    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_ps(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_ps(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_ps(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum8);
    
    float sum = 0.0;
    float* res = (float*)&vec_sum0;

    sum += res[0];
    sum += res[1];
    sum += res[2];
    sum += res[3];
    sum += res[4];
    sum += res[5];
    sum += res[6];
    sum += res[7];

    for(; i<arr_len; i++){
       sum += arr[i];
    }

    return sum;
}

int main(){

    int i=0;
    int arr_len = 0x0800;
    int itera = 0x400000;

    //float* f_arr = new float [arr_len];
    float *f_arr = (float*) memalign(64, arr_len*4);    
    for (i=0; i<arr_len; i++){
        f_arr[i] = i/97.0;
    }

    double result = 0.0;
    clock_t t;
    t = clock();
    for(i=0; i<itera; i++){
        result += f_in_sum_add(f_arr, arr_len);
    }
    t = clock() - t;

    double f_in_add_seconds = ((float)t) / CLOCKS_PER_SEC;

    printf("time: %ld, clicks: %f,\narray_len: %d,\nresult: %f\n", t, f_in_add_seconds, arr_len, result); 

    //delete f_arr;
}

