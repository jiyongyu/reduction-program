#!/usr/bin/python
import myModule as mmod
import numpy as np
import time
import os.path
import subprocess


def main():

    arr_len = 0x1000
    itera = 0x1000
    d_arr = np.empty(arr_len, dtype=np.float64)
    for i in range (0, arr_len):
        d_arr[i] = i/97.0
    
    d_numpy_result = 0.0
    d_add_result = 0.0
    d_fma_result = 0.0
    ts_start = time.time()
    for i in range(0, itera):
        d_numpy_result += np.sum(d_arr)
    ts_end1 = time.time()
    for i in range(0, itera):
        d_add_result += mmod.addSumArr(d_arr)
    ts_end2 = time.time()
    for i in range(0, itera):
        d_fma_result += mmod.fmaSumArr(d_arr)
    ts_end3 = time.time()
    
    numpy_time_in_s = ts_end1 - ts_start
    add_time_in_s = ts_end2 - ts_end1
    fma_time_in_s = ts_end3 - ts_end2

    print "numpy sum", numpy_time_in_s, d_numpy_result
    print "add time", add_time_in_s, d_add_result
    print "fma time", fma_time_in_s, d_fma_result

if __name__ == "__main__":
    main()
