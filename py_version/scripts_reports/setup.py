from distutils.core import setup, Extension
import numpy as np

ext = Extension('myModule', \
                ["myModule.c"], \
                extra_compile_args = ["-O3", "-mavx2", "-mfma", "-fabi-version=0"])
setup( \
        name="myModule", \
        version='2.0', \
        include_dirs = [np.get_include()], \
        ext_modules=[ext]
        )

