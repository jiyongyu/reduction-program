#!/usr/bin/python
from fma_lib import fmaSum, addSum
import numpy as np
import time
import os.path
import subprocess

def calc(arr, itera):

    d_fma_result = 0.0
    for i in range(0, itera):
        d_fma_result += fmaSum(arr)
    return d_fma_result

def main():

    arr_len = 0x1000
    itera = 0x100000
    d_arr = np.empty(arr_len, dtype=np.float64)
    for i in range (0, arr_len):
        d_arr[i] = i/97.0
    
    result = calc(d_arr, itera)

    print result

if __name__ == "__main__":
    main()
