#include <stdio.h>
#include <immintrin.h>
#include "Python.h"
#include "numpy/arrayobject.h"

#define ADD 0
#define FMA 1

/* Python functions */
static PyObject* py_fmaSum(PyObject *self, PyObject *args);
static PyObject* py_addSum(PyObject *self, PyObject *args);
static PyObject* py_sum_helper(PyObject *self, PyObject *args, int op);

/* C functions */
double sum(int op, double *arr, int arr_len);
double _sum_fma(double* arr, int arr_len);
double _sum_add(double* arr, int arr_len);

/*double vector_sum(bool op = FMA, double *arr);*/
/*double matrix_sum(bool op = FMA, );*/

static PyObject*
py_fmaSum(PyObject *self, PyObject *args)
{
    return py_sum_helper(self, args, FMA);
}

static PyObject*
py_addSum(PyObject *self, PyObject *args)
{
    return py_sum_helper(self, args, ADD);
}

static PyObject*
py_sum_helper(PyObject *self, PyObject *args, int op){

    PyObject *arr_obj;
    
    // Parse arguments
    if (!PyArg_ParseTuple(args, "O", &arr_obj))
        return NULL;

    PyObject *arr = PyArray_FROM_OTF(arr_obj, NPY_DOUBLE, NPY_IN_ARRAY);
    if(arr == NULL) goto fail;

    int arr_len = (int)PyArray_DIM(arr, 0);
    double* arr_ptr = (double*)PyArray_DATA(arr);
#ifdef DISPLAY_ARR
    printDoubleArrInfo(arr_ptr, arr_len);
#endif
    double result;
    if (op == ADD)
        result = sum(ADD, arr_ptr, arr_len);
    else // op == FMA
        result = sum(FMA, arr_ptr, arr_len);

    Py_DECREF(arr);

    return Py_BuildValue("d", result);

fail:
    Py_XDECREF(arr);
    return Py_BuildValue("d", 0.0);
}

// Bind Python function names to our C functions
static PyMethodDef fma_lib_methods[] = {
    {"fmaSum", py_fmaSum, METH_VARARGS,
     "sum up a double floating point array by fmaing"},
    {"addSum", py_addSum, METH_VARARGS,
     "sum up a double floating point array by adding"},
    {NULL, NULL, 0, NULL}
};

// Initialize module
PyMODINIT_FUNC
initfma_lib(void){
    (void) Py_InitModule("fma_lib", fma_lib_methods);
    import_array();
}

double sum(int op, double *arr, int arr_len){

    volatile double returned_sum = 0;
    if (op == ADD)
        returned_sum = _sum_add(arr, arr_len);
    else // op == FMA
        returned_sum = _sum_fma(arr, arr_len);
    
    return returned_sum;
}

double _sum_add(double* arr, int arr_len){

    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = arr_len - arr_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_add_pd(a0, vec_sum0);
        vec_sum1 = _mm256_add_pd(a1, vec_sum1);
        vec_sum2 = _mm256_add_pd(a2, vec_sum2);
        vec_sum3 = _mm256_add_pd(a3, vec_sum3);
        vec_sum4 = _mm256_add_pd(a4, vec_sum4);
        vec_sum5 = _mm256_add_pd(a5, vec_sum5);
        vec_sum6 = _mm256_add_pd(a6, vec_sum6);
        vec_sum7 = _mm256_add_pd(a7, vec_sum7);
        vec_sum8 = _mm256_add_pd(a8, vec_sum8);
        vec_sum9 = _mm256_add_pd(a9, vec_sum9);
        vec_sum10 = _mm256_add_pd(a10, vec_sum10);
        vec_sum11 = _mm256_add_pd(a11, vec_sum11);
        vec_sum12 = _mm256_add_pd(a12, vec_sum12);
        vec_sum13 = _mm256_add_pd(a13, vec_sum13);
        vec_sum14 = _mm256_add_pd(a14, vec_sum14);
    }

    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_pd(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_pd(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    
    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}

double _sum_fma(double* arr, int arr_len){

    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d constReg = _mm256_set_pd(1.0, 1.0, 1.0, 1.0);
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = arr_len - arr_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_fmadd_pd(a0, constReg, vec_sum0);
        vec_sum1 = _mm256_fmadd_pd(a1, constReg, vec_sum1);
        vec_sum2 = _mm256_fmadd_pd(a2, constReg, vec_sum2);
        vec_sum3 = _mm256_fmadd_pd(a3, constReg, vec_sum3);
        vec_sum4 = _mm256_fmadd_pd(a4, constReg, vec_sum4);
        vec_sum5 = _mm256_fmadd_pd(a5, constReg, vec_sum5);
        vec_sum6 = _mm256_fmadd_pd(a6, constReg, vec_sum6);
        vec_sum7 = _mm256_fmadd_pd(a7, constReg, vec_sum7);
        vec_sum8 = _mm256_fmadd_pd(a8, constReg, vec_sum8);
        vec_sum9 = _mm256_fmadd_pd(a9, constReg, vec_sum9);
        vec_sum10 = _mm256_fmadd_pd(a10, constReg, vec_sum10);
        vec_sum11 = _mm256_fmadd_pd(a11, constReg, vec_sum11);
        vec_sum12 = _mm256_fmadd_pd(a12, constReg, vec_sum12);
        vec_sum13 = _mm256_fmadd_pd(a13, constReg, vec_sum13);
        vec_sum14 = _mm256_fmadd_pd(a14, constReg, vec_sum14);
    }

    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_pd(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_pd(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    
    for(; i<arr_len; i++){
       sum += arr[i];
    }
    
    return sum;
}
