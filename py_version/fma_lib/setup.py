from distutils.core import setup, Extension
import numpy as np

ext = Extension('fma_lib', \
                sources = ["fma_lib.c"], \
                extra_compile_args = ["-O3", "-mavx2", "-mfma", "-fabi-version=0"])
setup( \
        name="fma_lib", \
        version='1.0', \
        include_dirs = [np.get_include()], \
        ext_modules=[ext]
        )

