#!/usr/bin/python
from fma_lib import fmaSum, addSum
import numpy as np
import time
import os.path
import subprocess
from sys import getsizeof

def main():

    arr_len = 0x1000
    itera = 0x200000

    d_arr = np.empty(arr_len, dtype=np.float64)
    d_empty_arr = np.empty(0, dtype=np.float64)
    for i in range (0, arr_len):
        d_arr[i] = i/97.0
    print "actual size = ", getsizeof(d_arr)
    print "double array with length =", arr_len, "size =", arr_len * 8, "B"
    print "extra size =", getsizeof(d_arr) - arr_len * 8
    print "empty np array size = ", getsizeof(d_empty_arr)
    d_numpy_result = 0.0
    d_add_result = 0.0
    d_fma_result = 0.0
    ts_start = time.time()
    for i in range(0, itera):
        d_numpy_result += np.sum(d_arr)
    ts_end1 = time.time()
    for i in range(0, itera):
        d_add_result += addSum(d_arr)
    ts_end2 = time.time()
    for i in range(0, itera):
        d_fma_result += fmaSum(d_arr)
    ts_end3 = time.time()
    
    numpy_time_in_s = ts_end1 - ts_start
    add_time_in_s = ts_end2 - ts_end1
    fma_time_in_s = ts_end3 - ts_end2

    print "numpy sum", numpy_time_in_s, d_numpy_result
    print "add time", add_time_in_s, d_add_result
    print "fma time", fma_time_in_s, d_fma_result

if __name__ == "__main__":
    main()
