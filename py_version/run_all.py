#!/usr/bin/python
import myModule as mmod
import numpy as np
import time
import os.path
import subprocess

SIZE_UNITS = ['B', 'KB', 'MB', 'GB']

DIR_OF_THIS_SCRIPT = os.path.abspath( os.path.dirname( __file__) )

ARR_LEN = [ 0x10, 0x20, 0x40, 0x80, 0xc0, \
            0x100, 0x200, 0x400, 0x800, 0xc00, \
            0x1000, 0x2000, 0x4000, 0x8000, 0xc000, \
            0x10000, 0x20000, 0x40000, 0x80000, 0xc0000, \
            0x100000, 0x200000, 0x400000, 0x800000, 0xc00000, \
            0x1000000, 0x2000000, 0x4000000, 0x8000000]

const = 0x20000000

def createFile(dest, built_in_result, FMA_result, built_in_runtime, FMA_runtime, correct, speedup):
    f = open(dest, 'w')
    f.write("arr_len\tarr_size\tbuilt_in_result\tFMA_result\tbuilt_in_runtime\tFMA_runtime\tcorrect\tspeedup\n")
    for i in range(0, len(ARR_LEN)):
        size = float(ARR_LEN[i] * 4.0) # bytes
        unit = 0; # byte
        while size >= 1024:
            size = size / 1024
            unit = unit + 1
        string = str(ARR_LEN[i]) + '\t' \
                    + str(size)+SIZE_UNITS[unit] + '\t' \
                    + str(built_in_result[i]) + '\t' \
                    + str(FMA_result[i]) + '\t' \
                    + str(built_in_runtime[i]) + '\t' \
                    + str(FMA_runtime[i]) + '\t' \
                    + str(correct[i]) + '\t' \
                    + str(speedup[i]) + '\n' 
        f.write(string)
    f.close()

def main():

    built_in_result = []
    built_in_time = []
    FMA_result = []
    FMA_time = []
    correct = []
    speedup = []

    for arr_len in ARR_LEN:
        # create the array
        f_arr = np.empty(arr_len, dtype=np.float32)
        for i in range (0, arr_len):
            f_arr[i] = i/97.0

        itera = const / arr_len
    
        f_numpy_result = 0.0
        f_myModule_result = 0.0
        float_start = time.time()
        for i in range(0, itera):
            f_numpy_result += np.sum(f_arr)
        float_end1 = time.time()
        for i in range(0, itera):
            f_myModule_result += mmod.sumfarr(f_arr)
        float_end2 = time.time()
    
        numpy_time_in_s = float_end1 - float_start
        myModule_time_in_s = float_end2 - float_end1
    
        built_in_result.append(f_numpy_result)
        built_in_time.append(numpy_time_in_s)
        FMA_result.append(f_myModule_result)
        FMA_time.append(myModule_time_in_s)

        correct.append(f_numpy_result == f_myModule_result)
        speedup.append(numpy_time_in_s / myModule_time_in_s)

    reportFile = DIR_OF_THIS_SCRIPT + "/report.txt"
    createFile(reportFile, built_in_result, FMA_result, built_in_time, FMA_time, correct, speedup)

if __name__ == "__main__":
    main()
