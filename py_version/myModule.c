#include <stdio.h>
#include <immintrin.h>
#include "Python.h"
#include "numpy/arrayobject.h"

// #define DISPLAY_ARR

void printFloatArrInfo(float *arr, int array_len);
void printDoubleArrInfo(double *arr, int array_len);

float sumFloat(float *arr, int array_len);
double sumDouble(double *arr, int array_len);

float floatArrSum(float* arr, int arr_len);
double doubleArrSum(double* arr, int arr_len);

// sum method for single floating points array
static PyObject*
py_sumfarr(PyObject *self, PyObject *args)
{
    PyObject *arr_obj;
    
    // Parse arguments
    if (!PyArg_ParseTuple(args, "O", &arr_obj))
        return NULL;

    PyObject *arr = PyArray_FROM_OTF(arr_obj, NPY_FLOAT, NPY_IN_ARRAY);
    if(arr == NULL) goto fail;

    int arr_len = (int)PyArray_DIM(arr, 0);
    float* arr_ptr = (float*)PyArray_DATA(arr);
#ifdef DISPLAY_ARR
    printFloatArrInfo(arr_ptr, arr_len);
#endif
    float sum = floatArrSum(arr_ptr, arr_len);

    Py_DECREF(arr);

    return Py_BuildValue("f", sum);

fail:
    Py_XDECREF(arr);
    return Py_BuildValue("f", 0.0);
}

// sum method for double floating points array
static PyObject*
py_sumdarr(PyObject *self, PyObject *args)
{
    PyObject *arr_obj;
    
    // Parse arguments
    if (!PyArg_ParseTuple(args, "O", &arr_obj))
        return NULL;

    PyObject *arr = PyArray_FROM_OTF(arr_obj, NPY_DOUBLE, NPY_IN_ARRAY);
    if(arr == NULL) goto fail;

    int arr_len = (int)PyArray_DIM(arr, 0);
    double* arr_ptr = (double*)PyArray_DATA(arr);
#ifdef DISPLAY_ARR
    printDoubleArrInfo(arr_ptr, arr_len);
#endif
    double sum = doubleArrSum(arr_ptr, arr_len);

    Py_DECREF(arr);

    return Py_BuildValue("d", sum);

fail:
    Py_XDECREF(arr);
    return Py_BuildValue("d", 0.0);
}

// Bind Python function names to our C functions
static PyMethodDef myModule_methods[] = {
    {"sumfarr", py_sumfarr, METH_VARARGS,
     "sum up a single floating point array"},
    {"sumdarr", py_sumdarr, METH_VARARGS,
     "sum up a double floating point array"},
    {NULL, NULL, 0, NULL}
};

// Initialize module
void initmyModule(void){
    (void) Py_InitModule("myModule", myModule_methods);
    import_array();
}

void printFloatArrInfo(float *arr, int array_len){
    int i=0;
    printf("array length = %d\n", array_len);
    for(i=0; i<array_len; i++){
        printf("arr[%d] = %f\n", i, arr[i]);
    }
    return;
}

void printDoubleArrInfo(double *arr, int array_len){
    int i=0;
    printf("array length = %d\n", array_len);
    for(i=0; i<array_len; i++){
        printf("arr[%d] = %f\n", i, arr[i]);
    }
    return;
}

float floatArrSum(float* arr, int arr_len){

    volatile float returned_sum = 0;    
    returned_sum = sumFloat(arr, arr_len);

    return returned_sum;
}

double doubleArrSum(double* arr, int arr_len){

    volatile double returned_sum = 0;    
    returned_sum = sumDouble(arr, arr_len);

    return returned_sum;
}

float sumFloat(float *arr, int array_len){
    __m256 a0, a1, a2, a3, a4, a5, a6, a7,
           a8, a9, a10, a11, a12, a13, a14;
   
    __m256 constReg = _mm256_set_ps(1.0, 1.0, 1.0, 1.0,
                                    1.0, 1.0, 1.0, 1.0);

    __m256 vec_sum0 = _mm256_setzero_ps();
    __m256 vec_sum1 = _mm256_setzero_ps();
    __m256 vec_sum2 = _mm256_setzero_ps();
    __m256 vec_sum3 = _mm256_setzero_ps();
    __m256 vec_sum4 = _mm256_setzero_ps();
    __m256 vec_sum5 = _mm256_setzero_ps();
    __m256 vec_sum6 = _mm256_setzero_ps();
    __m256 vec_sum7 = _mm256_setzero_ps();
    __m256 vec_sum8 = _mm256_setzero_ps();
    __m256 vec_sum9 = _mm256_setzero_ps();
    __m256 vec_sum10 = _mm256_setzero_ps();
    __m256 vec_sum11 = _mm256_setzero_ps();
    __m256 vec_sum12 = _mm256_setzero_ps();
    __m256 vec_sum13 = _mm256_setzero_ps();
    __m256 vec_sum14 = _mm256_setzero_ps();

    int i;
    int simd_bound = array_len - array_len % 120;

    for(i=0; i<simd_bound; i+=120){
        // loading data to register
        a0 = _mm256_load_ps(arr+i+8*0);
        a1 = _mm256_load_ps(arr+i+8*1);
        a2 = _mm256_load_ps(arr+i+8*2);
        a3 = _mm256_load_ps(arr+i+8*3);
        a4 = _mm256_load_ps(arr+i+8*4);
        a5 = _mm256_load_ps(arr+i+8*5);
        a6 = _mm256_load_ps(arr+i+8*6);
        a7 = _mm256_load_ps(arr+i+8*7);
        a8 = _mm256_load_ps(arr+i+8*8);
        a9 = _mm256_load_ps(arr+i+8*9);
        a10 = _mm256_load_ps(arr+i+8*10);
        a11 = _mm256_load_ps(arr+i+8*11);
        a12 = _mm256_load_ps(arr+i+8*12);
        a13 = _mm256_load_ps(arr+i+8*13);
        a14 = _mm256_load_ps(arr+i+8*14);

        // arith operation
        vec_sum0 = _mm256_fmadd_ps(a0, constReg, vec_sum0);
        vec_sum1 = _mm256_fmadd_ps(a1, constReg, vec_sum1);
        vec_sum2 = _mm256_fmadd_ps(a2, constReg, vec_sum2);
        vec_sum3 = _mm256_fmadd_ps(a3, constReg, vec_sum3);
        vec_sum4 = _mm256_fmadd_ps(a4, constReg, vec_sum4);
        vec_sum5 = _mm256_fmadd_ps(a5, constReg, vec_sum5);
        vec_sum6 = _mm256_fmadd_ps(a6, constReg, vec_sum6);
        vec_sum7 = _mm256_fmadd_ps(a7, constReg, vec_sum7);
        vec_sum8 = _mm256_fmadd_ps(a8, constReg, vec_sum8);
        vec_sum9 = _mm256_fmadd_ps(a9, constReg, vec_sum9);
        vec_sum10 = _mm256_fmadd_ps(a10, constReg, vec_sum10);
        vec_sum11 = _mm256_fmadd_ps(a11, constReg, vec_sum11);
        vec_sum12 = _mm256_fmadd_ps(a12, constReg, vec_sum12);
        vec_sum13 = _mm256_fmadd_ps(a13, constReg, vec_sum13);
        vec_sum14 = _mm256_fmadd_ps(a14, constReg, vec_sum14);
    }

    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_ps(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_ps(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_ps(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_ps(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_ps(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_ps(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_ps(vec_sum0,  vec_sum8);
    
    float sum = 0.0;
    float* res = (float*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    sum += res[4]; 
    sum += res[5]; 
    sum += res[6]; 
    sum += res[7]; 
    
    for(; i<array_len; i++){
       sum += arr[i];
    }

    return sum;
}

double sumDouble(double *arr, int array_len){
    __m256d a0, a1, a2, a3, a4, a5, a6, a7,
            a8, a9, a10, a11, a12, a13, a14;
    
    __m256d constReg = _mm256_set_pd(1.0, 1.0, 1.0, 1.0);
    
    __m256d vec_sum0 = _mm256_setzero_pd();
    __m256d vec_sum1 = _mm256_setzero_pd();
    __m256d vec_sum2 = _mm256_setzero_pd();
    __m256d vec_sum3 = _mm256_setzero_pd();
    __m256d vec_sum4 = _mm256_setzero_pd();
    __m256d vec_sum5 = _mm256_setzero_pd();
    __m256d vec_sum6 = _mm256_setzero_pd();
    __m256d vec_sum7 = _mm256_setzero_pd();
    __m256d vec_sum8 = _mm256_setzero_pd();
    __m256d vec_sum9 = _mm256_setzero_pd();
    __m256d vec_sum10 = _mm256_setzero_pd();
    __m256d vec_sum11 = _mm256_setzero_pd();
    __m256d vec_sum12 = _mm256_setzero_pd();
    __m256d vec_sum13 = _mm256_setzero_pd();
    __m256d vec_sum14 = _mm256_setzero_pd();

    int i;
    int simd_bound = array_len - array_len % 60;

    for(i=0; i<simd_bound; i+=60){
        // loading data to register
        a0 = _mm256_load_pd(arr+i+4*0);
        a1 = _mm256_load_pd(arr+i+4*1);
        a2 = _mm256_load_pd(arr+i+4*2);
        a3 = _mm256_load_pd(arr+i+4*3);
        a4 = _mm256_load_pd(arr+i+4*4);
        a5 = _mm256_load_pd(arr+i+4*5);
        a6 = _mm256_load_pd(arr+i+4*6);
        a7 = _mm256_load_pd(arr+i+4*7);
        a8 = _mm256_load_pd(arr+i+4*8);
        a9 = _mm256_load_pd(arr+i+4*9);
        a10 = _mm256_load_pd(arr+i+4*10);
        a11 = _mm256_load_pd(arr+i+4*11);
        a12 = _mm256_load_pd(arr+i+4*12);
        a13 = _mm256_load_pd(arr+i+4*13);
        a14 = _mm256_load_pd(arr+i+4*14);

        // arith operation
        vec_sum0 = _mm256_fmadd_pd(a0, constReg, vec_sum0);
        vec_sum1 = _mm256_fmadd_pd(a1, constReg, vec_sum1);
        vec_sum2 = _mm256_fmadd_pd(a2, constReg, vec_sum2);
        vec_sum3 = _mm256_fmadd_pd(a3, constReg, vec_sum3);
        vec_sum4 = _mm256_fmadd_pd(a4, constReg, vec_sum4);
        vec_sum5 = _mm256_fmadd_pd(a5, constReg, vec_sum5);
        vec_sum6 = _mm256_fmadd_pd(a6, constReg, vec_sum6);
        vec_sum7 = _mm256_fmadd_pd(a7, constReg, vec_sum7);
        vec_sum8 = _mm256_fmadd_pd(a8, constReg, vec_sum8);
        vec_sum9 = _mm256_fmadd_pd(a9, constReg, vec_sum9);
        vec_sum10 = _mm256_fmadd_pd(a10, constReg, vec_sum10);
        vec_sum11 = _mm256_fmadd_pd(a11, constReg, vec_sum11);
        vec_sum12 = _mm256_fmadd_pd(a12, constReg, vec_sum12);
        vec_sum13 = _mm256_fmadd_pd(a13, constReg, vec_sum13);
        vec_sum14 = _mm256_fmadd_pd(a14, constReg, vec_sum14);
    }

    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum1);
    vec_sum2  = _mm256_add_pd(vec_sum2,  vec_sum3);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum5);
    vec_sum6  = _mm256_add_pd(vec_sum6,  vec_sum7);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum9);
    vec_sum10 = _mm256_add_pd(vec_sum10, vec_sum11);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum13);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum2);
    vec_sum4  = _mm256_add_pd(vec_sum4,  vec_sum6);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum10);
    vec_sum12 = _mm256_add_pd(vec_sum12, vec_sum14);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum4);
    vec_sum8  = _mm256_add_pd(vec_sum8,  vec_sum12);
    vec_sum0  = _mm256_add_pd(vec_sum0,  vec_sum8);
    
    double sum = 0.0;
    double* res = (double*)&vec_sum0;
    
    sum += res[0]; 
    sum += res[1]; 
    sum += res[2]; 
    sum += res[3]; 
    
    for(; i<array_len; i++){
       sum += arr[i];
    }
    
    return sum;
}
