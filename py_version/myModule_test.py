#!/usr/bin/python
import myModule as mmod
import numpy as np
import time

def measureElapsedTime(f_arr, d_arr, arr_len):

    print "======== Elpased Time ========"
    
    print "--------float--------"

    float_start = time.time()
    f_numpy_result = np.sum(f_arr)
    float_end1 = time.time()
    f_myModule_result = mmod.sumfarr(f_arr)
    float_end2 = time.time()
    
    numpy_time_in_s = float_end2 - float_end1
    myModule_time_in_s = float_end1 - float_start

    print "elapsed time for python sum = " + str(numpy_time_in_s) + " seconds"
    print "elapsed time for my sum = " + str(myModule_time_in_s) + " seconds"

    print "--------double--------"
    
    float_start = time.time()
    d_numpy_result = np.sum(d_arr)
    float_end1 = time.time()
    d_myModule_result = mmod.sumdarr(d_arr)
    float_end2 = time.time()
    
    numpy_time_in_s = float_end2 - float_end1
    myModule_time_in_s = float_end1 - float_start
    
    print "elapsed time for python sum = " + str(numpy_time_in_s) + " seconds"
    print "elapsed time for my sum = " + str(myModule_time_in_s) + " seconds"


arr_len = 18820

f_arr = np.empty(arr_len, dtype=np.float32)
for i in range (0, arr_len):
    f_arr[i] = i/97.0

d_arr = np.empty(arr_len, dtype=np.float64)
for i in range (0, arr_len):
    d_arr[i] = i/97.0


print "========== Result =========="
print "--------float--------"
print "python sum gives " + str(np.sum(f_arr))
print "my sum function gives " + str(mmod.sumfarr(f_arr))

print "--------double--------"
print "python sum gives " + str(np.sum(d_arr))
print "my sum function gives " + str(mmod.sumdarr(d_arr))

measureElapsedTime(f_arr, d_arr, arr_len)
