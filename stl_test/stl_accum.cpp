#include <iostream>
#include <cassert>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include <numeric>
#include "../vector_class/vectorclass.h"

using namespace std;

float sumFloat(float *arr, int array_len){
    float sum = 0.0;
    sum = accumulate(arr, arr + array_len-16, sum); 
    return sum;
}

int main(int argc, char **argv){

    int array_len = atoi(argv[argc-1]);
    long long int iter = 0x2000000000 / array_len;

	int i, j;
    
    //float *arr_float = new float [array_len];
    float* arr_float = (float*) memalign(64, array_len*4);
    volatile float passed_sum, returned_sum;
    clock_t t;
    t = clock();
    for (long long int i=0; i<iter; i++){
        returned_sum = sumFloat(arr_float, array_len);
        passed_sum += returned_sum;
    }
    t = clock() - t;
    double seconds = ((float)t) / CLOCKS_PER_SEC;
	printf("time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n", t, seconds, array_len, passed_sum);
    printf("%f\n", seconds);
   
    delete [] arr_float; 
    return 0;
}
