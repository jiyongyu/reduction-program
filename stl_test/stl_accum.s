	.file	"stl_accum.cpp"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB1:
	.text
.LHOTB1:
	.p2align 4,,15
	.globl	_Z8sumFloatPfi
	.type	_Z8sumFloatPfi, @function
_Z8sumFloatPfi:
.LFB5708:
	.cfi_startproc
	movslq	%esi, %rsi
	vxorps	%xmm0, %xmm0, %xmm0
	leaq	-64(,%rsi,4), %rax
	addq	%rdi, %rax
	cmpq	%rax, %rdi
	je	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	vaddss	(%rdi), %xmm0, %xmm0
	addq	$4, %rdi
	cmpq	%rdi, %rax
	jne	.L3
	rep ret
.L4:
	rep ret
	.cfi_endproc
.LFE5708:
	.size	_Z8sumFloatPfi, .-_Z8sumFloatPfi
	.section	.text.unlikely
.LCOLDE1:
	.text
.LHOTE1:
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"time: %ld clicks : %f seconds\narray_len: %d\ntotal sum: %f\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%f\n"
	.section	.text.unlikely
.LCOLDB5:
	.section	.text.startup,"ax",@progbits
.LHOTB5:
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB5709:
	.cfi_startproc
	pushq	%r14
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	movslq	%edi, %rdi
	pushq	%r12
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	movl	$10, %edx
	pushq	%rbx
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	subq	$32, %rsp
	.cfi_def_cfa_offset 80
	movq	-8(%rsi,%rdi,8), %rdi
	xorl	%esi, %esi
	call	strtol
	movslq	%eax, %rcx
	movq	%rax, %r13
	movabsq	$137438953472, %rax
	cqto
	leal	0(,%r13,4), %r14d
	movl	$64, %edi
	idivq	%rcx
	movslq	%r14d, %r14
	movq	%r14, %rsi
	movq	%rax, %rbp
	call	memalign
	movq	%rax, %rbx
	call	clock
	xorl	%r8d, %r8d
	testq	%rbp, %rbp
	movq	%rax, %r12
	leaq	-64(%rbx,%r14), %rsi
	vxorps	%xmm2, %xmm2, %xmm2
	jle	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	cmpq	%rbx, %rsi
	je	.L16
	vmovaps	%xmm2, %xmm0
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L13:
	vaddss	(%rdx), %xmm0, %xmm0
	addq	$4, %rdx
	cmpq	%rdx, %rsi
	jne	.L13
.L12:
	vmovss	%xmm0, 28(%rsp)
	addq	$1, %r8
	cmpq	%rbp, %r8
	vmovss	28(%rsp), %xmm1
	vmovss	24(%rsp), %xmm0
	vaddss	%xmm1, %xmm0, %xmm0
	vmovss	%xmm0, 24(%rsp)
	jne	.L14
.L15:
	call	clock
	vxorps	%xmm2, %xmm2, %xmm2
	subq	%r12, %rax
	vmovss	24(%rsp), %xmm1
	movq	%rax, %rdx
	vcvtss2sd	%xmm1, %xmm1, %xmm1
	movl	%r13d, %ecx
	movl	$.LC3, %esi
	vcvtsi2ssq	%rax, %xmm2, %xmm2
	vdivss	.LC2(%rip), %xmm2, %xmm2
	movl	$1, %edi
	movl	$2, %eax
	vcvtss2sd	%xmm2, %xmm2, %xmm2
	vmovapd	%xmm2, %xmm0
	vmovsd	%xmm2, 8(%rsp)
	call	__printf_chk
	vmovsd	8(%rsp), %xmm2
	movl	$.LC4, %esi
	movl	$1, %edi
	movl	$1, %eax
	vmovapd	%xmm2, %xmm0
	call	__printf_chk
	testq	%rbx, %rbx
	je	.L19
	movq	%rbx, %rdi
	call	_ZdaPv
.L19:
	addq	$32, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 48
	xorl	%eax, %eax
	popq	%rbx
	.cfi_def_cfa_offset 40
	popq	%rbp
	.cfi_def_cfa_offset 32
	popq	%r12
	.cfi_def_cfa_offset 24
	popq	%r13
	.cfi_def_cfa_offset 16
	popq	%r14
	.cfi_def_cfa_offset 8
	ret
.L16:
	.cfi_restore_state
	vmovaps	%xmm2, %xmm0
	jmp	.L12
	.cfi_endproc
.LFE5709:
	.size	main, .-main
	.section	.text.unlikely
.LCOLDE5:
	.section	.text.startup
.LHOTE5:
	.section	.text.unlikely
.LCOLDB6:
	.section	.text.startup
.LHOTB6:
	.p2align 4,,15
	.type	_GLOBAL__sub_I__Z8sumFloatPfi, @function
_GLOBAL__sub_I__Z8sumFloatPfi:
.LFB5773:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE5773:
	.size	_GLOBAL__sub_I__Z8sumFloatPfi, .-_GLOBAL__sub_I__Z8sumFloatPfi
	.section	.text.unlikely
.LCOLDE6:
	.section	.text.startup
.LHOTE6:
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z8sumFloatPfi
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1232348160
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 4.9.3-8ubuntu2~14.04) 4.9.3"
	.section	.note.GNU-stack,"",@progbits
