#include<iostream>
#include<cassert>
#include<cstdlib>
#include <ctime>
#include <stdio.h>
#include <malloc.h>
#include <sys/time.h>
#include "../../vector_class/vectorclass.h"
//#define TESTING
using namespace std;

double ptimer() {
    struct timeval timestr;
    void *tzp=0;

    gettimeofday(&timestr, (struct timezone*)tzp);
    double tmr=(double)timestr.tv_sec + 1.0E-06*(double)timestr.tv_usec;
    return tmr;
}


main(){
  Vec8f a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15;
  a0 =  1.0;  
  a1 =  1.0;
  a2 =  1.0;
  a3 =  1.0;
  a4 =  1.0;
  a5 =  1.0;
  a6 =  1.0;
  a7 =  1.0;
  a8 =  1.0;
  a9 =  1.0;
  a10 = 1.0; 
  a11 = 1.0; 
  a12 = 1.0; 
  a13 = 1.0; 
  a14 = 1.0; 
  a15 = 1.0; 

  double t0 = ptimer();
#ifdef TESTING
  int N = 1;
#else
  int N = 5000;
#endif
  for(int i=0;i<N;i++){  
    for(int j=0;j<N;j++){
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      
      a0 = a0 + a0;
      a1 = a1 + a1;
      a2 = a2 + a2;
      a3 = a3 + a3;
      a4 = a4 + a4;
      a5 = a5 + a5;
      a6 = a6 + a6;
      a7 = a7 + a7;
      a8 = mul_add(a8,1.0,a8);
      a9 = mul_add(a9,1.0,a9);
      a10 = mul_add(a10,1.0,a10);
      a11 = mul_add(a11,1.0,a11);
      a12 = mul_add(a12,1.0,a12);
      a13 = mul_add(a13,1.0,a13);
      a14 = mul_add(a14,1.0,a14);
      a15 = mul_add(a15,1.0,a15);
      


   }
  }
  double t1 = ptimer();
  double ft = t1-t0;
  cout << "Final time = " << ft << " seconds\n";
  double numOps = float((float)(N) * (float)(N) * 2 * 14 * 8 * 8)/(float)(1024*1024*1024);
  double flops = numOps/ft;
  cout << "Final GFlops = " << flops << " GFlops\n";
  float *C = new float[16*8];
  a0.store(C + 0*8);
  a1.store(C + 1*8);
  a2.store(C + 2*8);
  a3.store(C + 3*8);
  a4.store(C + 4*8);
  a5.store(C + 5*8);
  a6.store(C + 6*8);
  a7.store(C + 7*8);
  a8.store(C + 8*8);
  a9.store(C + 9*8);
  a10.store(C + 10*8);
  a11.store(C + 11*8);
  a12.store(C + 12*8);
  a13.store(C + 13*8);
  a14.store(C + 14*8);
  a15.store(C + 15*8);
  for(int i=0; i<16*8;i++){
    cout << C[i] << ",";
  }
  cout << "\n";
}
