	.file	"fpFma.4.cpp"
	.text
	.p2align 4,,15
	.globl	_Z6ptimerv
	.type	_Z6ptimerv, @function
_Z6ptimerv:
.LFB4182:
	.cfi_startproc
	subq	$24, %rsp
	.cfi_def_cfa_offset 32
	xorl	%esi, %esi
	movq	%rsp, %rdi
	call	gettimeofday
	vcvtsi2sdq	8(%rsp), %xmm0, %xmm0
	vcvtsi2sdq	(%rsp), %xmm1, %xmm1
	vfmadd132sd	.LC0(%rip), %xmm1, %xmm0
	addq	$24, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE4182:
	.size	_Z6ptimerv, .-_Z6ptimerv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Final time = "
.LC3:
	.string	" seconds\n"
.LC4:
	.string	"Final GFlops = "
.LC6:
	.string	" GFlops\n"
.LC7:
	.string	","
.LC8:
	.string	"\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB4183:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	andq	$-32, %rsp
	subq	$576, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	_Z6ptimerv
	vmovsd	%xmm0, 440(%rsp)
	movl	$5000, %edx
	vmovaps	.LC1(%rip), %ymm0
	vmovaps	%ymm0, %ymm5
	vmovaps	%ymm0, %ymm6
	vmovaps	%ymm0, 480(%rsp)
	vmovaps	%ymm0, %ymm7
	vmovaps	%ymm0, %ymm8
	vmovaps	%ymm0, 544(%rsp)
	vmovaps	%ymm0, %ymm9
	vmovaps	%ymm0, %ymm10
	vmovaps	%ymm0, 512(%rsp)
	vmovaps	%ymm0, %ymm11
	vmovaps	%ymm0, %ymm12
	vmovaps	%ymm0, %ymm1
	vmovaps	%ymm0, %ymm2
	vmovaps	%ymm0, %ymm3
	vmovaps	%ymm0, %ymm4
	vmovaps	%ymm0, %ymm13
	vmovaps	%ymm0, %ymm14
	vmovaps	%ymm0, %ymm15
	.p2align 4,,10
	.p2align 3
.L4:
	vmovaps	%ymm15, 544(%rsp)
	movl	$5000, %eax
	vmovaps	%ymm14, 512(%rsp)
	vmovaps	%ymm13, 448(%rsp)
	.p2align 4,,10
	.p2align 3
.L7:
	vaddps	%ymm1, %ymm1, %ymm1
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	subl	$1, %eax
	vaddps	%ymm2, %ymm2, %ymm2
	vmovaps	480(%rsp), %ymm14
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vaddps	%ymm3, %ymm3, %ymm3
	vmovaps	544(%rsp), %ymm15
	vmovaps	%ymm14, %ymm13
	vaddps	%ymm4, %ymm4, %ymm4
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	vfmadd132ps	%ymm0, %ymm14, %ymm13
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vaddps	%ymm9, %ymm9, %ymm9
	vmovaps	448(%rsp), %ymm14
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vaddps	%ymm10, %ymm10, %ymm10
	vmovaps	544(%rsp), %ymm15
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vaddps	%ymm12, %ymm12, %ymm12
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vaddps	%ymm11, %ymm11, %ymm11
	vmovaps	%ymm13, 480(%rsp)
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vaddps	%ymm8, %ymm8, %ymm8
	vmovaps	544(%rsp), %ymm15
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vfmadd132ps	%ymm0, %ymm14, %ymm14
	vaddps	%ymm7, %ymm7, %ymm7
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	vaddps	%ymm6, %ymm6, %ymm6
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vaddps	%ymm5, %ymm5, %ymm5
	vmovaps	544(%rsp), %ymm15
	vaddps	%ymm1, %ymm1, %ymm1
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	vaddps	%ymm2, %ymm2, %ymm2
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vaddps	%ymm3, %ymm3, %ymm3
	vmovaps	544(%rsp), %ymm15
	vaddps	%ymm4, %ymm4, %ymm4
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	vaddps	%ymm9, %ymm9, %ymm9
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vaddps	%ymm10, %ymm10, %ymm10
	vmovaps	544(%rsp), %ymm15
	vaddps	%ymm12, %ymm12, %ymm12
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vmovaps	512(%rsp), %ymm15
	vaddps	%ymm11, %ymm11, %ymm11
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 512(%rsp)
	vaddps	%ymm8, %ymm8, %ymm8
	vmovaps	544(%rsp), %ymm15
	vmovaps	512(%rsp), %ymm13
	vaddps	%ymm7, %ymm7, %ymm7
	vfmadd132ps	%ymm0, %ymm15, %ymm15
	vmovaps	%ymm15, 544(%rsp)
	vfmadd132ps	%ymm0, %ymm13, %ymm13
	vmovaps	%ymm13, 512(%rsp)
	vmovaps	%ymm14, %ymm13
	vaddps	%ymm6, %ymm6, %ymm6
	vfmadd132ps	%ymm0, %ymm14, %ymm13
	vmovaps	%ymm13, 448(%rsp)
	vaddps	%ymm5, %ymm5, %ymm5
	vaddps	%ymm1, %ymm1, %ymm1
	vaddps	%ymm2, %ymm2, %ymm2
	vaddps	%ymm3, %ymm3, %ymm3
	vaddps	%ymm4, %ymm4, %ymm4
	vaddps	%ymm9, %ymm9, %ymm9
	vaddps	%ymm10, %ymm10, %ymm10
	vaddps	%ymm12, %ymm12, %ymm12
	vaddps	%ymm11, %ymm11, %ymm11
	vaddps	%ymm8, %ymm8, %ymm8
	vaddps	%ymm7, %ymm7, %ymm7
	vaddps	%ymm6, %ymm6, %ymm6
	vaddps	%ymm5, %ymm5, %ymm5
	vaddps	%ymm1, %ymm1, %ymm1
	vaddps	%ymm2, %ymm2, %ymm2
	vaddps	%ymm3, %ymm3, %ymm3
	vaddps	%ymm4, %ymm4, %ymm4
	vaddps	%ymm9, %ymm9, %ymm9
	vaddps	%ymm10, %ymm10, %ymm10
	vaddps	%ymm12, %ymm12, %ymm12
	vaddps	%ymm11, %ymm11, %ymm11
	vaddps	%ymm8, %ymm8, %ymm8
	vaddps	%ymm7, %ymm7, %ymm7
	vaddps	%ymm6, %ymm6, %ymm6
	vaddps	%ymm5, %ymm5, %ymm5
	vaddps	%ymm1, %ymm1, %ymm1
	vaddps	%ymm2, %ymm2, %ymm2
	vaddps	%ymm3, %ymm3, %ymm3
	vaddps	%ymm4, %ymm4, %ymm4
	vaddps	%ymm9, %ymm9, %ymm9
	vaddps	%ymm10, %ymm10, %ymm10
	vaddps	%ymm12, %ymm12, %ymm12
	vaddps	%ymm11, %ymm11, %ymm11
	vaddps	%ymm8, %ymm8, %ymm8
	vaddps	%ymm7, %ymm7, %ymm7
	vaddps	%ymm6, %ymm6, %ymm6
	vaddps	%ymm5, %ymm5, %ymm5
	vaddps	%ymm1, %ymm1, %ymm1
	vaddps	%ymm2, %ymm2, %ymm2
	vaddps	%ymm3, %ymm3, %ymm3
	vaddps	%ymm4, %ymm4, %ymm4
	vaddps	%ymm9, %ymm9, %ymm9
	vaddps	%ymm10, %ymm10, %ymm10
	vaddps	%ymm12, %ymm12, %ymm12
	vaddps	%ymm11, %ymm11, %ymm11
	vaddps	%ymm8, %ymm8, %ymm8
	vaddps	%ymm7, %ymm7, %ymm7
	vaddps	%ymm6, %ymm6, %ymm6
	vaddps	%ymm5, %ymm5, %ymm5
	vaddps	%ymm1, %ymm1, %ymm1
	vaddps	%ymm2, %ymm2, %ymm2
	vaddps	%ymm3, %ymm3, %ymm3
	vaddps	%ymm4, %ymm4, %ymm4
	vaddps	%ymm9, %ymm9, %ymm9
	vaddps	%ymm10, %ymm10, %ymm10
	vaddps	%ymm12, %ymm12, %ymm12
	vaddps	%ymm11, %ymm11, %ymm11
	vaddps	%ymm8, %ymm8, %ymm8
	vaddps	%ymm7, %ymm7, %ymm7
	vaddps	%ymm6, %ymm6, %ymm6
	vaddps	%ymm5, %ymm5, %ymm5
	vaddps	%ymm1, %ymm1, %ymm1
	vaddps	%ymm2, %ymm2, %ymm2
	vaddps	%ymm3, %ymm3, %ymm3
	vaddps	%ymm4, %ymm4, %ymm4
	vaddps	%ymm9, %ymm9, %ymm9
	vaddps	%ymm10, %ymm10, %ymm10
	vaddps	%ymm12, %ymm12, %ymm12
	vaddps	%ymm11, %ymm11, %ymm11
	vaddps	%ymm8, %ymm8, %ymm8
	vaddps	%ymm7, %ymm7, %ymm7
	vaddps	%ymm6, %ymm6, %ymm6
	vaddps	%ymm5, %ymm5, %ymm5
	jne	.L7
	subl	$1, %edx
	vmovaps	512(%rsp), %ymm14
	jne	.L4
	vmovaps	%ymm15, (%rsp)
	vmovaps	%ymm11, %ymm0
	vmovaps	%ymm3, %ymm15
	vmovaps	%ymm14, 32(%rsp)
	vmovaps	%ymm4, %ymm14
	vmovaps	%ymm13, 64(%rsp)
	vmovaps	%ymm2, 96(%rsp)
	vmovaps	%ymm1, 128(%rsp)
	vmovaps	%ymm10, 160(%rsp)
	vmovaps	%ymm12, 192(%rsp)
	vmovaps	%ymm9, 224(%rsp)
	vmovaps	%ymm8, 256(%rsp)
	vmovaps	%ymm7, 288(%rsp)
	vmovaps	%ymm6, 320(%rsp)
	vmovaps	%ymm5, 352(%rsp)
	vmovaps	%ymm0, 384(%rsp)
	vmovaps	%ymm14, 448(%rsp)
	vmovaps	%ymm15, 512(%rsp)
	vzeroupper
	xorl	%ebx, %ebx
	call	_Z6ptimerv
	vsubsd	440(%rsp), %xmm0, %xmm1
	movl	$.LC2, %esi
	movl	$_ZSt4cout, %edi
	vmovsd	%xmm1, 544(%rsp)
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	544(%rsp), %xmm0
	movq	%rax, %rdi
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC3, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$.LC4, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	vmovsd	.LC5(%rip), %xmm0
	movq	%rax, %rdi
	vdivsd	544(%rsp), %xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$.LC6, %esi
	movq	%rax, %rdi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	$512, %edi
	call	_Znam
	vmovaps	480(%rsp), %ymm2
	movq	%rax, %r12
	vmovups	%ymm2, (%rax)
	vmovaps	(%rsp), %ymm15
	vmovups	%ymm15, 32(%rax)
	vmovaps	32(%rsp), %ymm14
	vmovups	%ymm14, 64(%rax)
	vmovaps	64(%rsp), %ymm13
	vmovups	%ymm13, 96(%rax)
	vmovaps	128(%rsp), %ymm4
	vmovups	%ymm4, 128(%rax)
	vmovaps	96(%rsp), %ymm3
	vmovups	%ymm3, 160(%rax)
	vmovaps	512(%rsp), %ymm2
	vmovups	%ymm2, 192(%rax)
	vmovaps	448(%rsp), %ymm1
	vmovups	%ymm1, 224(%rax)
	vmovaps	224(%rsp), %ymm12
	vmovups	%ymm12, 256(%rax)
	vmovaps	160(%rsp), %ymm11
	vmovups	%ymm11, 288(%rax)
	vmovaps	192(%rsp), %ymm10
	vmovups	%ymm10, 320(%rax)
	vmovaps	384(%rsp), %ymm9
	vmovups	%ymm9, 352(%rax)
	vmovaps	256(%rsp), %ymm8
	vmovups	%ymm8, 384(%rax)
	vmovaps	288(%rsp), %ymm7
	vmovups	%ymm7, 416(%rax)
	vmovaps	320(%rsp), %ymm6
	vmovups	%ymm6, 448(%rax)
	vmovaps	352(%rsp), %ymm5
	vmovups	%ymm5, 480(%rax)
	vzeroupper
	.p2align 4,,10
	.p2align 3
.L9:
	vmovss	(%r12,%rbx), %xmm0
	movl	$_ZSt4cout, %edi
	addq	$4, %rbx
	vcvtps2pd	%xmm0, %xmm0
	call	_ZNSo9_M_insertIdEERSoT_
	movl	$1, %edx
	movl	$.LC7, %esi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	cmpq	$512, %rbx
	jne	.L9
	movl	$.LC8, %esi
	movl	$_ZSt4cout, %edi
	call	_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	leaq	-16(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4183:
	.size	main, .-main
	.p2align 4,,15
	.type	_GLOBAL__sub_I__Z6ptimerv, @function
_GLOBAL__sub_I__Z6ptimerv:
.LFB4251:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	call	_ZNSt8ios_base4InitC1Ev
	movl	$__dso_handle, %edx
	movl	$_ZStL8__ioinit, %esi
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	jmp	__cxa_atexit
	.cfi_endproc
.LFE4251:
	.size	_GLOBAL__sub_I__Z6ptimerv, .-_GLOBAL__sub_I__Z6ptimerv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z6ptimerv
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	2696277389
	.long	1051772663
	.section	.rodata.cst32,"aM",@progbits,32
	.align 32
.LC1:
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.long	1065353216
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	2147483648
	.long	1078254739
	.hidden	__dso_handle
	.ident	"GCC: (GNU) 4.8.5 20150623 (Red Hat 4.8.5-4)"
	.section	.note.GNU-stack,"",@progbits
