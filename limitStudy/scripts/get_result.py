#!/usr/bin/env python

import os
import subprocess
import numpy as np
import matplotlib.pyplot as plt

DIR_OF_THIS_SCRIPT = os.path.abspath( os.path.dirname( __file__) )

BENCHMARK = 'benchmark2'
SRC_PATH = '../src/' + BENCHMARK + '/'
ASM_PATH = '../src/' + BENCHMARK + '/'
EXE_PATH = '../release/'
PERF_PROGRAM = '../src/perf_eval'

GXX = 'g++'
CFLAGS = ['-O3', '-mavx2', '-mfma', '-fabi-version=0']

def main():

    # remove executable files
    subprocess.check_call(['rm', '-rf', EXE_PATH + '*'])

    # search test files
    TESTCASES = []
    for file in os.listdir(SRC_PATH) :
        if file.endswith('.cpp') :
            TESTCASES.append(os.path.splitext(file)[0])

    TESTCASES = sorted(TESTCASES, key=lambda x: int(x.split('.')[1]))
    print TESTCASES

    # collect IPC
    IPC = [0.0] * len(TESTCASES)
    for i in range(0, len(TESTCASES)) :

        SRC_FILE = SRC_PATH + TESTCASES[i] + '.cpp'
        ASM_FILE = ASM_PATH + TESTCASES[i] + '.s'
        EXE_FILE = EXE_PATH + TESTCASES[i] + '.out'
        
        # assembly & build        
        subprocess.check_call([GXX] + CFLAGS + ['-S', SRC_FILE, '-o', ASM_FILE])
        subprocess.check_call([GXX, ASM_FILE, '-o', EXE_FILE])

        # get output
        perf_output = subprocess.check_output([PERF_PROGRAM, EXE_FILE])
        IPC[i] = float(perf_output.rsplit(None, 1)[-1])

    # plot diagram
    x = np.arange(len(TESTCASES))
    y = IPC
    plt.xticks(x, TESTCASES)
    plt.plot(x,y)
    plt.ylabel('IPC')
    plt.title('Histogram of IPC')
    plt.axis([-1, len(TESTCASES), 0.8, 2.2])
    plt.grid(True)
    plt.show()


if __name__ == '__main__' :
    main()

