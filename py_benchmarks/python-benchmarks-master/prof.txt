running build_ext
running build_src
build_src
building extension "arc_distance_pythran" sources
build_src: building npy-pkg config files
new_compiler returns distutils.unixccompiler.UnixCCompiler
customize UnixCCompiler using build_ext
********************************************************************************
distutils.unixccompiler.UnixCCompiler
linker_exe    = ['x86_64-linux-gnu-gcc', '-pthread']
compiler_so   = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-fPIC']
archiver      = ['ar', 'rc']
preprocessor  = ['x86_64-linux-gnu-gcc', '-pthread', '-E']
linker_so     = ['x86_64-linux-gnu-gcc', '-pthread', '-shared', '-Wl,-O1', '-Wl,-Bsymbolic-functions', '-Wl,-Bsymbolic-functions', '-Wl,-z,relro', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-D_FORTIFY_SOURCE=2', '-g', '-fstack-protector', '--param=ssp-buffer-size=4', '-Wformat', '-Werror=format-security']
compiler_cxx  = ['c++', '-pthread']
ranlib        = None
compiler      = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes']
libraries     = []
library_dirs  = []
include_dirs  = ['/usr/include/python2.7']
********************************************************************************
new_compiler returns distutils.unixccompiler.UnixCCompiler
customize UnixCCompiler using build_ext
********************************************************************************
distutils.unixccompiler.UnixCCompiler
linker_exe    = ['x86_64-linux-gnu-gcc', '-pthread']
compiler_so   = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-fPIC']
archiver      = ['ar', 'rc']
preprocessor  = ['x86_64-linux-gnu-gcc', '-pthread', '-E']
linker_so     = ['x86_64-linux-gnu-gcc', '-pthread', '-shared', '-Wl,-O1', '-Wl,-Bsymbolic-functions', '-Wl,-Bsymbolic-functions', '-Wl,-z,relro', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-D_FORTIFY_SOURCE=2', '-g', '-fstack-protector', '--param=ssp-buffer-size=4', '-Wformat', '-Werror=format-security']
compiler_cxx  = ['c++', '-pthread']
ranlib        = None
compiler      = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes']
libraries     = []
library_dirs  = []
include_dirs  = ['/usr/include/python2.7']
********************************************************************************
building 'arc_distance_pythran' extension
compiling C++ sources
C compiler: c++ -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -fPIC

creating /tmp/tmp53v4Jf/tmp
compile options: '-DENABLE_PYTHON_MODULE -I/usr/local/lib/python2.7/dist-packages/pythran -I/usr/local/lib/python2.7/dist-packages/numpy/core/include -I/usr/include/python2.7 -c'
extra options: '-std=c++11 -fno-math-errno -w -fwhole-program -fvisibility=hidden'
c++: /tmp/tmpaCJERI.cpp
exec_command(['c++', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-fPIC', '-DENABLE_PYTHON_MODULE', '-I/usr/local/lib/python2.7/dist-packages/pythran', '-I/usr/local/lib/python2.7/dist-packages/numpy/core/include', '-I/usr/include/python2.7', '-c', '/tmp/tmpaCJERI.cpp', '-o', '/tmp/tmp53v4Jf/tmp/tmpaCJERI.o', '-std=c++11', '-fno-math-errno', '-w', '-fwhole-program', '-fvisibility=hidden'],)
Retaining cwd: /home/jiyongyu/CGO/reduction-program/py_benchmarks/python-benchmarks-master
_preserve_environment([])
_update_environment(...)
_exec_command_posix(...)
Running os.system('( c++ -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -fPIC -DENABLE_PYTHON_MODULE -I/usr/local/lib/python2.7/dist-packages/pythran -I/usr/local/lib/python2.7/dist-packages/numpy/core/include -I/usr/include/python2.7 -c /tmp/tmpaCJERI.cpp -o /tmp/tmp53v4Jf/tmp/tmpaCJERI.o -std=c++11 -fno-math-errno -w -fwhole-program -fvisibility=hidden ; echo $? > /tmp/tmphzUwzG/g_VM7X ) 2>&1 | tee /tmp/tmphzUwzG/viFVOS ')
_update_environment(...)
c++ -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security /tmp/tmp53v4Jf/tmp/tmpaCJERI.o -lcblas -lblas -o /tmp/tmpPngjaE/arc_distance_pythran.so -fvisibility=hidden -Wl,-strip-all
exec_command(['c++', '-pthread', '-shared', '-Wl,-O1', '-Wl,-Bsymbolic-functions', '-Wl,-Bsymbolic-functions', '-Wl,-z,relro', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-D_FORTIFY_SOURCE=2', '-g', '-fstack-protector', '--param=ssp-buffer-size=4', '-Wformat', '-Werror=format-security', '/tmp/tmp53v4Jf/tmp/tmpaCJERI.o', '-lcblas', '-lblas', '-o', '/tmp/tmpPngjaE/arc_distance_pythran.so', '-fvisibility=hidden', '-Wl,-strip-all'],)
Retaining cwd: /home/jiyongyu/CGO/reduction-program/py_benchmarks/python-benchmarks-master
_preserve_environment([])
_update_environment(...)
_exec_command_posix(...)
Running os.system('( c++ -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security /tmp/tmp53v4Jf/tmp/tmpaCJERI.o -lcblas -lblas -o /tmp/tmpPngjaE/arc_distance_pythran.so -fvisibility=hidden -Wl,-strip-all ; echo $? > /tmp/tmphzUwzG/uhJF0H ) 2>&1 | tee /tmp/tmphzUwzG/dCXV0s ')
_update_environment(...)
running build_ext
running build_src
build_src
building extension "growcut_pythran" sources
build_src: building npy-pkg config files
new_compiler returns distutils.unixccompiler.UnixCCompiler
customize UnixCCompiler using build_ext
********************************************************************************
distutils.unixccompiler.UnixCCompiler
linker_exe    = ['x86_64-linux-gnu-gcc', '-pthread']
compiler_so   = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-fPIC']
archiver      = ['ar', 'rc']
preprocessor  = ['x86_64-linux-gnu-gcc', '-pthread', '-E']
linker_so     = ['x86_64-linux-gnu-gcc', '-pthread', '-shared', '-Wl,-O1', '-Wl,-Bsymbolic-functions', '-Wl,-Bsymbolic-functions', '-Wl,-z,relro', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-D_FORTIFY_SOURCE=2', '-g', '-fstack-protector', '--param=ssp-buffer-size=4', '-Wformat', '-Werror=format-security']
compiler_cxx  = ['c++', '-pthread']
ranlib        = None
compiler      = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes']
libraries     = []
library_dirs  = []
include_dirs  = ['/usr/include/python2.7']
********************************************************************************
new_compiler returns distutils.unixccompiler.UnixCCompiler
customize UnixCCompiler using build_ext
********************************************************************************
distutils.unixccompiler.UnixCCompiler
linker_exe    = ['x86_64-linux-gnu-gcc', '-pthread']
compiler_so   = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-fPIC']
archiver      = ['ar', 'rc']
preprocessor  = ['x86_64-linux-gnu-gcc', '-pthread', '-E']
linker_so     = ['x86_64-linux-gnu-gcc', '-pthread', '-shared', '-Wl,-O1', '-Wl,-Bsymbolic-functions', '-Wl,-Bsymbolic-functions', '-Wl,-z,relro', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-D_FORTIFY_SOURCE=2', '-g', '-fstack-protector', '--param=ssp-buffer-size=4', '-Wformat', '-Werror=format-security']
compiler_cxx  = ['c++', '-pthread']
ranlib        = None
compiler      = ['x86_64-linux-gnu-gcc', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes']
libraries     = []
library_dirs  = []
include_dirs  = ['/usr/include/python2.7']
********************************************************************************
building 'growcut_pythran' extension
compiling C++ sources
C compiler: c++ -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -fPIC

creating /tmp/tmpLZ7OaG/tmp
compile options: '-DENABLE_PYTHON_MODULE -I/usr/local/lib/python2.7/dist-packages/pythran -I/usr/local/lib/python2.7/dist-packages/numpy/core/include -I/usr/include/python2.7 -c'
extra options: '-std=c++11 -fno-math-errno -w -fwhole-program -fvisibility=hidden'
c++: /tmp/tmpsTlGAS.cpp
exec_command(['c++', '-pthread', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-fPIC', '-DENABLE_PYTHON_MODULE', '-I/usr/local/lib/python2.7/dist-packages/pythran', '-I/usr/local/lib/python2.7/dist-packages/numpy/core/include', '-I/usr/include/python2.7', '-c', '/tmp/tmpsTlGAS.cpp', '-o', '/tmp/tmpLZ7OaG/tmp/tmpsTlGAS.o', '-std=c++11', '-fno-math-errno', '-w', '-fwhole-program', '-fvisibility=hidden'],)
Retaining cwd: /home/jiyongyu/CGO/reduction-program/py_benchmarks/python-benchmarks-master
_preserve_environment([])
_update_environment(...)
_exec_command_posix(...)
Running os.system('( c++ -pthread -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -fPIC -DENABLE_PYTHON_MODULE -I/usr/local/lib/python2.7/dist-packages/pythran -I/usr/local/lib/python2.7/dist-packages/numpy/core/include -I/usr/include/python2.7 -c /tmp/tmpsTlGAS.cpp -o /tmp/tmpLZ7OaG/tmp/tmpsTlGAS.o -std=c++11 -fno-math-errno -w -fwhole-program -fvisibility=hidden ; echo $? > /tmp/tmphzUwzG/aBzktP ) 2>&1 | tee /tmp/tmphzUwzG/4bZAmS ')
_update_environment(...)
c++ -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security /tmp/tmpLZ7OaG/tmp/tmpsTlGAS.o -lcblas -lblas -o /tmp/tmpyqJQpI/growcut_pythran.so -fvisibility=hidden -Wl,-strip-all
exec_command(['c++', '-pthread', '-shared', '-Wl,-O1', '-Wl,-Bsymbolic-functions', '-Wl,-Bsymbolic-functions', '-Wl,-z,relro', '-fno-strict-aliasing', '-DNDEBUG', '-g', '-fwrapv', '-O2', '-Wall', '-Wstrict-prototypes', '-D_FORTIFY_SOURCE=2', '-g', '-fstack-protector', '--param=ssp-buffer-size=4', '-Wformat', '-Werror=format-security', '/tmp/tmpLZ7OaG/tmp/tmpsTlGAS.o', '-lcblas', '-lblas', '-o', '/tmp/tmpyqJQpI/growcut_pythran.so', '-fvisibility=hidden', '-Wl,-strip-all'],)
Retaining cwd: /home/jiyongyu/CGO/reduction-program/py_benchmarks/python-benchmarks-master
_preserve_environment([])
_update_environment(...)
_exec_command_posix(...)
Running os.system('( c++ -pthread -shared -Wl,-O1 -Wl,-Bsymbolic-functions -Wl,-Bsymbolic-functions -Wl,-z,relro -fno-strict-aliasing -DNDEBUG -g -fwrapv -O2 -Wall -Wstrict-prototypes -D_FORTIFY_SOURCE=2 -g -fstack-protector --param=ssp-buffer-size=4 -Wformat -Werror=format-security /tmp/tmpLZ7OaG/tmp/tmpsTlGAS.o -lcblas -lblas -o /tmp/tmpyqJQpI/growcut_pythran.so -fvisibility=hidden -Wl,-strip-all ; echo $? > /tmp/tmphzUwzG/p3jOCS ) 2>&1 | tee /tmp/tmphzUwzG/nerU0R ')
_update_environment(...)
