"""
Profiling support
"""
import cProfile
import pstats
import StringIO

class OneTimeProfiler(object):
    """
    A class to perform simply profiling. To use:
    otp = OneTimeProfiler()
    <stuff you want to profile>
    otp.stop()
    """

    def __init__(self, numfunc=30):
        self._numfunc = numfunc
        self._pr = cProfile.Profile()
        self._pr.enable()

    def stop(self):
        """
        stop profiling and print results
        """
        self._pr.disable()
        pstring = StringIO.StringIO()
        pst = pstats.Stats(self._pr,
                           stream=pstring).sort_stats('cumulative')
        pst.print_stats(self._numfunc)
        print pstring.getvalue()
